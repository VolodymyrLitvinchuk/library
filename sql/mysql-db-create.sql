DROP database IF EXISTS Library;

CREATE database Library;

USE Library;

CREATE TABLE Author
(
    id            INT         NOT NULL AUTO_INCREMENT,
    first_name_EN VARCHAR(35) NOT NULL,
    first_name_UA VARCHAR(35) NOT NULL,
    last_name_EN  VARCHAR(35) NOT NULL,
    last_name_UA  VARCHAR(35) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Publication
(
    id      INT         NOT NULL AUTO_INCREMENT,
    name_EN VARCHAR(50) NOT NULL,
    name_UA VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Book
(
    id               INT          NOT NULL AUTO_INCREMENT,
    title_EN         VARCHAR(100) NOT NULL,
    title_UA         VARCHAR(100) NOT NULL,
    language         VARCHAR(50)  NOT NULL,
    author           INT          NOT NULL,
    publication_id   INT          NOT NULL,
    publication_Date date         NOT NULL,
    copies_owned     INT          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (publication_id) REFERENCES Publication (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (author) REFERENCES Author(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT UNIQUE (title_EN,language,author,publication_id,publication_Date)
);

CREATE TABLE Book_Available
(
    book_id  INT NOT NULL,
    quantity INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES Book (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE User_Role
(
    id   INT         NOT NULL,
    role VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE User_Status
(
    id     INT         NOT NULL,
    status VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE User
(
    login      VARCHAR(50)  NOT NULL,
    password   VARCHAR(256)  NOT NULL,
    email      VARCHAR(200) NOT NULL,
    first_name VARCHAR(35)  NOT NULL,
    last_name  VARCHAR(35)  NOT NULL,
    roleFK     INT          NOT NULL,
    statusFK   INT          NOT NULL,
    PRIMARY KEY (login),
    CONSTRAINT UNIQUE (email),
    FOREIGN KEY (roleFK) REFERENCES User_Role (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (statusFK) REFERENCES User_Status (id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE Book_Order_Status
(
    id              INT         NOT NULL,
    status_value_EN VARCHAR(50) NOT NULL,
    status_value_UA VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Book_Order_Type
(
    id            INT         NOT NULL,
    type_value_EN VARCHAR(50) NOT NULL,
    type_value_UA VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE Book_Order
(
    id                    INT         NOT NULL AUTO_INCREMENT,
    book_id               INT         NOT NULL,
    reader                VARCHAR(50) NOT NULL,
    order_type            INT         NOT NULL,
    order_date            date        NOT NULL,
    returned_date_planned date        NOT NULL,
    order_status          INT         NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (book_id) REFERENCES Book (id) ON UPDATE CASCADE,
    FOREIGN KEY (reader) REFERENCES User (login) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (order_status) REFERENCES Book_Order_Status (id) ON UPDATE CASCADE,
    FOREIGN KEY (order_type) REFERENCES Book_Order_Type (id) ON UPDATE CASCADE
);

CREATE TABLE Fine_Status
(
    id              INT         NOT NULL,
    status_value_EN VARCHAR(20) NOT NULL,
    status_value_UA VARCHAR(20) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Fine
(
    id          INT         NOT NULL AUTO_INCREMENT,
    order_id    INT         NOT NULL,
    fine_amount INT         NOT NULL,
    fine_status INT         NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES Book_Order (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (fine_status) REFERENCES Fine_Status (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Fine_Payment
(
    id           INT  NOT NULL AUTO_INCREMENT,
    fine_id      INT  NOT NULL,
    payment_date date NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (fine_id) REFERENCES Fine (id) ON DELETE CASCADE ON UPDATE CASCADE
);




INSERT INTO User_Role VALUES (0, "Reader");
INSERT INTO User_Role VALUES (1, "Librarian");
INSERT INTO User_Role VALUES (2, "Admin");

INSERT INTO User_Status VALUES (0, "Active");
INSERT INTO User_Status VALUES (1, "Locked");
INSERT INTO User_Status VALUES (2, "Suspended");

INSERT INTO User VALUES ("admin", "wY+6P/kkImZCbenRwE9xPQ==", "2001litvinchuk2001@gmail.com", "Volodymyr", "Litvinchuk",2,0);

INSERT INTO Fine_Status VALUES (0, "Unpaid", "Не оплачений");
INSERT INTO Fine_Status VALUES (1, "Paid", "Оплачений");

INSERT INTO Book_Order_Status VALUES (0, "Pending book issuing confirmation", "Очікується підтвердження видачі книги");
INSERT INTO Book_Order_Status VALUES (1, "Book was issued to the reader", "Книга видана читачеві");
INSERT INTO Book_Order_Status VALUES (2, "Pending book returning confirmation", "Очікується підтвердження повернення книги");
INSERT INTO Book_Order_Status VALUES (3, "Book was returned", "Книга повернута до бібліотеки");
INSERT INTO Book_Order_Status VALUES (4, "Order canceled", "Замовлення скасоване");

INSERT INTO Book_Order_Type VALUES (0, "Reading Room", "Читальний зал");
INSERT INTO Book_Order_Type VALUES (1, "On a subscription", "На підписку");


INSERT INTO Publication VALUES (DEFAULT, "Folio", "Фоліо");
INSERT INTO Publication VALUES (DEFAULT, "School-Kharkiv", "Школа-Харків");
INSERT INTO Publication VALUES (DEFAULT, "BookChef", "BookChef");
INSERT INTO Publication VALUES (DEFAULT, "Mahaon", "Махаон");
INSERT INTO Publication VALUES (DEFAULT, "School", "Школа");


INSERT INTO Author VALUES (DEFAULT, "Ivan","Іван","Nechuy-Levytskyi","Нечуй-Левицький");
INSERT INTO Author VALUES (DEFAULT, "Mykola","Микола","Hohol'","Гоголь");
INSERT INTO Author VALUES (DEFAULT, "Mykhailo","Михайло","Kotsyubynskyi'","Коцюбинський");
INSERT INTO Author VALUES (DEFAULT, "Ivan","Іван","Franko'","Франко");
INSERT INTO Author VALUES (DEFAULT, "Anatoly","Анатолій","Dimarov'","Дімаров");
INSERT INTO Author VALUES (DEFAULT, "Ivan","Іван","Kotlyarevskyi'","Котляревський");
INSERT INTO Author VALUES (DEFAULT, "Taras","Тарас","Shevchenko'","Шевченко");
INSERT INTO Author VALUES (DEFAULT, "Lesya","Леся","Ukrayinka","Українка");
INSERT INTO Author VALUES (DEFAULT, "Lina","Ліна","Kostenko","Костенко");
INSERT INTO Author VALUES (DEFAULT, "Vsevolod","Всеволод","Nestayko","Нестайко");
INSERT INTO Author VALUES (DEFAULT, "Vasyl","Василь","Stefanyk","Стефаник");
INSERT INTO Author VALUES (DEFAULT, "Vasyl","Василь","Shklyar","Шкляр");
INSERT INTO Author VALUES (DEFAULT, "Valer'yan","Валер'ян","Pidmohylʹnyy","Підмогильний");
INSERT INTO Author VALUES (DEFAULT, "Hryhoriy","Григорій","Skovoroda","Сковорода");
INSERT INTO Author VALUES (DEFAULT, "Vasyl","Василь","Stus","Стус");

INSERT INTO Book VALUES (DEFAULT, "The Kaidash family" ,"Кайдашева сiм'я","Українська", 1, 1, "2022-06-01",3);
INSERT INTO Book VALUES (DEFAULT, "Taras Bulba" ,"Тарас Бульба","Українська", 2, 2, "2021-03-01",2);
INSERT INTO Book VALUES (DEFAULT, "Shadows of Forgotten Ancestors" ,"Тіні забутих предків","Українська", 3, 1, "2020-01-15",3);
INSERT INTO Book VALUES (DEFAULT, "Zakhar Berkut" ,"Захар Беркут","Українська", 4, 1, "2018-05-15",1);
INSERT INTO Book VALUES (DEFAULT, "And there will be people" ,"І будуть люди","Українська", 5, 1, "2022-08-01",5);
INSERT INTO Book VALUES (DEFAULT, "Aeneid" ,"Енеїда","Українська", 6, 3, "2018-05-20",4);
INSERT INTO Book VALUES (DEFAULT, "Evenings at the farm near Dykanka" ,"Вечори на хуторі біля Диканьки","Українська", 2, 4, "2010-10-20",1);
INSERT INTO Book VALUES (DEFAULT, "Kobzar" ,"Кобзар","Українська", 7, 5, "2005-05-25",3);
INSERT INTO Book VALUES (DEFAULT, "Forest Song" ,"Лісова пісня","Українська", 8, 1, "2000-09-01",4);
INSERT INTO Book VALUES (DEFAULT, "Marusya Churay" ,"Маруся Чурай","Українська", 9, 3, "2015-08-05",2);
INSERT INTO Book VALUES (DEFAULT, "Toreadors from Vasyukivka" ,"Тореадори з Васюківки","Українська", 10, 4, "2021-01-20",5);
INSERT INTO Book VALUES (DEFAULT, "Stone cross" ,"Камінний хрест","Українська", 11, 1, "2019-06-10",1);
INSERT INTO Book VALUES (DEFAULT, "Kharakternyk" ,"Характерник","Українська", 12, 5, "2011-10-15",3);
INSERT INTO Book VALUES (DEFAULT, "Diary" ,"Щоденник","Українська", 7, 5, "2005-05-25",2);
INSERT INTO Book VALUES (DEFAULT, "Hymn" ,"Гімн","Українська", 4, 1, "2002-02-20",3);
INSERT INTO Book VALUES (DEFAULT, "Moses" ,"Мойсей","Українська", 4, 1, "2020-01-20",3);
INSERT INTO Book VALUES (DEFAULT, "City" ,"Місто","Українська", 13, 4, "2007-10-11",1);
INSERT INTO Book VALUES (DEFAULT, "The master of the fireplace" ,"Камінний господар","Українська", 8, 1, "2009-09-11",5);
INSERT INTO Book VALUES (DEFAULT, "De libertate" ,"De libertate","Українська", 14, 1, "2013-11-21",5);
INSERT INTO Book VALUES (DEFAULT, "The phenomenon of the day" ,"Феномен доби","Українська", 15, 2, "2022-01-15",1);
INSERT INTO Book VALUES (DEFAULT, "Obsessed" ,"Одержима","Українська", 8, 5, "2021-05-20",2);
INSERT INTO Book VALUES (DEFAULT, "Key" ,"Ключ","Українська", 12, 1, "2001-07-12",4);

INSERT INTO Book_Available VALUES (1,3);
INSERT INTO Book_Available VALUES (2,2);
INSERT INTO Book_Available VALUES (3,3);
INSERT INTO Book_Available VALUES (4,1);
INSERT INTO Book_Available VALUES (5,5);
INSERT INTO Book_Available VALUES (6,4);
INSERT INTO Book_Available VALUES (7,1);
INSERT INTO Book_Available VALUES (8,3);
INSERT INTO Book_Available VALUES (9,4);
INSERT INTO Book_Available VALUES (10,2);
INSERT INTO Book_Available VALUES (11,5);
INSERT INTO Book_Available VALUES (12,1);
INSERT INTO Book_Available VALUES (13,3);
INSERT INTO Book_Available VALUES (14,2);
INSERT INTO Book_Available VALUES (15,3);
INSERT INTO Book_Available VALUES (16,3);
INSERT INTO Book_Available VALUES (17,1);
INSERT INTO Book_Available VALUES (18,5);
INSERT INTO Book_Available VALUES (19,5);
INSERT INTO Book_Available VALUES (20,1);
INSERT INTO Book_Available VALUES (21,2);
INSERT INTO Book_Available VALUES (22,4);
