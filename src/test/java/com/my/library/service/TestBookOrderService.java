package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.book.BookAvailableDAO;
import com.my.library.dao.abstr.bookOrder.*;
import com.my.library.dao.abstr.user.UserDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.bookOrder.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.dao.entity.user.*;
import com.my.library.dto.BookOrderDTO;
import com.my.library.exception.*;
import com.my.library.service.abstr.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookOrderService {
    @Mock
    private BookOrderStatusDAO bookOrderStatusDAO = mock(BookOrderStatusDAO.class);
    @Mock
    private BookOrderTypeDAO bookOrderTypeDAO = mock(BookOrderTypeDAO.class);
    @Mock
    private BookOrderDAO bookOrderDAO = mock(BookOrderDAO.class);
    @Mock
    private UserDAO userDAO = mock(UserDAO.class);
    @Mock
    private BookAvailableDAO bookAvailableDAO = mock(BookAvailableDAO.class);
    @Mock
    private Connection con = mock(Connection.class);
    @Mock
    private FineService fineService = mock(FineService.class);

    private static final BookOrderService bookOrderService = ServiceFactory.getBookOrderService();
    private static Book book;
    private static MockedStatic<DAOFactory> daoFactoryMockedStatic;
    private static MockedStatic<DataSource> dataSourceMockedStatic;
    private static User reader;
    private static final UserRole userRole = new UserRole(0, "Reader");
    private static final BookOrderType orderType0 = new BookOrderType(0,"Type0","Тип0");
    private static final BookOrderStatus orderStatus0 = new BookOrderStatus(0,"Status0","Статус0");
    private static final BookOrderStatus orderStatus1 = new BookOrderStatus(1,"Status1","Статус1");
    private static final BookOrderStatus orderStatus2 = new BookOrderStatus(2,"Status2","Статус2");
    private static final BookOrderStatus orderStatus3 = new BookOrderStatus(3,"Status3","Статус3");
    private static final BookOrderStatus orderStatus4 = new BookOrderStatus(4,"Status4","Статус4");
    private static BookOrder bookOrder;
    private static BookOrderDTO bookOrderDTO;

    @BeforeAll
    static void objectsCreating() throws ParseException {
        Author author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);

        Publication publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        book = builder.get();
        book.setId(1);

        reader = new User(
                "login",
                "12345",
                "test@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                userRole);
        reader.setStatus(new UserStatus(0,"Active"));

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFormat.parse("2022-12-17");
        Date dateReturned = dateFormat.parse("2022-12-27");
        bookOrder = new BookOrder(
                book.getId(),
                reader.getLogin(),
                orderType0,
                new java.sql.Date(date.getTime()),
                new java.sql.Date(dateReturned.getTime()),
                orderStatus0);

        bookOrderDTO = new BookOrderDTO(
                bookOrder.getBookIdFK(),
                bookOrder.getReaderFK(),
                bookOrder.getType().getId(),
                bookOrder.getDate(),
                bookOrder.getDateReturnPlanned());

    }

    @BeforeEach
    void mockitoSetUp(){
        dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
        dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);

        daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
        daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
        daoFactoryMockedStatic.when(DAOFactory::getBookOrderTypeDAO).thenReturn(bookOrderTypeDAO);
        daoFactoryMockedStatic.when(DAOFactory::getBookOrderStatusDAO).thenReturn(bookOrderStatusDAO);
        daoFactoryMockedStatic.when(DAOFactory::getBookAvailableDAO).thenReturn(bookAvailableDAO);
        daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);

    }
    @AfterEach
    void mockitoTearDown(){
        daoFactoryMockedStatic.close();
        dataSourceMockedStatic.close();
    }

    @Test
    @DisplayName("Test isAlreadyOrdered()")
    void test1() throws DAOException {
        Mockito.doReturn(true).when(bookOrderDAO).hasReaderUnfinishedOrder(reader.getLogin(),book.getId());
        Assertions.assertTrue(ServiceFactory.getBookOrderService().isAlreadyOrdered(reader.getLogin(),book.getId()));
    }

    @Test
    @DisplayName("Test insert()")
    void test2() throws DAOException {
        Mockito.doReturn(Optional.of(reader)).when(userDAO).get(bookOrderDTO.getReaderLogin());
        Mockito.doReturn(false).when(bookOrderDAO).hasReaderUnfinishedOrder(reader.getLogin(),book.getId());
        Mockito.doReturn(Optional.of(orderType0)).when(bookOrderTypeDAO).get(bookOrderDTO.getOrderTypeID());
        Mockito.doReturn(Optional.of(orderStatus0)).when(bookOrderStatusDAO).get(bookOrderDTO.getOrderStatusID());
        Mockito.doAnswer((i)->{
            BookOrder order = (BookOrder) i.getArguments()[0];
            order.setId(1);
            return null;
        }).when(bookOrderDAO).insert(any(BookOrder.class),any(Connection.class));
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(bookOrderDTO.getBookID());
        Mockito.doReturn(true).when(bookAvailableDAO).update(anyInt(),anyInt(),any(Connection.class));

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookOrderService().insert(bookOrderDTO));
    }

    @Test
    @DisplayName("Test getAllByStatus()")
    void test3() throws DAOException {
        Mockito.doReturn(Optional.of(List.of(bookOrder))).when(bookOrderDAO).getAllByStatus(anyInt());
        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookOrderService().getAllByStatus(1));
    }

    @Test
    @DisplayName("Test getLastPageNumberForReader()")
    void test4() throws DAOException, ServiceException {
        Mockito.doReturn(Optional.of(6)).when(bookOrderDAO).countAllReaderOrders(anyString());
        Assertions.assertEquals(2,ServiceFactory.getBookOrderService().getLastPageNumberForReader(5,reader.getLogin()));
        Mockito.doReturn(Optional.of(0)).when(bookOrderDAO).countAllReaderOrders(anyString());
        Assertions.assertEquals(1,ServiceFactory.getBookOrderService().getLastPageNumberForReader(5,reader.getLogin()));
    }

    @Test
    @DisplayName("Test getLastPageNumberForLibrarian()")
    void test5() throws DAOException, ServiceException {
        Mockito.doReturn(Optional.of(6)).when(bookOrderDAO).countAllActiveOrders();
        Assertions.assertEquals(2,ServiceFactory.getBookOrderService().getLastPageNumberForLibrarian(5));
        Mockito.doReturn(Optional.of(0)).when(bookOrderDAO).countAllActiveOrders();
        Assertions.assertEquals(1,ServiceFactory.getBookOrderService().getLastPageNumberForLibrarian(5));
    }

    @Test
    @DisplayName("Test isOrderActive()")
    void test6() throws ServiceException, DAOException {
        bookOrder.setStatus(orderStatus1);
        Mockito.doReturn(Optional.of(bookOrder)).when(bookOrderDAO).get(anyInt());
        Assertions.assertTrue(ServiceFactory.getBookOrderService().isOrderActive(1));
    }

    @Test
    @DisplayName("Test issueBook()")
    void test7() throws DAOException {
        bookOrder.setStatus(orderStatus0);
        Mockito.doReturn(Optional.of(bookOrder)).when(bookOrderDAO).get(anyInt());
        Mockito.doReturn(Optional.of(orderStatus1)).when(bookOrderStatusDAO).get(1);
        Mockito.doAnswer((i)->{
            BookOrder order = (BookOrder) i.getArguments()[0];
            order.setStatus(orderStatus1);
            return null;
        }).when(bookOrderDAO).updateStatus(bookOrder,orderStatus1,con);

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookOrderService().issueBook(1));
    }

    @Test
    @DisplayName("Test returnBookReader()")
    void test8() throws DAOException, ServiceException {
        bookOrder.setStatus(orderStatus1);
        Mockito.doReturn(Optional.of(bookOrder)).when(bookOrderDAO).get(anyInt());
        Mockito.doReturn(Optional.of(orderStatus2)).when(bookOrderStatusDAO).get(2);
        Mockito.doAnswer((i)->{
            BookOrder order = (BookOrder) i.getArguments()[0];
            order.setStatus(orderStatus2);
            return null;
        }).when(bookOrderDAO).updateStatus(bookOrder,orderStatus2,con);

        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)) {
            serviceFactoryMockedStatic.when(ServiceFactory::getBookOrderService).thenReturn(bookOrderService);
            serviceFactoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);
            Mockito.doReturn(false).when(fineService).orderIsFined(anyInt());

            Assertions.assertDoesNotThrow(() -> ServiceFactory.getBookOrderService().returnBookReader(1));
        }
    }

    @Test
    @DisplayName("Test returnBookLibrarian()")
    void test9() throws DAOException {
        bookOrder.setStatus(orderStatus2);
        Mockito.doReturn(Optional.of(bookOrder)).when(bookOrderDAO).get(anyInt());
        Mockito.doReturn(Optional.of(orderStatus3)).when(bookOrderStatusDAO).get(3);
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        Mockito.doReturn(true).when(bookAvailableDAO).update(anyInt(),anyInt(),any(Connection.class));
        Mockito.doAnswer((i)->{
            BookOrder order = (BookOrder) i.getArguments()[0];
            order.setStatus(orderStatus3);
            return null;
        }).when(bookOrderDAO).updateStatus(bookOrder,orderStatus3,con);

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookOrderService().returnBookLibrarian(1));
    }

    @Test
    @DisplayName("Test cancel()")
    void test10() throws DAOException {
        bookOrder.setStatus(orderStatus0);
        Mockito.doReturn(Optional.of(bookOrder)).when(bookOrderDAO).get(anyInt());
        Mockito.doReturn(Optional.of(orderStatus4)).when(bookOrderStatusDAO).get(4);
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        Mockito.doReturn(true).when(bookAvailableDAO).update(anyInt(),anyInt(),any(Connection.class));
        Mockito.doAnswer((i)->{
            BookOrder order = (BookOrder) i.getArguments()[0];
            order.setStatus(orderStatus4);
            return null;
        }).when(bookOrderDAO).updateStatus(bookOrder,orderStatus4,con);

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookOrderService().cancel(1));
    }
}
