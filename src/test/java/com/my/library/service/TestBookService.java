package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.book.AuthorDAO;
import com.my.library.dao.abstr.book.BookAvailableDAO;
import com.my.library.dao.abstr.book.BookDAO;
import com.my.library.dao.abstr.book.PublicationDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import com.my.library.dao.entity.builder.BookBuilder;
import com.my.library.dao.entity.builder.BookBuilderImpl;
import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.dto.BookDTO;
import com.my.library.dto.builder.BuilderBookDTO;
import com.my.library.dto.builder.BuilderBookDTOImpl;
import com.my.library.exception.DAOException;
import com.my.library.exception.ServiceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.ORDER_BY_LIMIT_OFFSET;
import static com.my.library.dao.mysql.Constant.SEARCH_BOOK;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookService {
    @Mock
    private AuthorDAO authorDAO = mock(AuthorDAO.class);
    @Mock
    private PublicationDAO publicationDAO = mock(PublicationDAO.class);
    @Mock
    private BookAvailableDAO bookAvailableDAO = mock(BookAvailableDAO.class);
    @Mock
    private BookDAO bookDAO = mock(BookDAO.class);
    @Mock
    private Connection con = mock(Connection.class);

    private static Author author;
    private static Author author2;
    private static Publication publication;
    private static Publication publication2;
    private static Book book;
    private static BookDTO bookDTO;
    private static MockedStatic<DAOFactory> daoFactoryMockedStatic;
    private static MockedStatic<DataSource> dataSourceMockedStatic;

    @BeforeAll
    static void objectsCreating() throws ParseException {
        author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);
        author2 = new AuthorBuilderImpl()
                .setFirstNameEN("Sasha")
                .setFirstNameUA("Саша")
                .setLastNameEN("Poginets")
                .setLastNameUA("Погінець")
                .get();
        author2.setId(2);

        publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);
        publication2 = new Publication("OlenaPublication", "ОленаВидавництво");
        publication2.setId(2);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        book = builder.get();


        BuilderBookDTO builderBookDTO = new BuilderBookDTOImpl();
        builderBookDTO
                .setTitleEN(book.getTitle_EN())
                .setTitleUA(book.getTitle_UA())
                .setLanguage(book.getLanguage())
                .setPublicationDate(book.getPublication_Date())
                .setCopiesOwned(book.getCopiesOwned())
                .setAuthorFirstNameEN(book.getAuthor().getFirstNameEN())
                .setAuthorFirstNameUA(book.getAuthor().getFirstNameUA())
                .setAuthorLastNameEN(book.getAuthor().getLastNameEN())
                .setAuthorLastNameUA(book.getAuthor().getLastNameUA())
                .setPublicationNameEN(book.getPublication().getNameEN())
                .setPublicationNameUA(book.getPublication().getNameUA());

        bookDTO = builderBookDTO.get();
        if(bookDTO==null) throw new RuntimeException();

        bookDTO.setId(book.getId());
        bookDTO.setAuthorId(book.getAuthor().getId());
        bookDTO.setPublicationId(book.getPublication().getId());
        bookDTO.setAvailableQuantity(3);
    }

    @BeforeEach
    void mockitoSetUp(){
        dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
        dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);

        daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
        daoFactoryMockedStatic.when(DAOFactory::getBookDAO).thenReturn(bookDAO);
        daoFactoryMockedStatic.when(DAOFactory::getAuthorDAO).thenReturn(authorDAO);
        daoFactoryMockedStatic.when(DAOFactory::getPublicationDAO).thenReturn(publicationDAO);
        daoFactoryMockedStatic.when(DAOFactory::getBookAvailableDAO).thenReturn(bookAvailableDAO);

    }
    @AfterEach
    void mockitoTearDown(){
        daoFactoryMockedStatic.close();
        dataSourceMockedStatic.close();
    }

    @Test
    @DisplayName("Test deleteBook()")
    void test14() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        book.setId(1);
        book.setCopiesOwned(3);
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(1);

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookService().deleteBook(1));
    }

    @Test
    @DisplayName("Test updateBookPublicationId()")
    void test13() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doReturn(Optional.of(publication2)).when(publicationDAO).get(2);
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            book.setPublication(publication2);
            return null;
        }).when(bookDAO).updatePublication(any(Book.class),any(Publication.class));

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookService().updateBookPublicationId(1,2));
    }

    @Test
    @DisplayName("Test updateBookAuthorId()")
    void test12() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doReturn(Optional.of(author2)).when(authorDAO).get(2);
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            book.setAuthor(author2);
            return null;
        }).when(bookDAO).updateAuthor(any(Book.class),any(Author.class));

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookService().updateBookAuthorId(1,2));
    }

    @Test
    @DisplayName("Test updateBookCopiesOwned()")
    void test11() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        Mockito.doReturn(true).when(bookAvailableDAO).update(anyInt(),anyInt(),any(Connection.class));
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            Integer newCopiesOwned = (Integer) i.getArguments()[1];
            book.setCopiesOwned(newCopiesOwned);
            return null;
        }).when(bookDAO).updateCopiesOwned(any(Book.class),anyInt(), any(Connection.class));

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookService().updateBookCopiesOwned(1,5));
    }

    @Test
    @DisplayName("Test updateBookLanguage()")
    void test10() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            String newLanguage = (String) i.getArguments()[1];
            book.setLanguage(newLanguage);
            return null;
        }).when(bookDAO).updateLanguage(any(Book.class),any(String.class));

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookService().updateBookLanguage(
                1,
                "Китайська"
        ));
    }

    @Test
    @DisplayName("Test updateBookTitleUA()")
    void test9() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            String newTitleUA = (String) i.getArguments()[1];
            book.setTitle_UA(newTitleUA);
            return null;
        }).when(bookDAO).updateTitleUA(any(Book.class),any(String.class));

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookService().updateBookTitleUA(
                1,
                "НоваНазва"
        ));
    }

    @Test
    @DisplayName("Test updateBookTitleEN()")
    void test8() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            String newTitleEN = (String) i.getArguments()[1];
            book.setTitle_EN(newTitleEN);
            return null;
        }).when(bookDAO).updateTitleEN(any(Book.class),any(String.class));

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookService().updateBookTitleEN(
                1,
                "NewTitle"
        ));
    }

    @Test
    @DisplayName("Test getLastPageNumberSearchBooks()")
    void test7() throws DAOException, ServiceException {
        Mockito.doReturn(Optional.of(6)).when(bookDAO).getBookQuantity(anyString());
        Assertions.assertEquals(2,ServiceFactory.getBookService().getLastPageNumberSearchBooks(
                ServiceFactory.getBookService().buildSearchQuery("","","", "en"),
                5));

        Mockito.doReturn(Optional.of(0)).when(bookDAO).getBookQuantity(anyString());
        Assertions.assertEquals(1,ServiceFactory.getBookService().getLastPageNumberSearchBooks(
                ServiceFactory.getBookService().buildSearchQuery("","","", "en"),
                5));
    }

    @Test
    @DisplayName("Test getLastPageNumber()")
    void test6() throws DAOException, ServiceException {
        Mockito.doReturn(Optional.of(6)).when(bookDAO).getBookQuantity();
        Assertions.assertEquals(2,ServiceFactory.getBookService().getLastPageNumber(5));

        Mockito.doReturn(Optional.of(0)).when(bookDAO).getBookQuantity();
        Assertions.assertEquals(1,ServiceFactory.getBookService().getLastPageNumber(5));
    }

    @Test
    @DisplayName("Test getBooks()")
    void test5() throws DAOException {
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        Mockito.doReturn(Optional.of(List.of(book))).when(bookDAO).getBooksByOrderOffsetLimit(any(),anyInt(),anyInt());
        book.setId(1);

        Assertions.assertDoesNotThrow(()->{
            List<BookDTO> list = ServiceFactory.getBookService().getBooks(
                    BookOrderBy.BOOK_TITLE_EN,
                    0,
                    5
            );
            Assertions.assertEquals(1,list.size());
        });
    }

    @Test
    @DisplayName("Test searchBooks()")
    void test4() throws DAOException {
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        Mockito.doReturn(Optional.of(List.of(book))).when(bookDAO).searchBooks(anyString(),any(),anyInt(),anyInt());
        book.setId(1);

        Assertions.assertDoesNotThrow(()->{
            List<BookDTO> list = ServiceFactory.getBookService().searchBooks(
                    ServiceFactory.getBookService().buildSearchQuery("","","", "en"),
                    BookOrderBy.BOOK_TITLE_EN,
                    0,
                    5
            );
            Assertions.assertEquals(1,list.size());
        });
    }

    @Test
    @DisplayName("Test get()")
    void test3() throws DAOException {
        Mockito.doReturn(Optional.of(book)).when(bookDAO).get(anyInt());
        Mockito.doReturn(Optional.of(3)).when(bookAvailableDAO).get(anyInt());
        book.setId(1);

        Assertions.assertDoesNotThrow(()->ServiceFactory.getBookService().getBook(1));

    }

    @Test
    @DisplayName("Test add()")
    void test2() throws DAOException {
        Mockito.doReturn(Optional.of(author)).when(authorDAO).getByFirstLastNameEN(anyString(), anyString());
        Mockito.doReturn(Optional.of(publication)).when(publicationDAO).getByNameEN(anyString());
        Mockito.doAnswer((i)->{
            Book book = (Book) i.getArguments()[0];
            book.setId(1);
            return null;
        }).when(bookDAO).insert(any(Book.class),any(Connection.class));
        Mockito.doReturn(true).when(bookAvailableDAO).insert(anyInt(),anyInt(),any(Connection.class));

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getBookService().add(bookDTO));
    }

    @Test
    @DisplayName("Test buildSearchQuery()")
    void test1(){
        String query = ServiceFactory.getBookService().buildSearchQuery("","","", "en");
        Assertions.assertEquals(SEARCH_BOOK+ORDER_BY_LIMIT_OFFSET, query);

        query = ServiceFactory.getBookService().buildSearchQuery("title","","surname", "en");
        Assertions.assertEquals(SEARCH_BOOK+"WHERE title_EN LIKE '%%title%%' AND last_name_EN LIKE '%%surname%%' "+ORDER_BY_LIMIT_OFFSET, query);

        query = ServiceFactory.getBookService().buildSearchQuery("title","name","surname", "ua");
        Assertions.assertEquals(SEARCH_BOOK+"WHERE title_UA LIKE '%%title%%' AND first_name_UA LIKE '%%name%%' AND last_name_UA LIKE '%%surname%%' "+ORDER_BY_LIMIT_OFFSET, query);
    }
}
