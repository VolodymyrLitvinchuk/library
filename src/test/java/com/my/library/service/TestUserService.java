package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.user.UserDAO;
import com.my.library.dao.entity.user.User;
import com.my.library.dao.entity.user.UserRole;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.dto.UserDTO;
import com.my.library.exception.*;
import com.my.library.service.abstr.UserService;
import com.my.library.util.ConvertorUtil;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUserService {
    @Mock
    UserDAO userDAO = mock(UserDAO.class);

    @Test
    @DisplayName("Test signIn()")
    void test1() throws DAOException {
        UserRole role = new UserRole(0,"Reader");
        UserStatus status = new UserStatus(0,"Active");
        User user = new User("login",
                "cjEdBQatscigJl4ZpzUidw==",
                "abc@gmail.com",
                "Vova",
                "Litvinchuk",
                role);
        user.setStatus(status);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)){
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);

            Mockito.doReturn(Optional.of(user)).when(userDAO).get("login");

            UserService userService = ServiceFactory.getUserService();
            Assertions.assertDoesNotThrow(()->{
                userService.signIn("login","12345Afk@");
            });
            Assertions.assertThrows(IncorrectPasswordException.class,()-> userService.signIn("login","12345Afk"));
            Assertions.assertThrows(IncorrectLoginException.class,()-> userService.signIn("loginn","12345Afk@"));
        }
    }

    @Test
    @DisplayName("Test signUp()")
    void test2() throws DAOException, ServiceException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
            MockedStatic<ConvertorUtil> convertorUtilMockedStatic = Mockito.mockStatic(ConvertorUtil.class)) {

            User user1 = new User(
                    "login","ABCabc13", "2001litvinchuk@gmail.com",
                    "Volodymyr", "Litvinchuk",new UserRole(1,"Reader")
            );
            convertorUtilMockedStatic.when(()-> ConvertorUtil.convertDTOtoUser(any(),any())).thenReturn(user1);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);

            Mockito.doAnswer((i) -> {
                User user = (User) i.getArguments()[0];
                user.setStatus(new UserStatus(0, "Active"));
                return null;
            }).when(userDAO).insert(any(User.class));


            ServiceFactory.getUserService().signUp(userDTO, "ABCabc13", "ABCabc13");
            Assertions.assertEquals("Active", userDTO.getStatus());
        }
    }

    @Test
    @DisplayName("Test updateStatus()")
    void test3() throws DAOException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );
        User user = new User(
                "login","password123","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                new UserRole(1, "Reader")
        );
        user.setStatus(new UserStatus(1,"Active"));

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {

            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            Mockito.doReturn(Optional.of(user)).when(userDAO).get("login");

            ServiceFactory.getUserService().updateStatus(userDTO);
            Assertions.assertEquals("Active",userDTO.getStatus());

        }
    }

    @Test
    @DisplayName("Test getLastPageNumber()")
    void test4() throws DAOException {
        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            Mockito.doReturn(Optional.of(10)).when(userDAO).countAllUsers();

            Assertions.assertDoesNotThrow(()->{
                int lastPage = ServiceFactory.getUserService().getLastPageNumber(5);
                Assertions.assertEquals(2,lastPage);

                Mockito.doReturn(Optional.of(11)).when(userDAO).countAllUsers();
                lastPage = ServiceFactory.getUserService().getLastPageNumber(5);
                Assertions.assertEquals(3,lastPage);

                Mockito.doReturn(Optional.of(1)).when(userDAO).countAllUsers();
                lastPage = ServiceFactory.getUserService().getLastPageNumber(5);
                Assertions.assertEquals(1,lastPage);
            });

        }
    }

    @Test
    @DisplayName("Test updateLogin()")
    void test5() throws DAOException, ServiceException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
             MockedStatic<ConvertorUtil> convertorUtilMockedStatic = Mockito.mockStatic(ConvertorUtil.class)) {

            User user1 = new User(
                    "login","ABCabc13", "2001litvinchuk@gmail.com",
                    "Volodymyr", "Litvinchuk",new UserRole(1,"Reader")
            );
            convertorUtilMockedStatic.when(()-> ConvertorUtil.convertDTOtoUser(any(),any())).thenReturn(user1);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);

            Mockito.doAnswer((i) -> {
                User user = (User) i.getArguments()[0];
                user.setLogin("newLogin");
                return null;
            }).when(userDAO).updateLogin(user1, "newLogin");


            ServiceFactory.getUserService().updateLogin(userDTO, "newLogin");
            Assertions.assertEquals("newLogin", userDTO.getLogin());
        }
    }

    @Test
    @DisplayName("Test updateEmail()")
    void test6() throws DAOException, ServiceException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );
        String newEmail = "2001litvinchuknew@gmail.com";

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {

            User user1 = new User(
                    "login","ABCabc13", "2001litvinchuk@gmail.com",
                    "Volodymyr", "Litvinchuk",new UserRole(1,"Reader")
            );

            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            Mockito.doReturn(Optional.of(user1)).when(userDAO).getByEmail("2001litvinchuk@gmail.com");

            Mockito.doAnswer((i) -> {
                User user = (User) i.getArguments()[0];
                user.setEmail(newEmail);
                return null;
            }).when(userDAO).updateEmail(user1, newEmail);


            ServiceFactory.getUserService().updateEmail(userDTO, newEmail);
            Assertions.assertEquals(newEmail, userDTO.getEmail());
        }
    }

    @Test
    @DisplayName("Test updateFirstName()")
    void test7() throws DAOException, ServiceException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );
        String newFirstName = "Володимир";

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {

            User user1 = new User(
                    "login","ABCabc13", "2001litvinchuk@gmail.com",
                    "Volodymyr", "Litvinchuk",new UserRole(1,"Reader")
            );

            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            Mockito.doReturn(Optional.of(user1)).when(userDAO).get("login");

            Mockito.doAnswer((i) -> {
                User user = (User) i.getArguments()[0];
                user.setFirstName(newFirstName);
                return null;
            }).when(userDAO).updateFirstName(user1, newFirstName);

            ServiceFactory.getUserService().updateFirstName(userDTO, newFirstName);
            Assertions.assertEquals(newFirstName, userDTO.getFirstName());

            Mockito.doReturn(Optional.empty()).when(userDAO).get(any());
            Assertions.assertThrows(ServiceException.class, ()-> ServiceFactory.getUserService().updateFirstName(userDTO, newFirstName));
        }
    }

    @Test
    @DisplayName("Test updateLastName()")
    void test8() throws DAOException, ServiceException {
        UserDTO userDTO = new UserDTO(
                "login","2001litvinchuk@gmail.com",
                "Volodymyr", "Litvinchuk",
                "Reader",null
        );
        String newLastName = "Літвінчук";

        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {

            User user1 = new User(
                    "login","ABCabc13", "2001litvinchuk@gmail.com",
                    "Volodymyr", "Litvinchuk",new UserRole(1,"Reader")
            );

            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            Mockito.doReturn(Optional.of(user1)).when(userDAO).get("login");

            Mockito.doAnswer((i) -> {
                User user = (User) i.getArguments()[0];
                user.setLastName(newLastName);
                return null;
            }).when(userDAO).updateLastName(user1, newLastName);

            ServiceFactory.getUserService().updateLastName(userDTO, newLastName);
            Assertions.assertEquals(newLastName, userDTO.getLastName());

            Mockito.doReturn(Optional.empty()).when(userDAO).get(any());
            Assertions.assertThrows(ServiceException.class, ()-> ServiceFactory.getUserService().updateLastName(userDTO, newLastName));
        }
    }

}
