package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.book.AuthorDAO;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dto.AuthorDTO;
import com.my.library.exception.DAOException;
import com.my.library.exception.ServiceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestAuthorService {
    @Mock
    private AuthorDAO authorDAO = mock(AuthorDAO.class);

    private static Author author;
    private static Author author2;
    private static MockedStatic<DAOFactory> daoFactoryMockedStatic;

    @BeforeAll
    static void authorCreate(){
        author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author2 = new AuthorBuilderImpl()
                .setFirstNameEN("Andrey")
                .setFirstNameUA("Андрій")
                .setLastNameEN("Pylypchuk")
                .setLastNameUA("Пилипчук")
                .get();
    }

    @BeforeEach
    void mockInit(){
        daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
        daoFactoryMockedStatic.when(DAOFactory::getAuthorDAO).thenReturn(authorDAO);
    }

    @AfterEach
    void mockClose(){
        daoFactoryMockedStatic.close();
    }

    @Test
    @DisplayName("Test getAuthors()")
    void test1() throws DAOException {
        Mockito.doReturn(Optional.of(List.of(author,author2)))
                .when(authorDAO).getAuthorsByOrderOffsetLimit(any(),anyInt(),anyInt());
        Assertions.assertDoesNotThrow(()->{
            List<AuthorDTO> list = ServiceFactory.getAuthorService().getAuthors(AuthorOrderBy.ID,0,5);
            Assertions.assertEquals(2,list.size());
        });
    }

    @Test
    @DisplayName("Test getLastPageNumber()")
    void test2() throws ServiceException, DAOException {
        Mockito.doReturn(Optional.of(6)).when(authorDAO).getAuthorQuantity();
        Assertions.assertEquals(2,ServiceFactory.getAuthorService().getLastPageNumber(5));
        Mockito.doReturn(Optional.of(0)).when(authorDAO).getAuthorQuantity();
        Assertions.assertEquals(1,ServiceFactory.getAuthorService().getLastPageNumber(5));
    }

    @Test
    @DisplayName("Test updateFirstNameEN()")
    void test3() throws DAOException {
        author.setId(1);
        Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
        Mockito.doAnswer((i)->{
            Author author = (Author) i.getArguments()[0];
            author.setFirstNameEN("NewFirstName");
            return null;
        }).when(authorDAO).updateFirstNameEN(any(Author.class),anyString());

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getAuthorService().updateFirstNameEN(1,"NewFirstName"));
    }

    @Test
    @DisplayName("Test updateFirstNameUA()")
    void test4() throws DAOException {
        author.setId(1);
        Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
        Mockito.doAnswer((i)->{
            Author author = (Author) i.getArguments()[0];
            author.setFirstNameUA("НоваНазва");
            return null;
        }).when(authorDAO).updateFirstNameUA(any(Author.class),anyString());

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getAuthorService().updateFirstNameUA(1,"НоваНазва"));
    }

    @Test
    @DisplayName("Test updateLastNameEN()")
    void test5() throws DAOException {
        author.setId(1);
        Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
        Mockito.doAnswer((i)->{
            Author author = (Author) i.getArguments()[0];
            author.setLastNameEN("NewLastName");
            return null;
        }).when(authorDAO).updateLastNameEN(any(Author.class),anyString());

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getAuthorService().updateLastNameEN(1,"NewLastName"));
    }

    @Test
    @DisplayName("Test updateLastNameUA()")
    void test6() throws DAOException {
        author.setId(1);
        Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
        Mockito.doAnswer((i)->{
            Author author = (Author) i.getArguments()[0];
            author.setLastNameUA("НоваНазва");
            return null;
        }).when(authorDAO).updateLastNameUA(any(Author.class),anyString());

        Assertions.assertDoesNotThrow(()-> ServiceFactory.getAuthorService().updateLastNameUA(1,"НоваНазва"));
    }

    @Test
    @DisplayName("Test delete()")
    void test7() throws DAOException {
        Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
        Assertions.assertDoesNotThrow(()->ServiceFactory.getAuthorService().delete(1));
    }
}
