package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.fine.FineDAO;
import com.my.library.dao.entity.bookOrder.*;
import com.my.library.dao.entity.fine.*;
import com.my.library.exception.*;
import com.my.library.service.abstr.FineService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestFineService {
    @Mock
    FineDAO fineDAO = mock(FineDAO.class);
    @Spy
    FineService fineService = ServiceFactory.getFineService();

    @Test
    @DisplayName("Test calculateAmountOfFine()")
    void test1() throws ServiceException, DAOException {
        Calendar calReturn = Calendar.getInstance();  calReturn.setTime(new java.util.Date());  calReturn.add(Calendar.DATE, -2);
        Calendar cal = Calendar.getInstance();  cal.setTime(new java.util.Date());  cal.add(Calendar.DATE, -4);

        BookOrder order = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(cal.getTime().getTime()),
                new Date(calReturn.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );
        order.setId(1);

        cal.add(Calendar.DATE, 10);
        BookOrder order2 = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(new java.util.Date().getTime()),
                new Date(cal.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );
        order2.setId(2);

        Fine fine = new Fine(1,10); fine.setId(1);
        fine.setStatus(new FineStatus(1,"Paid","Оплачений"));

        Mockito.lenient().doReturn(Optional.of(List.of(fine))).when(fineDAO).getByOrderID(1);
        Mockito.lenient().doReturn(Optional.of(new ArrayList())).when(fineDAO).getByOrderID(2);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);

            Assertions.assertEquals(10, ServiceFactory.getFineService().calculateAmountOfFine(order));
            Assertions.assertEquals(0, ServiceFactory.getFineService().calculateAmountOfFine(order2));
        }
    }

    @Test
    @DisplayName("Test needToBeFined()")
    void test2(){
        Calendar calReturn = Calendar.getInstance();  calReturn.setTime(new java.util.Date());  calReturn.add(Calendar.DATE, -2);
        Calendar cal = Calendar.getInstance();  cal.setTime(new java.util.Date());  cal.add(Calendar.DATE, -4);

        BookOrder order = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(cal.getTime().getTime()),
                new Date(calReturn.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );

        Assertions.assertTrue(ServiceFactory.getFineService().needToBeFined(order));
    }

    @Test
    @DisplayName("Test needToBeUpdated()")
    void test3() throws DAOException, ServiceException {
        Calendar calReturn = Calendar.getInstance();  calReturn.setTime(new java.util.Date());  calReturn.add(Calendar.DATE, -2);
        Calendar cal = Calendar.getInstance();  cal.setTime(new java.util.Date());  cal.add(Calendar.DATE, -4);

        BookOrder order = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(cal.getTime().getTime()),
                new Date(calReturn.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );
        order.setId(1);
        Fine fine = new Fine(1,10);

        Mockito.doReturn(Optional.of(fine)).when(fineDAO).getByOrderIdUnpaid(1);
        Mockito.doReturn(20).when(fineService).calculateAmountOfFine(order);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
            MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            serviceFactoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);

            Assertions.assertTrue(ServiceFactory.getFineService().needToBeUpdated(order));
            fine.setAmount(20);
            Assertions.assertFalse(ServiceFactory.getFineService().needToBeUpdated(order));
        }
    }

    @Test
    @DisplayName("Test update()")
    void test4() throws ServiceException, DAOException {
        Calendar calReturn = Calendar.getInstance();  calReturn.setTime(new java.util.Date());  calReturn.add(Calendar.DATE, -2);
        Calendar cal = Calendar.getInstance();  cal.setTime(new java.util.Date());  cal.add(Calendar.DATE, -4);

        BookOrder order = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(cal.getTime().getTime()),
                new Date(calReturn.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );
        order.setId(1);

        Fine fine = new Fine(1,10);

        Mockito.doReturn(Optional.of(fine)).when(fineDAO).getByOrderIdUnpaid(1);
        Mockito.doReturn(20).when(fineService).calculateAmountOfFine(order);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
            MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            serviceFactoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);

            Mockito.doAnswer((i)->{
                Fine fineArg = (Fine) i.getArguments()[0];
                fineArg.setAmount(20);
                return null;
            }).when(fineDAO).updateAmount(fine,20);

            Assertions.assertTrue(ServiceFactory.getFineService().update(order));
            Assertions.assertEquals(20,fine.getAmount());

        }
    }

    @Test
    @DisplayName("Test insert()")
    void test5() throws ServiceException, DAOException {
        Calendar calReturn = Calendar.getInstance();  calReturn.setTime(new java.util.Date());  calReturn.add(Calendar.DATE, -2);
        Calendar cal = Calendar.getInstance();  cal.setTime(new java.util.Date());  cal.add(Calendar.DATE, -4);

        BookOrder order = new BookOrder(
                1,
                "reader",
                new BookOrderType(1,"type","тип"),
                new Date(cal.getTime().getTime()),
                new Date(calReturn.getTime().getTime()),
                new BookOrderStatus(1,"status", "статус")
        );
        order.setId(1);

        Mockito.doReturn(20).when(fineService).calculateAmountOfFine(order);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
            MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            serviceFactoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);

            Mockito.doAnswer((i)->{
                Fine fineArg = (Fine) i.getArguments()[0];
                fineArg.setId(1);
                return null;
            }).when(fineDAO).insert(any(Fine.class));

            Assertions.assertTrue(ServiceFactory.getFineService().insertFine(order));

            Mockito.doReturn(0).when(fineService).calculateAmountOfFine(order);
            Assertions.assertFalse(ServiceFactory.getFineService().insertFine(order));
        }
    }

    @Test
    @DisplayName("Test getFineAmountByOrderID()")
    void test6() throws DAOException {
        Fine fine = new Fine(1,10);
        Mockito.doReturn(Optional.of(fine)).when(fineDAO).getByOrderIdUnpaid(1);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);

            Assertions.assertDoesNotThrow(()->{
                int amount = ServiceFactory.getFineService().getFineAmountByOrderID(1);
                Assertions.assertEquals(10,amount);

                Mockito.doReturn(Optional.empty()).when(fineDAO).getByOrderIdUnpaid(1);
                amount = ServiceFactory.getFineService().getFineAmountByOrderID(1);
                Assertions.assertEquals(0,amount);
            });

            Assertions.assertThrows(ServiceException.class, ()->{
               Mockito.doThrow(DAOException.class).when(fineDAO).getByOrderIdUnpaid(1);
               ServiceFactory.getFineService().getFineAmountByOrderID(1);
            });
        }
    }

    @Test
    @DisplayName("Test orderIsFined()")
    void test7() throws DAOException {
        Fine fine = new Fine(1,10);
        Mockito.doReturn(Optional.of(fine)).when(fineDAO).getByOrderIdUnpaid(1);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);

            Assertions.assertDoesNotThrow(()->{
               ServiceFactory.getFineService().orderIsFined(1);
            });

            Assertions.assertThrows(ServiceException.class, ()->{
                Mockito.doThrow(DAOException.class).when(fineDAO).getByOrderIdUnpaid(1);
                ServiceFactory.getFineService().orderIsFined(1);
            });
        }
    }
}
