package com.my.library.service;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.bookOrder.BookOrderDAO;
import com.my.library.dao.abstr.fine.FineDAO;
import com.my.library.dto.ReaderDTO;
import com.my.library.exception.DAOException;
import com.my.library.service.abstr.ReaderService;
import com.my.library.service.abstr.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.ORDER_CANCELED_STATUS;
import static com.my.library.dao.mysql.Constant.ORDER_FINISHED_STATUS;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestReaderService {
    @Mock
    BookOrderDAO bookOrderDAO = mock(BookOrderDAO.class);
    @Mock
    FineDAO fineDAO = mock(FineDAO.class);
    @Mock
    UserService userService = mock(UserService.class);

    @Test
    @DisplayName("Test getReader()")
    void test1() throws DAOException {
        Mockito.doReturn(Optional.of(5)).when(bookOrderDAO).countAllReaderOrders("login");
        Mockito.doReturn(Optional.of(2)).when(bookOrderDAO).countReaderOrders("login",ORDER_FINISHED_STATUS);
        Mockito.doReturn(Optional.of(3)).when(bookOrderDAO).countReaderOrders("login",ORDER_CANCELED_STATUS);
        Mockito.doReturn(Optional.of(0)).when(fineDAO).countReaderFines("login",0);
        Mockito.doReturn(Optional.of(4)).when(fineDAO).countReaderFines("login",1);

        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)){
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);

            Assertions.assertDoesNotThrow(()->{
                ReaderDTO readerDTO = ServiceFactory.getReaderService().getReader("login");
                Assertions.assertEquals(0,readerDTO.getActiveOrders());
                Assertions.assertEquals(2,readerDTO.getFinishedOrders());
                Assertions.assertEquals(3,readerDTO.getCanceledOrders());
                Assertions.assertEquals(0,readerDTO.getUnPaidFines());
                Assertions.assertEquals(4,readerDTO.getPaidFines());
            });
        }
    }

    @Test
    @DisplayName("Test tryToChangeReaderStatusToActive()")
    void test2() throws DAOException {
        ReaderService readerService = ServiceFactory.getReaderService();
        try(MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class);
            MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn(Optional.of(new ArrayList())).when(bookOrderDAO).getAllByReaderFK("login");
            Assertions.assertDoesNotThrow(()->{
                readerService.tryToChangeReaderStatusToActive("login");
                Mockito.verify(userService).updateStatus(anyString(),anyString());
            });
        }
    }
}
