package com.my.library.dao.mysql.fine;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.bookOrder.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.dao.entity.fine.*;
import com.my.library.dao.entity.user.*;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestFinePaymentDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final UserRole userRole = new UserRole(0, "Reader");
    private static final BookOrderType orderType0 = new BookOrderType(0,"Type0","Тип0");
    private static final BookOrderStatus orderStatus0 = new BookOrderStatus(0,"Status0","Статус0");
    private static Fine fine;
    private static final FineStatus fineStatus0 = new FineStatus(0,"Unpaid","Неоплачений");
    private static final FineStatus fineStatus1 = new FineStatus(1,"Paid","Оплачений");
    private static FinePayment payment;

    @BeforeAll
    static void globalSetUp() throws ParseException {
        LogManagerForTesting.create("FinePaymentDAO");

        LogManagerForTesting.create("BookOrderDAO");

        Author author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);

        Publication publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        Book book = builder.get();
        book.setId(1);

        User reader = new User(
                "login",
                "12345",
                "test@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                userRole);
        reader.setStatus(new UserStatus(0,"Active"));

        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFormat.parse("2022-12-17");
        Date dateReturned = dateFormat.parse("2022-12-27");
        BookOrder bookOrder = new BookOrder(
                1,
                "login",
                orderType0,
                new java.sql.Date(date.getTime()),
                new java.sql.Date(dateReturned.getTime()),
                orderStatus0);
        bookOrder.setId(1);

        fine = new Fine(bookOrder.getId(), 100);
        fine.setId(1);
    }

    @BeforeEach
    void objectsCreate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2022-12-20");
        payment = new FinePayment(fine.getId(), new java.sql.Date(date.getTime()));
    }

    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(), eq(Statement.RETURN_GENERATED_KEYS));
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();
            Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);

            Assertions.assertNull(payment.getId());
            DAOFactory.getFinePaymentDAO().insert(payment, con);
            Assertions.assertNotNull(payment.getId());
        }
    }

    @Test
    @DisplayName("Test getByFinePaymentID()")
    void test2() throws ParseException, DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(fine.getId()).when(resultSet).getInt(2);
            Mockito.doReturn(payment.getDate()).when(resultSet).getDate(3);

            Optional<FinePayment> optional = DAOFactory.getFinePaymentDAO().getByFinePaymentID(1);
            Assertions.assertTrue(optional.isPresent());
            FinePayment payment = optional.get();

            Assertions.assertEquals(1, payment.getFineIdFK());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("2022-12-20");
            Assertions.assertEquals(new java.sql.Date(date.getTime()), payment.getDate());
            Assertions.assertEquals(1, payment.getId());
        }
    }

    @Test
    @DisplayName("Test getByFineID()")
    void test3() throws ParseException, DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(fine.getId()).when(resultSet).getInt(2);
            Mockito.doReturn(payment.getDate()).when(resultSet).getDate(3);

            Optional<FinePayment> optional = DAOFactory.getFinePaymentDAO().getByFineID(1);
            Assertions.assertTrue(optional.isPresent());
            FinePayment payment = optional.get();

            Assertions.assertEquals(1, payment.getFineIdFK());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("2022-12-20");
            Assertions.assertEquals(new java.sql.Date(date.getTime()), payment.getDate());
            Assertions.assertEquals(1, payment.getId());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(fine.getId()).when(resultSet).getInt(2);
            Mockito.doReturn(payment.getDate()).when(resultSet).getDate(3);

            Optional<List<FinePayment>> optional =
                    DAOFactory.getFinePaymentDAO().getAll();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1, optional.get().size());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            payment.setId(1);
            DAOFactory.getFinePaymentDAO().delete(payment, con);
            Assertions.assertNull(payment.getId());
        }
    }



    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}
