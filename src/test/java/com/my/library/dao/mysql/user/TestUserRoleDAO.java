package com.my.library.dao.mysql.user;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.UserRole;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUserRoleDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final UserRole role = new UserRole(0,"Reader");

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("UserRoleDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(role.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(role.getValue()).when(resultSet).getString(2);

            Optional<UserRole> optional = DAOFactory.getUserRoleDAO().get(0);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals("Reader", optional.get().getValue());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(role.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(role.getValue()).when(resultSet).getString(2);

            Optional<List<UserRole>> optional =
                    DAOFactory.getUserRoleDAO().getAll();
            Assertions.assertTrue(optional.isPresent());
            List<UserRole> list = optional.get();

            Assertions.assertTrue(list.contains(role));
            Assertions.assertEquals(1, list.size());
        }
    }

    @Test
    @DisplayName("Test insert()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            Assertions.assertTrue(DAOFactory.getUserRoleDAO().insert(role));
        }
    }

    @Test
    @DisplayName("Test update()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserRoleDAO().update(role, "newName");
            Assertions.assertEquals("newName", role.getValue());
        }

    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserRoleDAO().delete(role);
            Assertions.assertNull(role.getId());
        }
    }

    @Test
    @DisplayName("Test getByValue()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            role.setId(0);
            Mockito.doReturn(role.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(role.getValue()).when(resultSet).getString(2);

            Optional<UserRole> optional = DAOFactory.getUserRoleDAO().getByValue("Reader");
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(0, optional.get().getId());
        }
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
