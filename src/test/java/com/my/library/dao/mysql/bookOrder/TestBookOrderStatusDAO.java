package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookOrderStatusDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final BookOrderStatus orderStatus0 = new BookOrderStatus(0,"Status0","Статус0");
    private static final BookOrderStatus orderStatus1 = new BookOrderStatus(1,"Status1","Статус1");

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("BookOrderStatusDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(0).when(resultSet).getInt(1);
            Mockito.doReturn(orderStatus0.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(orderStatus0.getValueUA()).when(resultSet).getString(3);

            Optional<BookOrderStatus> optional =
                    DAOFactory.getBookOrderStatusDAO().get(0);
            Assertions.assertTrue(optional.isPresent());

            BookOrderStatus status = optional.get();
            Assertions.assertEquals("Status0", status.getValueEN());
            Assertions.assertEquals("Статус0", status.getValueUA());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(0).doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(orderStatus0.getValueEN()).doReturn(orderStatus1.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(orderStatus0.getValueUA()).doReturn(orderStatus1.getValueUA()).when(resultSet).getString(3);

            Optional<List<BookOrderStatus>> optional =
                    DAOFactory.getBookOrderStatusDAO().getAll();
            Assertions.assertTrue(optional.isPresent());

            List<BookOrderStatus> list = optional.get();

            Assertions.assertTrue(list.contains(orderStatus0));
            Assertions.assertTrue(list.contains(orderStatus1));
            Assertions.assertEquals(2, list.size());
        }
    }

    @Test
    @DisplayName("Test updateEN()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            DAOFactory.getBookOrderStatusDAO().updateEN(orderStatus0, "newStatus");
            Assertions.assertEquals("newStatus", orderStatus0.getValueEN());
        }
    }

    @Test
    @DisplayName("Test updateUA()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getBookOrderStatusDAO().updateUA(orderStatus0, "новийСтатус");
            Assertions.assertEquals("новийСтатус", orderStatus0.getValueUA());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getBookOrderStatusDAO().delete(orderStatus0);
            Assertions.assertNull(orderStatus0.getId());
        }
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
