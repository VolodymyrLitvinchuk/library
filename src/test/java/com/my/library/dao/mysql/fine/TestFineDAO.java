package com.my.library.dao.mysql.fine;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.abstr.fine.*;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.bookOrder.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.dao.entity.fine.*;
import com.my.library.dao.entity.user.*;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestFineDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);
    @Mock
    private static FineStatusDAO fineStatusDAO = mock(FineStatusDAO.class);

    private static final FineDAO fineDAO = DAOFactory.getFineDAO();
    private static final UserRole userRole = new UserRole(0, "Reader");
    private static final BookOrderType orderType0 = new BookOrderType(0,"Type0","Тип0");
    private static final BookOrderStatus orderStatus0 = new BookOrderStatus(0,"Status0","Статус0");
    private static Fine fine;
    private static final FineStatus fineStatus0 = new FineStatus(0,"Unpaid","Неоплачений");
    private static final FineStatus fineStatus1 = new FineStatus(1,"Paid","Оплачений");

    @BeforeAll
    static void globalSetUp() throws SQLException, ParseException {
        LogManagerForTesting.create("FineDAO");

        LogManagerForTesting.create("BookOrderDAO");

        Author author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);

        Publication publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        Book book = builder.get();
        book.setId(1);

        User reader = new User(
                "login",
                "12345",
                "test@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                userRole);
        reader.setStatus(new UserStatus(0,"Active"));
    }

    @BeforeEach
    void objectsCreate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2022-12-17");
        Date dateReturned = dateFormat.parse("2022-12-27");
        BookOrder bookOrder = new BookOrder(
                1,
                "login",
                orderType0,
                new java.sql.Date(date.getTime()),
                new java.sql.Date(dateReturned.getTime()),
                orderStatus0);
        bookOrder.setId(1);

        fine = new Fine(bookOrder.getId(), 100);
    }


    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getFineStatusDAO).thenReturn(fineStatusDAO);
            Mockito.doReturn(Optional.of(fineStatus0)).when(fineStatusDAO).get(0);
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(),eq(Statement.RETURN_GENERATED_KEYS));
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();
            Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);

            DAOFactory.getFineDAO().insert(fine);
            Assertions.assertNotNull(fine.getId());
            Assertions.assertNotNull(fine.getStatus());

        }
    }

    @Test
    @DisplayName("Test getByFineID()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getFineStatusDAO).thenReturn(fineStatusDAO);
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(100).when(resultSet).getInt(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(fineStatus0)).when(fineStatusDAO).get(0);

            Optional<Fine> optional = DAOFactory.getFineDAO().getByFineID(1);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(fineStatus0,optional.get().getStatus());
        }
    }

    @Test
    @DisplayName("Test getByBookOrderID()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getFineStatusDAO).thenReturn(fineStatusDAO);
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(100).when(resultSet).getInt(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(fineStatus0)).when(fineStatusDAO).get(0);

            Optional<List<Fine>> optional = DAOFactory.getFineDAO().getByOrderID(1);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1,optional.get().size());
            Assertions.assertEquals(fineStatus0,optional.get().get(0).getStatus());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getFineStatusDAO).thenReturn(fineStatusDAO);
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(100).when(resultSet).getInt(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(fineStatus0)).when(fineStatusDAO).get(0);

            Optional<List<Fine>> optional =
                    DAOFactory.getFineDAO().getAll();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1, optional.get().size());
        }
    }

    @Test
    @DisplayName("Test getByBookOrderIdUnpaid()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getFineDAO).thenReturn(fineDAO);
            daoFactoryMockedStatic.when(DAOFactory::getFineStatusDAO).thenReturn(fineStatusDAO);
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(100).when(resultSet).getInt(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(fineStatus0)).when(fineStatusDAO).get(0);

            Optional<Fine> optional = DAOFactory.getFineDAO().getByOrderIdUnpaid(1);
            Assertions.assertTrue(optional.isPresent());
        }
    }

    @Test
    @DisplayName("Test updateStatus()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            fine.setId(1);
            fine.setStatus(fineStatus0);
            Assertions.assertEquals(0, fine.getStatus().getId());
            DAOFactory.getFineDAO().updateStatus(fine, fineStatus1, con);
            Assertions.assertEquals(1, fine.getStatus().getId());
        }

    }

    @Test
    @DisplayName("Test updateAmount()")
    void test7() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            fine.setId(1);
            Assertions.assertEquals(100, fine.getAmount());
            DAOFactory.getFineDAO().updateAmount(fine, 200);
            Assertions.assertEquals(200, fine.getAmount());
        }
    }

    @Test
    @DisplayName("Test countReaderFines()")
    void test8() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);

            Optional<Integer> optionalInteger = DAOFactory.getFineDAO().countReaderFines("login", 1);
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(1, optionalInteger.get());


            Mockito.doReturn(0).when(resultSet).getInt(1);
            optionalInteger = DAOFactory.getFineDAO().countReaderFines("login", 0);
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(0, optionalInteger.get());
        }

    }

    @Test
    @DisplayName("Test delete()")
    void test9() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            fine.setId(1);

            DAOFactory.getFineDAO().delete(fine, con);
            Assertions.assertNull(fine.getId());
        }
    }


    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}
