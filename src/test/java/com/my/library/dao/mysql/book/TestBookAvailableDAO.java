package com.my.library.dao.mysql.book;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookAvailableDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("BookAvailableDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(3).when(resultSet).getInt(2);

            Optional<Integer> optionalInteger = DAOFactory.getBookAvailableDAO().get(1);
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(3, optionalInteger.get());
        }
    }

    @Test
    @DisplayName("Test insert()")
    void test2() throws SQLException, DAOException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();

        Assertions.assertTrue(DAOFactory.getBookAvailableDAO().insert(1,2,con));
    }

    @Test
    @DisplayName("Test update()")
    void test3() throws SQLException, DAOException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();

        Assertions.assertTrue(DAOFactory.getBookAvailableDAO().update(1,3,con));
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
