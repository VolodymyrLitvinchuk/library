package com.my.library.dao.mysql.fine;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestFineStatusDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final FineStatus fineStatus0 = new FineStatus(0,"Unpaid","Не оплачений");
    private static final FineStatus fineStatus1 = new FineStatus(1,"Paid","Оплачений");

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("FineStatusDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(0).when(resultSet).getInt(1);
            Mockito.doReturn(fineStatus0.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(fineStatus0.getValueUA()).when(resultSet).getString(3);

            Optional<FineStatus> optional =
                    DAOFactory.getFineStatusDAO().get(0);
            Assertions.assertTrue(optional.isPresent());

            FineStatus status = optional.get();
            Assertions.assertEquals("Unpaid", status.getValueEN());
            Assertions.assertEquals("Не оплачений", status.getValueUA());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(0).doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(fineStatus0.getValueEN()).doReturn(fineStatus1.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(fineStatus0.getValueUA()).doReturn(fineStatus1.getValueUA()).when(resultSet).getString(3);

            Optional<List<FineStatus>> optional =
                    DAOFactory.getFineStatusDAO().getAll();
            Assertions.assertTrue(optional.isPresent());

            List<FineStatus> list = optional.get();

            Assertions.assertTrue(list.contains(fineStatus1));
            Assertions.assertTrue(list.contains(fineStatus0));
            Assertions.assertEquals(2, list.size());
        }
    }

    @Test
    @DisplayName("Test updateEN()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getFineStatusDAO().updateEN(fineStatus0, "newStatus");
            Assertions.assertEquals("newStatus", fineStatus0.getValueEN());
        }
    }

    @Test
    @DisplayName("Test updateUA()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getFineStatusDAO().updateUA(fineStatus0, "новийСтатус");
            Assertions.assertEquals("новийСтатус", fineStatus0.getValueUA());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getFineStatusDAO().delete(fineStatus0);
            Assertions.assertNull(fineStatus0.getId());
        }
    }

    @Test
    @DisplayName("Test insert()")
    void test6() throws SQLException, DAOException {
        try (MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            fineStatus0.setId(0);
            Assertions.assertTrue(DAOFactory.getFineStatusDAO().insert(fineStatus0));
        }
    }
    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
