package com.my.library.dao.mysql.user;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.abstr.user.*;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.*;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUserDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);
    @Mock
    private static Statement statement = mock(Statement.class);
    @Mock
    private static UserRoleDAO userRoleDAO = mock(UserRoleDAO.class);
    @Mock
    private static UserStatusDAO userStatusDAO = mock(UserStatusDAO.class);

    private static final UserDAO userDAO = DAOFactory.getUserDAO();
    private static final UserStatus status = new UserStatus(0,"Active");
    private static final UserStatus status1 = new UserStatus(1,"Locked");
    private static final UserRole role = new UserRole(0,"Reader");
    private static User user;


    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("UserDAO");

        user = new User(
                "reader",
                "ABCabc13",
                "2001litvinchuk2001@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                role
        );

    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserStatusDAO).thenReturn(userStatusDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserRoleDAO).thenReturn(userRoleDAO);

            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();

            Mockito.doReturn(user.getLogin()).when(resultSet).getString(1);
            Mockito.doReturn(user.getPasswordHash()).when(resultSet).getString(2);
            Mockito.doReturn(user.getEmail()).when(resultSet).getString(3);
            Mockito.doReturn(user.getFirstName()).when(resultSet).getString(4);
            Mockito.doReturn(user.getLastName()).when(resultSet).getString(5);
            Mockito.doReturn(0).when(resultSet).getInt(6);
            Mockito.doReturn(0).when(resultSet).getInt(7);

            Mockito.doReturn(Optional.of(status)).when(userStatusDAO).get(0);
            Mockito.doReturn(Optional.of(role)).when(userRoleDAO).get(0);


            Optional<User> optional = DAOFactory.getUserDAO().get("reader");
            Assertions.assertTrue(optional.isPresent(), "Cannot get Default user with login=reader");
        }
    }

    @Test
    @DisplayName("Test getByEmail()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserStatusDAO).thenReturn(userStatusDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserRoleDAO).thenReturn(userRoleDAO);

            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();

            Mockito.doReturn(user.getLogin()).when(resultSet).getString(1);
            Mockito.doReturn(user.getPasswordHash()).when(resultSet).getString(2);
            Mockito.doReturn(user.getEmail()).when(resultSet).getString(3);
            Mockito.doReturn(user.getFirstName()).when(resultSet).getString(4);
            Mockito.doReturn(user.getLastName()).when(resultSet).getString(5);
            Mockito.doReturn(0).when(resultSet).getInt(6);
            Mockito.doReturn(0).when(resultSet).getInt(7);

            Mockito.doReturn(Optional.of(status)).when(userStatusDAO).get(0);
            Mockito.doReturn(Optional.of(role)).when(userRoleDAO).get(0);

            Optional<User> optional = DAOFactory.getUserDAO().getByEmail("2001litvinchuk2001@gmail.com");
            Assertions.assertTrue(optional.isPresent(), "Cannot get Default user with email=2001litvinchuk2001@gmail.com");
        }
    }

    @Test
    @DisplayName("Test insert()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserStatusDAO).thenReturn(userStatusDAO);
            Mockito.doReturn(Optional.of(status)).when(userStatusDAO).get(0);

            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            DAOFactory.getUserDAO().insert(user);
            Assertions.assertNotNull(user.getStatus());
        }

    }

    @Test
    @DisplayName("Test getAll()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getUserDAO).thenReturn(userDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserStatusDAO).thenReturn(userStatusDAO);
            daoFactoryMockedStatic.when(DAOFactory::getUserRoleDAO).thenReturn(userRoleDAO);

            Mockito.doReturn(statement).when(con).createStatement();
            Mockito.doReturn(resultSet).when(statement).executeQuery(anyString());
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();

            Mockito.doReturn(user.getLogin()).when(resultSet).getString(1);
            Mockito.doReturn(user.getPasswordHash()).when(resultSet).getString(2);
            Mockito.doReturn(user.getEmail()).when(resultSet).getString(3);
            Mockito.doReturn(user.getFirstName()).when(resultSet).getString(4);
            Mockito.doReturn(user.getLastName()).when(resultSet).getString(5);
            Mockito.doReturn(0).when(resultSet).getInt(6);
            Mockito.doReturn(0).when(resultSet).getInt(7);

            Mockito.doReturn(Optional.of(status)).when(userStatusDAO).get(0);
            Mockito.doReturn(Optional.of(role)).when(userRoleDAO).get(0);

            user.setStatus(status);


            Optional<List<User>> optionalUsers = DAOFactory.getUserDAO().getAll();
            Assertions.assertTrue(optionalUsers.isPresent(), "Cannot get all users");
            List<User> users = optionalUsers.get();


            Assertions.assertTrue(users.contains(user), "List must contain user login=reader");
            Assertions.assertEquals(1, users.size());
        }
    }

    @Test
    @DisplayName("Test updateLogin()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            DAOFactory.getUserDAO().updateLogin(user, "newLogin");
            Assertions.assertEquals("newLogin", user.getLogin(), "Login must be equals");
        }
    }

    @Test
    @DisplayName("Test updatePassword()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().updatePassword(user, "123");
            Assertions.assertEquals("123", user.getPasswordHash(), "Password must be equals");
        }
    }

    @Test
    @DisplayName("Test updateEmail()")
    void test7() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().updateEmail(user, "newEmail@test.com");
            Assertions.assertEquals("newEmail@test.com", user.getEmail(), "Email must be equals");
        }
    }

    @Test
    @DisplayName("Test updateFirstName()")
    void test8() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().updateFirstName(user, "bob");
            Assertions.assertEquals("bob", user.getFirstName(), "First name must be equals");
        }
    }

    @Test
    @DisplayName("Test updateLastName()")
    void test9() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().updateLastName(user, "newLastName");
            Assertions.assertEquals("newLastName", user.getLastName(), "Last name must be equals");
        }
    }

    @Test
    @DisplayName("Test updateStatus()")
    void test10() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().updateStatus(user, status1);
            Assertions.assertEquals("Locked", user.getStatus().getValue());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test11() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserDAO().delete(user);
            Assertions.assertNull(user.getStatus());
        }
    }

    @Test
    @DisplayName("Test countAllUsers()")
    void test12() throws SQLException, DAOException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(2).when(resultSet).getInt(1);

            Optional<Integer> optional = DAOFactory.getUserDAO().countAllUsers();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(2,optional.get());

        }
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();
    }

}
