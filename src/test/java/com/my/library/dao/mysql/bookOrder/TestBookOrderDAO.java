package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.abstr.bookOrder.*;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.bookOrder.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.*;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookOrderDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static Statement statement = mock(Statement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);
    @Mock
    private static BookOrderTypeDAO bookOrderTypeDAO = mock(BookOrderTypeDAO.class);
    @Mock
    private static BookOrderStatusDAO bookOrderStatusDAO = mock(BookOrderStatusDAO.class);

    private static final BookOrderDAO bookOrderDAO = DAOFactory.getBookOrderDAO();

    private static Author author;
    private static Publication publication;
    private static Book book;
    private static User reader;
    private static final UserRole userRole = new UserRole(0, "Reader");
    private static final BookOrderType orderType0 = new BookOrderType(0,"Type0","Тип0");
    private static final BookOrderType orderType1 = new BookOrderType(1,"Type1","Тип1");
    private static final BookOrderStatus orderStatus0 = new BookOrderStatus(0,"Status0","Статус0");
    private static final BookOrderStatus orderStatus1 = new BookOrderStatus(1,"Status1","Статус1");
    private static BookOrder bookOrder;

    @BeforeEach
    void objectsCreate() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2022-12-17");
        Date dateReturned = dateFormat.parse("2022-12-27");
        bookOrder = new BookOrder(
                1,
                "login",
                orderType0,
                new java.sql.Date(date.getTime()),
                new java.sql.Date(dateReturned.getTime()),
                orderStatus0);

    }

    @BeforeAll
    static void globalSetUp() throws ParseException {
        LogManagerForTesting.create("BookOrderDAO");

        author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);

        publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        book = builder.get();
        book.setId(1);

        reader = new User(
                "login",
                "12345",
                "test@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                userRole);
        reader.setStatus(new UserStatus(0,"Active"));
    }

    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(),eq(Statement.RETURN_GENERATED_KEYS));
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();
        Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
        Mockito.doReturn(true).when(resultSet).next();
        Mockito.doReturn(1).when(resultSet).getInt(1);

        Assertions.assertNull(bookOrder.getId());
        DAOFactory.getBookOrderDAO().insert(bookOrder,con);
        Assertions.assertNotNull(bookOrder.getId());
        Assertions.assertEquals(1,bookOrder.getId());

    }

    @Test
    @DisplayName("Test get()")
    void test2() throws ParseException, DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderTypeDAO).thenReturn(bookOrderTypeDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderStatusDAO).thenReturn(bookOrderStatusDAO);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(reader.getLogin()).when(resultSet).getString(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(orderType0)).when(bookOrderTypeDAO).get(0);
            Mockito.doReturn(0).when(resultSet).getInt(7);
            Mockito.doReturn(Optional.of(orderStatus0)).when(bookOrderStatusDAO).get(0);
            Mockito.doReturn(bookOrder.getDate()).when(resultSet).getDate(5);
            Mockito.doReturn(bookOrder.getDateReturnPlanned()).when(resultSet).getDate(6);



            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(1);
            Assertions.assertTrue(optional.isPresent());

            BookOrder bookOrder2 = optional.get();

            Assertions.assertEquals(1, bookOrder2.getId());
            Assertions.assertEquals(1, bookOrder2.getBookIdFK());
            Assertions.assertEquals("login", bookOrder2.getReaderFK());
            Assertions.assertEquals(0, bookOrder2.getType().getId());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("2022-12-17");
            Date dateReturned = dateFormat.parse("2022-12-27");

            Assertions.assertEquals(date, bookOrder2.getDate());
            Assertions.assertEquals(dateReturned, bookOrder2.getDateReturnPlanned());
            Assertions.assertEquals(0, bookOrder2.getStatus().getId());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test3() throws SQLException, DAOException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderTypeDAO).thenReturn(bookOrderTypeDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderStatusDAO).thenReturn(bookOrderStatusDAO);
            Mockito.doReturn(statement).when(con).createStatement();
            Mockito.doReturn(resultSet).when(statement).executeQuery(anyString());
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(reader.getLogin()).when(resultSet).getString(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(orderType0)).when(bookOrderTypeDAO).get(0);
            Mockito.doReturn(0).when(resultSet).getInt(7);
            Mockito.doReturn(Optional.of(orderStatus0)).when(bookOrderStatusDAO).get(0);
            Mockito.doReturn(bookOrder.getDate()).when(resultSet).getDate(5);
            Mockito.doReturn(bookOrder.getDateReturnPlanned()).when(resultSet).getDate(6);

            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAll();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1,optional.get().size());
            Assertions.assertEquals(1, optional.get().get(0).getId());
            Assertions.assertEquals(1, optional.get().get(0).getBookIdFK());
            Assertions.assertEquals("login", optional.get().get(0).getReaderFK());
            Assertions.assertEquals(0, optional.get().get(0).getType().getId());
        }
    }

    @Test
    @DisplayName("Test getAllByReaderFK()")
    void test4() throws SQLException, DAOException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderTypeDAO).thenReturn(bookOrderTypeDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderStatusDAO).thenReturn(bookOrderStatusDAO);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(reader.getLogin()).when(resultSet).getString(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(orderType0)).when(bookOrderTypeDAO).get(0);
            Mockito.doReturn(0).when(resultSet).getInt(7);
            Mockito.doReturn(Optional.of(orderStatus0)).when(bookOrderStatusDAO).get(0);
            Mockito.doReturn(bookOrder.getDate()).when(resultSet).getDate(5);
            Mockito.doReturn(bookOrder.getDateReturnPlanned()).when(resultSet).getDate(6);

            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAllByReaderFK(reader.getLogin());
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1,optional.get().size());
            Assertions.assertEquals(1, optional.get().get(0).getId());
            Assertions.assertEquals(1, optional.get().get(0).getBookIdFK());
            Assertions.assertEquals("login", optional.get().get(0).getReaderFK());
            Assertions.assertEquals(0, optional.get().get(0).getType().getId());
        }
    }

    @Test
    @DisplayName("Test getAllByStatus()")
    void test5() throws SQLException, DAOException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderDAO).thenReturn(bookOrderDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderTypeDAO).thenReturn(bookOrderTypeDAO);
            daoFactoryMockedStatic.when(DAOFactory::getBookOrderStatusDAO).thenReturn(bookOrderStatusDAO);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(2);
            Mockito.doReturn(reader.getLogin()).when(resultSet).getString(3);
            Mockito.doReturn(0).when(resultSet).getInt(4);
            Mockito.doReturn(Optional.of(orderType0)).when(bookOrderTypeDAO).get(0);
            Mockito.doReturn(0).when(resultSet).getInt(7);
            Mockito.doReturn(Optional.of(orderStatus0)).when(bookOrderStatusDAO).get(0);
            Mockito.doReturn(bookOrder.getDate()).when(resultSet).getDate(5);
            Mockito.doReturn(bookOrder.getDateReturnPlanned()).when(resultSet).getDate(6);

            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAllByStatus(0);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(1,optional.get().size());
            Assertions.assertEquals(1, optional.get().get(0).getId());
            Assertions.assertEquals(1, optional.get().get(0).getBookIdFK());
            Assertions.assertEquals("login", optional.get().get(0).getReaderFK());
            Assertions.assertEquals(0, optional.get().get(0).getType().getId());
        }
    }



    @Test
    @DisplayName("Test updateType()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            bookOrder.setId(1);

            DAOFactory.getBookOrderDAO().updateType(bookOrder, orderType1);
            Assertions.assertEquals(1, bookOrder.getType().getId());
        }
    }

    @Test
    @DisplayName("Test updateStatus()")
    void test7() throws DAOException, SQLException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();

        bookOrder.setId(1);

        DAOFactory.getBookOrderDAO().updateStatus(bookOrder, orderStatus1, con);
        Assertions.assertEquals(1, bookOrder.getStatus().getId());

    }

    @Test
    @DisplayName("Test hasReaderUnfinishedOrder() and countReaderOrders() and countAllReaderOrders() and countAllActiveOrders()")
    void test8() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            bookOrder.setId(1);

            Assertions.assertTrue(DAOFactory.getBookOrderDAO().hasReaderUnfinishedOrder("login", 1));
            Mockito.doReturn(false).when(resultSet).next();
            Assertions.assertFalse(DAOFactory.getBookOrderDAO().hasReaderUnfinishedOrder("login", 2));


            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Optional<Integer> optionalInteger = DAOFactory.getBookOrderDAO().countReaderOrders("login", 1);
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(1, optionalInteger.get());

            Mockito.doReturn(0).when(resultSet).getInt(1);
            optionalInteger = DAOFactory.getBookOrderDAO().countReaderOrders("login", 0);
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(0, optionalInteger.get());


            Mockito.doReturn(1).when(resultSet).getInt(1);
            optionalInteger = DAOFactory.getBookOrderDAO().countAllReaderOrders("login");
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(1, optionalInteger.get());

            optionalInteger = DAOFactory.getBookOrderDAO().countAllActiveOrders();
            Assertions.assertTrue(optionalInteger.isPresent());
            Assertions.assertEquals(1, optionalInteger.get());
        }

    }

    @Test
    @DisplayName("Test delete()")
    void test9() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            bookOrder.setId(1);

            DAOFactory.getBookOrderDAO().delete(bookOrder);
            Assertions.assertNull(bookOrder.getId());
        }
    }

    @Test
    @DisplayName("Test userHasUnfinishedOrders()")
    void test10() throws SQLException, DAOException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();

            Assertions.assertTrue(DAOFactory.getBookOrderDAO().userHasUnfinishedOrders(reader.getLogin()));
        }
    }

    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}

