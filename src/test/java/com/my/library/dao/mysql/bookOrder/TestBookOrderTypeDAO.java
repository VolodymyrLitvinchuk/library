package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookOrderTypeDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final BookOrderType orderType0 = new BookOrderType(0,"Type0","Тип0");
    private static final BookOrderType orderType1 = new BookOrderType(1,"Type1","Тип1");

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("BookOrderTypeDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(0).when(resultSet).getInt(1);
            Mockito.doReturn(orderType0.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(orderType0.getValueUA()).when(resultSet).getString(3);

            Optional<BookOrderType> optional =
                    DAOFactory.getBookOrderTypeDAO().get(0);
            Assertions.assertTrue(optional.isPresent());

            BookOrderType type = optional.get();
            Assertions.assertEquals("Type0", type.getValueEN());
            Assertions.assertEquals("Тип0", type.getValueUA());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(0).doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(orderType0.getValueEN()).doReturn(orderType1.getValueEN()).when(resultSet).getString(2);
            Mockito.doReturn(orderType0.getValueUA()).doReturn(orderType1.getValueUA()).when(resultSet).getString(3);


            Optional<List<BookOrderType>> optional =
                    DAOFactory.getBookOrderTypeDAO().getAll();
            Assertions.assertTrue(optional.isPresent());

            List<BookOrderType> list = optional.get();

            Assertions.assertTrue(list.contains(orderType0));
            Assertions.assertTrue(list.contains(orderType1));
        }
    }

    @Test
    @DisplayName("Test updateEN()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            DAOFactory.getBookOrderTypeDAO().updateEN(orderType0, "newStatus");
            Assertions.assertEquals("newStatus", orderType0.getValueEN());

        }
    }

    @Test
    @DisplayName("Test updateUA()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getBookOrderTypeDAO().updateUA(orderType0, "новийСтатус");
            Assertions.assertEquals("новийСтатус", orderType0.getValueUA());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getBookOrderTypeDAO().delete(orderType0);
            Assertions.assertNull(orderType0.getId());
        }
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
