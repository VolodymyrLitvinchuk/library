package com.my.library.dao.mysql.book;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestPublicationDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static Publication publication;
    private static Publication publication2;

    @BeforeEach
    void objectsCreate(){
        publication = new Publication("Volodymyr", "Володимир");
        publication2 = new Publication("OlenaPublication", "ОленаВидавництво");
    }

    @BeforeAll
    static void globalSetUp() throws SQLException{
        LogManagerForTesting.create("PublicationDAO");
    }

    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(),eq(Statement.RETURN_GENERATED_KEYS));
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();
        Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
        Mockito.doReturn(true).when(resultSet).next();
        Mockito.doReturn(1).when(resultSet).getInt(1);

        Assertions.assertNull(publication.getId());
        DAOFactory.getPublicationDAO().insert(publication, con);
        Assertions.assertNotNull(publication.getId());
    }

    @Test
    @DisplayName("Test get()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(publication.getNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(publication.getNameUA()).when(resultSet).getString(3);

            Optional<Publication> optional = DAOFactory.getPublicationDAO().get(3);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals("Volodymyr", optional.get().getNameEN());
            Assertions.assertEquals("Володимир", optional.get().getNameUA());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(1).doReturn(2).when(resultSet).getInt(1);
            Mockito.doReturn(publication.getNameEN()).doReturn(publication2.getNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(publication.getNameUA()).doReturn(publication2.getNameUA()).when(resultSet).getString(3);

            publication.setId(1);
            publication2.setId(2);

            Optional<List<Publication>> optional =
                    DAOFactory.getPublicationDAO().getAll();

            Assertions.assertTrue(optional.isPresent());
            Assertions.assertTrue(optional.get().contains(publication));
            Assertions.assertTrue(optional.get().contains(publication2));
        }
    }

    @Test
    @DisplayName("Test updateEN()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            publication.setId(1);

            DAOFactory.getPublicationDAO().updateEN(publication, "Vova");
            Assertions.assertEquals("Vova", publication.getNameEN());

        }
    }

    @Test
    @DisplayName("Test updateUA()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            publication.setId(1);

            DAOFactory.getPublicationDAO().updateUA(publication, "Вова");
            Assertions.assertEquals("Вова", publication.getNameUA());
        }
    }

    @Test
    @DisplayName("Test getByNameEN()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(publication.getNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(publication.getNameUA()).when(resultSet).getString(3);

            Optional<Publication> optional =
                    DAOFactory.getPublicationDAO().getByNameEN("Vova");
            Assertions.assertTrue(optional.isPresent());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test7() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            publication.setId(1);

            DAOFactory.getPublicationDAO().delete(publication, con);
            Assertions.assertNull(publication.getId());
        }
    }

    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}
