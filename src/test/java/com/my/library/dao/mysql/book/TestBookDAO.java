package com.my.library.dao.mysql.book;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.abstr.book.*;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);
    @Mock
    private static AuthorDAO authorDAO = mock(AuthorDAO.class);
    @Mock
    private static PublicationDAO publicationDAO = mock(PublicationDAO.class);

    private static final BookDAO bookDAO = DAOFactory.getBookDAO();

    private static Author author;
    private static Author author2;
    private static Publication publication;
    private static Publication publication2;
    private static Book book;

    @BeforeEach
    void objectsCreate() throws ParseException {
        author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);
        author2 = new AuthorBuilderImpl()
                .setFirstNameEN("Sasha")
                .setFirstNameUA("Саша")
                .setLastNameEN("Poginets")
                .setLastNameUA("Погінець")
                .get();
        author2.setId(2);

        publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);
        publication2 = new Publication("OlenaPublication", "ОленаВидавництво");
        publication2.setId(2);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        book = builder.get();

    }

    @BeforeAll
    static void globalSetUp() throws SQLException {
        LogManagerForTesting.create("BookDAO");
        con = DataSource.getConnection();
    }

    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(),eq(Statement.RETURN_GENERATED_KEYS));
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();
        Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
        Mockito.doReturn(true).when(resultSet).next();
        Mockito.doReturn(1).when(resultSet).getInt(1);

        Assertions.assertNotNull(book);
        Assertions.assertNull(book.getId());
        DAOFactory.getBookDAO().insert(book,con);
        Assertions.assertNotNull(book.getId());
        Assertions.assertEquals(1,book.getId());

    }

    @Test
    @DisplayName("Test get()")
    void test2() throws ParseException, DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class);
            MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            daoFactoryMockedStatic.when(DAOFactory::getBookDAO).thenReturn(bookDAO);
            daoFactoryMockedStatic.when(DAOFactory::getAuthorDAO).thenReturn(authorDAO);
            daoFactoryMockedStatic.when(DAOFactory::getPublicationDAO).thenReturn(publicationDAO);
            Mockito.doReturn(Optional.of(author)).when(authorDAO).get(1);
            Mockito.doReturn(Optional.of(publication)).when(publicationDAO).get(1);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();

            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(1).when(resultSet).getInt(5);
            Mockito.doReturn(1).when(resultSet).getInt(6);
            Mockito.doReturn(book.getTitle_EN()).when(resultSet).getString(2);
            Mockito.doReturn(book.getTitle_UA()).when(resultSet).getString(3);
            Mockito.doReturn(book.getLanguage()).when(resultSet).getString(4);
            Mockito.doReturn(book.getPublication_Date()).when(resultSet).getDate(7);
            Mockito.doReturn(3).when(resultSet).getInt(8);


            Optional<Book> optionalBook =
                    DAOFactory.getBookDAO().get(6);
            Assertions.assertTrue(optionalBook.isPresent());
            Book book = optionalBook.get();

            Assertions.assertEquals("Name", book.getTitle_EN());
            Assertions.assertEquals("Назва", book.getTitle_UA());
            Assertions.assertEquals("English", book.getLanguage());
            Assertions.assertEquals("Volodymyr", book.getAuthor().getFirstNameEN());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("2010-01-01");
            Assertions.assertEquals(new java.sql.Date(date.getTime()), book.getPublication_Date());
        }
    }

    @Test
    @DisplayName("Test updateTitleEN()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);
            DAOFactory.getBookDAO().updateTitleEN(book, "NameName");
            Assertions.assertEquals("NameName", book.getTitle_EN());
        }
    }

    @Test
    @DisplayName("Test updateTitleUA()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);
            DAOFactory.getBookDAO().updateTitleUA(book, "НазваНазва");
            Assertions.assertEquals("НазваНазва", book.getTitle_UA());
        }
    }

    @Test
    @DisplayName("Test updateLanguage()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);

            DAOFactory.getBookDAO().updateLanguage(book, "Ukrainian");
            Assertions.assertEquals("Ukrainian", book.getLanguage());
        }
    }

    @Test
    @DisplayName("Test updatePublication()")
    void test7() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);

            DAOFactory.getBookDAO().updatePublication(book, publication2);
            Assertions.assertEquals(publication2, book.getPublication());
            Assertions.assertEquals(2,book.getPublication().getId());

        }
    }

    @Test
    @DisplayName("Test updatePublicationDate()")
    void test8() throws ParseException, DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse("2022-10-10");

            DAOFactory.getBookDAO().updatePublicationDate(book, new java.sql.Date(date.getTime()));
            Assertions.assertEquals("2022-10-10", dateFormat.format(book.getPublication_Date()));
        }
    }

    @Test
    @DisplayName("Test updateCopiesOwned()")
    void test9() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);
            DAOFactory.getBookDAO().updateCopiesOwned(book, 33, con);
            Assertions.assertEquals(33, book.getCopiesOwned());
        }
    }

    @Test
    @DisplayName("Test updateAuthor()")
    void test10() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);

            Assertions.assertNotEquals(author2, book.getAuthor());
            DAOFactory.getBookDAO().updateAuthor(book, author2);
            Assertions.assertEquals(author2, book.getAuthor());
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test11() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            book.setId(1);

            DAOFactory.getBookDAO().delete(book, con);
            Assertions.assertNull(book.getId());

        }
    }

    @Test
    @DisplayName("Test getBookQuantity()")
    void test13() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(5).when(resultSet).getInt(1);

            book.setId(1);

            Optional<Integer> optional = DAOFactory.getBookDAO().getBookQuantity();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(5, optional.get());
        }
    }

    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}
