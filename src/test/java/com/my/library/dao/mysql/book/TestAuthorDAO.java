package com.my.library.dao.mysql.book;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestAuthorDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);
    @Mock
    private static Statement statement = mock(Statement.class);

    private static Author author;
    private static Author author2;

    @BeforeEach
    void authorCreate(){
        author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author2 = new AuthorBuilderImpl()
                .setFirstNameEN("Andrey")
                .setFirstNameUA("Андрій")
                .setLastNameEN("Pylypchuk")
                .setLastNameUA("Пилипчук")
                .get();
    }

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("AuthorDAO");
    }

    @Test
    @DisplayName("Test insert()")
    void test1() throws DAOException, SQLException {
        Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString(), eq(Statement.RETURN_GENERATED_KEYS));
        Mockito.doReturn(1).when(preparedStatement).executeUpdate();
        Mockito.doReturn(resultSet).when(preparedStatement).getGeneratedKeys();
        Mockito.doReturn(true).when(resultSet).next();
        Mockito.doReturn(1).when(resultSet).getInt(1);

        Assertions.assertNull(author.getId());
        DAOFactory.getAuthorDAO().insert(author,con);
        Assertions.assertNotNull(author.getId(), "Author id must be non zero");
        Assertions.assertEquals(1,author.getId());
    }

    @Test
    @DisplayName("Test getByFirstLastNameEN()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(author.getFirstNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(author.getFirstNameUA()).when(resultSet).getString(3);
            Mockito.doReturn(author.getLastNameEN()).when(resultSet).getString(4);
            Mockito.doReturn(author.getLastNameUA()).when(resultSet).getString(5);

            Optional<Author> optionalAuthor =
                    DAOFactory.getAuthorDAO().getByFirstLastNameEN(
                            "Volodymyr", "Litvinchuk"
                    );

            Assertions.assertTrue(optionalAuthor.isPresent());
        }
    }

    @Test
    @DisplayName("Test get()")
    void test3() throws DAOException, SQLException {

        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(1).when(resultSet).getInt(1);
            Mockito.doReturn(author.getFirstNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(author.getFirstNameUA()).when(resultSet).getString(3);
            Mockito.doReturn(author.getLastNameEN()).when(resultSet).getString(4);
            Mockito.doReturn(author.getLastNameUA()).when(resultSet).getString(5);

            author.setId(1);

            Optional<Author> optionalAuthor = DAOFactory.getAuthorDAO().get(1);
            Assertions.assertTrue(optionalAuthor.isPresent(), "Cannot get existing author from DB");
            Author author1 = optionalAuthor.get();
            Assertions.assertEquals(author, author1, "Authors must be equals");
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test4() throws DAOException, SQLException {
        author.setId(1);
        author2.setId(2);

        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(statement).when(con).createStatement();
            Mockito.doReturn(resultSet).when(statement).executeQuery(anyString());
            Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
            Mockito.doReturn(1).doReturn(2).when(resultSet).getInt(1);
            Mockito.doReturn(author.getFirstNameEN()).doReturn(author2.getFirstNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(author.getFirstNameUA()).doReturn(author2.getFirstNameUA()).when(resultSet).getString(3);
            Mockito.doReturn(author.getLastNameEN()).doReturn(author2.getLastNameEN()).when(resultSet).getString(4);
            Mockito.doReturn(author.getLastNameUA()).doReturn(author2.getLastNameUA()).when(resultSet).getString(5);


            Optional<List<Author>> optionalAuthors = DAOFactory.getAuthorDAO().getAll();
            Assertions.assertTrue(optionalAuthors.isPresent(), "Cannot get all authors");
            List<Author> authors = optionalAuthors.get();
            Assertions.assertTrue(authors.contains(author) && authors.contains(author2),
                    "List must contain two authors");
        }
    }

    @Test
    @DisplayName("Test updateFirstNameEN()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            author.setId(1);
            DAOFactory.getAuthorDAO().updateFirstNameEN(author, "Andry");
            Assertions.assertEquals("Andry", author.getFirstNameEN(), "FirstNameEN must be equals");
        }
    }

    @Test
    @DisplayName("Test updateFirstNameUA()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            author.setId(1);

            DAOFactory.getAuthorDAO().updateFirstNameUA(author, "Ендрю");
            Assertions.assertEquals("Ендрю", author.getFirstNameUA(), "FirstNameUA must be equals");
        }
    }

    @Test
    @DisplayName("Test updateLastNameEN()")
    void test7() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            author.setId(1);

            DAOFactory.getAuthorDAO().updateLastNameEN(author, "Prokopchuk");
            Assertions.assertEquals("Prokopchuk", author.getLastNameEN(), "LastNameEN must be equals");
        }
    }

    @Test
    @DisplayName("Test updateLastNameUA()")
    void test8() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            author.setId(1);

            DAOFactory.getAuthorDAO().updateLastNameUA(author, "Прокопчук");
            Assertions.assertEquals("Прокопчук", author.getLastNameUA(), "LastNameUA must be equals");
        }
    }

    @Test
    @DisplayName("Test delete()")
    void test9() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            author.setId(1);

            DAOFactory.getAuthorDAO().delete(author, con);
            Assertions.assertNull(author.getFirstNameEN());
            Assertions.assertNull(author.getFirstNameUA());
            Assertions.assertNull(author.getLastNameEN());
            Assertions.assertNull(author.getLastNameUA());
            Assertions.assertEquals(0, author.getId());
        }
    }

    @Test
    @DisplayName("Test getAuthorQuantity()")
    void test10() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(3).when(resultSet).getInt(1);

            Optional<Integer> optional = DAOFactory.getAuthorDAO().getAuthorQuantity();
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(3, optional.get());

        }
    }

    @Test
    @DisplayName("Test getAuthorsByOrderOffsetLimit()")
    void test11() throws DAOException, SQLException {
        author.setId(1);
        author2.setId(2);

        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
            Mockito.doReturn(1).doReturn(2).when(resultSet).getInt(1);
            Mockito.doReturn(author.getFirstNameEN()).doReturn(author2.getFirstNameEN()).when(resultSet).getString(2);
            Mockito.doReturn(author.getFirstNameUA()).doReturn(author2.getFirstNameUA()).when(resultSet).getString(3);
            Mockito.doReturn(author.getLastNameEN()).doReturn(author2.getLastNameEN()).when(resultSet).getString(4);
            Mockito.doReturn(author.getLastNameUA()).doReturn(author2.getLastNameUA()).when(resultSet).getString(5);


            Optional<List<Author>> optionalAuthors =
                    DAOFactory.getAuthorDAO()
                            .getAuthorsByOrderOffsetLimit(AuthorOrderBy.ID, 0, 4);
            Assertions.assertTrue(optionalAuthors.isPresent());
            Assertions.assertEquals(2, optionalAuthors.get().size());
            Assertions.assertEquals("Volodymyr", optionalAuthors.get().get(0).getFirstNameEN());
        }
    }

    @AfterAll
    static void GlobalTearDown() throws SQLException {
        LogManagerForTesting.drop();
        con.close();
    }
}
