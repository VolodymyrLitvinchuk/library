package com.my.library.dao.mysql.user;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.LogManagerForTesting;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUserStatusDAO {
    @Mock
    private static Connection con = mock(Connection.class);
    @Mock
    private static PreparedStatement preparedStatement = mock(PreparedStatement.class);
    @Mock
    private static ResultSet resultSet = mock(ResultSet.class);

    private static final UserStatus status = new UserStatus(0,"Active");
    private static final UserStatus status1 = new UserStatus(1,"Locked");

    @BeforeAll
    static void globalSetUp(){
        LogManagerForTesting.create("UserStatusDAO");
    }

    @Test
    @DisplayName("Test get()")
    void test1() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            Mockito.doReturn(status.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(status.getValue()).when(resultSet).getString(2);

            Optional<UserStatus> optional = DAOFactory.getUserStatusDAO().get(0);
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals("Active", optional.get().getValue());
        }
    }

    @Test
    @DisplayName("Test getAll()")
    void test2() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).doReturn(true).doReturn(false).when(resultSet).next();
            Mockito.doReturn(status.getId()).doReturn(status1.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(status.getValue()).doReturn(status1.getValue()).when(resultSet).getString(2);

            Optional<List<UserStatus>> optional =
                    DAOFactory.getUserStatusDAO().getAll();
            Assertions.assertTrue(optional.isPresent());
            List<UserStatus> list = optional.get();

            Assertions.assertTrue(list.contains(status));
            Assertions.assertTrue(list.contains(status1));
            Assertions.assertEquals(2, list.size());
        }
    }

    @Test
    @DisplayName("Test insert()")
    void test3() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();

            Assertions.assertTrue(DAOFactory.getUserStatusDAO().insert(status1));
        }
    }

    @Test
    @DisplayName("Test update()")
    void test4() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserStatusDAO().update(status, "newName");
            Assertions.assertEquals("newName", status.getValue());
        }

    }

    @Test
    @DisplayName("Test delete()")
    void test5() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(1).when(preparedStatement).executeUpdate();


            DAOFactory.getUserStatusDAO().delete(status);
            Assertions.assertNull(status.getId());
        }
    }

    @Test
    @DisplayName("Test getByValue()")
    void test6() throws DAOException, SQLException {
        try(MockedStatic<DataSource> dataSourceMockedStatic = Mockito.mockStatic(DataSource.class)) {
            dataSourceMockedStatic.when(DataSource::getConnection).thenReturn(con);
            Mockito.doReturn(preparedStatement).when(con).prepareStatement(anyString());
            Mockito.doReturn(resultSet).when(preparedStatement).executeQuery();
            Mockito.doReturn(true).when(resultSet).next();
            status.setId(0);
            Mockito.doReturn(status.getId()).when(resultSet).getInt(1);
            Mockito.doReturn(status.getValue()).when(resultSet).getString(2);

            Optional<UserStatus> optional = DAOFactory.getUserStatusDAO().getByValue("Active");
            Assertions.assertTrue(optional.isPresent());
            Assertions.assertEquals(0, optional.get().getId());
        }
    }

    @AfterAll
    static void GlobalTearDown(){
        LogManagerForTesting.drop();

    }
}
