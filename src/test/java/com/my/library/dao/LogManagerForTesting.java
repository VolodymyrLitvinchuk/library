package com.my.library.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LogManagerForTesting {
    private LogManagerForTesting(){}
    private static final Logger log = LoggerFactory.getLogger(LogManagerForTesting.class);
    private static String testClassName;

    public static void create(String testClassName) {
        LogManagerForTesting.testClassName = testClassName;
        log.info("MySQL "+testClassName+" TESTING STARTED ----->");

    }

    public static void drop() {
        log.info("MySQL "+testClassName+" TESTING FINISHED -----X");
    }
}
