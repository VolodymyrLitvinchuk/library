package com.my.library.dto.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestBuilderBookDTOImpl {
    @Test
    @DisplayName("Test normal building")
    void test1() throws ParseException {
        BuilderBookDTO builder = new BuilderBookDTOImpl();
        builder.setTitleEN("title")
                .setTitleUA("назва")
                .setLanguage("English")
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublicationDate(new java.sql.Date(date.getTime()));

        builder.setAuthorFirstNameEN("Volodymyr")
                .setAuthorFirstNameUA("Володимир")
                .setAuthorLastNameEN("Litvinchuk")
                .setAuthorLastNameUA("Літвінчук")
                .setPublicationNameEN("Publication")
                .setPublicationNameUA("Видавницво");

        Assertions.assertNotNull(builder.get());
    }

    @Test
    @DisplayName("Test not normal building")
    void test2() throws ParseException {
        BuilderBookDTO builder = new BuilderBookDTOImpl();
        builder.setTitleEN("title")
                .setTitleUA("назва")
                .setLanguage("English")
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublicationDate(new java.sql.Date(date.getTime()));

        builder.setAuthorFirstNameEN("Volodymyr")
                .setAuthorFirstNameUA("Володимир")
                .setAuthorLastNameUA("Літвінчук")
                .setPublicationNameEN("Publication")
                .setPublicationNameUA("Видавницво");

        Assertions.assertNull(builder.get());
    }
}
