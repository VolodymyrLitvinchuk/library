package com.my.library.controller;

import com.my.library.controller.command.CommandFactory;
import com.my.library.controller.command.constant.CommandName;
import com.my.library.controller.command.implementation.base.SignInCommand;
import com.my.library.exception.ServiceException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.CommandName.SIGN_IN_COMMAND;
import static com.my.library.controller.command.constant.Page.CONTROLLER_PAGE;
import static com.my.library.controller.command.constant.Page.SIGN_IN_PAGE;
import static com.my.library.controller.command.constant.Parameter.COMMAND;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestController {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);
    @Mock
    private RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
    @Mock
    private SignInCommand signInCommand = mock(SignInCommand.class);

    private MockedStatic<CommandFactory> commandFactoryMockedStatic;

    @BeforeEach
    void mockitoInit(){
        commandFactoryMockedStatic = Mockito.mockStatic(CommandFactory.class);
        commandFactoryMockedStatic.when(()-> CommandFactory.createCommand(SIGN_IN_COMMAND))
                .thenReturn(signInCommand);
    }

    @AfterEach
    void mockitoClose(){
        commandFactoryMockedStatic.close();
    }

    @Test
    @DisplayName("Test doPost()")
    void test1() throws ServiceException {
        Mockito.doReturn(CONTROLLER_PAGE + "?" + "command=" + CommandName.SIGN_IN_COMMAND)
                .when(signInCommand).execute(request);
        Mockito.doReturn(SIGN_IN_COMMAND).when(request).getParameter(COMMAND);

        Assertions.assertDoesNotThrow(()->{
            new Controller().doPost(request,response);
            Mockito.verify(response).sendRedirect(CONTROLLER_PAGE + "?" + "command=" + SIGN_IN_COMMAND);
        });
    }

    @Test
    @DisplayName("Test doGet()")
    void test2() throws ServiceException {
        Mockito.doReturn(SIGN_IN_PAGE)
                .when(signInCommand).execute(request);
        Mockito.doReturn(SIGN_IN_COMMAND).when(request).getParameter(COMMAND);
        Mockito.doReturn(requestDispatcher).when(request).getRequestDispatcher(SIGN_IN_PAGE);

        Assertions.assertDoesNotThrow(()->{
            ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
            new Controller().doGet(request,response);
            Mockito.verify(request).getRequestDispatcher(captor.capture());
            Assertions.assertEquals(SIGN_IN_PAGE, captor.getValue());
        });
    }



}
