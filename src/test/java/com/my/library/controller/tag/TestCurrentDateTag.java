package com.my.library.controller.tag;

import jakarta.servlet.jsp.JspContext;
import jakarta.servlet.jsp.JspWriter;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.LocalDate;

import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestCurrentDateTag {
    @Mock
    private JspWriter jspWriter = mock(JspWriter.class);
    @Mock
    private JspContext jspContext = mock(JspContext.class);

    @Test
    @DisplayName("Test doTag()")
    void test1() throws IOException {
        Mockito.doReturn(jspWriter).when(jspContext).getOut();
        CurrentDateTag tag = new CurrentDateTag();
        tag.setJspContext(jspContext);

        Assertions.assertDoesNotThrow(tag::doTag);
        Mockito.verify(jspWriter).print(LocalDate.now().toString());

    }

}
