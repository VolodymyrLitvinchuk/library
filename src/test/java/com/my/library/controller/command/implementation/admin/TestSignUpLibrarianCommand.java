package com.my.library.controller.command.implementation.admin;

import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import com.my.library.util.ConvertorUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_REGISTER_LIBRARIAN;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestSignUpLibrarianCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private UserService userService = mock(UserService.class);

    private final UserDTO librarianDTO = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Librarian",null
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<ConvertorUtil> convertorMockedStatic = Mockito.mockStatic(ConvertorUtil.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);
            convertorMockedStatic.when(()->ConvertorUtil.convertRequestToUserDTOLibrarian(request))
                            .thenReturn(librarianDTO);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn("ABCabc13").when(request).getParameter(PASSWORD);
            Mockito.doReturn("ABCabc13").when(request).getParameter(REPEAT_PASSWORD);
            Mockito.doReturn(session).when(request).getSession();

            new SignUpLibrarianCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE, SUCCEED_REGISTER_LIBRARIAN);
        }
    }
}
