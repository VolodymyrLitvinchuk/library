package com.my.library.controller.command.implementation.admin;

import com.my.library.dao.orderByEnums.UserOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import com.my.library.util.orderByFactory.UserOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.USERS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestShowUsersCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private UserService userService = mock(UserService.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<UserOrderByTypeFactory> factoryMockedStatic = Mockito.mockStatic(UserOrderByTypeFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);
            factoryMockedStatic.when(()->UserOrderByTypeFactory.getOrderByType(anyString(),anyString()))
                            .thenReturn(UserOrderBy.LOGIN);

            Mockito.doReturn("ByLogin").when(request).getParameter(SORT_TYPE);
            Mockito.doReturn("descending").when(request).getParameter(SORT_ORDER);
            Mockito.doReturn("5").when(request).getParameter(PAGE_SIZE);
            Mockito.doReturn("1").when(request).getParameter(CURRENT_PAGE);

            Assertions.assertEquals(USERS_PAGE, new ShowUsersCommand().execute(request));
        }
    }
}
