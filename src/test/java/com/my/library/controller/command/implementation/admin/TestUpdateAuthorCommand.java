package com.my.library.controller.command.implementation.admin;

import com.my.library.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.EDIT_AUTHOR_PAGE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUpdateAuthorCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        Mockito.when(request.getMethod()).thenReturn("GET");
        Mockito.doReturn(session).when(request).getSession();

        Assertions.assertEquals(EDIT_AUTHOR_PAGE, new UpdateAuthorCommand().execute(request));

    }
}
