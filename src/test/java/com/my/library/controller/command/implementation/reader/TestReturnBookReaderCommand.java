package com.my.library.controller.command.implementation.reader;

import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookOrderService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestReturnBookReaderCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookOrderService bookOrderService = mock(BookOrderService.class);


    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            factoryMockedStatic.when(ServiceFactory::getBookOrderService).thenReturn(bookOrderService);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn("1").when(request).getParameter(BOOK_ORDER_ID);

            new ReturnBookReaderCommand().execute(request);
            Mockito.verify(session, Mockito.times(0)).setAttribute(eq(ERROR),anyString());

        }
    }
}
