package com.my.library.controller.command.implementation.admin;

import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBlockUserCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private UserService userService = mock(UserService.class);

    @Test
    @DisplayName("Test execute()")
    void test1(){
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn("login").when(request).getParameter(LOGIN);

            new BlockUserCommand().execute(request);
            Mockito.verify(request, Mockito.times(0)).setAttribute(eq(ERROR), any());
        }
    }

}
