package com.my.library.controller.command.implementation.base;

import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.MAIN_PAGE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestDefaultCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);

    @Test
    @DisplayName("Test execute()")
    void test1(){
        Assertions.assertEquals(MAIN_PAGE, new DefaultCommand().execute(request));
    }
}
