package com.my.library.controller.command.implementation.admin.updateAuthor;

import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.AuthorService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUpdateAuthorLastNameEnCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private AuthorService authorService = mock(AuthorService.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getAuthorService).thenReturn(authorService);

            Mockito.when(request.getMethod()).thenReturn("POST");
            Mockito.doReturn("1").when(request).getParameter(AUTHOR_ID);
            Mockito.doReturn("NewLastName").when(request).getParameter(NEW_AUTHOR_LAST_NAME_EN);
            Mockito.doReturn(session).when(request).getSession();

            new UpdateAuthorLastNameEnCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE, SUCCEED_UPDATE);
        }
    }
}
