package com.my.library.controller.command.implementation.reader;

import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import com.my.library.util.ConvertorUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.SIGN_IN_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestSignUpReaderCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private UserService userService = mock(UserService.class);

    private final UserDTO reader = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Reader",null
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<ConvertorUtil> utilMockedStatic = Mockito.mockStatic(ConvertorUtil.class)){
            utilMockedStatic.when(()->ConvertorUtil.convertRequestToUserDTO(request)).thenReturn(reader);
            factoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn("ABCabc13").when(request).getParameter(PASSWORD);
            Mockito.doReturn("ABCabc13").when(request).getParameter(REPEAT_PASSWORD);

            Assertions.assertEquals(
                    SIGN_IN_PAGE,
                    new SignUpReaderCommand().execute(request));

        }
    }
}
