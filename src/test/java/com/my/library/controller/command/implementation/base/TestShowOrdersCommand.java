package com.my.library.controller.command.implementation.base;

import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookOrderService;
import com.my.library.util.orderByFactory.BookOrderOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestShowOrdersCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookOrderService bookOrderService = mock(BookOrderService.class);

    private final UserDTO librarianDTO = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Librarian",null
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<BookOrderOrderByTypeFactory> convertorUtilMockedStatic = Mockito.mockStatic(BookOrderOrderByTypeFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getBookOrderService).thenReturn(bookOrderService);
            convertorUtilMockedStatic.when(()->BookOrderOrderByTypeFactory.getOrderByType(anyString(),anyString()))
                    .thenReturn(BookOrderOrderBy.STATUS);

            Mockito.doReturn("byStatus").when(request).getParameter(SORT_TYPE);
            Mockito.doReturn("ascending").when(request).getParameter(SORT_ORDER);
            Mockito.doReturn("5").when(request).getParameter(PAGE_SIZE);
            Mockito.doReturn("1").when(request).getParameter(CURRENT_PAGE);
            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn(librarianDTO).when(session).getAttribute(LOGGED_USER);

            new ShowOrdersCommand().execute(request);
            Mockito.verify(request).setAttribute(eq(MESSAGE), any());

        }
    }
}
