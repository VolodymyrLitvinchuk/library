package com.my.library.controller.command.implementation.admin;

import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.AuthorService;
import com.my.library.util.orderByFactory.AuthorOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.AUTHORS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestShowAuthorsCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private AuthorService authorService = mock(AuthorService.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<AuthorOrderByTypeFactory> factoryMockedStatic = Mockito.mockStatic(AuthorOrderByTypeFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getAuthorService).thenReturn(authorService);
            factoryMockedStatic.when(()->AuthorOrderByTypeFactory.getOrderByType(anyString(),anyString(),anyString()))
                            .thenReturn(AuthorOrderBy.ID);

            Mockito.doReturn("ById").when(request).getParameter(SORT_TYPE);
            Mockito.doReturn("descending").when(request).getParameter(SORT_ORDER);
            Mockito.doReturn("5").when(request).getParameter(PAGE_SIZE);
            Mockito.doReturn("1").when(request).getParameter(CURRENT_PAGE);
            Mockito.doReturn("en").when(session).getAttribute(LOCALE);
            Mockito.doReturn(session).when(request).getSession();

            Assertions.assertEquals(AUTHORS_PAGE, new ShowAuthorsCommand().execute(request));


        }
    }
}
