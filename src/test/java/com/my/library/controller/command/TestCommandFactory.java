package com.my.library.controller.command;

import com.my.library.controller.command.implementation.base.DefaultCommand;
import com.my.library.controller.command.implementation.base.SignInCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.my.library.controller.command.constant.CommandName.SIGN_IN_COMMAND;

public class TestCommandFactory {
    @Test
    @DisplayName("Test get sign-in command")
    void test1(){
        Command command = CommandFactory.createCommand(SIGN_IN_COMMAND);
        Assertions.assertInstanceOf(SignInCommand.class, command);
    }

    @Test
    @DisplayName("Test default command")
    void test2(){
        Command command = CommandFactory.createCommand("unknown-command");
        Assertions.assertInstanceOf(DefaultCommand.class, command);
    }
}
