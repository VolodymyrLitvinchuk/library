package com.my.library.controller.command.implementation.admin;

import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.BOOK_ID;
import static com.my.library.controller.command.constant.Parameter.MESSAGE;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_DELETE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestDeleteBookCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookService bookService = mock(BookService.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getBookService).thenReturn(bookService);

            Mockito.when(request.getMethod()).thenReturn("POST");
            Mockito.doReturn("1").when(request).getParameter(BOOK_ID);
            Mockito.doReturn(session).when(request).getSession();

            new DeleteBookCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE, SUCCEED_DELETE);


        }
    }
}
