package com.my.library.controller.command.implementation.reader;

import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookOrderService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.CommandName.GO_TO_BOOK_ORDER_PAGE_COMMAND;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestGoToBookOrderPageCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookOrderService bookOrderService = mock(BookOrderService.class);

    private final UserDTO reader = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Reader","Active"
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            factoryMockedStatic.when(ServiceFactory::getBookOrderService).thenReturn(bookOrderService);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn(reader).when(session).getAttribute(LOGGED_USER);
            Mockito.doReturn("1").when(request).getParameter(BOOK_ID);

            Assertions.assertEquals(
                    "controller?command="+ GO_TO_BOOK_ORDER_PAGE_COMMAND,
                    new GoToBookOrderPageCommand().execute(request));

        }
    }
}
