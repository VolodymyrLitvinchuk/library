package com.my.library.controller.command.implementation.reader;

import com.my.library.dto.BookDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.BOOK_BY_ID_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestGetBookByIdCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private BookService bookService = mock(BookService.class);

    private final BookDTO bookDTO = new BookDTO(null,null,null,null,null,null,null,null,null,null,null);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            factoryMockedStatic.when(ServiceFactory::getBookService).thenReturn(bookService);

            Mockito.doReturn(bookDTO).when(bookService).getBook(anyInt());
            Mockito.doReturn("1").when(request).getParameter(BOOK_ID);

            Assertions.assertEquals(BOOK_BY_ID_PAGE, new GetBookByIdCommand().execute(request));

        }
    }
}
