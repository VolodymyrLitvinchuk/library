package com.my.library.controller.command.implementation.base;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.MAIN_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestSignOutCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);

    @Test
    @DisplayName("Test execute()")
    void test1(){
        Mockito.doReturn(session).when(request).getSession();

        Assertions.assertEquals(MAIN_PAGE, new SignOutCommand().execute(request));
        Mockito.verify(session).setAttribute(eq(LOGGED_USER), eq(null));

    }
}
