package com.my.library.controller.command.implementation.admin;

import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.entity.builder.*;
import com.my.library.dto.BookDTO;
import com.my.library.dto.builder.*;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookService;
import com.my.library.util.ConvertorUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_ADD_BOOK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestAddBookCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookService bookService = mock(BookService.class);

    private static BookDTO bookDTO;

    @BeforeAll
    static void objectsCreating() throws ParseException {
        Author author = new AuthorBuilderImpl()
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();
        author.setId(1);

        Publication publication = new Publication("VolodymyrPublication", "ВолодимирВидавництво");
        publication.setId(1);

        BookBuilder builder = new BookBuilderImpl();
        builder.setTitle_EN("Name")
                .setTitle_UA("Назва")
                .setLanguage("English")
                .setAuthor(author)
                .setPublication(publication)
                .setCopiesOwned(3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2010-01-01");
        builder.setPublication_Date(new java.sql.Date(date.getTime()));

        Book book = builder.get();


        BuilderBookDTO builderBookDTO = new BuilderBookDTOImpl();
        builderBookDTO
                .setTitleEN(book.getTitle_EN())
                .setTitleUA(book.getTitle_UA())
                .setLanguage(book.getLanguage())
                .setPublicationDate(book.getPublication_Date())
                .setCopiesOwned(book.getCopiesOwned())
                .setAuthorFirstNameEN(book.getAuthor().getFirstNameEN())
                .setAuthorFirstNameUA(book.getAuthor().getFirstNameUA())
                .setAuthorLastNameEN(book.getAuthor().getLastNameEN())
                .setAuthorLastNameUA(book.getAuthor().getLastNameUA())
                .setPublicationNameEN(book.getPublication().getNameEN())
                .setPublicationNameUA(book.getPublication().getNameUA());

        bookDTO = builderBookDTO.get();
        if(bookDTO==null) throw new RuntimeException();

        bookDTO.setAuthorId(book.getAuthor().getId());
        bookDTO.setPublicationId(book.getPublication().getId());
        bookDTO.setAvailableQuantity(3);
    }

    @Test
    @DisplayName("Test execute()")
    void test1(){
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<ConvertorUtil> convertorUtilMockedStatic = Mockito.mockStatic(ConvertorUtil.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getBookService).thenReturn(bookService);
            convertorUtilMockedStatic.when(()->ConvertorUtil.convertRequestToBookDTO(any())).thenReturn(bookDTO);

            Mockito.when(request.getMethod()).thenReturn("POST");
            Mockito.doReturn(session).when(request).getSession();

            new AddBookCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE, SUCCEED_ADD_BOOK);


        }
    }
}
