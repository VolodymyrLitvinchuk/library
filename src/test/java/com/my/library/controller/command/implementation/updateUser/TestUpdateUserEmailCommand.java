package com.my.library.controller.command.implementation.updateUser;

import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUpdateUserEmailCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private UserService userService = mock(UserService.class);

    private final UserDTO reader = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Reader",null
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            factoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn(reader).when(session).getAttribute(LOGGED_USER);
            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn("123sdklvsl@gmail.com").when(request).getParameter(EMAIL);

            new UpdateUserEmailCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE,SUCCEED_UPDATE);

        }
    }
}
