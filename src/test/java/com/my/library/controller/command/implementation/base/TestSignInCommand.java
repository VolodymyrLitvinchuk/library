package com.my.library.controller.command.implementation.base;

import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Page.MAIN_PAGE;
import static com.my.library.controller.command.constant.Parameter.LOGIN;
import static com.my.library.controller.command.constant.Parameter.PASSWORD;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestSignInCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private UserService userService = mock(UserService.class);

    private final UserDTO librarianDTO = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Librarian",null
    );

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn(librarianDTO).when(userService).signIn(anyString(),anyString());
            Mockito.doReturn("login").when(request).getParameter(LOGIN);
            Mockito.doReturn("password").when(request).getParameter(PASSWORD);
            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn(session).when(request).getSession();

            Assertions.assertEquals(MAIN_PAGE, new SignInCommand().execute(request));
        }
    }
}
