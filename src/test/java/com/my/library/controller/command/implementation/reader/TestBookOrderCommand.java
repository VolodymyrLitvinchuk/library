package com.my.library.controller.command.implementation.reader;

import com.my.library.dto.BookOrderDTO;
import com.my.library.dto.UserDTO;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookOrderService;
import com.my.library.util.ConvertorUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestBookOrderCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookOrderService bookOrderService = mock(BookOrderService.class);

    private final UserDTO reader = new UserDTO(
            "login","2001litvinchuk@gmail.com",
            "Volodymyr", "Litvinchuk",
            "Reader","Active"
    );

    private final BookOrderDTO bookOrderDTO = new BookOrderDTO(
            1,
            reader.getLogin(),
            1,
            null,
            null
    );

    @Test
    @DisplayName("Test execute()")
    void test1(){
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<ConvertorUtil> utilMockedStatic = Mockito.mockStatic(ConvertorUtil.class)){
            factoryMockedStatic.when(ServiceFactory::getBookOrderService).thenReturn(bookOrderService);
            utilMockedStatic.when(()->ConvertorUtil.convertRequestToBookOrderDTO(request)).thenReturn(bookOrderDTO);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn(reader).when(session).getAttribute(LOGGED_USER);

            new BookOrderCommand().execute(request);
            Mockito.verify(session, Mockito.times(0)).setAttribute(eq(ERROR),any());

        }
    }
}
