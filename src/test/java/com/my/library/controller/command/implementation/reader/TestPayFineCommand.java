package com.my.library.controller.command.implementation.reader;

import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.FineService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_PAID;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestPayFineCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private FineService fineService = mock(FineService.class);


    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> factoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)){
            factoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);

            Mockito.doReturn("POST").when(request).getMethod();
            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn("1").when(request).getParameter(BOOK_ORDER_ID);

            new PayFineCommand().execute(request);
            Mockito.verify(session).setAttribute(MESSAGE,SUCCEED_PAID);

        }
    }
}
