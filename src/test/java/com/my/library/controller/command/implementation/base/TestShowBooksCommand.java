package com.my.library.controller.command.implementation.base;

import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.BookService;
import com.my.library.util.orderByFactory.BookOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestShowBooksCommand {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private BookService bookService = mock(BookService.class);

    @Test
    @DisplayName("Test execute()")
    void test1() throws ServiceException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);
            MockedStatic<BookOrderByTypeFactory> convertorUtilMockedStatic = Mockito.mockStatic(BookOrderByTypeFactory.class)){
            serviceFactoryMockedStatic.when(ServiceFactory::getBookService).thenReturn(bookService);
            convertorUtilMockedStatic.when(()->BookOrderByTypeFactory.getOrderByType(anyString(),anyString(),anyString()))
                    .thenReturn(BookOrderBy.BOOK_TITLE_EN);

            Mockito.doReturn("byTitle").when(request).getParameter(SORT_TYPE);
            Mockito.doReturn("descending").when(request).getParameter(SORT_ORDER);
            Mockito.doReturn("5").when(request).getParameter(PAGE_SIZE);
            Mockito.doReturn("1").when(request).getParameter(CURRENT_PAGE);
            Mockito.doReturn("en").when(session).getAttribute(LOCALE);
            Mockito.doReturn(session).when(request).getSession();

            new ShowBooksCommand().execute(request);
            Mockito.verify(request).setAttribute(eq(MESSAGE), any());

        }
    }
}
