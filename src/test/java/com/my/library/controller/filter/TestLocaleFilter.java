package com.my.library.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.my.library.controller.command.constant.Parameter.LOCALE;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestLocaleFilter {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private FilterChain filterChain = mock(FilterChain.class);
    @Mock
    private FilterConfig config = mock(FilterConfig.class);

    @Test
    @DisplayName("Test doFilter()")
    void test1() throws ServletException, IOException {
        Mockito.doReturn("en").when(config).getInitParameter("defaultLocale");
        Mockito.doReturn("").when(request).getParameter(LOCALE);
        Mockito.doReturn(session).when(request).getSession();
        Mockito.doReturn("").when(session).getAttribute(LOCALE);

        LocaleFilter localeFilter = new LocaleFilter();
        localeFilter.init(config);
        localeFilter.doFilter(request,response,filterChain);
        Mockito.verify(session).setAttribute(LOCALE,"en");
    }
}
