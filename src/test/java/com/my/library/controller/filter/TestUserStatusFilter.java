package com.my.library.controller.filter;

import com.my.library.dto.UserDTO;
import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.my.library.controller.command.constant.Parameter.LOGGED_USER;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestUserStatusFilter {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private FilterChain filterChain = mock(FilterChain.class);
    @Mock
    private UserService userService = mock(UserService.class);


    private static final UserDTO userDTO = new UserDTO(
            "login",
            "email@gmail.com",
            "Volodymyr",
            "Listvinchuk",
            "Reader",
            "Active"
    );

    @Test
    @DisplayName("Test doFilter()")
    void test1() throws ServletException, IOException {
        try(MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class)) {
            serviceFactoryMockedStatic.when(ServiceFactory::getUserService).thenReturn(userService);

            Mockito.doReturn(session).when(request).getSession();
            Mockito.doReturn(userDTO).when(session).getAttribute(LOGGED_USER);

            new UserStatusFilter().doFilter(request,response,filterChain);
            Mockito.verify(session).setAttribute(LOGGED_USER,userDTO);

        }
    }
}
