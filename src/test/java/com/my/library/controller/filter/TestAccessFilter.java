package com.my.library.controller.filter;

import com.my.library.dto.UserDTO;
import jakarta.servlet.FilterChain;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.my.library.controller.command.constant.Page.ACCESS_DENIED_PAGE;
import static com.my.library.controller.command.constant.Parameter.COMMAND;
import static com.my.library.controller.command.constant.Parameter.LOGGED_USER;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestAccessFilter {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);
    @Mock
    private HttpSession session = mock(HttpSession.class);
    @Mock
    private FilterChain filterChain = mock(FilterChain.class);
    @Mock
    private RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);

    private static final UserDTO userDTO = new UserDTO(
            "login",
            "email@gmail.com",
            "Volodymyr",
            "Listvinchuk",
            "Reader",
            "Active"
    );

    @Test
    @DisplayName("Test doFilter()")
    void test1() throws ServletException, IOException {
        Mockito.doReturn("/controller").when(request).getServletPath();
        Mockito.doReturn("sign-out").when(request).getParameter(COMMAND);
        Mockito.doReturn(session).when(request).getSession();
        Mockito.doReturn(userDTO).when(session).getAttribute(LOGGED_USER);

        new AccessFilter().doFilter(request,response,filterChain);
        Mockito.verify(filterChain).doFilter(request,response);
    }

    @Test
    @DisplayName("Test doFilter() denied")
    void test2() throws ServletException, IOException {
        Mockito.doReturn("/controller").when(request).getServletPath();
        Mockito.doReturn("sign-out").when(request).getParameter(COMMAND);
        Mockito.doReturn(session).when(request).getSession();
        Mockito.doReturn(null).when(session).getAttribute(LOGGED_USER);
        Mockito.doReturn(requestDispatcher).when(request).getRequestDispatcher(ACCESS_DENIED_PAGE);

        new AccessFilter().doFilter(request,response,filterChain);
        Mockito.verify(requestDispatcher).forward(request,response);
    }
}
