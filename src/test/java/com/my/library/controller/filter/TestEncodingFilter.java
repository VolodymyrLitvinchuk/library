package com.my.library.controller.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestEncodingFilter {
    @Mock
    private HttpServletRequest request = mock(HttpServletRequest.class);
    @Mock
    private HttpServletResponse response = mock(HttpServletResponse.class);
    @Mock
    private FilterChain filterChain = mock(FilterChain.class);
    @Mock
    private FilterConfig config = mock(FilterConfig.class);

    @Test
    @DisplayName("Test doFilter()")
    void test1() throws ServletException, IOException {
        Mockito.doReturn("UTF-8").when(config).getInitParameter("encoding");
        Mockito.doReturn("CP-1252").when(request).getCharacterEncoding();

        EncodingFilter encodingFilter = new EncodingFilter();
        encodingFilter.init(config);
        encodingFilter.doFilter(request,response,filterChain);

        Mockito.verify(request).setCharacterEncoding("UTF-8");
        Mockito.verify(response).setCharacterEncoding("UTF-8");

    }
}
