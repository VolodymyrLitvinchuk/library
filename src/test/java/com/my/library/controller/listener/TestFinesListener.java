package com.my.library.controller.listener;

import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.FineService;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;

@ExtendWith(MockitoExtension.class)
public class TestFinesListener {
    @Mock
    private FineService fineService = mock(FineService.class);
    @Mock
    private Timer timer = mock(Timer.class);
    @Mock
    private ServletContextEvent servletContextEvent = mock(ServletContextEvent.class);
    @Mock
    private ServletContext servletContext = mock(ServletContext.class);

    MockedStatic<ServiceFactory> serviceFactoryMockedStatic = Mockito.mockStatic(ServiceFactory.class);

    @BeforeEach
    void mockitoStaticSetUp(){
        serviceFactoryMockedStatic.when(ServiceFactory::getFineService).thenReturn(fineService);
    }

    @AfterEach
    void mockitoStaticTearDown(){
        serviceFactoryMockedStatic.close();
    }

    @Test
    @DisplayName("Test contextInitialized()")
    void test1(){
        try (MockedConstruction<Timer> timerMock = mockConstruction(Timer.class)) {
            Mockito.doReturn(servletContext).when(servletContextEvent).getServletContext();

            new FinesListener().contextInitialized(servletContextEvent);
            Timer timer = timerMock.constructed().get(0);
            timer.cancel();
            Mockito.verify(fineService).checkFines();
            Mockito.verify(timer).scheduleAtFixedRate(any(TimerTask.class),any(Date.class),anyLong());
            Mockito.verify(servletContext).setAttribute(anyString(),any(Timer.class));
        }
    }

    @Test
    @DisplayName("Test contextDestroyed")
    void test2(){
        Mockito.doReturn(servletContext).when(servletContextEvent).getServletContext();
        Mockito.doReturn(timer).when(servletContext).getAttribute("TIMER");

        new FinesListener().contextDestroyed(servletContextEvent);
        Mockito.verify(timer).cancel();
        Mockito.verify(servletContext).removeAttribute("TIMER");
    }

}
