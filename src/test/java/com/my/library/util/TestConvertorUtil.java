package com.my.library.util;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.book.BookAvailableDAO;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.dto.*;
import com.my.library.dao.entity.user.*;
import com.my.library.exception.DAOException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static com.my.library.controller.command.constant.Parameter.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class TestConvertorUtil {
    @Mock
    BookAvailableDAO bookAvailableDAO = DAOFactory.getBookAvailableDAO();

    @Test
    @DisplayName("Test convertUserToDTO()")
    void test1() throws DAOException {
        User user = new User(
                "login",
                "PB4TiQYS/RMGdeW8bgDHSw==",
                "2001litvinchuk2001@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                new UserRole(0,"Reader")
        );
        user.setStatus(new UserStatus(0,"Active"));

        UserDTO userDTO = ConvertorUtil.convertUserToDTO(user);

        User convertedUser = ConvertorUtil.convertDTOtoUser(userDTO,"12345ad");

        Assertions.assertEquals(user,convertedUser);
    }

    @Test
    @DisplayName("Test convertAuthorToDTO()")
    void test2(){
        Author author = new Author(
                1,
                "Volodymyr",
                "Володимир",
                "Litvinchuk",
                "Літвінчук");
        AuthorDTO authorDTO = ConvertorUtil.convertAuthorToDTO(author);
        Assertions.assertEquals(author.getId(), authorDTO.getId());
        Assertions.assertEquals(author.getFirstNameEN(), authorDTO.getFirstNameEN());
        Assertions.assertEquals(author.getFirstNameUA(), authorDTO.getFirstNameUA());
        Assertions.assertEquals(author.getLastNameEN(), authorDTO.getLastNameEN());
        Assertions.assertEquals(author.getLastNameUA(), authorDTO.getLastNameUA());
    }

    @Test
    @DisplayName("Test convertBookToDTO()")
    void test3() throws DAOException {
        Author author = new Author(
                1,
                "Volodymyr",
                "Володимир",
                "Litvinchuk",
                "Літвінчук");
        Publication publication = new Publication(1, "Folio", "Фоліо");
        Book book = new Book(
                "title",
                "назва",
                "Українська",
                author,
                publication,
                new java.sql.Date(new Date().getTime()),
                3
        );
        book.setId(1);

        Mockito.doReturn(Optional.of(1)).when(bookAvailableDAO).get(1);
        try (MockedStatic<DAOFactory> daoFactoryMockedStatic = Mockito.mockStatic(DAOFactory.class)) {
            daoFactoryMockedStatic.when(DAOFactory::getBookAvailableDAO).thenReturn(bookAvailableDAO);

            Assertions.assertDoesNotThrow(()->{
                ConvertorUtil.convertBookToDTO(book);
            });

            Assertions.assertNotNull(ConvertorUtil.convertBookToDTO(book));
        }
    }

    @Test
    @DisplayName("Test convertRequestToUserDTO()")
    void test4(){
        HttpServletRequest request = mock(HttpServletRequest.class);
        Mockito.doReturn("login").when(request).getParameter(LOGIN);
        Mockito.doReturn("2001litvinchuk@gmail.com").when(request).getParameter(EMAIL);
        Mockito.doReturn("Volodymyr").when(request).getParameter(FIRST_NAME);
        Mockito.doReturn("Litvinchuk").when(request).getParameter(LAST_NAME);

        Assertions.assertNotNull(ConvertorUtil.convertRequestToUserDTO(request));
    }

    @Test
    @DisplayName("Test convertRequestToUserDTOLibrarian()")
    void test5(){
        HttpServletRequest request = mock(HttpServletRequest.class);
        Mockito.doReturn("login").when(request).getParameter(LOGIN);
        Mockito.doReturn("2001litvinchuk@gmail.com").when(request).getParameter(EMAIL);
        Mockito.doReturn("Volodymyr").when(request).getParameter(FIRST_NAME);
        Mockito.doReturn("Litvinchuk").when(request).getParameter(LAST_NAME);

        Assertions.assertNotNull(ConvertorUtil.convertRequestToUserDTOLibrarian(request));
        Assertions.assertEquals("Librarian", ConvertorUtil.convertRequestToUserDTOLibrarian(request).getRole());
    }

    @Test
    @DisplayName("Test convertRequestToBookDTO()")
    void test6(){
        HttpServletRequest request = mock(HttpServletRequest.class);
        Mockito.doReturn("title").when(request).getParameter(TITLE_EN);
        Mockito.doReturn("назва").when(request).getParameter(TITLE_UA);
        Mockito.doReturn("Украхнська").when(request).getParameter(LANGUAGE);
        Mockito.doReturn("Volodymyr").when(request).getParameter(AUTHOR_FIRST_NAME_EN);
        Mockito.doReturn("Володимир").when(request).getParameter(AUTHOR_FIRST_NAME_UA);
        Mockito.doReturn("Litvinchuk").when(request).getParameter(AUTHOR_LAST_NAME_EN);
        Mockito.doReturn("Літвінчук").when(request).getParameter(AUTHOR_LAST_NAME_UA);
        Mockito.doReturn("Folio").when(request).getParameter(PUBLICATION_NAME_EN);
        Mockito.doReturn("Фоліо").when(request).getParameter(PUBLICATION_NAME_UA);
        Mockito.doReturn("1").when(request).getParameter(COPIES_OWNED);
        Mockito.doReturn("2023-01-10").when(request).getParameter(PUBLICATION_DATE);

        Assertions.assertNotNull(ConvertorUtil.convertRequestToBookDTO(request));
    }

    @Test
    @DisplayName("Test convertRequestToBookOrderDTO()")
    void test7(){
        HttpServletRequest request = mock(HttpServletRequest.class);
        UserDTO userDTO = new UserDTO(
                "login",
                "2001litvinchuk2001@gmail.com",
                "Volodymyr",
                "Litvinchuk",
                "Role",
                "Active"
        );
        HttpSession session = mock(HttpSession.class);
        Mockito.doReturn(session).when(request).getSession();
        Mockito.doReturn(userDTO).when(session).getAttribute(LOGGED_USER);
        Mockito.doReturn("1").when(request).getParameter(ORDER_DAYS);
        Mockito.doReturn("1").when(request).getParameter(BOOK_ID);
        Mockito.doReturn("0").when(request).getParameter(ORDER_TYPE);

        Assertions.assertNotNull(ConvertorUtil.convertRequestToBookOrderDTO(request));
    }

    @Test
    @DisplayName("Test convertToBookOrderForReaderDTO()")
    void test8(){
        BookOrderType type = new BookOrderType(
                1,
                "type",
                "тип"
        );
        BookOrderStatus status = new BookOrderStatus(
                1,
                "status",
                "Статус"
        );
        BookOrder bookOrder = new BookOrder(
                1,
                "login",
                type,
                new java.sql.Date(new Date().getTime()),
                new java.sql.Date(new Date().getTime()),
                status
        );
        bookOrder.setId(1);

        Assertions.assertEquals("login", ConvertorUtil.convertToBookOrderForReaderDTO(bookOrder, 10).getReaderLogin());
    }

    @Test
    @DisplayName("Test convertToBookOrderForLibrarianDTO()")
    void test9(){
        BookOrderType type = new BookOrderType(
                1,
                "type",
                "тип"
        );
        BookOrderStatus status = new BookOrderStatus(
                1,
                "status",
                "Статус"
        );
        BookOrder bookOrder = new BookOrder(
                1,
                "login",
                type,
                new java.sql.Date(new Date().getTime()),
                new java.sql.Date(new Date().getTime()),
                status
        );
        bookOrder.setId(1);
        ReaderDTO readerDTO = new ReaderDTO("login",1,1,1,1,1);

        Assertions.assertEquals("login", ConvertorUtil.convertToBookOrderForLibrarianDTO(bookOrder, 10,readerDTO).getReaderLogin());
    }

}
