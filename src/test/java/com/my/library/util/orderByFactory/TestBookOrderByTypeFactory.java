package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.BookOrderBy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.my.library.controller.command.constant.ParameterValue.*;
import static com.my.library.controller.command.constant.ParameterValue.DESC_ORDER;

public class TestBookOrderByTypeFactory {
    @Test
    @DisplayName("Test getOrderByType")
    void test1(){
        Assertions.assertEquals(
                BookOrderBy.BOOK_TITLE_EN,
                BookOrderByTypeFactory.getOrderByType(SORT_BY_TITLE, ACS_ORDER, "en"));
        Assertions.assertEquals(
                BookOrderBy.BOOK_TITLE_EN_DESC,
                BookOrderByTypeFactory.getOrderByType(SORT_BY_TITLE, DESC_ORDER, "en"));
        Assertions.assertEquals(
                BookOrderBy.AUTHOR_LAST_NAME_UA_DESC,
                BookOrderByTypeFactory.getOrderByType(SORT_BY_AUTHOR, DESC_ORDER, "ua"));
    }
}
