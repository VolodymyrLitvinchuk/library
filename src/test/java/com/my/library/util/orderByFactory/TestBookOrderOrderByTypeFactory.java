package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.my.library.controller.command.constant.ParameterValue.*;

public class TestBookOrderOrderByTypeFactory {
    @Test
    @DisplayName("Test getOrderByType")
    void test1(){
        Assertions.assertEquals(
                BookOrderOrderBy.ID,
                BookOrderOrderByTypeFactory.getOrderByType(SORT_BY_ID, ACS_ORDER));
        Assertions.assertEquals(
                BookOrderOrderBy.READER_LOGIN,
                BookOrderOrderByTypeFactory.getOrderByType(SORT_BY_READER, ACS_ORDER));
        Assertions.assertEquals(
                BookOrderOrderBy.STATUS_DESC,
                BookOrderOrderByTypeFactory.getOrderByType(SORT_BY_BOOK_ORDER_STATUS, DESC_ORDER));
    }
}
