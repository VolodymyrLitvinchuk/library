package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.UserOrderBy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.my.library.controller.command.constant.ParameterValue.*;
import static com.my.library.controller.command.constant.ParameterValue.DESC_ORDER;

public class TestUserOrderByTypeFactory {
    @Test
    @DisplayName("Test getOrderByType")
    void test1(){
        Assertions.assertEquals(
                UserOrderBy.LOGIN,
                UserOrderByTypeFactory.getOrderByType(SORT_BY_LOGIN, ACS_ORDER));
        Assertions.assertEquals(
                UserOrderBy.FIRST_NAME,
                UserOrderByTypeFactory.getOrderByType(SORT_BY_FIRST_NAME, ACS_ORDER));
        Assertions.assertEquals(
                UserOrderBy.FIRST_NAME_DESC,
                UserOrderByTypeFactory.getOrderByType(SORT_BY_FIRST_NAME, DESC_ORDER));
    }
}
