package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.AuthorOrderBy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.my.library.controller.command.constant.ParameterValue.*;

public class TestAuthorOrderByTypeFactory {
    @Test
    @DisplayName("Test getOrderByType")
    void test1(){
        Assertions.assertEquals(
                AuthorOrderBy.ID,
                AuthorOrderByTypeFactory.getOrderByType(SORT_BY_ID, ACS_ORDER, "en"));
        Assertions.assertEquals(
                AuthorOrderBy.FIRST_NAME_EN,
                AuthorOrderByTypeFactory.getOrderByType(SORT_BY_FIRST_NAME, ACS_ORDER, "en"));
        Assertions.assertEquals(
                AuthorOrderBy.FIRST_NAME_UA_DESC,
                AuthorOrderByTypeFactory.getOrderByType(SORT_BY_FIRST_NAME, DESC_ORDER, "ua"));
    }
}
