package com.my.library.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestHasher {
    @Test
    @DisplayName("Test hashing")
    void test1() throws Exception {
        String h1 = Hasher.hash("admin");
        String h2 = Hasher.hash("admin");

        System.out.println(Hasher.hash("12345Afk@"));
        Assertions.assertEquals(h2,h1);
    }
}
