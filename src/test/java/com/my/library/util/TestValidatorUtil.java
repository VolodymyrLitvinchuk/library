package com.my.library.util;

import com.my.library.dto.BookDTO;
import com.my.library.exception.IncorrectFormatException;
import com.my.library.exception.IncorrectPasswordException;
import com.my.library.exception.ServiceException;
import com.my.library.exception.constant.Message;
import com.my.library.util.constant.Regex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

public class TestValidatorUtil {
    @Test
    @DisplayName("Test normal validateFormat()")
    void test1(){
        String firstName = "Volodymyr";
        String lastName = "Litvinchuk";
        String login = "gezter123";
        String password = "@dknKAdf123@&";
        String email = "2001litvinchuk2001@gmail.com";

        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateFormat(firstName, Regex.NAME_REGEX, Message.ENTER_CORRECT_FIRST_NAME));
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateFormat(lastName, Regex.NAME_REGEX, Message.ENTER_CORRECT_LAST_NAME));
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateFormat(login, Regex.LOGIN_REGEX, Message.ENTER_CORRECT_LOGIN));
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateFormat(password, Regex.PASSWORD_REGEX, Message.ENTER_CORRECT_PASSWORD));
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateFormat(email, Regex.EMAIL_REGEX, Message.ENTER_CORRECT_EMAIL));

    }

    @Test
    @DisplayName("Test not-normal validateFormat()")
    void test2(){
        String firstName = "Vol1odymyr";
        String lastName = "L1itvinchuk";
        String login = "1";
        String password = "12345";
        String email = "2001litvinchuk2001@gmail";

        Assertions.assertThrows(IncorrectFormatException.class ,()-> ValidatorUtil.validateFormat(firstName, Regex.NAME_REGEX, Message.ENTER_CORRECT_FIRST_NAME));
        Assertions.assertThrows(IncorrectFormatException.class ,()-> ValidatorUtil.validateFormat(lastName, Regex.NAME_REGEX, Message.ENTER_CORRECT_LAST_NAME));
        Assertions.assertThrows(IncorrectFormatException.class ,()-> ValidatorUtil.validateFormat(login, Regex.LOGIN_REGEX, Message.ENTER_CORRECT_LOGIN));
        Assertions.assertThrows(IncorrectFormatException.class ,()-> ValidatorUtil.validateFormat(password, Regex.PASSWORD_REGEX, Message.ENTER_CORRECT_PASSWORD));
        Assertions.assertThrows(IncorrectFormatException.class ,()-> ValidatorUtil.validateFormat(email, Regex.EMAIL_REGEX, Message.ENTER_CORRECT_EMAIL));
    }

    @Test
    @DisplayName("Test verifyPassword()")
    void test3(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.verifyPasswordHash("admin","wY+6P/kkImZCbenRwE9xPQ=="));
        Assertions.assertThrows(IncorrectPasswordException.class, ()-> ValidatorUtil.verifyPasswordHash("admin1","wY+6P/kkImZCbenRwE9xPQ=="));
    }

    @Test
    @DisplayName("Test validateBookID()")
    void test4(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateBookID(1));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validateBookID(-1));
    }

    @Test
    @DisplayName("Test validateAuthorID()")
    void test5(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateAuthorID(1));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validateAuthorID(-1));
    }

    @Test
    @DisplayName("Test validatePublicationID()")
    void test6(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validatePublicationID(1));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validatePublicationID(-1));
    }

    @Test
    @DisplayName("Test validateCopiesOwned()")
    void test7(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateCopiesOwned(1));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validateCopiesOwned(-1));
    }

    @Test
    @DisplayName("Test validatePublicationDate()")
    void test8(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validatePublicationDate(new java.sql.Date(cal.getTime().getTime())));
        cal.add(Calendar.DATE, 3);
        Assertions.assertThrows(IncorrectFormatException.class, ()-> ValidatorUtil.validatePublicationDate(new java.sql.Date(cal.getTime().getTime())));
    }

    @Test
    @DisplayName("Test validateBook()")
    void test9(){
        BookDTO bookDTO = new BookDTO(
                "title",
                "Назва",
                "English",
                new java.sql.Date(new Date().getTime()),
                3,
                "Volodymyr",
                "Володимир",
                "Litvinchuk",
                "Літвінчук",
                "Publication",
                "Видавництво"
        );

        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateBook(bookDTO));
    }

    @Test
    @DisplayName("Test validateShowingParams()")
    void test10(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateShowingParams("byId","descending", "5","1"));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validateShowingParams("","descending", "5","1"));
    }

    @Test
    @DisplayName("Test validateSearchingParams()")
    void test11(){
        Assertions.assertDoesNotThrow(()-> ValidatorUtil.validateSearchingParams("title","", "","en"));
        Assertions.assertThrows(ServiceException.class, ()-> ValidatorUtil.validateSearchingParams("","name", "",""));
    }

}
