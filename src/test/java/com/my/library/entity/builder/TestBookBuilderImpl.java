package com.my.library.entity.builder;

import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.entity.builder.BookBuilderImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Date;

public class TestBookBuilderImpl {
    @Test
    @DisplayName("Test normal building")
    void test1(){
        BookBuilderImpl builder = new BookBuilderImpl();
        Book book = builder.setTitle_EN("title")
                .setTitle_UA("назва")
                .setLanguage("English")
                .setAuthor(new Author("abc","абв","asd","фів"))
                .setPublication(new Publication(0,"Volodymyr","Володимир"))
                .setPublication_Date(new Date(123214))
                .setCopiesOwned(3)
                .get();

        Assertions.assertNotNull(book,"Book was not built");
    }
    @Test
    @DisplayName("Test bad building")
    void test2(){
        BookBuilderImpl builder = new BookBuilderImpl();
        Book book = builder.setTitle_EN("title")
                .setTitle_UA("назва")
                .setLanguage("English")
                .setPublication_Date(new Date(123214))
                .setCopiesOwned(3)
                .get();

        Assertions.assertNull(book,"Book must be not built");

        BookBuilderImpl builder1 = new BookBuilderImpl();
        book = builder.setTitle_EN("title")
                .setTitle_UA("назва")
                .setPublication_Date(new Date(123214))
                .get();
        Assertions.assertNull(book,"Book must be not built");
    }
}
