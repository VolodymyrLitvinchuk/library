package com.my.library.entity.builder;

import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestAuthorBuilderImpl {
    @Test
    @DisplayName("Test normal building")
    void test1(){
        AuthorBuilderImpl builder = new AuthorBuilderImpl();
        Author author = builder
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .setLastNameUA("Літвінчук")
                .get();

        Assertions.assertNotNull(author, "Author object was not builded");

        Author author1 = new Author(
                "Volodymyr",
                "Володимир",
                "Litvinchuk",
                "Літвінчук");
        Assertions.assertEquals(author1,author,"Authors must be equals");
    }

    @Test
    @DisplayName("Test invalid building")
    void test2(){
        AuthorBuilderImpl builder = new AuthorBuilderImpl();
        Author author = builder
                .setFirstNameEN("Volodymyr")
                .setFirstNameUA("Володимир")
                .setLastNameEN("Litvinchuk")
                .get();
        Assertions.assertNull(author,"Author object must be null");
    }
}
