<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="profile.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
    <script src="js/bootstrap.min.js"></script>
    <script>
        window.onclick = function(event) {
            switch (event.target){
                case modalPass:
                    modalPass.style.display = "none";
                    break;
                case modalFirstName:
                    modalFirstName.style.display = "none";
                    break;
                case modalLastName:
                    modalLastName.style.display = "none";
                    break;
                case modalLogin:
                    modalLogin.style.display = "none";
                    break;
                case modalEmail:
                    modalEmail.style.display = "none";
                    break;
            }
        }
    </script>
</head>

<body>
<c:set var="localeAction" value="profile.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null}">
    <c:redirect url="signIn.jsp"/>
</c:if>

<div style="max-width: 800px; margin-right: auto; margin-left: auto">
    <div class="card">
        <div class="card-body" style="padding: 20px">
            <h5 style="text-align: center"><fmt:message key="inf.about.user"/></h5>
            <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
            <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
            <br><br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="first.name"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">${sessionScope.loggedUser.firstName}</p>
                </div>
                <div class="col-sm-2">
                    <button id="changeFirstNameBnt" type="button" class="btn btn-secondary"><fmt:message key="change.button"/></button>
                    <div id="changeFirstNameModal" class="modal">
                        <div class="modal-content">
                            <h5 style="text-align: center; margin-bottom: 50px"><fmt:message key="change.first.name"/></h5>

                            <form method="post" action="controller" style="text-align: center">
                                <input type="hidden" name="command" value="update-user-first-name">

                                <div class="form-floating text-muted">
                                    <input type="text" name="firstName" id="firstName" class="form-control input-sm"
                                           placeholder="<fmt:message key="new.first.name"/>"
                                           pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                                           required title="<fmt:message key="first.name.hint"/>">
                                    <label for="firstName"><fmt:message key="new.first.name"/></label>
                                    <br>
                                </div>
                                <br>
                                <button class="btn" style="background-color: #d7f2ff; " type="submit"><fmt:message key="change.button"/></button>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modalFirstName = document.getElementById("changeFirstNameModal");
                        var btnFirstName = document.getElementById("changeFirstNameBnt");

                        btnFirstName.onclick = function () {
                            modalFirstName.style.display = "block";
                        }
                    </script>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="last.name"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">${sessionScope.loggedUser.lastName}</p>
                </div>
                <div class="col-sm-2">
                    <button id="changeLastNameBnt" type="button" class="btn btn-secondary"><fmt:message key="change.button"/></button>
                    <div id="changeLastNameModal" class="modal">
                        <div class="modal-content">
                            <h5 style="text-align: center; margin-bottom: 50px"><fmt:message key="change.last.name"/></h5>

                            <form method="post" action="controller" style="text-align: center">
                                <input type="hidden" name="command" value="update-user-last-name">

                                <div class="form-floating text-muted">
                                    <input type="text" name="lastName" id="lastName" class="form-control input-sm"
                                           placeholder="<fmt:message key="new.last.name"/>"
                                           pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                                           required title="<fmt:message key="last.name.hint"/>">
                                    <label for="lastName"><fmt:message key="new.last.name"/></label>
                                    <br>
                                </div>
                                <br>
                                <button class="btn" style="background-color: #d7f2ff;" type="submit"><fmt:message key="change.button"/></button>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modalLastName = document.getElementById("changeLastNameModal");
                        var btnLastName = document.getElementById("changeLastNameBnt");

                        btnLastName.onclick = function () {
                            modalLastName.style.display = "block";
                        }
                    </script>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="login"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">${sessionScope.loggedUser.login}</p>
                </div>
                <div class="col-sm-2">
                    <button id="changeLoginBnt" type="button" class="btn btn-secondary"><fmt:message key="change.button"/></button>
                    <div id="changeLoginModal" class="modal">
                        <div class="modal-content">
                            <h5 style="text-align: center; margin-bottom: 50px"><fmt:message key="change.login"/></h5>

                            <form method="post" action="controller" style="text-align: center">
                                <input type="hidden" name="command" value="update-user-login">

                                <div class="form-floating text-muted">
                                    <input type="text" name="new-login" id="new-login" class="form-control input-sm"
                                           placeholder="<fmt:message key="new.login"/>" pattern=".{5,}" required
                                           title="<fmt:message key="login.hint"/> ">
                                    <label for="new-login"><fmt:message key="new.login"/></label>
                                </div>
                                <br>
                                <button class="btn" style="background-color: #d7f2ff;" type="submit"><fmt:message key="change.button"/></button>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modalLogin = document.getElementById("changeLoginModal");
                        var btnLogin = document.getElementById("changeLoginBnt");

                        btnLogin.onclick = function () {
                            modalLogin.style.display = "block";
                        }
                    </script>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="password.label"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">*****</p>
                </div>
                <div class="col-sm-2">
                    <button id="changePasswdBnt" type="button" class="btn btn-secondary"><fmt:message key="change.button"/></button>
                    <div id="changePasswdModal" class="modal">
                        <div class="modal-content">
                            <h5 style="text-align: center; margin-bottom: 50px;"><fmt:message key="change.password"/></h5>
                            <form method="post" action="controller" style="text-align: center">
                                <input type="hidden" name="command" value="update-user-pass">
                                <div class="form-floating text-muted">
                                    <input type="password" name="current-password" id="current-password"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="current.password"/>"
                                           pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                                           title="<fmt:message key="password.hint"/>">
                                    <label for="current-password"><fmt:message key="current.password"/></label>
                                    <br>
                                </div>
                                <div class="form-floating text-muted">
                                    <input type="password" name="new-password" id="new-password"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="new.password"/>"
                                           pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                                           title="<fmt:message key="password.hint"/>">
                                    <label for="new-password"><fmt:message key="new.password"/></label>
                                    <br>
                                </div>
                                <div class="form-floating text-muted">
                                    <input type="password" name="repeat-password" id="repeat-password"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="password.repeat"/>"
                                           pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                                           title="<fmt:message key="password.hint"/>">
                                    <label for="repeat-password"><fmt:message key="password.repeat"/></label>
                                    <br>
                                </div>
                                <br>
                                <button class="btn" style="background-color: #d7f2ff;" type="submit"><fmt:message key="change.button"/></button>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modalPass = document.getElementById("changePasswdModal");
                        var btnPass = document.getElementById("changePasswdBnt");

                        btnPass.onclick = function () {
                            modalPass.style.display = "block";
                        }
                    </script>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="email"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">${sessionScope.loggedUser.email}</p>
                </div>
                <div class="col-sm-2">
                    <button id="changeEmailBnt" type="button" class="btn btn-secondary"><fmt:message key="change.button"/></button>
                    <div id="changeEmailModal" class="modal">
                        <div class="modal-content">
                            <h5 style="text-align: center; margin-bottom: 50px"><fmt:message key="change.email"/></h5>

                            <form method="post" action="controller" style="text-align: center">
                                <input type="hidden" name="command" value="update-user-email">

                                <div class="form-floating text-muted">
                                    <input type="text" name="email" id="email" class="form-control input-sm"
                                           pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
                                           required placeholder="<fmt:message key="new.email"/>">
                                    <label for="email"><fmt:message key="new.email"/></label>
                                    <br>
                                </div>
                                <button class="btn" style="background-color: #d7f2ff;" type="submit"><fmt:message key="change.button"/></button>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modalEmail = document.getElementById("changeEmailModal");
                        var btnEmail = document.getElementById("changeEmailBnt");

                        btnEmail.onclick = function () {
                            modalEmail.style.display = "block";
                        }
                    </script>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-sm-3">
                    <p class="mb-0"><fmt:message key="role"/></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-muted mb-0">${sessionScope.loggedUser.role}</p>
                </div>
            </div>
            <br>
        </div>
    </div>


</div>

</body>
</html>