<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="edit.author.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="editAuthor.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Admin' or requestScope.authorId eq null}">
    <c:redirect url="authors.jsp"/>
</c:if>

<br>
<div style="max-width: 1100px; margin-right: auto; margin-left: auto">
    <div class="card">
        <div class="card-body">
            <p style="text-align: center; font-size: 20px; margin-bottom: 5px"><fmt:message key="author.editing"/>  ID=${requestScope.authorId}</p>
            <br>
            <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
            <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
            <br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-author-first-name-en">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="authorFirstNameEN" value="${requestScope.authorFirstNameEN}">
                    <input type="hidden" name="authorFirstNameUA" value="${requestScope.authorFirstNameUA}">
                    <input type="hidden" name="authorLastNameEN" value="${requestScope.authorLastNameEN}">
                    <input type="hidden" name="authorLastNameUA" value="${requestScope.authorLastNameUA}">
                    <div class="col-sm-3">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="authorFirstNameEN"/></p>
                    </div>
                    <div class="col-sm-3">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorFirstNameEN}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newAuthorFirstNameEN" required
                               pattern="[A-Za-z,.'\-\s]{1,30}"
                               title="<fmt:message key="authorFirstNameEN.hint"/>"
                               placeholder="<fmt:message key="newAuthorFirstNameEN"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-author-first-name-ua">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="authorFirstNameEN" value="${requestScope.authorFirstNameEN}">
                    <input type="hidden" name="authorFirstNameUA" value="${requestScope.authorFirstNameUA}">
                    <input type="hidden" name="authorLastNameEN" value="${requestScope.authorLastNameEN}">
                    <input type="hidden" name="authorLastNameUA" value="${requestScope.authorLastNameUA}">

                    <div class="col-sm-3">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="authorFirstNameUA"/></p>
                    </div>
                    <div class="col-sm-3">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorFirstNameUA}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newAuthorFirstNameUA" required
                               pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                               title="<fmt:message key="authorFirstNameUA.hint"/>"
                               placeholder="<fmt:message key="newAuthorFirstNameUA"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-author-last-name-en">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="authorFirstNameEN" value="${requestScope.authorFirstNameEN}">
                    <input type="hidden" name="authorFirstNameUA" value="${requestScope.authorFirstNameUA}">
                    <input type="hidden" name="authorLastNameEN" value="${requestScope.authorLastNameEN}">
                    <input type="hidden" name="authorLastNameUA" value="${requestScope.authorLastNameUA}">
                    <div class="col-sm-3">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="authorLastNameEN"/></p>
                    </div>
                    <div class="col-sm-3">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorLastNameEN}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newAuthorLastNameEN" required
                               pattern="[A-Za-z,.'\-\s]{1,30}"
                               title="<fmt:message key="authorLastNameEN.hint"/>"
                               placeholder="<fmt:message key="newAuthorLastNameEN"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-author-last-name-ua">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="authorFirstNameEN" value="${requestScope.authorFirstNameEN}">
                    <input type="hidden" name="authorFirstNameUA" value="${requestScope.authorFirstNameUA}">
                    <input type="hidden" name="authorLastNameEN" value="${requestScope.authorLastNameEN}">
                    <input type="hidden" name="authorLastNameUA" value="${requestScope.authorLastNameUA}">

                    <div class="col-sm-3">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="authorLastNameUA"/></p>
                    </div>
                    <div class="col-sm-3">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorLastNameUA}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newAuthorLastNameUA" required
                               pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                               title="<fmt:message key="authorLastNameUA.hint"/>"
                               placeholder="<fmt:message key="newAuthorLastNameUA"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

        </div>
    </div>

</div>


</body>
</html>
