<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="contact.us"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
</head>

<body style="background-image: url('img/library.jpg'); background-size: 100%; background-repeat: no-repeat; background-position-y: 75px">
<c:set var="localeAction" value="contactUs.jsp"/>
<%@ include file="/jspf/header.jspf" %>


<div class="card" style="max-width: 600px; align-items: center; margin-left: auto; margin-right: auto; padding: 20px; margin-top: 250px">

    <h4 style="font-weight: normal; user-select: none; margin-bottom: 30px"><fmt:message key="contact.us"/></h4>

    <p style="user-select: none; margin-bottom: 30px; "><fmt:message key="contact.us.explanation.part1"/></p>
    <p style="margin-bottom: 0; user-select: none; color: grey"><fmt:message key="contact.us.explanation.part2"/></p>

    <%@ page import="com.my.library.dao.mysql.Constant" %>
    <p><%=Constant.CURRENT_ADMIN_EMAIL%></p>

</div>

</body>
</html>
