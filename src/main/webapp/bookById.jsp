<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="show.book.by.id.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="bookById.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br>
<c:if test="${sessionScope.loggedUser eq null  or requestScope.bookId eq null}">
    <c:redirect url="orders.jsp"/>
</c:if>

<br>
<div style="max-width: 700px; margin-right: auto; margin-left: auto">
    <div class="card">
        <div class="card-body">
            <p style="text-align: center; font-size: 25px; margin-bottom: 5px"><fmt:message key="show.book.by.id"/>
                ID=${requestScope.bookId}</p>

            <br>


            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="titleEN"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.titleEN}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="titleUA"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.titleUA}</p>
                </div>

            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="language"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.language}</p>
                </div>
            </div>
            <br><br>
            <p style="text-align: center; font-size: 20px"><fmt:message key="author"/></p>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="authorFirstNameEN"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.authorFirstNameEN}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="authorFirstNameUA"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.authorFirstNameUA}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="authorLastNameEN"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.authorLastNameEN}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="authorLastNameUA"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.authorLastNameUA}</p>
                </div>
            </div>
            <br><br>
            <p style="text-align: center; font-size: 20px"><fmt:message key="publication"/></p>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="publicationNameEN"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.publicationNameEN}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="publicationNameUA"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.publicationNameUA}</p>
                </div>
            </div>
            <br>
            <div class="row" style="padding-left: 80px">
                <div class="col-lg-5">
                    <p class="text-muted mb-0" style="margin: 5px"><fmt:message key="publicationDate"/></p>
                </div>
                <div class="col-lg-7">
                    <p class=" mb-0" style="margin: 5px">${requestScope.publicationDate}</p>
                </div>
            </div>
            <br>

        </div>
    </div>

</div>

</body>
</html>
