<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="forgot.password.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/signin.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body class="text-center">
<main class="form-signin w-100 m-auto">
    <div class="position-absolute top-0 start-0">
        <div style="float: left; margin-left: 100px; margin-top: 20px">
            <a href="/Library" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                <span class="fs-3" style="font-family: Andale Mono; font-weight:bold; color: #0d6efd">LIBRARY</span>
            </a>
        </div>
    </div>
    <div class="position-absolute top-0 end-0" style="margin: 10px">
        <form method="post" action="forgotPassword.jsp" style="padding-left: 20px; margin-right: 10px">
            <label>
                <select class="form-select" data-width="fit" name="locale" onchange='submit()'>
                    <option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''} >English</option>
                    <option data-content='<span class="flag-icon flag-icon-ua"></span> Українська'
                            value="ua" ${sessionScope.locale == 'ua' ? 'selected' : ''} >Українська</option>
                </select>
            </label>
        </form>
    </div>

    <form method="POST" action="controller">
        <input type="hidden" name="command" value="forgot-password">

        <h1 class="h3 mb-3 fw-normal"><fmt:message key="enter.login"/></h1>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="login" id="login" class="form-control" required
                   placeholder="<fmt:message key="login"/>" value="${requestScope.login}">
            <label for="login"><fmt:message key="login"/></label>
        </div>
        <br>
        <p style="color: grey"><fmt:message key="forgot.password.explanation"/></p>

        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>

        <br>
        <button class="w-100 btn btn-lg btn-primary" type="submit"><fmt:message key="send.button"/></button>
        <br>
        <br>
        <a href="signIn.jsp" class="link-secondary"><fmt:message key="go.to.sign.in.page"/></a>

    </form >
</main>
</body>
</html>
