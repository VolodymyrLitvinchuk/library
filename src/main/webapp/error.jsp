<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="error.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>

<div class="d-flex align-items-center justify-content-center vh-100">
    <div class="text-center">
        <h3 class=" fw-bold">500</h3>
        <p class="fs-3" style="font-size: 40px">
            <span class="text-danger"><fmt:message key="opps"/></span>
            <fmt:message key="internal.server.error"/>
        </p>

        <a href="/Library" class="btn btn-primary"><fmt:message key="go.to.main.page.button"/> </a>
    </div>
</div>

</body>
</html>
