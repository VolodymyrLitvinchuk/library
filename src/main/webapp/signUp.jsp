<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="sign.up.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/signin.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body class="text-center">
<main class="form-signin w-100 m-auto">
    <div class="position-absolute top-0 start-0">
        <div style="float: left; margin-left: 100px; margin-top: 20px">
            <a href="/Library" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                <span class="fs-3" style="font-family: Andale Mono; font-weight:bold; color: #0d6efd">LIBRARY</span>
            </a>
        </div>
    </div>
    <div class="position-absolute top-0 end-0" style="margin: 10px">
        <form method="post" action="signUp.jsp" style="padding-left: 20px; margin-right: 10px">
            <label>
                <select class="form-select" data-width="fit" name="locale" onchange='submit()'>
                    <option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''} >English</option>
                    <option data-content='<span class="flag-icon flag-icon-ua"></span> Українська'
                            value="ua" ${sessionScope.locale == 'ua' ? 'selected' : ''} >Українська</option>
                </select>
            </label>
        </form>
    </div>

    <form method="POST" action="controller">
        <input type="hidden" name="command" value="sign-up-reader">


        <h1 class="h3 mb-3 fw-normal"><fmt:message key="sign.up"/></h1>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="login" id="login" class="form-control" value="${requestScope.login}"
                   placeholder="<fmt:message key="login"/>" pattern=".{5,}" required
                   title="<fmt:message key="login.hint"/> ">
            <label for="login"><fmt:message key="login"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="password" name="password" id="password" class="form-control"
                   placeholder="<fmt:message key="password"/>"
                   pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                   title="<fmt:message key="password.hint"/>" >
            <label for="password"><fmt:message key="password"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="password" name="repeat-password" id="repeat-password" class="form-control"
                   placeholder="<fmt:message key="password.repeat"/>"
                   pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                   title="<fmt:message key="password.hint"/>">
            <label for="repeat-password"><fmt:message key="password.repeat"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="email" name="email" id="email" class="form-control" value="${requestScope.email}"
                   pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
                   required placeholder="<fmt:message key="email"/>" >
            <label for="email"><fmt:message key="email"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="firstName" id="firstName" class="form-control" value="${requestScope.firstName}"
                   placeholder="<fmt:message key="first.name"/>" pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                   required title="<fmt:message key="first.name.hint"/>">
            <label for="firstName"><fmt:message key="first.name"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="lastName" id="lastName" class="form-control" value="${requestScope.lastName}"
                   placeholder="<fmt:message key="last.name"/>" pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                   required title="<fmt:message key="last.name.hint"/>">
            <label for="lastName"><fmt:message key="last.name"/></label>
            <br>
        </div>

        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>

        <br>
        <br>

        <button class="w-100 btn btn-lg btn-primary" type="submit"><fmt:message key="sign.up.button"/></button>
        <br>
        <br>
        <a href="signIn.jsp" class="link-secondary"><fmt:message key="sign.in.suggestion"/></a>


    </form >
</main>
</body>
</html>
