<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="book.order.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="orderBook.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Reader' or sessionScope.loggedUser.status eq 'Suspended' or requestScope.bookId eq null}">
    <c:redirect url="books.jsp"/>
</c:if>

<br>
<div style="max-width: 1100px; margin-right: auto; margin-left: auto">
    <div class="card">
        <div class="card-body">
            <p style="text-align: center; font-size: 20px; margin-bottom: 5px"><fmt:message key="book.ordering"/> ID=${requestScope.bookId}</p>
            <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
            <br>

            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="book.title"/></p>
                        </div>
                        <div class="col-lg-7">
                            <c:if test="${sessionScope.locale eq 'en'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.titleEN}</p>
                            </c:if>
                            <c:if test="${sessionScope.locale eq 'ua'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.titleUA}</p>
                            </c:if>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="author.first.name"/></p>
                        </div>
                        <div class="col-lg-7">
                            <c:if test="${sessionScope.locale eq 'en'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorFirstNameEN}</p>
                            </c:if>
                            <c:if test="${sessionScope.locale eq 'ua'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorFirstNameUA}</p>
                            </c:if>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="author.last.name"/></p>
                        </div>
                        <div class="col-lg-7">
                            <c:if test="${sessionScope.locale eq 'en'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorLastNameEN}</p>
                            </c:if>
                            <c:if test="${sessionScope.locale eq 'ua'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorLastNameUA}</p>
                            </c:if>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="language"/></p>
                        </div>
                        <div class="col-lg-7">
                            <p class="text-muted mb-0" style="margin: 5px">${requestScope.language}</p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="publication"/></p>
                        </div>
                        <div class="col-lg-7">
                            <c:if test="${sessionScope.locale eq 'en'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.publicationNameEN}</p>
                            </c:if>
                            <c:if test="${sessionScope.locale eq 'ua'}">
                                <p class="text-muted mb-0" style="margin: 5px">${requestScope.publicationNameUA}</p>
                            </c:if>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="mb-0" style="margin: 5px"><fmt:message key="publicationDate"/></p>
                        </div>
                        <div class="col-lg-7">
                            <p class="text-muted mb-0" style="margin: 5px">${requestScope.publicationDate}</p>
                        </div>
                    </div>
                    <br>
                </div>

                <div class="col-lg-6"><br>
                    <form method="post" action="controller">
                        <input type="hidden" name="command" value="order-book">
                        <input type="hidden" name="bookId" value="${requestScope.bookId}">
                        <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                        <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                        <input type="hidden" name="language" value="${requestScope.language}">
                        <input type="hidden" name="authorFirstNameEN" value="${requestScope.authorFirstNameEN}">
                        <input type="hidden" name="authorFirstNameUA" value="${requestScope.authorFirstNameUA}">
                        <input type="hidden" name="authorLastNameEN" value="${requestScope.authorLastNameEN}">
                        <input type="hidden" name="authorLastNameUA" value="${requestScope.authorLastNameUA}">
                        <input type="hidden" name="publicationNameEN" value="${requestScope.publicationNameEN}">
                        <input type="hidden" name="publicationNameUA" value="${requestScope.publicationNameUA}">
                        <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">

                        <label class="pad-10" for="orderType" style="width: 150px;"><fmt:message key="order.type"/></label>
                        <select name="orderType" id="orderType" required class="form-select pad-10 d-inline-flex" style="width: 250px;">
                            <option value="0" >
                                <fmt:message key="subscription.order"/></option>
                            <option value="1" >
                                <fmt:message key="reading.room.order"/></option>
                        </select><br>

                        <label class="pad-10" for="orderDays" style="width: 150px;"><fmt:message key="order.days.label"/></label>
                        <input type="number" min="1" max="10" class="form-control pad-10 d-inline-flex" style="width: 250px;"
                               required name="orderDays" id="orderDays"
                               placeholder="<fmt:message key="order.days.placeholder"/>"><br>

                        <div class="form-check" style="padding-left: 35px; padding-right: 35px"><br>
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" required>
                            <label class="form-check-label" for="flexCheckChecked">
                                <fmt:message key="book.order.terms1"/> ${requestScope.fineAmount}<fmt:message key="book.order.terms2"/>
                            </label>
                        </div>

                        <br>
                        <div class="col-md-12 text-center">
                            <button type="submit" class="w-50 btn btn-primary"><fmt:message key="book.order.button"/></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>
