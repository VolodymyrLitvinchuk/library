<%@ attribute name="error" required="true" type="java.lang.String"%>
<%@ attribute name="locale" required="true" type="java.lang.String"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<c:if test="${not empty error}">
    <div style="text-align: center">
        <span class="text-danger align-items-center"><fmt:message key="${error}"/></span></div>
</c:if>

