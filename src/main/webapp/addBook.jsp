<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tag" uri="/WEB-INF/myTags.tld" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="books.add.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="addBook.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Admin'}">
    <c:redirect url="books.jsp"/>
</c:if>


    <div class="container" style="max-width: 800px">
        <form method="POST" action="controller">
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="command" value="add-book">

                    <p style="text-align: center"><fmt:message key="book"/></p>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="titleEN" required value="${requestScope.titleEN}"
                                   pattern="[A-Za-z,.'\-\s]{1,30}"
                                   title="<fmt:message key="titleEN.hint"/>"
                                   placeholder="<fmt:message key="titleEN"/>">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="titleUA" required value="${requestScope.titleUA}"
                                   pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                                   title="<fmt:message key="titleUA.hint"/>"
                                   placeholder="<fmt:message key="titleUA"/>">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="language" required value="${requestScope.language}"
                                   pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїєA-Za-z]{1,30}"
                                   title="<fmt:message key="language.hint"/>"
                                   placeholder="<fmt:message key="language"/>">
                        </div>
                        <div class="col">
                            <input type="number" min="1" max="100" class="form-control" name="copiesOwned"
                                   required value="${requestScope.copiesOwned}"
                                   title="<fmt:message key="copiesOwned.hint"/>"
                                   placeholder="<fmt:message key="copiesOwned"/>">
                        </div>
                    </div>
                    <br><br>


                    <p style="text-align: center"><fmt:message key="author"/></p>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="authorFirstNameEN" required value="${requestScope.authorFirstNameEN}"
                                   pattern="[A-Za-z'\-]{1,30}"
                                   title="<fmt:message key="authorFirstNameEN.hint"/>"
                                   placeholder="<fmt:message key="authorFirstNameEN"/>">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="authorFirstNameUA" required value="${requestScope.authorFirstNameUA}"
                                   pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє'\-]{1,30}"
                                   title="<fmt:message key="authorFirstNameUA.hint"/>"
                                   placeholder="<fmt:message key="authorFirstNameUA"/>">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="authorLastNameEN" required value="${requestScope.authorLastNameEN}"
                                   pattern="[A-Za-z'\-]{1,30}"
                                   title="<fmt:message key="authorLastNameEN.hint"/>"
                                   placeholder="<fmt:message key="authorLastNameEN"/>">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="authorLastNameUA" required value="${requestScope.authorLastNameUA}"
                                   pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє'\-]{1,30}"
                                   title="<fmt:message key="authorLastNameUA.hint"/>"
                                   placeholder="<fmt:message key="authorLastNameUA"/>">
                        </div>
                    </div>
                    <br><br>


                    <p style="text-align: center"><fmt:message key="publication"/></p>
                    <div class="row">
                        <div class="col">
                            <input type="date" class="form-control" name="publicationDate"  required value="${requestScope.publicationDate}"
                                   max="<tag:currentDate/>"
                                   title="<fmt:message key="publicationDate.hint"/>"
                                   placeholder="<fmt:message key="publicationDate"/>">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="publicationNameEN"  required value="${requestScope.publicationNameEN}"
                                   pattern="[A-Za-z,.'\-\s]{1,30}"
                                   title="<fmt:message key="publicationNameEN.hint"/>"
                                   placeholder="<fmt:message key="publicationNameEN"/>">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="publicationNameUA"  required value="${requestScope.publicationNameUA}"
                                   pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                                   title="<fmt:message key="publicationNameUA.hint"/>"
                                   placeholder="<fmt:message key="publicationNameUA"/>">
                        </div>
                    </div>

                    <br>
                    <div style="text-align: center">
                        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
                        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
                        <br><br>
                    </div>

                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="add.book.button"/></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</body>
</html>