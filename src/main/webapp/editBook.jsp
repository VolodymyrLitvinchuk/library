<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="edit.book.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="editBook.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Admin' or requestScope.bookId eq null}">
    <c:redirect url="books.jsp"/>
</c:if>

<br>
<div style="max-width: 1100px; margin-right: auto; margin-left: auto">
    <div class="card">
        <div class="card-body">
            <p style="text-align: center; font-size: 20px; margin-bottom: 5px"><fmt:message key="book.editing"/>  ID=${requestScope.bookId}</p>
            <br>
            <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
            <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
            <br>
            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-title-en">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="titleEN"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.titleEN}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newTitleEN" required
                               pattern="[A-Za-z,.'\-\s]{1,30}"
                               title="<fmt:message key="titleEN.hint"/>"
                               placeholder="<fmt:message key="new.titleEN"/>">
                    </div>
                    <div class="col-sm-2">
                    <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-title-ua">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="titleUA"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.titleUA}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newTitleUA" required
                               pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                               title="<fmt:message key="titleUA.hint"/>"
                               placeholder="<fmt:message key="new.titleUA"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-book-language">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="language"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.language}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="newLanguage" required
                               pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїєA-Za-z]{1,30}"
                               title="<fmt:message key="language.hint"/>"
                               placeholder="<fmt:message key="new.language"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-copies-owned">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="copiesOwned"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.copiesOwned}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" min="1" max="100" class="form-control" required name="newCopiesOwned"
                               title="<fmt:message key="copiesOwned.hint"/>"
                               placeholder="<fmt:message key="new.copiesOwned"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-book-author-id">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="authorId"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.authorId}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" min="1" class="form-control" required name="newAuthorId"
                               placeholder="<fmt:message key="new.authorId"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-book-publication-id">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="publicationId"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.publicationId}</p>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" min="1" class="form-control" required name="newPublicationId"
                               placeholder="<fmt:message key="new.publicationId"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>

            <form method="post" action="controller">
                <div class="row" >
                    <input type="hidden" name="command" value="update-publication-date">
                    <input type="hidden" name="bookId" value="${requestScope.bookId}">
                    <input type="hidden" name="titleEN" value="${requestScope.titleEN}">
                    <input type="hidden" name="titleUA" value="${requestScope.titleUA}">
                    <input type="hidden" name="language" value="${requestScope.language}">
                    <input type="hidden" name="authorId" value="${requestScope.authorId}">
                    <input type="hidden" name="publicationId" value="${requestScope.publicationId}">
                    <input type="hidden" name="publicationDate" value="${requestScope.publicationDate}">
                    <input type="hidden" name="copiesOwned" value="${requestScope.copiesOwned}">
                    <div class="col-sm-2">
                        <p class="mb-0" style="margin: 5px"><fmt:message key="publicationDate"/></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="text-muted mb-0" style="margin: 5px">${requestScope.publicationDate}</p>
                    </div>
                    <div class="col-sm-3">
                        <jsp:useBean id="now" class="java.util.Date" />
                        <input type="date" class="form-control" name="newPublicationDate" required
                               max="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>"
                               title="<fmt:message key="publicationDate.hint"/>"
                               placeholder="<fmt:message key="new.publicationDate"/>">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-secondary"><fmt:message key="update.button"/></button>
                    </div>
                </div>
            </form><br>
        </div>
    </div>

</div>


</body>
</html>
