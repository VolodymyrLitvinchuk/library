<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="sign.up.librarian.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="signUpLibrarian.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Admin'}">
    <c:redirect url="books.jsp"/>
</c:if>


<div class="container" style="max-width: 400px; padding: 15px;">
    <form method="POST" action="controller">
        <input type="hidden" name="command" value="sign-up-librarian">


        <h1 class="h3 mb-3 fw-normal"><fmt:message key="sign.up.librarian"/></h1>
        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
        <br>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="login" id="login" class="form-control" value="${requestScope.login}"
                   placeholder="<fmt:message key="login"/>" pattern=".{5,}" required
                   title="<fmt:message key="login.hint"/> ">
            <label for="login"><fmt:message key="login"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="password" name="password" id="password" class="form-control"
                   placeholder="<fmt:message key="password"/>"
                   pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                   title="<fmt:message key="password.hint"/>">
            <label for="password"><fmt:message key="password"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="password" name="repeat-password" id="repeat-password" class="form-control"
                   placeholder="<fmt:message key="password.repeat"/>"
                   pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,20}$" required
                   title="<fmt:message key="password.hint"/>">
            <label for="repeat-password"><fmt:message key="password.repeat"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="email" name="email" id="email" class="form-control" value="${requestScope.email}"
                   pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
                   required placeholder="<fmt:message key="email"/>">
            <label for="email"><fmt:message key="email"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="firstName" id="firstName" class="form-control" value="${requestScope.firstName}"
                   placeholder="<fmt:message key="first.name"/>" pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                   required title="<fmt:message key="first.name.hint"/>">
            <label for="firstName"><fmt:message key="first.name"/></label>
        </div>

        <div class="form-floating" style="margin-bottom: 5px">
            <input type="text" name="lastName" id="lastName" class="form-control" value="${requestScope.lastName}"
                   placeholder="<fmt:message key="last.name"/>" pattern="^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\- ]{1,30}"
                   required title="<fmt:message key="last.name.hint"/>">
            <label for="lastName"><fmt:message key="last.name"/></label>
            <br>
        </div>

        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>

        <br><br>

        <button class="w-100 btn btn-lg btn-secondary" type="submit"><fmt:message key="sign.up.librarian.button"/></button>

    </form>
</div>

</body>
</html>
