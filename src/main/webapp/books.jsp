<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="books.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="books.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${requestScope.books eq null}">
    <c:redirect url="controller?command=show-books&currentPage=1&sortType=byTitle&sortOrder=ascending&itemsPerPage=10&error=${requestScope.error}&message=${requestScope.message}"/>
</c:if>





<div class="mask d-flex align-items-center h-100">
    <c:choose>
        <c:when test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Admin'}">
            <div style="width: 1500px; margin-right: auto; margin-left: auto;">
        </c:when>
        <c:otherwise>
            <div class="container">
        </c:otherwise>
    </c:choose>
        <div class="card" style="width: 1300px; margin-right: auto; margin-left: auto;">
            <div class="card-body" style="padding: 10px">
                <div style="float: left; ">
                <form method="get" action="controller">
                    <c:choose>
                        <c:when test="${not empty requestScope.titlePattern or not empty requestScope.authorFirstNamePattern or not empty requestScope.authorLastNamePattern }">
                            <input type="hidden" name="command" value="search-books">
                            <input type="hidden" name="titlePattern" value="${requestScope.titlePattern}">
                            <input type="hidden" name="authorFirstNamePattern" value="${requestScope.authorFirstNamePattern}">
                            <input type="hidden" name="authorLastNamePattern" value="${requestScope.authorLastNamePattern}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="command" value="show-books">
                        </c:otherwise>
                    </c:choose>
                    <input type="hidden" name="currentPage" value="1">

                    <label class="pad-10" for="sortType"><fmt:message key="sort.by"/> </label>
                    <select onchange="this.form.submit()" name="sortType" id="sortType" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="byTitle" <c:if test="${requestScope.sortType == 'byTitle'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.title"/></option>
                        <option value="byAuthor" <c:if test="${requestScope.sortType == 'byAuthor'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.author"/></option>
                        <option value="byPublication" <c:if test="${requestScope.sortType == 'byPublication'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.publication"/></option>
                        <option value="byPublicationDate" <c:if test="${requestScope.sortType == 'byPublicationDate'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.publication.date"/></option>
                    </select>

                    <label class="pad-10" for="sortOrder"><fmt:message key="sort.order"/></label>
                    <select onchange="this.form.submit()" name="sortOrder" id="sortOrder" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="ascending" <c:if test="${requestScope.sortOrder == 'ascending'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="asc.order"/></option>
                        <option value="descending" <c:if test="${requestScope.sortOrder == 'descending'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="desc.order"/></option>
                    </select>

                    <label class="pad-10" for="itemsPerPage"><fmt:message key="items.per.page"/></label>
                    <select onchange="this.form.submit()" name="itemsPerPage" id="itemsPerPage" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="5" <c:if test="${requestScope.itemsPerPage == '5'}"><c:out value="selected"/></c:if> >5</option>
                        <option value="10" <c:if test="${requestScope.itemsPerPage == '10'}"><c:out value="selected"/></c:if> >10</option>
                        <option value="15" <c:if test="${requestScope.itemsPerPage == '15'}"><c:out value="selected"/></c:if> >15</option>
                    </select>

                </form>
                </div>

                <div style="float: right; margin: 10px; display: flex">
                    <button id="searchButton" type="button" class="btn" style="background-color: #d7f2ff; margin-right: 20px"><fmt:message key="search.button"/></button>
                    <div id="showSearchModal" class="modal">
                        <div class="modal-content">
                            <p style="text-align: center; font-weight: bold"><fmt:message key="search.title"/> </p>
                            <br><br>
                            <form method="get" action="controller">
                                <input type="hidden" name="command" value="search-books">
                                <input type="hidden" name="currentPage" value="1">
                                <input type="hidden" name="sortType" value="${requestScope.sortType}">
                                <input type="hidden" name="sortOrder" value="${requestScope.sortOrder}">
                                <input type="hidden" name="itemsPerPage" value="${requestScope.itemsPerPage}">

                                <label class="pad-10" for="titlePattern"><fmt:message key="title.pattern"/></label>
                                <input type="text" class="form-control" name="titlePattern" id="titlePattern" value="${requestScope.titlePattern}"
                                <c:if test="${sessionScope.locale eq 'en'}">
                                       pattern="[A-Za-z,.'\-\s]{1,30}"
                                       title="<fmt:message key="titleEN.hint"/>"
                                </c:if>
                                <c:if test="${sessionScope.locale eq 'ua'}">
                                       pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє,.'\-\s]{1,30}"
                                       title="<fmt:message key="titleUA.hint"/>"
                                </c:if>
                                >

                                <label class="pad-10" for="authorFirstNamePattern"><fmt:message key="authorFirstName.pattern"/></label>
                                <input type="text"  class="form-control" name="authorFirstNamePattern" id="authorFirstNamePattern" value="${requestScope.authorFirstNamePattern}"
                                <c:if test="${sessionScope.locale eq 'en'}">
                                       pattern="[A-Za-z'\-]{1,30}"
                                       title="<fmt:message key="authorFirstNameEN.hint"/>"
                                </c:if>
                                <c:if test="${sessionScope.locale eq 'ua'}">
                                       pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє'\-]{1,30}"
                                       title="<fmt:message key="authorFirstNameUA.hint"/>"
                                </c:if>
                                >

                                <label class="pad-10" for="authorLastNamePattern"><fmt:message key="authorLastName.pattern"/></label>
                                <input type="text"  class="form-control" name="authorLastNamePattern" id="authorLastNamePattern"  value="${requestScope.authorLastNamePattern}"
                                <c:if test="${sessionScope.locale eq 'en'}">
                                       pattern="[A-Za-z'\-]{1,30}"
                                       title="<fmt:message key="authorLastNameEN.hint"/>"
                                </c:if>
                                <c:if test="${sessionScope.locale eq 'ua'}">
                                       pattern="[А-ЩЬЮЯҐІЇЄа-щьюяґіїє'\-]{1,30}"
                                       title="<fmt:message key="authorLastNameUA.hint"/>"
                                </c:if>
                                >

                                <br><br>
                                <div style="text-align: center">
                                    <button type="submit" class="btn" style="background-color: #d7f2ff"><fmt:message key="search.button"/></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <script>
                        var modal = document.getElementById("showSearchModal");
                        var btn = document.getElementById("searchButton");

                        btn.onclick = function() {
                            modal.style.display = "block";
                        }
                        window.onclick = function(event) {
                            if (event.target === modal) {
                                modal.style.display = "none";
                            }
                        }
                    </script>
                    <c:if test="${not empty requestScope.titlePattern or not empty requestScope.authorFirstNamePattern or not empty requestScope.authorLastNamePattern }">
                        <a href="books.jsp"><button class="btn btn-secondary" type="button" style="margin-right: 20px">
                            <fmt:message key="cancel.search.button"/></button></a>
                    </c:if>
                    <c:if test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Admin'}">
                        <a href="addBook.jsp" style="margin-left: auto;">
                            <button type="button" class="btn" style="background-color: #d7f2ff"><fmt:message key="add.book.button"/></button>
                        </a>
                    </c:if>
                </div>
            </div>
        </div>
        <br>

        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>

        <c:if test="${not empty requestScope.titlePattern or not empty requestScope.authorFirstNamePattern or not empty requestScope.authorLastNamePattern }">
            <div style="text-align: center">
                <span class="text-success">
                    <fmt:message key="search.results"/></span>
            </div>
        </c:if>
        <br>


        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive" style="position: relative;">
                    <table class="table mb-0" style="table-layout: fixed; width:100%">
                        <thead style="background-color: #d7f2ff ;">
                        <tr>
                            <th scope="col" style="width: 7%">ID</th>
                            <th scope="col"><fmt:message key="book.title"/></th>
                            <th scope="col"><fmt:message key="book.author"/></th>
                            <th scope="col" style="width: 11%"><fmt:message key="book.language"/></th>
                            <th scope="col" style="width: 11%"><fmt:message key="book.publication"/></th>
                            <th scope="col" style="width: 8%"><fmt:message key="book.publication.date"/></th>
                            <th scope="col" style="width: 8%"><fmt:message key="book.copies.available"/></th>
                            <c:if test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Reader'}">
                                <th scope="col" style="width: 9%; text-align: center"></th>
                            </c:if>
                            <c:if test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Admin'}">
                                <th scope="col" style="width: 8%; text-align: center"></th>
                                <th scope="col" style="width: 8%; text-align: center"></th>
                            </c:if>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach var="book" items="${requestScope.books}">
                            <tr>
                                <td><c:out value="${book.id}"/></td>
                            <c:choose>
                                <c:when test="${sessionScope.locale eq 'ua'}">
                                    <td><c:out value="${book.titleUA}"/></td>
                                    <td style="word-wrap:break-word;">
                                    <c:out value="${book.authorLastNameUA}"/>
                                    <c:out value="${book.authorFirstNameUA}"/>
                                    </td>
                                    <td><c:out value="${book.language}"/></td>
                                    <td><c:out value="${book.publicationNameUA}"/></td>
                                    <td><c:out value="${book.publicationDate}"/></td>
                                    <td style="text-align: center"><c:out value="${book.availableQuantity}"/></td>
                                </c:when>
                                <c:otherwise>
                                    <td><c:out value="${book.titleEN}"/></td>
                                    <td style="word-wrap:break-word;">
                                        <c:out value="${book.authorLastNameEN}"/>
                                        <c:out value="${book.authorFirstNameEN}"/>
                                    </td>
                                    <td><c:out value="${book.language}"/></td>
                                    <td><c:out value="${book.publicationNameEN}"/></td>
                                    <td><c:out value="${book.publicationDate}"/></td>
                                    <td style="text-align: center"><c:out value="${book.availableQuantity}"/></td>

                                </c:otherwise>
                            </c:choose>

                            <c:if test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Reader'}">
                                <td style="text-align: center">
                                    <form method="post" action="controller">
                                        <input type="hidden" name="command" value="goto-book-order-page">
                                        <input type="hidden" name="bookId" value="${book.id}">
                                        <input type="hidden" name="titleEN" value="${book.titleEN}">
                                        <input type="hidden" name="titleUA" value="${book.titleUA}">
                                        <input type="hidden" name="language" value="${book.language}">
                                        <input type="hidden" name="authorFirstNameEN" value="${book.authorFirstNameEN}">
                                        <input type="hidden" name="authorFirstNameUA" value="${book.authorFirstNameUA}">
                                        <input type="hidden" name="authorLastNameEN" value="${book.authorLastNameEN}">
                                        <input type="hidden" name="authorLastNameUA" value="${book.authorLastNameUA}">
                                        <input type="hidden" name="publicationNameEN" value="${book.publicationNameEN}">
                                        <input type="hidden" name="publicationNameUA" value="${book.publicationNameUA}">
                                        <input type="hidden" name="publicationDate" value="${book.publicationDate}">
                                        <c:choose>
                                            <c:when test="${book.availableQuantity eq 0}">
                                                <button type="submit" class="btn btn-light" disabled><fmt:message key="book.order.button"/></button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-light" ><fmt:message key="book.order.button"/></button>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </td>
                            </c:if>

                                <c:if test="${not empty sessionScope.loggedUser and sessionScope.loggedUser.role eq 'Admin'}">
                                    <td style="text-align: center">
                                        <form method="post" action="controller">
                                            <input type="hidden" name="command" value="update-book">
                                            <input type="hidden" name="bookId" value="${book.id}">
                                            <input type="hidden" name="titleEN" value="${book.titleEN}">
                                            <input type="hidden" name="titleUA" value="${book.titleUA}">
                                            <input type="hidden" name="language" value="${book.language}">
                                            <input type="hidden" name="authorId" value="${book.authorId}">
                                            <input type="hidden" name="publicationId" value="${book.publicationId}">
                                            <input type="hidden" name="publicationDate" value="${book.publicationDate}">
                                            <input type="hidden" name="copiesOwned" value="${book.copiesOwned}">
                                            <button type="submit" class="btn btn-secondary"><fmt:message key="book.edit.button"/></button>
                                        </form>
                                    </td>
                                    <td style="text-align: center">
                                        <form method="post" action="controller">
                                            <input type="hidden" name="command" value="delete-book">
                                            <input type="hidden" name="bookId" value="${book.id}">
                                            <button type="submit" class="btn btn-danger"><fmt:message key="book.delete.button"/></button>
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <c:if test="${not empty requestScope.lastPage}">
        <nav aria-label="...">
            <br><br>
            <ul class="pagination justify-content-center">
                <c:choose>
                    <c:when test="${requestScope.currentPage eq '1'}">
                    <li class="page-item disabled">
                    </c:when>
                    <c:otherwise>
                    <li class="page-item ">
                    </c:otherwise>
                </c:choose>

                    <c:choose>
                        <c:when test="${not empty requestScope.titlePattern or not empty requestScope.authorFirstNamePattern or not empty requestScope.authorLastNamePattern }">
                        <a class="page-link" href="controller?command=search-books&currentPage=${requestScope.currentPage-1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}&titlePattern=${requestScope.titlePattern}&authorFirstNamePattern=${requestScope.authorFirstNamePattern}&authorLastNamePattern=${requestScope.authorLastNamePattern}">
                            <fmt:message key="prev.button"/></a></li>
                        </c:when>
                        <c:otherwise>
                            <a class="page-link" href="controller?command=show-books&currentPage=${requestScope.currentPage-1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                                <fmt:message key="prev.button"/></a></li>
                        </c:otherwise>
                    </c:choose>

                <c:choose>
                    <c:when test="${requestScope.currentPage eq requestScope.lastPage}">
                    <li class="page-item disabled">
                    </c:when>
                    <c:otherwise>
                    <li class="page-item">
                    </c:otherwise>
                </c:choose>
                    <c:choose>
                        <c:when test="${not empty requestScope.titlePattern or not empty requestScope.authorFirstNamePattern or not empty requestScope.authorLastNamePattern }">
                        <a class="page-link" href="controller?command=search-books&currentPage=${requestScope.currentPage+1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}&titlePattern=${requestScope.titlePattern}&authorFirstNamePattern=${requestScope.authorFirstNamePattern}&authorLastNamePattern=${requestScope.authorLastNamePattern}">
                            <fmt:message key="next.button"/></a></li>
                        </c:when>
                        <c:otherwise>
                        <a class="page-link" href="controller?command=show-books&currentPage=${requestScope.currentPage+1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                            <fmt:message key="next.button"/></a></li>
                        </c:otherwise>
                    </c:choose>
            </ul>
        </nav>
        </c:if>

    </div>
</div>


</body>
</html>
