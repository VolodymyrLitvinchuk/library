<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="fines.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<c:set var="localeAction" value="fines.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role ne 'Reader'}">
    <c:redirect url="index.jsp"/>
</c:if>

<c:if test="${requestScope.fines eq null}">
    <c:redirect url="controller?command=show-fines&currentPage=1&sortType=byStatus&sortOrder=ascending&itemsPerPage=5&error=${requestScope.error}&message=${requestScope.message}"/>
</c:if>


<div class="mask d-flex align-items-center h-100">
    <div class="container">
        <div class="card" style="margin-left: auto; margin-right: auto">
            <div class="card-body" style="padding: 10px">
                <div>
                    <form method="get" action="controller">
                        <input type="hidden" name="command" value="show-fines">
                        <input type="hidden" name="currentPage" value="1">

                        <label class="pad-10" for="sortType"><fmt:message key="sort.by"/> </label>
                        <select onchange="this.form.submit()" name="sortType" id="sortType" class="form-select pad-10 w-auto d-inline-flex">
                            <option value="byStatus" <c:if test="${requestScope.sortType == 'byStatus'}"><c:out value="selected"/></c:if>>
                                <fmt:message key="by.status"/></option>
                            <option value="byOrderId" <c:if test="${requestScope.sortType == 'byOrderId'}"><c:out value="selected"/></c:if>>
                                <fmt:message key="by.order.id"/></option>
                            <option value="byAmount" <c:if test="${requestScope.sortType == 'byAmount'}"><c:out value="selected"/></c:if>>
                                <fmt:message key="by.amount"/></option>
                        </select>

                        <label class="pad-10" for="sortOrder"><fmt:message key="sort.order"/></label>
                        <select onchange="this.form.submit()" name="sortOrder" id="sortOrder" class="form-select pad-10 w-auto d-inline-flex">
                            <option value="ascending" <c:if test="${requestScope.sortOrder == 'ascending'}"><c:out value="selected"/></c:if>>
                                <fmt:message key="asc.order"/></option>
                            <option value="descending" <c:if test="${requestScope.sortOrder == 'descending'}"><c:out value="selected"/></c:if>>
                                <fmt:message key="desc.order"/></option>
                        </select>

                        <label  class="pad-10" for="itemsPerPage"><fmt:message key="items.per.page"/></label>
                        <select onchange="this.form.submit()" name="itemsPerPage" id="itemsPerPage" class="form-select pad-10 w-auto d-inline-flex">
                            <option value="5" <c:if test="${requestScope.itemsPerPage == '5'}"><c:out value="selected"/></c:if> >5</option>
                            <option value="10" <c:if test="${requestScope.itemsPerPage == '10'}"><c:out value="selected"/></c:if> >10</option>
                            <option value="15" <c:if test="${requestScope.itemsPerPage == '15'}"><c:out value="selected"/></c:if> >15</option>
                            <option value="1" <c:if test="${requestScope.itemsPerPage == '1'}"><c:out value="selected"/></c:if> >1</option>
                        </select>
                    </form>
                </div>

            </div>
        </div>
        <br>
        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
        <br>

        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive" style="position: relative;">
                    <table class="table mb-0" style="table-layout: fixed; width:100%">
                        <thead style="background-color: #d7f2ff ;">
                        <tr>
                            <th scope="col"><fmt:message key="fine.id"/></th>
                            <th scope="col"><fmt:message key="fine.order.id"/></th>
                            <th scope="col"><fmt:message key="fine.amount"/></th>
                            <th scope="col"><fmt:message key="fine.status"/></th>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach var="fine" items="${requestScope.fines}">
                            <tr style="<c:if test="${fine.statusId eq 1}"><c:out value="color: #808080"/></c:if>">
                                <td><c:out value="${fine.id}"/></td>
                                <td><c:out value="${fine.orderId}"/></td>
                                <td><c:out value="${fine.amount}"/></td>
                                <c:choose>
                                    <c:when test="${sessionScope.locale eq 'ua'}">
                                        <td><c:out value="${fine.statusUA}"/></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><c:out value="${fine.statusEN}"/></td>
                                    </c:otherwise>
                                </c:choose>

                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <c:if test="${not empty requestScope.lastPage}">
            <nav aria-label="...">
                <br><br>
                <ul class="pagination justify-content-center">
                    <c:choose>
                    <c:when test="${requestScope.currentPage eq '1'}">
                    <li class="page-item disabled">
                        </c:when>
                        <c:otherwise>
                    <li class="page-item ">
                        </c:otherwise>
                        </c:choose>
                        <a class="page-link" href="controller?command=show-fines&currentPage=${requestScope.currentPage-1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                            <fmt:message key="prev.button"/></a></li>

                    <c:choose>
                    <c:when test="${requestScope.currentPage eq requestScope.lastPage}">
                    <li class="page-item disabled">
                        </c:when>
                        <c:otherwise>
                    <li class="page-item">
                        </c:otherwise>
                        </c:choose>
                        <a class="page-link" href="controller?command=show-fines&currentPage=${requestScope.currentPage+1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                            <fmt:message key="next.button"/></a></li>
                </ul>
            </nav>
        </c:if>

    </div>
</div>


</body>
</html>