<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "ex" tagdir="/WEB-INF/tags" %>
<fmt:setLocale value="${sessionScope.locale}" scope="session"/>
<fmt:setBundle basename="library"/>

<!DOCTYPE html>
<html lang="${sessionScope.locale}">

<head>
    <title><fmt:message key="orders.title"/></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/my.css">
</head>

<body>
<c:set var="localeAction" value="orders.jsp"/>
<%@ include file="/jspf/header.jspf" %>
<br><br>
<c:if test="${sessionScope.loggedUser eq null or sessionScope.loggedUser.role eq 'Admin'}">
    <c:redirect url="books.jsp"/>
</c:if>
<c:if test="${requestScope.orders   eq null}">
    <c:redirect url="controller?command=show-orders&currentPage=1&sortType=byOrderStatus&sortOrder=ascending&itemsPerPage=5&error=${requestScope.error}&message=${requestScope.message}"/>
</c:if>


<div class="mask d-flex align-items-center h-100">
    <div style="width: 1500px; margin-right: auto; margin-left: auto;">
        <div class="card" style="width: 950px; margin-right: auto; margin-left: auto;">
            <div class="card-body" style="padding: 10px; text-align: center">
                <form method="get" action="controller">
                    <input type="hidden" name="command" value="show-orders">
                    <input type="hidden" name="currentPage" value="1">

                    <label class="pad-10" for="sortType"><fmt:message key="sort.by"/> </label>
                    <select onchange="this.form.submit()" name="sortType" id="sortType" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="byOrderStatus" <c:if test="${requestScope.sortType == 'byOrderStatus'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.status"/></option>
                        <option value="byId" <c:if test="${requestScope.sortType == 'byId'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.id"/></option>
                        <option value="byBookId" <c:if test="${requestScope.sortType == 'byBookId'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.book.id"/></option>
                        <option value="byReader" <c:if test="${requestScope.sortType == 'byReader'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.reader"/></option>
                        <option value="byOrderType" <c:if test="${requestScope.sortType == 'byOrderType'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.type"/></option>
                        <option value="byOrderDate" <c:if test="${requestScope.sortType == 'byOrderDate'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="by.date"/></option>
                    </select>

                    <label class="pad-10" for="sortOrder"><fmt:message key="sort.order"/></label>
                    <select onchange="this.form.submit()" name="sortOrder" id="sortOrder" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="ascending" <c:if test="${requestScope.sortOrder == 'ascending'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="asc.order"/></option>
                        <option value="descending" <c:if test="${requestScope.sortOrder == 'descending'}"><c:out value="selected"/></c:if>>
                            <fmt:message key="desc.order"/></option>
                    </select>

                    <label class="pad-10" for="itemsPerPage"><fmt:message key="items.per.page"/></label>
                    <select onchange="this.form.submit()" name="itemsPerPage" id="itemsPerPage" class="form-select pad-10 w-auto d-inline-flex">
                        <option value="5" <c:if test="${requestScope.itemsPerPage == '5'}"><c:out value="selected"/></c:if> >5</option>
                        <option value="10" <c:if test="${requestScope.itemsPerPage == '10'}"><c:out value="selected"/></c:if> >10</option>
                        <option value="15" <c:if test="${requestScope.itemsPerPage == '15'}"><c:out value="selected"/></c:if> >15</option>
                    </select>

                </form>
            </div>
        </div>
        <br>
        <ex:Message message="${requestScope.message}" locale="${sessionScope.locale}"/>
        <ex:Error error="${requestScope.error}" locale="${sessionScope.locale}"/>
        <br>


        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive" style="position: relative;">
                    <table class="table mb-0" style="table-layout: fixed; width:100%">
                        <thead style="background-color: #d7f2ff ;">
                        <tr>
                            <th scope="col" style="width: 4%">ID</th>
                            <th scope="col" style="width: 8%"><fmt:message key="book.id"/></th>
                            <c:if test="${sessionScope.loggedUser.role eq 'Librarian'}">
                            <th scope="col" style="width: 11%"><fmt:message key="reader.login"/></th>
                            </c:if>
                            <th scope="col" style="width: 15%"><fmt:message key="book.order.type"/></th>
                            <th scope="col" style="width: 10%"><fmt:message key="book.order.date"/></th>
                            <th scope="col" style="width: 12%"><fmt:message key="book.order.date.return"/></th>
                            <th scope="col" style="width: 26%"><fmt:message key="book.order.status"/></th>
                            <th scope="col" style="width: 6%"><fmt:message key="book.order.fineAmount"/></th>
                            <c:choose>
                                <c:when test="${sessionScope.loggedUser.role eq 'Librarian'}">
                                    <th scope="col" style="width: 15%"></th>
                                </c:when>
                                <c:when test="${sessionScope.loggedUser.role eq 'Reader'}">
                                    <th scope="col" style="width: 10%"></th>
                                </c:when>
                            </c:choose>
                        </tr>
                        </thead>

                        <tbody>
                        <c:forEach var="order" items="${requestScope.orders}">
                            <tr style="<c:if test="${order.orderStatusID eq 3 or order.orderStatusID eq 4}"><c:out value="color: #808080"/></c:if>">
                                <td><c:out value="${order.id}"/></td>
                                <td><a  href="controller?command=get-book-by-id&bookId=<c:out value="${order.bookID}"/>" style="<c:if test="${order.orderStatusID eq 3 or order.orderStatusID eq 4}"><c:out value="color: #808080"/></c:if>">
                                    <c:out value="${order.bookID}"/> </a></td>
                                <c:if test="${sessionScope.loggedUser.role eq 'Librarian'}">
                                    <td>
                                        <a href="#" id="myBtn<c:out value="${order.id}"/>">
                                            <c:out value="${order.readerLogin}"/></a>
                                        <div id="showReaderInfo<c:out value="${order.id}"/>" class="modal">
                                            <div class="modal-content">
                                                <p style="text-align: center; font-weight: bold"><fmt:message key="inf.about.reader"/> ${order.readerLogin}</p>
                                                <br>
                                                <p><fmt:message key="quantity.active.orders"/> - ${order.readerDTO.activeOrders}</p>
                                                <p><fmt:message key="quantity.finished.orders"/> - ${order.readerDTO.finishedOrders}</p>
                                                <p><fmt:message key="quantity.canceled.orders"/> - ${order.readerDTO.canceledOrders}</p>
                                                <p><fmt:message key="quantity.unpaid.fines"/> - ${order.readerDTO.unPaidFines}</p>
                                                <p><fmt:message key="quantity.paid.fines"/> - ${order.readerDTO.paidFines}</p>
                                            </div>
                                        </div>
                                        <script>
                                            var modal = document.getElementById("showReaderInfo<c:out value="${order.id}"/>");
                                            var btn = document.getElementById("myBtn<c:out value="${order.id}"/>");

                                            btn.onclick = function() {
                                                modal.style.display = "block";
                                            }
                                            window.onclick = function(event) {
                                                if (event.target === modal) {
                                                    modal.style.display = "none";
                                                }
                                            }
                                        </script>
                                    </td>
                                </c:if>
                                <c:choose>
                                    <c:when test="${sessionScope.locale eq 'ua'}">
                                        <td><c:out value="${order.orderTypeUA}"/></td>
                                        <td><c:out value="${order.orderDate}"/></td>
                                        <td><c:out value="${order.returnedDatePlanned}"/></td>
                                        <td><c:out value="${order.orderStatusUA}"/></td>
                                        <td style="color: red" >
                                            <c:if test="${order.fineAmount ne 0}">
                                            <c:out value="${order.fineAmount}"/>$</c:if></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><c:out value="${order.orderTypeEN}"/></td>
                                        <td><c:out value="${order.orderDate}"/></td>
                                        <td><c:out value="${order.returnedDatePlanned}"/></td>
                                        <td><c:out value="${order.orderStatusEN}"/></td>
                                        <td style="color: red">
                                            <c:if test="${order.fineAmount ne 0}">
                                            <c:out value="${order.fineAmount}"/>$</c:if></td>
                                    </c:otherwise>
                                </c:choose>

                                <c:choose>
                                    <c:when test="${sessionScope.loggedUser.role eq 'Librarian' and order.orderStatusID eq 0}">
                                        <td style="text-align: center; display: flex">
                                            <form method="post" action="controller">
                                                <input type="hidden" name="command" value="issue-book">
                                                <input type="hidden" name="bookOrderId" value="${order.id}">
                                                <button type="submit" class="btn btn-secondary"><fmt:message key="issue.book.button"/></button>
                                            </form>
                                            <form method="post" action="controller" style="margin-left: 5px">
                                                <input type="hidden" name="command" value="cancel-order">
                                                <input type="hidden" name="bookOrderId" value="${order.id}">
                                                <button type="submit" class="btn btn-danger"><fmt:message key="cancel.button"/></button>
                                            </form>
                                        </td>
                                    </c:when>
                                    <c:when test="${sessionScope.loggedUser.role eq 'Librarian' and order.orderStatusID eq 2 and order.fineAmount eq 0}">
                                        <td style="text-align: center">
                                            <form method="post" action="controller">
                                                <input type="hidden" name="command" value="return-book-librarian">
                                                <input type="hidden" name="bookOrderId" value="${order.id}">
                                                <button type="submit" class="btn btn-secondary"><fmt:message key="return.book.button"/></button>
                                            </form>
                                        </td>
                                    </c:when>
                                    <c:when test="${sessionScope.loggedUser.role eq 'Reader' and order.orderStatusID eq 1 and order.fineAmount eq 0}">
                                        <td style="text-align: center">
                                            <form method="post" action="controller">
                                                <input type="hidden" name="command" value="return-book-reader">
                                                <input type="hidden" name="bookOrderId" value="${order.id}">
                                                <button type="submit" class="btn btn-secondary"><fmt:message key="return.book.button"/></button>
                                            </form>
                                        </td>
                                    </c:when>
                                    <c:when test="${sessionScope.loggedUser.role eq 'Reader' and order.orderStatusID eq 1 and order.fineAmount ne 0}">
                                        <td style="text-align: center">
                                            <form method="post" action="controller">
                                                <input type="hidden" name="command" value="pay-fine">
                                                <input type="hidden" name="bookOrderId" value="${order.id}">
                                                <button type="submit" class="btn btn-danger"><fmt:message key="paid.fine.button"/></button>
                                            </form>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td style="text-align: center">
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <c:if test="${not empty requestScope.lastPage}">
            <nav aria-label="...">
                <br><br>
                <ul class="pagination justify-content-center">
                    <c:choose>
                    <c:when test="${requestScope.currentPage eq '1'}">
                    <li class="page-item disabled">
                        </c:when>
                        <c:otherwise>
                    <li class="page-item ">
                        </c:otherwise>
                        </c:choose>
                        <a class="page-link" href="controller?command=show-orders&currentPage=${requestScope.currentPage-1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                            <fmt:message key="prev.button"/></a></li>

                    <c:choose>
                    <c:when test="${requestScope.currentPage eq requestScope.lastPage}">
                    <li class="page-item disabled">
                        </c:when>
                        <c:otherwise>
                    <li class="page-item">
                        </c:otherwise>
                        </c:choose>
                        <a class="page-link"
                           href="controller?command=show-orders&currentPage=${requestScope.currentPage+1}&sortType=${requestScope.sortType}&sortOrder=${requestScope.sortOrder}&itemsPerPage=${requestScope.itemsPerPage}">
                            <fmt:message key="next.button"/></a></li>
                </ul>
            </nav>
        </c:if>

    </div>
</div>


</body>
</html>
