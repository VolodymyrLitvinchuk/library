package com.my.library.dao.connection;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataSource {

    private static final HikariConfig config = new HikariConfig();
    private static final HikariDataSource ds;

    static {
        Properties properties = getProperties();
        config.setJdbcUrl(properties.getProperty(Constants.URL_PROPERTY));
        config.setUsername(properties.getProperty(Constants.USER_NAME));
        config.setPassword(properties.getProperty(Constants.PASSWORD));
        config.setDriverClassName(properties.getProperty(Constants.DRIVER));
        config.addDataSourceProperty(Constants.CON_TIMEOUT, properties.getProperty(Constants.CON_TIMEOUT));
        config.addDataSourceProperty(Constants.MAX_POOL_SIZE, properties.getProperty(Constants.MAX_POOL_SIZE));
        config.addDataSourceProperty(Constants.CACHE_PREPARED_STATEMENT, properties.getProperty(Constants.CACHE_PREPARED_STATEMENT));
        config.addDataSourceProperty(Constants.CACHE_SIZE, properties.getProperty(Constants.CACHE_SIZE));
        config.addDataSourceProperty(Constants.CACHE_SQL_LIMIT, properties.getProperty(Constants.CACHE_SQL_LIMIT));
        ds = new HikariDataSource( config );
    }

    private static Properties getProperties() {
        Properties properties = new Properties();
        String connectionFile = "db-connection.properties";
        try (InputStream resource = DataSource.class.getClassLoader().getResourceAsStream(connectionFile)){
            properties.load(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
    private DataSource() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
