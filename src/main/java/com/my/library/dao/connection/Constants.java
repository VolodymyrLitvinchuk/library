package com.my.library.dao.connection;

public final class Constants {
    private Constants(){}

    public static final String URL_PROPERTY = "connection.url";
    public static final String USER_NAME = "user.name";
    public static final String PASSWORD = "password";
    public static final String CON_TIMEOUT = "connectionTimeout";
    public static final String MAX_POOL_SIZE = "maximum-pool-size";
    public static final String CACHE_PREPARED_STATEMENT = "cachePrepStmts";
    public static final String CACHE_SIZE = "prepStmtCacheSize";
    public static final String CACHE_SQL_LIMIT = "prepStmtCacheSqlLimit";
    public static final String DRIVER = "driver";
}
