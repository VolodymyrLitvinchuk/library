package com.my.library.dao.orderByEnums;

import static com.my.library.dao.mysql.Constant.*;

public enum BookOrderOrderBy {
    ID(BOOK_ORDER_ID_COLUMN, false){},
    BOOK_ID(BOOK_ORDER_BOOK_ID_COLUMN, false){},
    READER_LOGIN(BOOK_ORDER_READER_LOGIN_COLUMN, false){},
    TYPE(BOOK_ORDER_TYPE_COLUMN, false){},
    DATE(BOOK_ORDER_DATE_COLUMN, false){},
    RETURNED_DATE(BOOK_ORDER_RETURNED_DATE_PLANNED_COLUMN, false){},
    STATUS(BOOK_ORDER_STATUS_COLUMN, false){},

    ID_DESC(BOOK_ORDER_ID_COLUMN, true){},
    BOOK_ID_DESC(BOOK_ORDER_BOOK_ID_COLUMN, true){},
    READER_LOGIN_DESC(BOOK_ORDER_READER_LOGIN_COLUMN, true){},
    TYPE_DESC(BOOK_ORDER_TYPE_COLUMN, true){},
    DATE_DESC(BOOK_ORDER_DATE_COLUMN, true){},
    RETURNED_DATE_DESC(BOOK_ORDER_RETURNED_DATE_PLANNED_COLUMN, true){},
    STATUS_DESC(BOOK_ORDER_STATUS_COLUMN, true){};


    private final String value;

    BookOrderOrderBy(String value, boolean desc){
        if(desc) this.value = value + " desc";
        else this.value = value;
    }

    public String getValue() {
        return value;
    }
}
