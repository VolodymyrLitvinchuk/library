package com.my.library.dao.orderByEnums;

import static com.my.library.dao.mysql.Constant.*;

public enum AuthorOrderBy {
    ID(AUTHOR_ID_COLUMN, false){},
    FIRST_NAME_EN(AUTHOR_FIRST_NAME_EN_COLUMN, false),
    FIRST_NAME_UA(AUTHOR_FIRST_NAME_UA_COLUMN, false),
    LAST_NAME_EN(AUTHOR_LAST_NAME_EN_COLUMN, false),
    LAST_NAME_UA(AUTHOR_LAST_NAME_UA_COLUMN, false),

    ID_DESC(AUTHOR_ID_COLUMN, true){},
    FIRST_NAME_EN_DESC(AUTHOR_FIRST_NAME_EN_COLUMN, true),
    FIRST_NAME_UA_DESC(AUTHOR_FIRST_NAME_UA_COLUMN, true),
    LAST_NAME_EN_DESC(AUTHOR_LAST_NAME_EN_COLUMN, true),
    LAST_NAME_UA_DESC(AUTHOR_LAST_NAME_UA_COLUMN, true);


    private final String value;

    AuthorOrderBy(String value, boolean desc){
        if(desc) this.value = value + " desc";
        else this.value = value;
    }

    public String getValue() {
        return value;
    }
}
