package com.my.library.dao.orderByEnums;

import static com.my.library.dao.mysql.Constant.*;

public enum UserOrderBy {
    LOGIN(USER_LOGIN_COLUMN, false){},
    EMAIL(USER_EMAIL_COLUMN, false){},
    FIRST_NAME(USER_FIRST_NAME_COLUMN, false){},
    LAST_NAME(USER_LAST_NAME_COLUMN, false){},

    LOGIN_DESC(USER_LOGIN_COLUMN, true){},
    EMAIL_DESC(USER_EMAIL_COLUMN, true){},
    FIRST_NAME_DESC(USER_FIRST_NAME_COLUMN, true){},
    LAST_NAME_DESC(USER_LAST_NAME_COLUMN, true){};


    private final String value;

    UserOrderBy(String value, boolean desc){
        if(desc) this.value = value + " desc";
        else this.value = value;
    }

    public String getValue() {
        return value;
    }
}
