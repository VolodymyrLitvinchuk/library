package com.my.library.dao.orderByEnums;

import static com.my.library.dao.mysql.Constant.*;

public enum FineOrderBy {
    ORDER_ID(FINE_ORDER_ID_COLUMN, false),
    FINE_AMOUNT(FINE_AMOUNT_COLUMN, false),
    FINE_STATUS(FINE_STATUS_COLUMN, false),

    ORDER_ID_DESC(FINE_ORDER_ID_COLUMN, true),
    FINE_AMOUNT_DESC(FINE_AMOUNT_COLUMN, true),
    FINE_STATUS_DESC(FINE_STATUS_COLUMN, true);


    private final String value;

    FineOrderBy(String value, boolean desc){
        if(desc) this.value = value + " desc";
        else this.value = value;
    }

    public String getValue() {
        return value;
    }
}
