package com.my.library.dao.orderByEnums;

import static com.my.library.dao.mysql.Constant.*;

public enum BookOrderBy {
    BOOK_TITLE_EN(BOOK_TITLE_EN_COLUMN, false){},
    BOOK_TITLE_UA(BOOK_TITLE_UA_COLUMN, false){},

    AUTHOR_LAST_NAME_EN(AUTHOR_LAST_NAME_EN_COLUMN, false){},
    AUTHOR_LAST_NAME_UA(AUTHOR_LAST_NAME_UA_COLUMN, false){},

    PUBLICATION_NAME_EN(PUBLICATION_NAME_EN_COLUMN, false){},
    PUBLICATION_NAME_UA(PUBLICATION_NAME_UA_COLUMN, false){},

    PUBLICATION_DATE(PUBLICATION_DATE_COLUMN, false){},

    BOOK_TITLE_EN_DESC(BOOK_TITLE_EN_COLUMN, true){},
    BOOK_TITLE_UA_DESC(BOOK_TITLE_UA_COLUMN, true){},

    AUTHOR_LAST_NAME_EN_DESC(AUTHOR_LAST_NAME_EN_COLUMN, true){},
    AUTHOR_LAST_NAME_UA_DESC(AUTHOR_LAST_NAME_UA_COLUMN, true){},

    PUBLICATION_NAME_EN_DESC(PUBLICATION_NAME_EN_COLUMN, true){},
    PUBLICATION_NAME_UA_DESC(PUBLICATION_NAME_UA_COLUMN, true){},

    PUBLICATION_DATE_DESC(PUBLICATION_DATE_COLUMN, true){};

    private final String value;

    BookOrderBy(String value, boolean desc){
        if(desc) this.value = value + " desc";
        else this.value = value;
    }

    public String getValue() {
        return value;
    }
}
