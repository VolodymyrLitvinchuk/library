package com.my.library.dao.entity.fine;

import java.sql.Date;
import java.util.Objects;

public class FinePayment {
    private Integer id;
    private final Integer fineIdFK;
    private Date date;

    public FinePayment(Integer fineIdFK, Date date) {
        this.fineIdFK = fineIdFK;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinePayment that = (FinePayment) o;
        return Objects.equals(id, that.id) && fineIdFK.equals(that.fineIdFK) && date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fineIdFK, date);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFineIdFK() {
        return fineIdFK;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
