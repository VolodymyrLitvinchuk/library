package com.my.library.dao.entity.bookOrder;

import java.sql.Date;
import java.util.Objects;

public class BookOrder {
    private Integer id;
    private final Integer bookIdFK;
    private final String readerFK;
    private BookOrderType type;
    private final Date date;
    private final Date dateReturnPlanned;
    private BookOrderStatus status;

    // id will be assigned after insertion
    public BookOrder(Integer bookIdFK,
                     String readerFK,
                     BookOrderType type,
                     Date date,
                     Date dateReturnPlanned,
                     BookOrderStatus status) {
        this.bookIdFK = bookIdFK;
        this.readerFK = readerFK;
        this.type = type;
        this.date = date;
        this.dateReturnPlanned = dateReturnPlanned;
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookOrder bookOrder = (BookOrder) o;
        return Objects.equals(id, bookOrder.id)
                && Objects.equals(bookIdFK, bookOrder.bookIdFK)
                && Objects.equals(readerFK, bookOrder.readerFK)
                && Objects.equals(type, bookOrder.type)
                && Objects.equals(date, bookOrder.date)
                && Objects.equals(dateReturnPlanned, bookOrder.dateReturnPlanned)
                && Objects.equals(status, bookOrder.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bookIdFK, readerFK, type, date, dateReturnPlanned, status);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookIdFK() {
        return bookIdFK;
    }

    public String getReaderFK() {
        return readerFK;
    }


    public BookOrderType getType() {
        return type;
    }

    public void setType(BookOrderType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public Date getDateReturnPlanned() {
        return dateReturnPlanned;
    }

    public BookOrderStatus getStatus() {
        return status;
    }

    public void setStatus(BookOrderStatus status) {
        this.status = status;
    }
}
