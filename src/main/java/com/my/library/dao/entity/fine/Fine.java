package com.my.library.dao.entity.fine;

import java.util.Objects;

public class Fine {
    private Integer id;
    private final Integer orderFK;
    private int amount;
    private FineStatus status;

    // after successful inserting id and status will be assigned
    public Fine(Integer orderFK, int amount) {
        this.orderFK = orderFK;
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fine fine = (Fine) o;
        return amount == fine.amount
                && Objects.equals(id, fine.id)
                && Objects.equals(orderFK, fine.orderFK)
                && Objects.equals(status, fine.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderFK, amount, status);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderFK() {
        return orderFK;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public FineStatus getStatus() {
        return status;
    }

    public void setStatus(FineStatus status) {
        this.status = status;
    }
}
