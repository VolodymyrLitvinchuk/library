package com.my.library.dao.entity.builder;

import com.my.library.dao.entity.book.Author;

public class AuthorBuilderImpl implements AuthorBuilder{
    private String firstNameEN;
    private String firstNameUA;
    private String lastNameEN;
    private String lastNameUA;


    /**
     * @param firstNameEN Author firstNameEN
     * @return this AuthorBuilder
     */
    @Override
    public AuthorBuilder setFirstNameEN(String firstNameEN) {
        this.firstNameEN = firstNameEN;
        return this;
    }

    /**
     * @param firstNameUA Author firstNameUA
     * @return this AuthorBuilder
     */
    @Override
    public AuthorBuilder setFirstNameUA(String firstNameUA) {
        this.firstNameUA = firstNameUA;
        return this;
    }

    /**
     * @param lastNameEN Author lastNameEN
     * @return this AuthorBuilder
     */
    @Override
    public AuthorBuilder setLastNameEN(String lastNameEN) {
        this.lastNameEN = lastNameEN;
        return this;
    }

    /**
     * @param lastNameUA  Author lastNameUA
     * @return this AuthorBuilder
     */
    @Override
    public AuthorBuilder setLastNameUA(String lastNameUA) {
        this.lastNameUA = lastNameUA;
        return this;
    }

    /**
     * @return Author object or null if object is not valid
     */
    @Override
    public Author get() {
        Author author = new Author(firstNameEN,firstNameUA,lastNameEN,lastNameUA);
        if(author.isValid()) return author;
        return null;
    }
}
