package com.my.library.dao.entity.bookOrder;

import java.util.Objects;

public class BookOrderStatus {
    private Integer id;
    private String valueEN;
    private String valueUA;

    public BookOrderStatus(Integer id, String valueEN, String valueUA) {
        this.id = id;
        this.valueEN = valueEN;
        this.valueUA = valueUA;
    }

    public boolean isValid(){
        return this.id!=null
                && this.valueEN!=null
                && this.valueUA!=null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookOrderStatus that = (BookOrderStatus) o;
        return Objects.equals(id, that.id) && valueEN.equals(that.valueEN) && valueUA.equals(that.valueUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, valueEN, valueUA);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValueEN() {
        return valueEN;
    }

    public void setValueEN(String valueEN) {
        this.valueEN = valueEN;
    }

    public String getValueUA() {
        return valueUA;
    }

    public void setValueUA(String valueUA) {
        this.valueUA = valueUA;
    }
}
