package com.my.library.dao.entity.builder;

import com.my.library.dao.entity.book.Author;

public interface AuthorBuilder {

    AuthorBuilder setFirstNameEN(String firstNameEN);

    AuthorBuilder setFirstNameUA(String firstNameUA);

    AuthorBuilder setLastNameEN(String lastNameEN);

    AuthorBuilder setLastNameUA(String lastNameUA);

    Author get();
}
