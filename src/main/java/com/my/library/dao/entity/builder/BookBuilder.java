package com.my.library.dao.entity.builder;

import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.entity.book.Author;

import java.sql.Date;

public interface BookBuilder {
    BookBuilder setTitle_EN(String title_EN);
    BookBuilder setTitle_UA(String title_UA);
    BookBuilder setLanguage(String language);
    BookBuilder setAuthor(Author author);
    BookBuilder setPublication(Publication publication);
    BookBuilder setPublication_Date(Date publication_Date);
    BookBuilder setCopiesOwned(int copiesOwned);
    Book get();
}
