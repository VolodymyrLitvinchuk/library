package com.my.library.dao.entity.book;

import java.sql.Date;
import java.util.Objects;

public class Book {
    private Integer id;
    private String title_EN;
    private String title_UA;
    private String language;

    private Author author;

    private Publication publication;
    private Date publication_Date;

    private Integer copiesOwned;

    public Book(String title_EN,
                String title_UA,
                String language,
                Author author,
                Publication publication,
                Date publication_Date,
                Integer copiesOwned) {
        this.title_EN = title_EN;
        this.title_UA = title_UA;
        this.language = language;
        this.author = author;
        this.publication = publication;
        this.publication_Date = publication_Date;
        this.copiesOwned = copiesOwned;
    }
    public boolean isValid(){
        return this.title_EN!=null && !this.title_EN.isBlank()
                && this.title_UA!=null && !this.title_UA.isBlank()
                && this.language!=null
                && this.author!=null
                && this.publication!=null
                && this.publication_Date!=null
                && this.copiesOwned!=null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id)
                && Objects.equals(title_EN, book.title_EN)
                && Objects.equals(title_UA, book.title_UA)
                && Objects.equals(language, book.language)
                && Objects.equals(author, book.author)
                && Objects.equals(publication, book.publication)
                && Objects.equals(publication_Date, book.publication_Date)
                && Objects.equals(copiesOwned, book.copiesOwned);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                title_EN,
                title_UA,
                language,
                author,
                publication,
                publication_Date,
                copiesOwned);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle_EN() {
        return title_EN;
    }

    public void setTitle_EN(String title_EN) {
        this.title_EN = title_EN;
    }

    public String getTitle_UA() {
        return title_UA;
    }

    public void setTitle_UA(String title_UA) {
        this.title_UA = title_UA;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Date getPublication_Date() {
        return publication_Date;
    }

    public void setPublication_Date(Date publication_Date) {
        this.publication_Date = publication_Date;
    }

    public Integer getCopiesOwned() {
        return copiesOwned;
    }

    public void setCopiesOwned(Integer copiesOwned) {
        this.copiesOwned = copiesOwned;
    }
}
