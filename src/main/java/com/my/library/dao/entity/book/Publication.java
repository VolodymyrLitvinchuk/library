package com.my.library.dao.entity.book;

import java.util.Objects;

public class Publication {
    private Integer id;
    private String nameEN;
    private String nameUA;

    public Publication(String nameEN, String nameUA) {
        this.id = null;
        this.nameEN = nameEN;
        this.nameUA = nameUA;
    }

    public Publication(Integer id, String nameEN, String nameUA) {
        this.id = id;
        this.nameEN = nameEN;
        this.nameUA = nameUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return Objects.equals(id, that.id) && nameEN.equals(that.nameEN) && nameUA.equals(that.nameUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameEN, nameUA);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameUA() {
        return nameUA;
    }

    public void setNameUA(String nameUA) {
        this.nameUA = nameUA;
    }
}
