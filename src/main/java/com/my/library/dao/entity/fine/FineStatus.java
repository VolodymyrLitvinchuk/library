package com.my.library.dao.entity.fine;

import java.util.Objects;

public class FineStatus {
    private Integer id;
    private String valueEN;
    private String valueUA;

    public FineStatus(Integer id, String valueEN, String valueUA) {
        this.id = id;
        this.valueEN = valueEN;
        this.valueUA = valueUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FineStatus that = (FineStatus) o;
        return id.equals(that.id) && valueEN.equals(that.valueEN) && valueUA.equals(that.valueUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, valueEN, valueUA);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValueEN() {
        return valueEN;
    }

    public void setValueEN(String valueEN) {
        this.valueEN = valueEN;
    }

    public String getValueUA() {
        return valueUA;
    }

    public void setValueUA(String valueUA) {
        this.valueUA = valueUA;
    }
}
