package com.my.library.dao.entity.book;

import java.util.Objects;

public class Author {
    private Integer id;
    private String firstNameEN;
    private String firstNameUA;
    private String lastNameEN;
    private String lastNameUA;

    public Author(String firstNameEN, String firstNameUA, String lastNameEN, String lastNameUA) {
        this.id = null;
        this.firstNameEN = firstNameEN;
        this.firstNameUA = firstNameUA;
        this.lastNameEN = lastNameEN;
        this.lastNameUA = lastNameUA;
    }

    public Author(Integer id, String firstNameEN, String firstNameUA, String lastNameEN, String lastNameUA) {
        this.id = id;
        this.firstNameEN = firstNameEN;
        this.firstNameUA = firstNameUA;
        this.lastNameEN = lastNameEN;
        this.lastNameUA = lastNameUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id)
                && firstNameEN.equals(author.firstNameEN)
                && firstNameUA.equals(author.firstNameUA)
                && lastNameEN.equals(author.lastNameEN)
                && lastNameUA.equals(author.lastNameUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstNameEN, firstNameUA, lastNameEN, lastNameUA);
    }

    public boolean isValid() {
        return firstNameEN != null && !firstNameEN.isBlank()
                && firstNameUA != null && !firstNameUA.isBlank()
                && lastNameEN != null && !lastNameEN.isBlank()
                && lastNameUA != null && !lastNameUA.isBlank();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstNameEN() {
        return firstNameEN;
    }

    public void setFirstNameEN(String firstNameEN) {
        this.firstNameEN = firstNameEN;
    }

    public String getFirstNameUA() {
        return firstNameUA;
    }

    public void setFirstNameUA(String firstNameUA) {
        this.firstNameUA = firstNameUA;
    }

    public String getLastNameEN() {
        return lastNameEN;
    }

    public void setLastNameEN(String lastNameEN) {
        this.lastNameEN = lastNameEN;
    }

    public String getLastNameUA() {
        return lastNameUA;
    }

    public void setLastNameUA(String lastNameUA) {
        this.lastNameUA = lastNameUA;
    }
}
