package com.my.library.dao.entity.builder;

import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Publication;

import java.sql.Date;

public class BookBuilderImpl implements BookBuilder{
    private String title_EN;
    private String title_UA;
    private String language;
    private Author author;
    private Publication publication;
    private Date publication_Date;
    private Integer copiesOwned;


    @Override
    public BookBuilder setTitle_EN(String title_EN) {
        this.title_EN = title_EN;
        return this;
    }

    @Override
    public BookBuilder setTitle_UA(String title_UA) {
        this.title_UA = title_UA;
        return this;
    }

    @Override
    public BookBuilder setLanguage(String language) {
        this.language = language;
        return this;
    }

    @Override
    public BookBuilder setAuthor(Author author) {
        this.author = author;
        return this;
    }

    @Override
    public BookBuilder setPublication(Publication publication) {
        this.publication = publication;
        return this;
    }

    @Override
    public BookBuilder setPublication_Date(Date publication_Date) {
        this.publication_Date = publication_Date;
        return this;
    }

    @Override
    public BookBuilder setCopiesOwned(int copiesOwned) {
        this.copiesOwned = copiesOwned;
        return this;
    }

    @Override
    public Book get() {
        Book book = new Book(
                title_EN,
                title_UA,
                language,
                author,
                publication,
                publication_Date,
                copiesOwned
        );
        if(book.isValid()) return book;
        return null;
    }
}
