package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.abstr.bookOrder.BookOrderStatusDAO;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlBookOrderStatusDAO implements BookOrderStatusDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlBookOrderStatusDAO.class);

    private BookOrderStatus mapOrderStatus(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            String valueEN = rs.getString(2);
            String valueUA = rs.getString(3);
            return new BookOrderStatus(id,valueEN,valueUA);

        }catch (SQLException ex){
            log.warn("Exception while mapping BookOrderStatus",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<BookOrderStatus> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order_Status WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                BookOrderStatus orderStatus = mapOrderStatus(rs);
                rs.close();
                return Optional.of(orderStatus);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book_Order_Status with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrderStatus>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order_Status")){
            ResultSet rs = statement.executeQuery();

            List<BookOrderStatus> list = new ArrayList<>();
            while(rs.next()){
                list.add(mapOrderStatus(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Book_Order_Status",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(BookOrderStatus orderStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Book_Order_Status VALUES (?,?,?)")){
            statement.setInt(1,orderStatus.getId());
            statement.setString(2, orderStatus.getValueEN());
            statement.setString(3, orderStatus.getValueUA());

            if(statement.executeUpdate()>0){
                log.info("New Book_Order_Status was inserted. Id={}",orderStatus.getId());
                return true;
            }
            return false;

        }catch (SQLException ex){
            log.warn("Exception while inserting new Book_Order_Status",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateEN(BookOrderStatus orderStatus, String newValueEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book_Order_Status SET status_value_EN=? WHERE id=?")){
            statement.setString(1,newValueEN);
            statement.setInt(2,orderStatus.getId());

            if(statement.executeUpdate()>0){
                orderStatus.setValueEN(newValueEN);
                log.info("Book_Order_Status valueEN was updated. Id={}",orderStatus.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book_Order_Status valueEN. Id={}",orderStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateUA(BookOrderStatus orderStatus, String newValueUA) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book_Order_Status SET status_value_UA=? WHERE id=?")){
            statement.setString(1,newValueUA);
            statement.setInt(2,orderStatus.getId());

            if(statement.executeUpdate()>0){
                orderStatus.setValueUA(newValueUA);
                log.info("Book_Order_Status valueUA was updated. Id={}",orderStatus.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book_Order_Status valueUA. Id={}",orderStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(BookOrderStatus orderStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Book_Order_Status WHERE id=?")){
            statement.setInt(1,orderStatus.getId());

            if(statement.executeUpdate()>0){
                log.info("Book_Order_Status was deleted. Id={}",orderStatus.getId());
                orderStatus.setId(null);
                orderStatus.setValueEN(null);
                orderStatus.setValueUA(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Book_Order_Status. Id={}",orderStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
