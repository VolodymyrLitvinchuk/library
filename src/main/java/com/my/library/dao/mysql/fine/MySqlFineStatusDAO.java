package com.my.library.dao.mysql.fine;

import com.my.library.dao.abstr.fine.FineStatusDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlFineStatusDAO implements FineStatusDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlFineStatusDAO.class);

    private FineStatus mapOrderStatus(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            String valueEN = rs.getString(2);
            String valueUA = rs.getString(3);
            return new FineStatus(id,valueEN,valueUA);

        }catch (SQLException ex){
            log.warn("Exception while mapping FineStatus",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<FineStatus> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine_Status WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                FineStatus fineStatus = mapOrderStatus(rs);
                rs.close();
                return Optional.of(fineStatus);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Fine_Status with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<FineStatus>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine_Status")){
            ResultSet rs = statement.executeQuery();

            List<FineStatus> list = new ArrayList<>();
            while(rs.next()){
                list.add(mapOrderStatus(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Fine_Status",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(FineStatus fineStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Fine_Status VALUES (?,?,?)")){
            statement.setInt(1,fineStatus.getId());
            statement.setString(2, fineStatus.getValueEN());
            statement.setString(3, fineStatus.getValueUA());

            if(statement.executeUpdate()>0){
                log.info("New Fine_Status was inserted. Id={}",fineStatus.getId());
                return true;
            }
            return false;

        }catch (SQLException ex){
            log.warn("Exception while inserting new Fine_Status",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateEN(FineStatus fineStatus, String newValueEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Fine_Status SET status_value_EN=? WHERE id=?")){
            statement.setString(1,newValueEN);
            statement.setInt(2,fineStatus.getId());

            if(statement.executeUpdate()>0){
                fineStatus.setValueEN(newValueEN);
                log.info("Fine_Status valueEN was updated. Id={}",fineStatus.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Fine_Status valueEN. Id={}",fineStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateUA(FineStatus fineStatus, String newValueUA) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Fine_Status SET status_value_UA=? WHERE id=?")){
            statement.setString(1,newValueUA);
            statement.setInt(2,fineStatus.getId());

            if(statement.executeUpdate()>0){
                fineStatus.setValueUA(newValueUA);
                log.info("Fine_Status valueUA was updated. Id={}",fineStatus.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Fine_Status valueUA. Id={}",fineStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(FineStatus fineStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Fine_Status WHERE id=?")){
            statement.setInt(1,fineStatus.getId());

            if(statement.executeUpdate()>0){
                log.info("Fine_Status was deleted. Id={}",fineStatus.getId());
                fineStatus.setId(null);
                fineStatus.setValueEN(null);
                fineStatus.setValueUA(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Fine_Status. Id={}",fineStatus.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
