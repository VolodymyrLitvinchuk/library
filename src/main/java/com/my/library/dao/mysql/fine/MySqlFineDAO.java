package com.my.library.dao.mysql.fine;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.fine.FineDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.fine.Fine;
import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.dao.orderByEnums.FineOrderBy;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;

public class MySqlFineDAO implements FineDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlFineDAO.class);

    private Fine mapFine(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            int orderFK = rs.getInt(2);
            int amount = rs.getInt(3);
            Fine fine = new Fine(orderFK,amount);
            fine.setId(id);

            Optional<FineStatus> optionalStatus =
                    DAOFactory.getFineStatusDAO().get(rs.getInt(4));
            if(optionalStatus.isEmpty()) return null;

            fine.setStatus(optionalStatus.get());
            return fine;

        }catch (Exception ex){
            log.warn("Exception while mapping Fine");
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Fine> getByFineID(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                Fine fine = mapFine(rs);
                rs.close();
                return Optional.ofNullable(fine);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Fine with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Fine>> getByOrderID(int orderFK) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine WHERE order_id=?")){
            statement.setInt(1,orderFK);
            ResultSet rs = statement.executeQuery();

            List<Fine> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapFine(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting Fines with orderFK={}",orderFK,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Fine>> getReaderFinesOrderByLimitOffsetOrder(FineOrderBy orderBy, String readerLogin, int limit, int offset) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    String.format(GET_READER_FINES_ORDER_BY_OFFSET_LIMIT, orderBy.getValue()))){
            statement.setString(1, readerLogin);
            statement.setInt(2, limit);
            statement.setInt(3, offset);

            ResultSet rs = statement.executeQuery();
            List<Fine> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapFine(rs));
            }
            return Optional.of(list);


        }catch (Exception e){
            log.warn("Exception while getReaderFinesOrderByLimitOffsetOrder()", e);
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<Fine> getByOrderIdUnpaid(int orderFK) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine WHERE order_id=? AND fine_status=0")){
            statement.setInt(1,orderFK);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                Fine fine = mapFine(rs);
                rs.close();
                return Optional.ofNullable(fine);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting unpaid Fine with orderFK={}",orderFK,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Fine>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine")){
            ResultSet rs = statement.executeQuery();

            List<Fine> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapFine(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Fines",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> countReaderFines(String readerLogin, int fineStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    COUNT_READER_FINES_BY_STATUS)){
            statement.setString(1, readerLogin);
            statement.setInt(2, fineStatus);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting ReaderFines quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> countReaderFines(String readerLogin) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    COUNT_READER_FINES)){
            statement.setString(1, readerLogin);

            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting ReaderFines quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void insert(Fine fine) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Fine VALUES (DEFAULT,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS)){
            statement.setInt(1, fine.getOrderFK());
            statement.setInt(2, fine.getAmount());
            statement.setInt(3,0); // status="unpaid"

            if(statement.executeUpdate()>0){
                ResultSet rs = statement.getGeneratedKeys();
                if(rs.next()){
                    fine.setId(rs.getInt(1));
                    rs.close();

                    Optional<FineStatus> optionalStatus =
                            DAOFactory.getFineStatusDAO().get(0);
                    fine.setStatus(optionalStatus.get());
                    log.info("Fine with id={} was inserted",fine.getId());
                }
                rs.close();
            }

        }catch (SQLException ex){
            log.warn("Exception while inserting Fine",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateAmount(Fine fine, int newAmount) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                "UPDATE Fine SET fine_amount=? WHERE id=?")){
            statement.setInt(1,newAmount);
            statement.setInt(2,fine.getId());

            if(statement.executeUpdate()>0){
                fine.setAmount(newAmount);
                log.info("Fine amount was updated. Id={}",fine.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating fine amount. Id={}",fine.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateStatus(Fine fine, FineStatus newStatus, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "UPDATE Fine SET fine_status=? WHERE id=?")){
            statement.setInt(1,newStatus.getId());
            statement.setInt(2,fine.getId());

            if(statement.executeUpdate()>0){
                fine.setStatus(newStatus);
                log.info("Fine status was updated. Id={}",fine.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating fine status. Id={}",fine.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(Fine fine, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM Fine WHERE id=?")){
            statement.setInt(1,fine.getId());

            if(statement.executeUpdate()>0){
                log.info("Fine with id={} was deleted",fine.getId());
                fine.setId(null);
                fine.setStatus(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting fine with id={}",fine.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
