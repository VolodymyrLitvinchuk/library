package com.my.library.dao.mysql.book;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.book.BookDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.entity.builder.BookBuilder;
import com.my.library.dao.entity.builder.BookBuilderImpl;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;

public class MySqlBookDAO implements BookDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlBookDAO.class);

    private Book mapBook(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);

            Optional<Author> optionalAuthor =
                    DAOFactory.getAuthorDAO().get(rs.getInt(5));
            if(optionalAuthor.isEmpty()) return null;

            BookBuilder builder = new BookBuilderImpl();
            builder.setAuthor(optionalAuthor.get())
                    .setTitle_EN(rs.getString(2))
                    .setTitle_UA(rs.getString(3))
                    .setLanguage(rs.getString(4));

            Optional<Publication> optionalPublication =
                    DAOFactory.getPublicationDAO().get(rs.getInt(6));
            if(optionalPublication.isEmpty()) return null;

            builder.setPublication(optionalPublication.get())
                    .setPublication_Date(rs.getDate(7))
                    .setCopiesOwned(rs.getInt(8));

            Book book = builder.get();
            if(book==null) return null;

            book.setId(id);
            return book;

        }catch (Exception ex){
            log.warn("Exception while mapping Book",ex);
            throw new DAOException(ex);
        }
    }

    private Book mapBookFromJoinedTables(ResultSet rs) throws DAOException {
        try{
            Author author = new Author(
                    rs.getInt(BOOK_AUTHOR_ID_COLUMN),
                    rs.getString(AUTHOR_FIRST_NAME_EN_COLUMN),
                    rs.getString(AUTHOR_FIRST_NAME_UA_COLUMN),
                    rs.getString(AUTHOR_LAST_NAME_EN_COLUMN),
                    rs.getString(AUTHOR_LAST_NAME_UA_COLUMN)
            );

            Publication publication = new Publication(
                    rs.getInt(BOOK_PUBLICATION_ID_COLUMN),
                    rs.getString(PUBLICATION_NAME_EN_COLUMN),
                    rs.getString(PUBLICATION_NAME_UA_COLUMN)
            );

            BookBuilder bookBuilder = new BookBuilderImpl();
            bookBuilder.setAuthor(author)
                    .setPublication(publication)
                    .setTitle_EN(rs.getString(BOOK_TITLE_EN_COLUMN))
                    .setTitle_UA(rs.getString(BOOK_TITLE_UA_COLUMN))
                    .setLanguage(rs.getString(BOOK_LANGUAGE_COLUMN))
                    .setPublication_Date(rs.getDate(PUBLICATION_DATE_COLUMN))
                    .setCopiesOwned(rs.getInt(BOOK_COPIES_OWNED_COLUMN));
            Book book = bookBuilder.get();
            if(book==null) return null;

            book.setId(rs.getInt(1));
            return book;

        }catch (Exception ex){
            log.warn("Exception while mapping Book from joined tables",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Book> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.ofNullable(mapBook(rs));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Book>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book")){
            ResultSet rs = statement.executeQuery();

            List<Book> books = new ArrayList<>();
            while (rs.next()){
                books.add(mapBook(rs));
            }
            return Optional.of(books);

        }catch (Exception ex){
            log.warn("Exception while getting all Books",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> getBookQuantity() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT count(*) FROM Book")){

            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> getBookQuantity(String query) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    query)){

            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book quantity using query",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Book>> searchBooks(String query, BookOrderBy orderBy, int offset, int limit) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(query,orderBy.getValue()))){

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet rs = statement.executeQuery();
            List<Book> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapBookFromJoinedTables(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while searchBooksEN", ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Book>> getBooksByOrderOffsetLimit(BookOrderBy orderBy, int offset, int limit) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(GET_BOOKS_BY_OFFSET_LIMIT_ORDER,orderBy.getValue()))) {

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet rs = statement.executeQuery();
            List<Book> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapBookFromJoinedTables(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while getting all BooksByOrderOffsetLimit", ex);
            throw new DAOException(ex);
        }
    }


    @Override
    public void insert(Book book, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Book VALUES (DEFAULT,?,?,?,?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1,book.getTitle_EN());
            statement.setString(2, book.getTitle_UA());
            statement.setString(3,book.getLanguage());
            statement.setInt(4,book.getAuthor().getId());
            statement.setInt(5,book.getPublication().getId());
            statement.setDate(6,book.getPublication_Date());
            statement.setInt(7,book.getCopiesOwned());

            if(statement.executeUpdate()>0){
                ResultSet rs = statement.getGeneratedKeys();
                if(rs.next()){
                    book.setId(rs.getInt(1));
                    rs.close();
                    log.info("Book was inserted with id={}",book.getId());
                }
                rs.close();
            }

        }catch (SQLException ex){
            log.warn("Exception while inserting book",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateTitleEN(Book book, String newTitleEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET title_EN=? WHERE id=?")){
            statement.setString(1,newTitleEN);
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book titleEN was updated. Book id={}",book.getId());
                book.setTitle_EN(newTitleEN);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book titleEN. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateTitleUA(Book book, String newTitleUA) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET title_UA=? WHERE id=?")){
            statement.setString(1,newTitleUA);
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book titleUA was updated. Book id={}",book.getId());
                book.setTitle_UA(newTitleUA);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book titleUA. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }


    @Override
    public void updateLanguage(Book book, String newLanguage) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET language=? WHERE id=?")){
            statement.setString(1,newLanguage);
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book language was updated. Book id={}",book.getId());
                book.setLanguage(newLanguage);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book language. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateAuthor(Book book, Author newAuthor) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET author=? WHERE id=?")){
            statement.setInt(1,newAuthor.getId());
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book authorID was updated. Book id={}",book.getId());
                book.setAuthor(newAuthor);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book authorID. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updatePublication(Book book, Publication newPublication) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET publication_id=? WHERE id=?")){
            statement.setInt(1,newPublication.getId());
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book publication_id was updated. Book id={}",book.getId());
                book.setPublication(newPublication);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book publication_id. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updatePublicationDate(Book book, Date newDate) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET publication_Date=? WHERE id=?")){
            statement.setDate(1,newDate);
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book publication_Date was updated. Book id={}",book.getId());
                book.setPublication_Date(newDate);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book publication_Date. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateCopiesOwned(Book book, Integer newCopiesOwned, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book SET copies_owned=? WHERE id=?")){
            statement.setInt(1,newCopiesOwned);
            statement.setInt(2,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book copies_owned was updated. Book id={}",book.getId());
                book.setCopiesOwned(newCopiesOwned);
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book copies_owned. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(Book book, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM Book WHERE id=?")){
            statement.setInt(1,book.getId());

            if(statement.executeUpdate()>0){
                log.info("Book with id={} was deleted",book.getId());
                book.setId(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Book. Book id={}",book.getId(),ex);
            throw new DAOException(ex);
        }
    }

}
