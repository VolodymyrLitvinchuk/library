package com.my.library.dao.mysql.fine;

import com.my.library.dao.abstr.fine.FinePaymentDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.fine.FinePayment;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlFinePaymentDAO implements FinePaymentDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlFinePaymentDAO.class);

    private FinePayment mapPayment(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            int fineFK = rs.getInt(2);
            Date date = rs.getDate(3);
            FinePayment payment = new FinePayment(fineFK,date);
            payment.setId(id);
            return payment;

        }catch (SQLException ex){
            log.warn("Exception while mapping FinePayment",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<FinePayment> getByFinePaymentID(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine_Payment WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                FinePayment payment = mapPayment(rs);
                rs.close();
                return Optional.of(payment);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting FinePayment by id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<FinePayment> getByFineID(int fineFK) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine_Payment WHERE fine_id=?")){
            statement.setInt(1,fineFK);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                FinePayment payment = mapPayment(rs);
                rs.close();
                return Optional.of(payment);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting FinePayment by fineFK={}",fineFK,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<FinePayment>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Fine_Payment")){
            ResultSet rs = statement.executeQuery();

            List<FinePayment> list = new ArrayList<>();
            while(rs.next()){
                list.add(mapPayment(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all FinePayments",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void insert(FinePayment payment, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Fine_Payment VALUES (DEFAULT,?,?)",
                Statement.RETURN_GENERATED_KEYS)){
            statement.setInt(1,payment.getFineIdFK());
            statement.setDate(2,payment.getDate());

            if(statement.executeUpdate()>0){
                ResultSet rs = statement.getGeneratedKeys();
                if(rs.next()){
                    payment.setId(rs.getInt(1));
                    log.info("FinePayment was inserted with id={}",payment.getId());
                }
                rs.close();
            }

        }catch (SQLException ex){
            log.warn("Exception while inserting FinePayment. FineID={}",payment.getFineIdFK(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(FinePayment payment, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM Fine_Payment WHERE id=?")){
            statement.setInt(1,payment.getId());

            if(statement.executeUpdate()>0){
                log.info("FinePayment with id={} was deleted",payment.getId());
                payment.setId(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting FinePayment. Id={}",payment.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
