package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.abstr.bookOrder.BookOrderTypeDAO;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlBookOrderTypeDAO implements BookOrderTypeDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlBookOrderTypeDAO.class);

    private BookOrderType mapOrderType(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            String valueEN = rs.getString(2);
            String valueUA = rs.getString(3);
            return new BookOrderType(id,valueEN,valueUA);

        }catch (SQLException ex){
            log.warn("Exception while mapping BookOrderType",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<BookOrderType> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order_Type WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                BookOrderType orderType = mapOrderType(rs);
                rs.close();
                return Optional.of(orderType);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book_Order_Type with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrderType>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order_Type")){
            ResultSet rs = statement.executeQuery();

            List<BookOrderType> list = new ArrayList<>();
            while(rs.next()){
                list.add(mapOrderType(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Book_Order_Type",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(BookOrderType orderType) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Book_Order_Type VALUES (?,?,?)")){
            statement.setInt(1,orderType.getId());
            statement.setString(2, orderType.getValueEN());
            statement.setString(3, orderType.getValueUA());

            if(statement.executeUpdate()>0){
                log.info("New Book_Order_Type was inserted. Id={}",orderType.getId());
                return true;
            }
            return false;

        }catch (Exception ex){
            log.warn("Exception while inserting new Book_Order_Type",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateEN(BookOrderType orderType, String newValueEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book_Order_Type SET type_value_EN=? WHERE id=?")){
            statement.setString(1,newValueEN);
            statement.setInt(2,orderType.getId());

            if(statement.executeUpdate()>0){
                orderType.setValueEN(newValueEN);
                log.info("Book_Order_Type valueEN was updated. Id={}",orderType.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book_Order_Type valueEN. Id={}",orderType.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateUA(BookOrderType orderType, String newValueUA) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book_Order_Type SET type_value_UA=? WHERE id=?")){
            statement.setString(1,newValueUA);
            statement.setInt(2,orderType.getId());

            if(statement.executeUpdate()>0){
                orderType.setValueUA(newValueUA);
                log.info("Book_Order_Type valueUA was updated. Id={}",orderType.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book_Order_Type valueUA. Id={}",orderType.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(BookOrderType orderType) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Book_Order_Type WHERE id=?")){
            statement.setInt(1,orderType.getId());

            if(statement.executeUpdate()>0){
                log.info("Book_Order_Type was deleted. Id={}",orderType.getId());
                orderType.setId(null);
                orderType.setValueEN(null);
                orderType.setValueUA(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Book_Order_Type. Id={}",orderType.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
