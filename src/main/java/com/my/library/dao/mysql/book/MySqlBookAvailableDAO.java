package com.my.library.dao.mysql.book;

import com.my.library.dao.abstr.book.BookAvailableDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class MySqlBookAvailableDAO implements BookAvailableDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlBookAvailableDAO.class);

    @Override
    public Optional<Integer> get(int BookId) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Available WHERE book_id=?")){
            statement.setInt(1,BookId);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return Optional.of(rs.getInt(2));
            }
            return Optional.empty();
        }catch (SQLException ex){
            log.warn("Exception while getting Book_Available where book_id={}",BookId,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(int BookId, int quantity, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Book_Available VALUES (?,?)")){
            statement.setInt(1,BookId);
            statement.setInt(2,quantity);
            if(statement.executeUpdate()>0){
                log.info("Book_Available was inserted. Book id={}",BookId);
                return true;
            }
            return false;
        }catch (SQLException ex){
            log.warn("Exception while inserting Book_Available. Book id={}",BookId,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean update(int BookId, int newQuantity, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "UPDATE Book_Available SET quantity=? WHERE book_id=?")){
            statement.setInt(1,newQuantity);
            statement.setInt(2,BookId);
            if(statement.executeUpdate()>0){
                log.info("Book_Available was updated. Book id={}",BookId);
                return true;
            }
            return false;
        }catch (SQLException ex){
            log.warn("Exception while updating quantity. Book id={}",BookId,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(int BookId) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Book_Available WHERE book_id=?")){
            statement.setInt(1,BookId);
            if(statement.executeUpdate()>0){
                log.info("Book_Available was deleted. Book id={}",BookId);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Book_Available. Book id={}",BookId,ex);
            throw new DAOException(ex);
        }
    }
}
