package com.my.library.dao.mysql.user;

import com.my.library.dao.abstr.user.UserRoleDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.UserRole;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlUserRoleDAO implements UserRoleDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlUserRoleDAO.class);

    private UserRole mapRole(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            String value = rs.getString(2);
            return new UserRole(id,value);

        }catch (SQLException ex){
            log.warn("Exception while mapping User role",ex);
            throw new DAOException(ex);
        }
    }
    @Override
    public Optional<UserRole> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Role WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                UserRole role = mapRole(rs);
                rs.close();
                return Optional.of(role);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting UserRole with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<UserRole> getByValue(String value) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Role WHERE role=?")){
            statement.setString(1,value);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                UserRole role = mapRole(rs);
                rs.close();
                return Optional.of(role);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting UserRole with value={}",value,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<UserRole>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Role")){
            ResultSet rs = statement.executeQuery();
            List<UserRole> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapRole(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all UserRole",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(UserRole role) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO User_Role VALUES (?,?)")){
            statement.setInt(1,role.getId());
            statement.setString(2,role.getValue());
            if(statement.executeUpdate()>0){
                log.info("New UserRole was inserted with id={}",role.getId());
                return true;
            }
            return false;

        }catch (SQLException ex){
            log.warn("Exception while inserting new UserRole",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void update(UserRole role, String newValue) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE User_Role SET role=? WHERE id=?")){
            statement.setString(1,newValue);
            statement.setInt(2,role.getId());
            if(statement.executeUpdate()>0){
                role.setValue(newValue);
                log.info("UserRole value was updated. Id={}",role.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating UserRole with id={}",role.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(UserRole role) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM User_Role WHERE id=?")){
            statement.setInt(1,role.getId());
            if(statement.executeUpdate()>0){
                log.info("UserRole was deleted with id={}",role.getId());
                role.setId(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting UserRole with id={}",role.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
