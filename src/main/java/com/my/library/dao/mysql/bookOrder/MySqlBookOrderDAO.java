package com.my.library.dao.mysql.bookOrder;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.bookOrder.BookOrderDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;

public class MySqlBookOrderDAO implements BookOrderDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlBookOrderDAO.class);

    private BookOrder mapBookOrder(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            int bookID = rs.getInt(2);
            String reader = rs.getString(3);

            Optional<BookOrderType> optionalType =
                    DAOFactory.getBookOrderTypeDAO().get(rs.getInt(4));
            if(optionalType.isEmpty()) return null;
            BookOrderType type = optionalType.get();

            Optional<BookOrderStatus> optionalStatus =
                    DAOFactory.getBookOrderStatusDAO().get(rs.getInt(7));
            if(optionalStatus.isEmpty()) return null;
            BookOrderStatus status = optionalStatus.get();

            Date date = rs.getDate(5);
            Date returnedDate = rs.getDate(6);

            BookOrder bookOrder = new BookOrder(bookID,reader,type,date,returnedDate,status);
            bookOrder.setId(id);

            return bookOrder;

        }catch (Exception ex){
            log.warn("Exception while mapping BookOrder",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<BookOrder> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order WHERE id=?")){
            statement.setInt(1,id);

            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                BookOrder bookOrder = mapBookOrder(rs);
                rs.close();
                return Optional.ofNullable(bookOrder);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Book_Order with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrder>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            Statement statement = connection.createStatement()){
            ResultSet rs = statement.executeQuery("SELECT * FROM Book_Order");

            List<BookOrder> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapBookOrder(rs));
            }
            rs.close();
            if(list.size()==0) return Optional.empty();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Book orders",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrder>> getAllByReaderFK(String readerFK) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order WHERE reader=?")){
            statement.setString(1,readerFK);
            ResultSet rs = statement.executeQuery();

            List<BookOrder> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapBookOrder(rs));
            }
            rs.close();
            if(list.size()==0) return Optional.empty();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Book orders",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrder>> getAllByStatus(int statusID) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order WHERE order_status=?")){
            statement.setInt(1,statusID);
            ResultSet rs = statement.executeQuery();

            List<BookOrder> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapBookOrder(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all Book orders",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrder>> getAllActiveOrderByOffsetLimit(BookOrderOrderBy orderBy, int offset, int limit) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(GET_ALL_ACTIVE_BOOK_ORDERS_BY_OFFSET_LIMIT_ORDER,orderBy.getValue()))) {

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet rs = statement.executeQuery();
            List<BookOrder> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapBookOrder(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while getting all BookOrderActiveOrderByOffsetLimit", ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<BookOrder>> getAllByReaderOrderByOffsetLimit(BookOrderOrderBy orderBy, int offset, int limit, String readerLogin) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(GET_ALL_READER_BOOK_ORDERS_BY_OFFSET_LIMIT_ORDER,orderBy.getValue()))) {

            statement.setString(1,readerLogin);
            statement.setInt(2, limit);
            statement.setInt(3, offset);

            ResultSet rs = statement.executeQuery();
            List<BookOrder> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapBookOrder(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while getting all BookOrderByReaderOrderByOffsetLimit", ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> countReaderOrders(String readerLogin, int orderStatus) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT COUNT(*) FROM Book_Order WHERE reader=? AND order_status=?")){
            statement.setString(1, readerLogin);
            statement.setInt(2, orderStatus);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting ReaderOrders quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> countAllReaderOrders(String readerLogin) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT COUNT(*) FROM Book_Order WHERE reader=?")){
            statement.setString(1, readerLogin);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting all ReaderOrders quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Integer> countAllActiveOrders() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT COUNT(*) FROM Book_Order WHERE order_status!=3 AND order_status!=4")){
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting all Active orders quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean hasReaderUnfinishedOrder(String readerLogin, int bookID) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order WHERE reader=? AND book_id=? AND order_status!=3 AND order_status!=4")){
            statement.setString(1,readerLogin);
            statement.setInt(2,bookID);
            ResultSet rs = statement.executeQuery();

            return rs.next();

        }catch (Exception ex){
            log.warn("Exception while hasReaderUnfinishedOrder()",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean userHasUnfinishedOrders(String readerFK) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Book_Order WHERE reader=?  AND order_status!=3 AND order_status!=4")){
            statement.setString(1,readerFK);
            ResultSet rs = statement.executeQuery();

            return rs.next();

        }catch (Exception ex){
            log.warn("Exception while checking userHaveUnfinishedOrders",ex);
            throw new DAOException(ex);
        }
    }


    @Override
    public void insert(BookOrder bookOrder, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Book_Order VALUES (DEFAULT,?,?,?,?,?,?)"
                ,Statement.RETURN_GENERATED_KEYS)){

            statement.setInt(1,bookOrder.getBookIdFK());
            statement.setString(2,bookOrder.getReaderFK());
            statement.setInt(3,bookOrder.getType().getId());
            statement.setDate(4,bookOrder.getDate());
            statement.setDate(5,bookOrder.getDateReturnPlanned());
            statement.setInt(6,bookOrder.getStatus().getId());

            if(statement.executeUpdate()>0){
                ResultSet rs = statement.getGeneratedKeys();
                if(rs.next()){
                    bookOrder.setId(rs.getInt(1));
                    log.info("New Book_Order was inserted with id={}",bookOrder.getId());
                    rs.close();
                }
                rs.close();
            }

        }catch (SQLException ex){
            log.warn("Exception while inserting new Book order.",ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateType(BookOrder bookOrder, BookOrderType newType) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Book_Order SET order_type=? WHERE id=?")){
            statement.setInt(1,newType.getId());
            statement.setInt(2,bookOrder.getId());

            if(statement.executeUpdate()>0){
                bookOrder.setType(newType);
                log.info("Book order type was updated. Id={}",bookOrder.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book order type. Id={}",bookOrder.getId(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateStatus(BookOrder bookOrder, BookOrderStatus newStatus, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "UPDATE Book_Order SET order_status=? WHERE id=?")){
            statement.setInt(1,newStatus.getId());
            statement.setInt(2,bookOrder.getId());

            if(statement.executeUpdate()>0){
                bookOrder.setStatus(newStatus);
                log.info("Book order status was updated. Id={}",bookOrder.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating Book order status. Id={}",bookOrder.getId(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void delete(BookOrder bookOrder) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM Book_Order WHERE id=?")){
            statement.setInt(1,bookOrder.getId());

            if(statement.executeUpdate()>0){
                log.info("Book order with id={} was deleted",bookOrder.getId());
                bookOrder.setId(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting Book order with id={}",bookOrder.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
