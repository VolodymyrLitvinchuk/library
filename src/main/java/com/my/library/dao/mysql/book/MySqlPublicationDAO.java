package com.my.library.dao.mysql.book;

import com.my.library.dao.abstr.book.PublicationDAO;
import com.my.library.dao.entity.book.Publication;
import com.my.library.dao.connection.DataSource;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlPublicationDAO implements PublicationDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlPublicationDAO.class);

    private Publication mapPublication(ResultSet rs) throws DAOException {
        try{
            Integer id = rs.getInt(1);
            String nameEN = rs.getString(2);
            String nameUA = rs.getString(3);
            return new Publication(id,nameEN,nameUA);
        }catch (SQLException ex){
            log.warn("Exception while mapping Publication",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Publication> get(Integer id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Publication WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return Optional.of(mapPublication(rs));
            }
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while getting Publication. Id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Publication> getByNameEN(String nameEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Publication WHERE name_EN=?")){
            statement.setString(1,nameEN);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return Optional.of(mapPublication(rs));
            }
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while getting Publication with nameEN={}",nameEN,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Publication>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Publication")){
            ResultSet rs = statement.executeQuery();
            List<Publication> publications = new ArrayList<>();
            while (rs.next()){
                publications.add(mapPublication(rs));
            }
            return Optional.of(publications);
        }catch (Exception ex){
            log.warn("Exception while getting all Publication",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void insert(Publication publication, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO Publication VALUES (DEFAULT,?,?)",
                Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, publication.getNameEN());
            statement.setString(2, publication.getNameUA());
            if(statement.executeUpdate()>0){
                ResultSet rs = statement.getGeneratedKeys();
                if(rs.next()){
                    publication.setId(rs.getInt(1));
                    log.info("New Publication inserted");
                }
            }
        }catch (SQLException ex){
            log.warn("Exception while inserting Book_Language",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateEN(Publication publication, String newName) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Publication SET name_EN=? WHERE id=?")){
            statement.setString(1,newName);
            statement.setInt(2,publication.getId());

            if(statement.executeUpdate()>0){
                publication.setNameEN(newName);
                log.info("Publication name_EN was updated. Id={}",publication.getId());
            }
        }catch (SQLException ex){
            log.warn("Exception while updating Publication with id={}",publication.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateUA(Publication publication, String newName) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Publication SET name_UA=? WHERE id=?")){
            statement.setString(1,newName);
            statement.setInt(2,publication.getId());

            if(statement.executeUpdate()>0){
                publication.setNameUA(newName);
                log.info("Publication name_UA was updated. Id={}",publication.getId());
            }
        }catch (SQLException ex){
            log.warn("Exception while updating Publication with id={}",publication.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(Publication publication, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM Publication WHERE id=?")){
            statement.setInt(1,publication.getId());

            if(statement.executeUpdate()>0){
                log.info("Publication was deleted. Id={}",publication.getId());
                publication.setId(null);
                publication.setNameEN(null);
                publication.setNameUA(null);
            }
        }catch (SQLException ex){
            log.warn("Exception while deleting Publication. Id={}",publication.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
