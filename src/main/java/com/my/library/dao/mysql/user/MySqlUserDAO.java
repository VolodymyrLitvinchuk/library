package com.my.library.dao.mysql.user;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.abstr.user.UserDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.orderByEnums.UserOrderBy;
import com.my.library.dao.entity.user.User;
import com.my.library.dao.entity.user.UserRole;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;

public class MySqlUserDAO implements UserDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlUserDAO.class);

    private User mapUser(ResultSet rs) throws DAOException {
        try{
            String login = rs.getString(1);
            String passwordHash = rs.getString(2);
            String email = rs.getString(3);
            String firstName = rs.getString(4);
            String lastName = rs.getString(5);
            Optional<UserRole> optionalRole = DAOFactory.getUserRoleDAO().get(rs.getInt(6));
            Optional<UserStatus> optionalStatus = DAOFactory.getUserStatusDAO().get(rs.getInt(7));

            if(optionalRole.isPresent() && optionalStatus.isPresent()){
                User user = new User(login,passwordHash,email,firstName,lastName,optionalRole.get());
                user.setStatus(optionalStatus.get());
                return user;
            }
            return null;
        }catch (Exception ex){
            log.warn("Exception while mapping User",ex);
            throw new DAOException(ex);
        }
    }

    private User mapUserFromJoinedTables(ResultSet rs) throws DAOException{
        try {
            UserRole userRole = new UserRole(
                    rs.getInt(USER_ROLE_COLUMN),
                    rs.getString(USER_ROLE_VALUE_COLUMN)
            );
            UserStatus userStatus = new UserStatus(
                    rs.getInt(USER_STATUS_COLUMN),
                    rs.getString(USER_STATUS_VALUE_COLUMN)
            );
            User user = new User(
                    rs.getString(USER_LOGIN_COLUMN),
                    rs.getString(USER_PASSWORD_COLUMN),
                    rs.getString(USER_EMAIL_COLUMN),
                    rs.getString(USER_FIRST_NAME_COLUMN),
                    rs.getString(USER_LAST_NAME_COLUMN),
                    userRole
            );
            user.setStatus(userStatus);

            return user;

        }catch (Exception e){
            log.warn("Exception while mapping User from joined tables",e);
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<User> get(String login) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM User WHERE login=?")){
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                User user = mapUser(resultSet);
                resultSet.close();
                return Optional.ofNullable(user);
            }
            resultSet.close();
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while getting user with login={}",login,ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public Optional<User> getByEmail(String email) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM User WHERE email=?")){
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                User user = mapUser(resultSet);
                resultSet.close();
                return Optional.ofNullable(user);
            }
            resultSet.close();
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while getting user with email={}",email,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<User>> getUsersByOrderOffsetLimit(UserOrderBy orderBy, int offset, int limit) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(GET_USERS_BY_OFFSET_LIMIT_ORDER,orderBy.getValue()))) {

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet rs = statement.executeQuery();
            List<User> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapUserFromJoinedTables(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while getting all UsersByOrderOffsetLimit", ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<User>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM User")){
            List<User> list = new ArrayList<>();
            while (resultSet.next()){
                list.add(mapUser(resultSet));
            }
            return Optional.of(list);
        }catch (Exception ex){
            log.warn("Exception while getting all users",ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public Optional<Integer> countAllUsers() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT count(*) FROM User")){

            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting User quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void  insert(User user) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO User VALUES (?,?,?,?,?,?,?)")) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setInt(6,user.getRole().getId());
            statement.setInt(7,0);

            if(statement.executeUpdate()>0){
                log.info("New user with login={} was inserted",user.getLogin());
                user.setStatus(DAOFactory.getUserStatusDAO().get(0).get());
            }
        } catch (SQLException ex) {
            log.warn("Exception while inserting new user with login={}",user.getLogin(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateLogin(User user, String newLogin) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET login=? WHERE login=?")) {
            statement.setString(1, newLogin);
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                log.info("User login was updated from-{} to-{}",user.getLogin(),newLogin);
                user.setLogin(newLogin);
            }

        } catch (SQLException ex) {
            log.warn("Exception while updating user login from-{} to-{}",user.getLogin(),newLogin,ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updatePassword(User user, String newPasswordHash) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET password=? WHERE login=?")) {
            statement.setString(1, newPasswordHash);
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                user.setPasswordHash(newPasswordHash);
                log.info("User password was updated, login={}",user.getLogin());
            }
        } catch (SQLException ex) {
            log.warn("Exception while updating user password, login={}",user.getLogin(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateEmail(User user, String newEmail) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET email=? WHERE login=?")) {
            statement.setString(1, newEmail);
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                user.setEmail(newEmail);
                log.info("User email was updated, login={}",user.getLogin());
            }
        } catch (SQLException ex) {
            log.warn("Exception while updating user email, login={}",user.getLogin(),ex);
            throw new DAOException(ex);
        }

    }

    @Override
    public void updateFirstName(User user, String newFirstName) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET first_name=? WHERE login=?")) {
            statement.setString(1, newFirstName);
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                user.setFirstName(newFirstName);
                log.info("User first name was updated. User login={}", user.getLogin());
            }
        } catch (SQLException ex) {
            log.warn("Exception while updating user first name. User login={}", user.getLogin(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateLastName(User user, String newLastName) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET last_name=? WHERE login=?")) {
            statement.setString(1, newLastName);
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                user.setLastName(newLastName);
                log.info("User last name was updated. User login={}", user.getLogin());
            }
        } catch (SQLException ex) {
            log.warn("Exception while updating user last name. User login={}", user.getLogin(),ex);
            throw new DAOException(ex);
        }
    }


    @Override
    public void updateStatus(User user, UserStatus newUserStatus) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("UPDATE User SET statusFK=? WHERE login=?")) {
            statement.setInt(1, newUserStatus.getId());
            statement.setString(2, user.getLogin());
            if(statement.executeUpdate()>0){
                user.setStatus(newUserStatus);
                log.info("User status was updated. User login={}", user.getLogin());
            }
        } catch (SQLException ex) {
            log.warn("Exception while updating user status. User login={}", user.getLogin(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(User user) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM User WHERE login=?")){
            statement.setString(1, user.getLogin());
            if(statement.executeUpdate()>0){
                log.info("User with login={} was deleted",user.getLogin());
                user.setStatus(null);
            }
        }catch (SQLException ex){
            log.warn("Exception while deleting user with login={}",user.getLogin(),ex);
            throw new DAOException(ex);
        }

    }
}
