package com.my.library.dao.mysql;

public final class Constant {
    private Constant(){}

    public static final String GET_BOOKS_BY_OFFSET_LIMIT_ORDER =
            "SELECT * FROM Book " +
                    "INNER JOIN Author ON Book.author=Author.id " +
                    "INNER JOIN Publication ON Book.publication_id=Publication.id " +
                    "ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String GET_USERS_BY_OFFSET_LIMIT_ORDER =
            "SELECT * FROM User "  +
                    "INNER JOIN User_Role on User.roleFK=User_Role.id " +
                    "INNER JOIN User_Status on User.statusFK=User_Status.id " +
                    "ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String GET_AUTHORS_BY_OFFSET_LIMIT_ORDER =
            "SELECT * FROM Author ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String GET_ALL_ACTIVE_BOOK_ORDERS_BY_OFFSET_LIMIT_ORDER =
            "SELECT * FROM Book_Order WHERE order_status!=3 AND order_status!=4 ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String GET_ALL_READER_BOOK_ORDERS_BY_OFFSET_LIMIT_ORDER =
            "SELECT * FROM Book_Order WHERE reader=? ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String COUNT_READER_FINES_BY_STATUS =
            "SELECT count(*) FROM Book_Order INNER JOIN Fine on Book_Order.id=Fine.order_id " +
                    "WHERE reader=? AND fine_status=?;";

    public static final String COUNT_READER_FINES =
            "SELECT count(*) FROM Book_Order INNER JOIN Fine on Book_Order.id=Fine.order_id WHERE reader=?";

    public static final String SEARCH_BOOK = "SELECT * FROM Book " +
            "INNER JOIN Author ON Book.author=Author.id " +
            "INNER JOIN Publication ON Book.publication_id=Publication.id ";

    public static final String ORDER_BY_LIMIT_OFFSET = " ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String GET_READER_FINES_ORDER_BY_OFFSET_LIMIT =
            "SELECT * FROM Fine WHERE order_id IN (SELECT id FROM Book_Order WHERE reader=?) ORDER BY %s LIMIT ? OFFSET ?;";

    public static final String USER_LOGIN_COLUMN = "login";
    public static final String USER_PASSWORD_COLUMN = "password";
    public static final String USER_EMAIL_COLUMN = "email";
    public static final String USER_FIRST_NAME_COLUMN = "first_name";
    public static final String USER_LAST_NAME_COLUMN = "last_name";
    public static final String USER_ROLE_COLUMN = "roleFK";
    public static final String USER_STATUS_COLUMN = "statusFK";

    public static final String USER_ROLE_VALUE_COLUMN = "role";
    public static final String USER_STATUS_VALUE_COLUMN = "status";

    public static final String BOOK_TITLE_EN_COLUMN = "title_EN";
    public static final String BOOK_TITLE_UA_COLUMN = "title_UA";
    public static final String BOOK_LANGUAGE_COLUMN = "language";
    public static final String BOOK_AUTHOR_ID_COLUMN = "author";
    public static final String BOOK_PUBLICATION_ID_COLUMN = "publication_id";
    public static final String PUBLICATION_DATE_COLUMN = "publication_Date";
    public static final String BOOK_COPIES_OWNED_COLUMN = "copies_owned";

    public static final String AUTHOR_ID_COLUMN = "id";
    public static final String AUTHOR_FIRST_NAME_EN_COLUMN = "first_name_EN";
    public static final String AUTHOR_FIRST_NAME_UA_COLUMN = "first_name_UA";
    public static final String AUTHOR_LAST_NAME_EN_COLUMN = "last_name_EN";
    public static final String AUTHOR_LAST_NAME_UA_COLUMN = "last_name_UA";

    public static final String PUBLICATION_NAME_EN_COLUMN = "name_EN";
    public static final String PUBLICATION_NAME_UA_COLUMN = "name_UA";

    public static final int FINE_VALUE_PER_DAY = 10;
    public static final String ACTIVE_USER_STATUS = "Active";
    public static final String LOCKED_USER_STATUS = "Locked";
    public static final String SUSPENDED_USER_STATUS = "Suspended";

    public static final String BOOK_ORDER_ID_COLUMN = "id";
    public static final String BOOK_ORDER_BOOK_ID_COLUMN = "book_id";
    public static final String BOOK_ORDER_READER_LOGIN_COLUMN = "reader";
    public static final String BOOK_ORDER_TYPE_COLUMN = "order_type";
    public static final String BOOK_ORDER_DATE_COLUMN = "order_date";
    public static final String BOOK_ORDER_RETURNED_DATE_PLANNED_COLUMN = "returned_date_planned";
    public static final String BOOK_ORDER_STATUS_COLUMN = "order_status";

    public static final int ORDER_FINISHED_STATUS = 3;
    public static final int ORDER_CANCELED_STATUS = 4;

    public static final String READER_ROLE = "Reader";
    public static final String LIBRARIAN_ROLE = "Librarian";
    public static final String ADMIN_ROLE = "Admin";

    public static final String CURRENT_ADMIN_EMAIL = "2001litvinchuk2001@gmail.com";

    public static final String FINE_ORDER_ID_COLUMN = "order_id";
    public static final String FINE_AMOUNT_COLUMN = "fine_amount";
    public static final String FINE_STATUS_COLUMN = "fine_status";
}
