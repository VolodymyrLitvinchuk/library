package com.my.library.dao.mysql.user;

import com.my.library.dao.abstr.user.UserStatusDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySqlUserStatusDAO implements UserStatusDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlUserStatusDAO.class);

    private UserStatus mapStatus(ResultSet rs) throws DAOException {
        try{
            int id = rs.getInt(1);
            String value = rs.getString(2);
            return new UserStatus(id,value);

        }catch (SQLException ex){
            log.warn("Exception while mapping User status",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<UserStatus> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Status WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                UserStatus status = mapStatus(rs);
                rs.close();
                return Optional.of(status);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting UserStatus with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<UserStatus> getByValue(String value) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Status WHERE status=?")){
            statement.setString(1,value);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                UserStatus status = mapStatus(rs);
                rs.close();
                return Optional.of(status);
            }
            rs.close();
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting UserStatus with value={}",value,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<UserStatus>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM User_Status")){
            ResultSet rs = statement.executeQuery();
            List<UserStatus> list = new ArrayList<>();
            while (rs.next()){
                list.add(mapStatus(rs));
            }
            rs.close();
            return Optional.of(list);

        }catch (Exception ex){
            log.warn("Exception while getting all UserStatus",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public boolean insert(UserStatus status) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO User_Status VALUES (?,?)")){
            statement.setInt(1,status.getId());
            statement.setString(2,status.getValue());
            if(statement.executeUpdate()>0){
                log.info("New UserStatus was inserted with id={}",status.getId());
                return true;
            }
            return false;

        }catch (SQLException ex){
            log.warn("Exception while inserting new UserStatus",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void update(UserStatus status, String newValue) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE User_Status SET status=? WHERE id=?")){
            statement.setString(1,newValue);
            statement.setInt(2,status.getId());
            if(statement.executeUpdate()>0){
                status.setValue(newValue);
                log.info("UserStatus value was updated. Id={}",status.getId());
            }

        }catch (SQLException ex){
            log.warn("Exception while updating UserStatus with id={}",status.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(UserStatus status) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "DELETE FROM User_Status WHERE id=?")){
            statement.setInt(1,status.getId());
            if(statement.executeUpdate()>0){
                log.info("UserStatus was deleted with id={}",status.getId());
                status.setId(null);
            }

        }catch (SQLException ex){
            log.warn("Exception while deleting UserStatus with id={}",status.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
