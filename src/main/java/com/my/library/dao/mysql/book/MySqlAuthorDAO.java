package com.my.library.dao.mysql.book;

import com.my.library.dao.abstr.book.AuthorDAO;
import com.my.library.dao.connection.DataSource;
import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.builder.AuthorBuilderImpl;
import com.my.library.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.GET_AUTHORS_BY_OFFSET_LIMIT_ORDER;

public class MySqlAuthorDAO implements AuthorDAO {
    private static final Logger log = LoggerFactory.getLogger(MySqlAuthorDAO.class);

    private Author mapAuthor(ResultSet rs) throws DAOException {
        try{
            Author author = new AuthorBuilderImpl()
                    .setFirstNameEN(rs.getString(2))
                    .setFirstNameUA(rs.getString(3))
                    .setLastNameEN(rs.getString(4))
                    .setLastNameUA(rs.getString(5))
                    .get();
            author.setId(rs.getInt(1));
            return author;

        }catch (SQLException ex){
            log.warn("Exception while mapping Author",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Author> get(int id) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Author WHERE id=?")){
            statement.setInt(1,id);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                Author author = mapAuthor(rs);
                rs.close();
                return Optional.of(author);
            }
            rs.close();
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while get Author with id={}",id,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<Author> getByFirstLastNameEN(String firstNameEN, String lastNameEN) throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Author WHERE first_name_EN=? AND last_name_EN=?")){
            statement.setString(1,firstNameEN);
            statement.setString(2,lastNameEN);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                Author author = mapAuthor(rs);
                rs.close();
                return Optional.of(author);
            }
            rs.close();
            return Optional.empty();
        }catch (Exception ex){
            log.warn("Exception while get Author with first_name_EN={}",firstNameEN,ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Author>> getAll() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Author")){
            List<Author> list = new ArrayList<>();
            while (resultSet.next()){
                list.add(mapAuthor(resultSet));
            }
            return Optional.of(list);
        }catch (Exception ex){
            log.warn("Exception while getting all authors",ex);
            throw new DAOException(ex);
        }
    }

    @Override
        public Optional<Integer> getAuthorQuantity() throws DAOException {
        try(Connection connection = DataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT count(*) FROM Author")){

            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                return Optional.of(rs.getInt(1));
            }
            return Optional.empty();

        }catch (Exception ex){
            log.warn("Exception while getting Author quantity",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public Optional<List<Author>> getAuthorsByOrderOffsetLimit(AuthorOrderBy orderBy, int offset, int limit) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     String.format(GET_AUTHORS_BY_OFFSET_LIMIT_ORDER,orderBy.getValue()))) {

            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet rs = statement.executeQuery();
            List<Author> list = new ArrayList<>();
            while (rs.next()) {
                list.add(mapAuthor(rs));
            }
            return Optional.of(list);

        } catch (Exception ex) {
            log.warn("Exception while getting AuthorsByOrderOffsetLimit", ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void insert(Author author, Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Author VALUES (DEFAULT,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, author.getFirstNameEN());
            statement.setString(2, author.getFirstNameUA());
            statement.setString(3, author.getLastNameEN());
            statement.setString(4, author.getLastNameUA());

            if(statement.executeUpdate()>0){
                ResultSet res = statement.getGeneratedKeys();
                if(res.next()){
                    author.setId(res.getInt(1));
                    log.info("New Author was inserted with id={}",author.getId());
                }
            }
        }catch (Exception ex){
            log.warn("Exception while inserting new author",ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateFirstNameEN(Author author, String newFirstNameEN) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE Author SET first_name_EN=? WHERE id=?")) {
            statement.setString(1, newFirstNameEN);
            statement.setInt(2, author.getId());
            if(statement.executeUpdate()>0){
                log.info("Author firstNameEN was updated. Author id={}",author.getId());
                author.setFirstNameEN(newFirstNameEN);
            }

        } catch (SQLException ex) {
            log.warn("Exception while updating author firstNameEN. Author id={}",author.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateFirstNameUA(Author author, String newFirstNameUA) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE Author SET first_name_UA=? WHERE id=?")) {
            statement.setString(1, newFirstNameUA);
            statement.setInt(2, author.getId());
            if(statement.executeUpdate()>0){
                log.info("Author firstNameUA was updated. Author id={}",author.getId());
                author.setFirstNameUA(newFirstNameUA);
            }

        } catch (SQLException ex) {
            log.warn("Exception while updating author firstNameUA. Author id={}",author.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateLastNameEN(Author author, String newLastNameEN) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE Author SET last_name_EN=? WHERE id=?")) {
            statement.setString(1, newLastNameEN);
            statement.setInt(2, author.getId());
            if(statement.executeUpdate()>0){
                log.info("Author lastNameEN was updated. Author id={}",author.getId());
                author.setLastNameEN(newLastNameEN);
            }

        } catch (SQLException ex) {
            log.warn("Exception while updating author lastNameEN. Author id={}",author.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void updateLastNameUA(Author author, String newLastNameUA) throws DAOException {
        try (Connection connection = DataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE Author SET last_name_UA=? WHERE id=?")) {
            statement.setString(1, newLastNameUA);
            statement.setInt(2, author.getId());
            if(statement.executeUpdate()>0){
                log.info("Author lastNameUA was updated. Author id={}",author.getId());
                author.setLastNameUA(newLastNameUA);
            }

        } catch (SQLException ex) {
            log.warn("Exception while updating author lastNameUA. Author id={}",author.getId(),ex);
            throw new DAOException(ex);
        }
    }

    @Override
    public void delete(Author author,Connection connection) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement("DELETE FROM Author WHERE id=?")){
            statement.setInt(1, author.getId());
            if(statement.executeUpdate()>0){
                author.setFirstNameEN(null);
                author.setFirstNameUA(null);
                author.setLastNameEN(null);
                author.setLastNameUA(null);
                log.info("Author with id={} was deleted", author.getId());
                author.setId(0);
            }
        }catch (SQLException ex){
            log.warn("Exception while deleting author with id={}",author.getId(),ex);
            throw new DAOException(ex);
        }
    }
}
