package com.my.library.dao.abstr.book;

import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dao.entity.book.Author;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface AuthorDAO {
    Optional<Author> get(int id) throws DAOException;
    Optional<Author> getByFirstLastNameEN(String firstNameEN, String lastNameEN) throws DAOException;
    Optional<List<Author>> getAll() throws DAOException;
    Optional<Integer> getAuthorQuantity() throws DAOException;
    Optional<List<Author>> getAuthorsByOrderOffsetLimit(AuthorOrderBy orderBy, int offset, int limit) throws DAOException;

    // after successful insertion author.setId() invoked
    // Second parameter is Connection object to perform sql-transaction
    void insert(Author author, Connection connection) throws DAOException;

    void updateFirstNameEN(Author author, String newFirstNameEN) throws DAOException;
    void updateFirstNameUA(Author author, String newFirstNameUA) throws DAOException;
    void updateLastNameEN(Author author, String newLastNameEN) throws DAOException;
    void updateLastNameUA(Author author, String newLastNameUA) throws DAOException;

    void delete(Author author, Connection connection) throws DAOException;
}
