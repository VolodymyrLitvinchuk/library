package com.my.library.dao.abstr.book;

import com.my.library.dao.entity.book.Publication;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface PublicationDAO {
    Optional<Publication> get(Integer id) throws DAOException;
    Optional<Publication> getByNameEN(String nameEN) throws DAOException;
    Optional<List<Publication>> getAll() throws DAOException;

    // After successful inserting id will be assigned
    void insert(Publication publication, Connection connection) throws DAOException;

    void updateEN(Publication publication, String newName) throws DAOException;
    void updateUA(Publication publication, String newName) throws DAOException;

    void delete(Publication publication, Connection connection) throws DAOException;
}
