package com.my.library.dao.abstr.user;

import com.my.library.dao.entity.user.UserRole;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface UserRoleDAO {
    Optional<UserRole> get(int id) throws DAOException;
    Optional<UserRole> getByValue(String value) throws DAOException;
    Optional<List<UserRole>> getAll() throws DAOException;

    boolean insert(UserRole role) throws DAOException;

    void update(UserRole role, String newValue) throws DAOException;

    void delete(UserRole role) throws DAOException;
}
