package com.my.library.dao.abstr.fine;

import com.my.library.dao.entity.fine.Fine;
import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.dao.orderByEnums.FineOrderBy;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface FineDAO {
    Optional<Fine> getByFineID(int id) throws DAOException;
    Optional<List<Fine>> getByOrderID(int orderFK) throws DAOException;
    Optional<List<Fine>> getReaderFinesOrderByLimitOffsetOrder(FineOrderBy orderBy, String readerLogin, int limit, int offset) throws DAOException;
    Optional<Fine> getByOrderIdUnpaid(int orderFK) throws DAOException;
    Optional<List<Fine>> getAll() throws DAOException;

    Optional<Integer> countReaderFines(String readerLogin, int fineStatus) throws DAOException;
    Optional<Integer> countReaderFines(String readerLogin) throws DAOException;

    // after successful inserting id and status will be assigned
    void insert(Fine fine) throws DAOException;

    void updateAmount(Fine fine, int newAmount) throws DAOException;
    void updateStatus(Fine fine, FineStatus newStatus, Connection connection) throws DAOException;

    void delete(Fine fine, Connection connection) throws DAOException;
}
