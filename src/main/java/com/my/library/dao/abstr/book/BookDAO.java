package com.my.library.dao.abstr.book;

import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.book.Publication;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface BookDAO {
    Optional<Book> get(int id) throws DAOException;
    Optional<List<Book>> getAll() throws DAOException;
    Optional<Integer> getBookQuantity() throws DAOException;
    Optional<Integer> getBookQuantity(String query) throws DAOException;

    Optional<List<Book>> searchBooks(String query, BookOrderBy orderBy, int offset, int limit) throws DAOException;

    Optional<List<Book>> getBooksByOrderOffsetLimit(BookOrderBy orderBy, int offset, int limit) throws DAOException;

    // After successful insertion id will be assigned
    void insert(Book book, Connection connection) throws DAOException;

    void updateTitleEN(Book book, String newTitleEN) throws DAOException;
    void updateTitleUA(Book book, String newTitleUA) throws DAOException;
    void updateLanguage(Book book, String newLanguage) throws DAOException;
    void updateAuthor(Book book, Author author) throws DAOException;
    void updatePublication(Book book, Publication newPublication) throws DAOException;
    void updatePublicationDate(Book book, Date newDate) throws DAOException;
    void updateCopiesOwned(Book book, Integer newCopiesOwned, Connection connection) throws DAOException;

    // After successful deleting id will be assigned to null
    void delete(Book book, Connection connection) throws DAOException;
}
