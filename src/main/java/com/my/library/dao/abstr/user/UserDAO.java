package com.my.library.dao.abstr.user;

import com.my.library.dao.orderByEnums.UserOrderBy;
import com.my.library.dao.entity.user.User;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface UserDAO {
    Optional<User> get(String login) throws DAOException;
    Optional<User> getByEmail(String email) throws DAOException;
    Optional<List<User>> getUsersByOrderOffsetLimit(UserOrderBy orderBy, int offset, int limit) throws DAOException;

    Optional<List<User>> getAll() throws DAOException;
    Optional<Integer> countAllUsers() throws DAOException;

    // after successful insertion status will be assigned
    void insert(User user) throws DAOException;

    void updateLogin(User user, String newLogin) throws DAOException;
    void updatePassword(User user, String newPassword) throws DAOException;
    void updateEmail(User user, String newEmail) throws DAOException;
    void updateFirstName(User user, String newFirstName) throws DAOException;
    void updateLastName(User user, String newLastName) throws DAOException;
    void updateStatus(User user, UserStatus newUserStatus) throws DAOException;

    // after successful deleting status will be assigned to null
    void delete(User user) throws DAOException;
}
