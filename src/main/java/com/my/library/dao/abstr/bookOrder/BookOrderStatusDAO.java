package com.my.library.dao.abstr.bookOrder;

import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface BookOrderStatusDAO {
    Optional<BookOrderStatus> get(int id) throws DAOException;
    Optional<List<BookOrderStatus>> getAll() throws DAOException;

    boolean insert(BookOrderStatus orderStatus) throws DAOException;

    void updateEN(BookOrderStatus orderStatus, String newValueEN) throws DAOException;
    void updateUA(BookOrderStatus orderStatus, String newValueUA) throws DAOException;

    void delete(BookOrderStatus orderStatus) throws DAOException;
}
