package com.my.library.dao.abstr.user;

import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface UserStatusDAO {
    Optional<UserStatus> get(int id) throws DAOException;
    Optional<UserStatus> getByValue(String value) throws DAOException;
    Optional<List<UserStatus>> getAll() throws DAOException;

    boolean insert(UserStatus status) throws DAOException;

    void update(UserStatus status, String newValue) throws DAOException;

    void delete(UserStatus status) throws DAOException;
}
