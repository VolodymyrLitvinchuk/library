package com.my.library.dao.abstr.fine;

import com.my.library.dao.entity.fine.FinePayment;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface FinePaymentDAO {
    Optional<FinePayment> getByFinePaymentID(int id) throws DAOException;
    Optional<FinePayment> getByFineID(int fineFK) throws DAOException;
    Optional<List<FinePayment>> getAll() throws DAOException;

    // after successful inserting id will be assigned
    void insert(FinePayment payment, Connection connection) throws DAOException;

    void delete(FinePayment payment, Connection connection) throws DAOException;

}
