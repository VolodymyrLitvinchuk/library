package com.my.library.dao.abstr.bookOrder;

import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public interface BookOrderDAO {
    Optional<BookOrder> get(int id) throws DAOException;
    Optional<List<BookOrder>> getAll() throws DAOException;
    Optional<List<BookOrder>> getAllByReaderFK(String readerFK) throws DAOException;
    Optional<List<BookOrder>> getAllByStatus(int statusID) throws DAOException;
    Optional<List<BookOrder>> getAllActiveOrderByOffsetLimit(BookOrderOrderBy orderBy, int offset, int limit) throws DAOException;
    Optional<List<BookOrder>> getAllByReaderOrderByOffsetLimit(BookOrderOrderBy orderBy, int offset, int limit, String readerLogin) throws DAOException;

    Optional<Integer> countReaderOrders(String readerLogin, int orderStatus) throws DAOException;
    Optional<Integer> countAllReaderOrders(String readerLogin) throws DAOException;
    Optional<Integer> countAllActiveOrders() throws DAOException;
    boolean hasReaderUnfinishedOrder(String readerLogin, int bookID) throws DAOException;

    // Check if reader has not-returned books
    boolean userHasUnfinishedOrders(String readerFK) throws DAOException;

    // id will be assigned after successful insertion into DataBase
    void insert(BookOrder bookOrder, Connection connection) throws DAOException;

    void updateType(BookOrder bookOrder, BookOrderType newType) throws DAOException;
    void updateStatus(BookOrder bookOrder, BookOrderStatus newStatus, Connection connection) throws DAOException;

    void delete(BookOrder bookOrder) throws DAOException;
}
