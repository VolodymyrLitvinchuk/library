package com.my.library.dao.abstr.bookOrder;

import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface BookOrderTypeDAO {
    Optional<BookOrderType> get(int id) throws DAOException;
    Optional<List<BookOrderType>> getAll() throws DAOException;

    boolean insert(BookOrderType orderType) throws DAOException;

    void updateEN(BookOrderType orderType, String newValueEN) throws DAOException;
    void updateUA(BookOrderType orderType, String newValueUA) throws DAOException;

    void delete(BookOrderType orderType) throws DAOException;
}
