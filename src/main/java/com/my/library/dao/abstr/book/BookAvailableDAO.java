package com.my.library.dao.abstr.book;

import com.my.library.exception.DAOException;

import java.sql.Connection;
import java.util.Optional;

public interface BookAvailableDAO {
    Optional<Integer> get(int BookId) throws DAOException;
    boolean insert(int BookId, int quantity, Connection connection) throws DAOException;
    boolean update(int BookId, int newQuantity, Connection connection) throws DAOException;
    void delete(int BookId) throws DAOException;
}
