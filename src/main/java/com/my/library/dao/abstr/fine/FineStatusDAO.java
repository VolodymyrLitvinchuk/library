package com.my.library.dao.abstr.fine;

import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.exception.DAOException;

import java.util.List;
import java.util.Optional;

public interface FineStatusDAO {
    Optional<FineStatus> get(int id) throws DAOException;
    Optional<List<FineStatus>> getAll() throws DAOException;

    boolean insert(FineStatus fineStatus) throws DAOException;

    void updateEN(FineStatus fineStatus, String newValueEN) throws DAOException;
    void updateUA(FineStatus fineStatus, String newValueUA) throws DAOException;

    void delete(FineStatus fineStatus) throws DAOException;
}
