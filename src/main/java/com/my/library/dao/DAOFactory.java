package com.my.library.dao;

import com.my.library.dao.abstr.book.*;
import com.my.library.dao.abstr.bookOrder.*;
import com.my.library.dao.abstr.fine.*;
import com.my.library.dao.abstr.user.*;
import com.my.library.dao.mysql.book.*;
import com.my.library.dao.mysql.bookOrder.*;
import com.my.library.dao.mysql.fine.*;
import com.my.library.dao.mysql.user.*;

public final class DAOFactory {
    private DAOFactory(){}

    private static final UserStatusDAO userStatusDAO;
    private static final UserRoleDAO userRoleDAO;
    private static final UserDAO userDAO;

    private static final AuthorDAO authorDAO;
    private static final BookAvailableDAO bookAvailableDAO;
    private static final PublicationDAO publicationDAO;
    private static final BookDAO bookDAO;

    private static final BookOrderStatusDAO bookOrderStatusDAO;
    private static final BookOrderTypeDAO bookOrderTypeDAO;
    private static final BookOrderDAO bookOrderDAO;

    private static final FineStatusDAO fineStatusDAO;
    private static final FineDAO fineDAO;
    private static final FinePaymentDAO finePaymentDAO;

    static {
        userStatusDAO = new MySqlUserStatusDAO();
        userRoleDAO = new MySqlUserRoleDAO();
        userDAO = new MySqlUserDAO();

        authorDAO = new MySqlAuthorDAO();
        bookAvailableDAO = new MySqlBookAvailableDAO();
        publicationDAO = new MySqlPublicationDAO();
        bookDAO = new MySqlBookDAO();

        bookOrderStatusDAO = new MySqlBookOrderStatusDAO();
        bookOrderTypeDAO = new MySqlBookOrderTypeDAO();
        bookOrderDAO = new MySqlBookOrderDAO();

        fineStatusDAO = new MySqlFineStatusDAO();
        fineDAO = new MySqlFineDAO();
        finePaymentDAO = new MySqlFinePaymentDAO();
    }

    public static UserStatusDAO getUserStatusDAO() {
        return userStatusDAO;
    }
    public static UserRoleDAO getUserRoleDAO() {
        return userRoleDAO;
    }
    public static UserDAO getUserDAO() {
        return userDAO;
    }

    public static AuthorDAO getAuthorDAO(){
        return authorDAO;
    }
    public static BookAvailableDAO getBookAvailableDAO(){
        return bookAvailableDAO;
    }
    public static PublicationDAO getPublicationDAO(){
        return publicationDAO;
    }
    public static BookDAO getBookDAO(){return bookDAO;}

    public static BookOrderStatusDAO getBookOrderStatusDAO() {
        return bookOrderStatusDAO;
    }
    public static BookOrderTypeDAO getBookOrderTypeDAO() {
        return bookOrderTypeDAO;
    }
    public static BookOrderDAO getBookOrderDAO() {
        return bookOrderDAO;
    }

    public static FineStatusDAO getFineStatusDAO() {
        return fineStatusDAO;
    }
    public static FineDAO getFineDAO() {
        return fineDAO;
    }
    public static FinePaymentDAO getFinePaymentDAO() {
        return finePaymentDAO;
    }
}
