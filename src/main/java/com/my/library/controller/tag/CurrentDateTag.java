package com.my.library.controller.tag;

import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Custom tag class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class CurrentDateTag extends SimpleTagSupport {
    /**
     * Method writes formatted current date to {@link JspWriter}.
     *
     * @throws IOException if IOException from {@link JspWriter} was thrown.
     */
    @Override
    public void doTag() throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String result = dateFormat.format(date.getTime());

        JspWriter writer = getJspContext().getOut();
        writer.print(result);
    }
}
