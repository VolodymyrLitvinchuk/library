package com.my.library.controller;

import com.my.library.controller.command.Command;
import com.my.library.controller.command.CommandFactory;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.my.library.controller.command.constant.Page.ERROR_PAGE;
import static com.my.library.controller.command.constant.Parameter.COMMAND;

/**
 * Controller class. This class implements front-controller pattern.
 * Chooses {@link Command} to execute and performs redirect or forward, depends on http-method (post or get).
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class Controller extends HttpServlet {
    /**
     * Slf4j logger
     */
    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    /**
     * Method invokes {@link Controller#process(HttpServletRequest)} and then forwards requestDispatcher.
     *
     * @param req User's request.
     * @param resp User's response.
     * @throws ServletException if ServletException from requestDispatcher was thrown.
     * @throws IOException if IOException from requestDispatcher was thrown.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(process(req)).forward(req, resp);
    }

    /**
     * Method invokes {@link Controller#process(HttpServletRequest)} and then sends redirect.
     *
     * @param req User's request.
     * @param resp User's response.
     * @throws IOException if IOException from HttpServletResponse was thrown.
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(process(req));
    }

    /**
     * Method obtains path to use in {@link Controller#doGet(HttpServletRequest, HttpServletResponse)}
     * and {@link Controller#doPost(HttpServletRequest, HttpServletResponse)} methods.
     * If there was an exception while executing a command then this method returns ERROR_PAGE from {@link com.my.library.controller.command.constant.Page}.
     *
     * @param req User's request.
     * @return Path.
     */
    private String process(HttpServletRequest req){
        Command command = CommandFactory.createCommand(req.getParameter(COMMAND));
        String path = ERROR_PAGE;
        try {
            path = command.execute(req);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return path;
    }
}
