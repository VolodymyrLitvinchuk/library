package com.my.library.controller.listener;

import com.my.library.service.ServiceFactory;
import com.my.library.service.abstr.FineService;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * FinesListener class.
 * Implementation of ServletContextListener interface.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class FinesListener implements ServletContextListener {
    /**
     * Slf4j logger.
     */
    private static final Logger log = LoggerFactory.getLogger(FinesListener.class);
    /**
     * Periodicity of {@link CheckFinesTimeTask#run()} invocation in milliseconds.
     * 86400000 milliseconds = 24 hours.
     */
    private static final long period = 86400000;

    /**
     * Method creates {@link Timer} object and schedules {@link CheckFinesTimeTask#run()} invocation.
     * First invocation time calculates by {@link FinesListener#getNextDayMidnight()}
     * and the next invocations will every 24 hours.
     * Also, in case the server was down in 00:00 (when fines check was scheduled),
     * this method performs an additional fines check just after context was initialized.
     *
     * @param sce Application's context event.
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServiceFactory.getFineService().checkFines();
        log.info("Fines were checked");

        ServletContext servletContext = sce.getServletContext();

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new CheckFinesTimeTask(), getNextDayMidnight(), period);

        servletContext.setAttribute("TIMER", timer);
    }

    /**
     * Method invokes {@link Timer#cancel()} if timer object from servletContext attribute is not null.
     *
     * @param sce Application's context event.
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();

        Timer timer = (Timer) servletContext.getAttribute("TIMER");
        if(timer!=null) timer.cancel();

        servletContext.removeAttribute("TIMER");
    }

    /**
     * Method returns next day's midnight in milliseconds.
     *
     * @return Time in milliseconds.
     */
    private Date getNextDayMidnight(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        return cal.getTime();
    }

    /**
     * Task that will be scheduled in {@link Timer#scheduleAtFixedRate(TimerTask, long, long)}.
     *
     * @author Volodymyr Litvinchuk
     * @version 1.0
     */
    private static class CheckFinesTimeTask extends TimerTask{
        /**
         * Method invokes {@link FineService#checkFines()}.
         */
        @Override
        public void run() {
            ServiceFactory.getFineService().checkFines();
            log.info("Fines were checked");
        }
    }
}
