package com.my.library.controller.command;

import com.my.library.controller.command.implementation.admin.updateBook.*;
import com.my.library.controller.command.implementation.base.*;
import com.my.library.controller.command.implementation.librarian.*;
import com.my.library.controller.command.implementation.reader.*;
import com.my.library.controller.command.implementation.admin.*;
import com.my.library.controller.command.implementation.admin.updateAuthor.*;
import com.my.library.controller.command.implementation.updateUser.*;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.CommandName.*;

/**
 * CommandFactory class.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class CommandFactory {
    private static final Map<String, Command> map = new HashMap<>();
    static {
        map.put(SIGN_IN_COMMAND, new SignInCommand());
        map.put(SIGN_OUT_COMMAND, new SignOutCommand());
        map.put(SIGN_UP_READER_COMMAND, new SignUpReaderCommand());
        map.put(SIGN_UP_LIBRARIAN_COMMAND, new SignUpLibrarianCommand());
        map.put(FORGOT_PASSWORD_COMMAND, new ForgotPasswordCommand());

        map.put(UPDATE_USER_LOGIN_COMMAND, new UpdateUserLoginCommand());
        map.put(UPDATE_USER_PASSWORD_COMMAND, new UpdateUserPasswordCommand());
        map.put(UPDATE_USER_EMAIL_COMMAND, new UpdateUserEmailCommand());
        map.put(UPDATE_USER_FIRST_NAME_COMMAND, new UpdateUserFirstNameCommand());
        map.put(UPDATE_USER_LAST_NAME_COMMAND, new UpdateUserLastNameCommand());

        map.put(GET_BOOK_BY_ID_COMMAND, new GetBookByIdCommand());
        map.put(ADD_BOOK_COMMAND, new AddBookCommand());
        map.put(SHOW_BOOKS_COMMAND, new ShowBooksCommand());
        map.put(SEARCH_BOOKS_COMMAND, new SearchBooksCommand());
        map.put(UPDATE_BOOK_COMMAND, new UpdateBookCommand());
        map.put(DELETE_BOOK_COMMAND, new DeleteBookCommand());
        map.put(UPDATE_BOOK_TITLE_EN_COMMAND, new UpdateTitleEnCommand());
        map.put(UPDATE_BOOK_TITLE_UA_COMMAND, new UpdateTitleUaCommand());
        map.put(UPDATE_BOOK_LANGUAGE_COMMAND, new UpdateBookLanguageCommand());
        map.put(UPDATE_COPIES_OWNED_COMMAND, new UpdateCopiesOwnedCommand());
        map.put(UPDATE_BOOK_AUTHOR_ID_COMMAND, new UpdateBookAuthorIdCommand());
        map.put(UPDATE_BOOK_PUBLICATION_ID_COMMAND, new UpdateBookPublicationIdCommand());
        map.put(UPDATE_PUBLICATION_DATE_COMMAND, new UpdatePublicationDateCommand());

        map.put(SHOW_USERS_COMMAND, new ShowUsersCommand());

        map.put(BLOCK_USER_COMMAND, new BlockUserCommand());
        map.put(UNBLOCK_USER_COMMAND, new UnblockUserCommand());
        map.put(DELETE_USER_COMMAND, new DeleteUserCommand());

        map.put(SHOW_AUTHORS_COMMAND, new ShowAuthorsCommand());
        map.put(UPDATE_AUTHOR_COMMAND, new UpdateAuthorCommand());
        map.put(UPDATE_AUTHOR_FIRST_NAME_EN_COMMAND, new UpdateAuthorFirstNameEnCommand());
        map.put(UPDATE_AUTHOR_FIRST_NAME_UA_COMMAND, new UpdateAuthorFirstNameUaCommand());
        map.put(UPDATE_AUTHOR_LAST_NAME_EN_COMMAND, new UpdateAuthorLastNameEnCommand());
        map.put(UPDATE_AUTHOR_LAST_NAME_UA_COMMAND, new UpdateAuthorLastNameUaCommand());
        map.put(DELETE_AUTHOR_COMMAND, new DeleteAuthorCommand());

        map.put(GO_TO_BOOK_ORDER_PAGE_COMMAND, new GoToBookOrderPageCommand());
        map.put(ORDER_BOOK_COMMAND, new BookOrderCommand());
        map.put(SHOW_ORDERS_COMMAND, new ShowOrdersCommand());
        map.put(CANCEL_ORDER_COMMAND, new CancelOrderCommand());
        map.put(ISSUE_BOOK_COMMAND, new IssueBookCommand());
        map.put(RETURN_BOOK_READER_COMMAND, new ReturnBookReaderCommand());
        map.put(RETURN_BOOK_LIBRARIAN_COMMAND, new ReturnBookLibrarianCommand());
        map.put(PAY_FINE_COMMAND, new PayFineCommand());

        map.put(SHOW_FINES_COMMAND, new ShowFinesCommand());
    }

    /**
     * Method returns {@link Command} implementation by CommandName passed in param.
     * If there is no such command then return {@link DefaultCommand}.
     *
     * @param commandName Command name.
     * @return {@link Command} implementation.
     */
    public static Command createCommand(String commandName) {
        return map.getOrDefault(commandName, new DefaultCommand());
    }
}
