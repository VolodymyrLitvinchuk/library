package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.util.orderByFactory.AuthorOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.AUTHORS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.ValidatorUtil.validateShowingParams;

/**
 * ShowAuthorsCommand class.
 * Used to get list of authors.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ShowAuthorsCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ShowAuthorsCommand.class);

    /**
     * Method sets list of authors to request attribute.
     * First of all method gets required params from request and validates them.
     * Then method gets {@link AuthorOrderBy} enum depends on request params.
     * Then method gets list of authors and sets it in request params.
     *
     * @param request User's request.
     * @return AUTHORS_PAGE from {@link com.my.library.controller.command.constant.Page}
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String sortType = request.getParameter(SORT_TYPE);
        String sortOrder = request.getParameter(SORT_ORDER);
        String pageSize = request.getParameter(PAGE_SIZE);
        String currentPage = request.getParameter(CURRENT_PAGE);
        String locale = (String) request.getSession().getAttribute(LOCALE);

        try {
            validateShowingParams(sortType, sortOrder, pageSize, currentPage);

            int pageSizeInt = Integer.parseInt(pageSize);
            int currentPageInt = Integer.parseInt(currentPage);
            int offset = (pageSizeInt*currentPageInt)-pageSizeInt;

            AuthorOrderBy orderBy = AuthorOrderByTypeFactory.getOrderByType(sortType, sortOrder, locale);

            request.setAttribute(AUTHORS, ServiceFactory.getAuthorService().getAuthors(orderBy,offset,pageSizeInt));
            request.setAttribute(LAST_PAGE, ServiceFactory.getAuthorService().getLastPageNumber(pageSizeInt));

            request.setAttribute(CURRENT_PAGE, currentPageInt);
            request.setAttribute(SORT_TYPE, sortType);
            request.setAttribute(SORT_ORDER, sortOrder);
            request.setAttribute(PAGE_SIZE, pageSize);
            request.setAttribute(ERROR, request.getParameter(ERROR));
            request.setAttribute(MESSAGE, request.getParameter(MESSAGE));

        }catch (ServiceException e){
            log.warn("Exception while ShowAuthorsCommand",e);
            request.setAttribute(ERROR, e.getMessage());
        }
        return AUTHORS_PAGE;
    }
}
