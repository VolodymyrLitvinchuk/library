package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.SIGN_IN_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.exception.constant.Message.ACCOUNT_LOCKED;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * SignInCommand class.
 * Used to perform singing in.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class SignInCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(SignInCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return SIGN_IN_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, LOGIN);
        transferStringFromSessionToRequest(request, ERROR);
        return SIGN_IN_PAGE;
    }

    /**
     * Method performs singing in.
     * First of all method gets required params from request and validates them.
     * Then method performs singing in using Service layer.
     * If there was an exception thrown from Service layer then return
     * "GET-path" to this command to execute executeGet() method from this class to display en error.
     * If signing in was successful then this method sets {@link UserDTO} to session.
     *
     * @param request User's request.
     * @return MAIN_PAGE from {@link com.my.library.controller.command.constant.Page} if signing in was successful.
     */
    private String executePost(HttpServletRequest request){
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        try {
            UserDTO userDTO = ServiceFactory.getUserService().signIn(login, password);
            request.getSession().setAttribute(LOGGED_USER,userDTO);
            return MAIN_PAGE;

        } catch (ServiceException e) {
            log.warn("Exception while SignInCommand",e);
            if(e.getMessage().equals(ACCOUNT_LOCKED)) return ACCOUNT_LOCKED_PAGE;
            request.getSession().setAttribute(ERROR, e.getMessage());
            request.getSession().setAttribute(LOGIN,login);
        }
        return CONTROLLER_PAGE + "?" + "command=" + SIGN_IN_COMMAND;
    }

}
