package com.my.library.controller.command.constant;

/**
 * Parameter class.
 * Contains all parameters' names.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class Parameter {
    private Parameter(){}

    public static final String COMMAND = "command";
    public static final String LOGIN = "login";
    public static final String NEW_LOGIN = "new-login";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PASSWORD = "password";
    public static final String CURRENT_PASSWORD = "current-password";
    public static final String NEW_PASSWORD = "new-password";
    public static final String REPEAT_PASSWORD = "repeat-password";
    public static final String ERROR = "error";
    public static final String MESSAGE = "message";
    public static final String LOGGED_USER = "loggedUser";
    public static final String LOCALE = "locale";

    // for Book adding
    public static final String BOOK_ID = "bookId";
    public static final String TITLE_EN = "titleEN";
    public static final String TITLE_UA = "titleUA";
    public static final String LANGUAGE = "language";
    public static final String COPIES_OWNED = "copiesOwned";
    public static final String AUTHOR_FIRST_NAME_EN = "authorFirstNameEN";
    public static final String AUTHOR_FIRST_NAME_UA = "authorFirstNameUA";
    public static final String AUTHOR_LAST_NAME_EN = "authorLastNameEN";
    public static final String AUTHOR_LAST_NAME_UA = "authorLastNameUA";
    public static final String PUBLICATION_DATE = "publicationDate";
    public static final String PUBLICATION_NAME_EN = "publicationNameEN";
    public static final String PUBLICATION_NAME_UA = "publicationNameUA";
    public static final String AUTHOR_ID = "authorId";
    public static final String PUBLICATION_ID = "publicationId";

    public static final String NEW_TITLE_EN = "newTitleEN";
    public static final String NEW_TITLE_UA = "newTitleUA";
    public static final String NEW_LANGUAGE = "newLanguage";
    public static final String NEW_AUTHOR_ID = "newAuthorId";
    public static final String NEW_PUBLICATION_ID = "newPublicationId";
    public static final String NEW_COPIES_OWNED = "newCopiesOwned";
    public static final String NEW_PUBLICATION_DATE = "newPublicationDate";

    public static final String NEW_AUTHOR_FIRST_NAME_EN = "newAuthorFirstNameEN";
    public static final String NEW_AUTHOR_FIRST_NAME_UA = "newAuthorFirstNameUA";
    public static final String NEW_AUTHOR_LAST_NAME_EN = "newAuthorLastNameEN";
    public static final String NEW_AUTHOR_LAST_NAME_UA = "newAuthorLastNameUA";


    public static final String SORT_TYPE = "sortType";
    public static final String SORT_ORDER = "sortOrder";
    public static final String PAGE_SIZE = "itemsPerPage";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String LAST_PAGE = "lastPage";
    public static final String BOOKS = "books";
    public static final String USERS = "users";
    public static final String AUTHORS = "authors";
    public static final String ORDERS = "orders";
    public static final String FINES = "fines";

    // Book order
    public static final String ORDER_TYPE = "orderType";
    public static final String ORDER_DAYS = "orderDays";
    public static final String FINE_AMOUNT = "fineAmount";

    public static final String BOOK_ORDER_ID = "bookOrderId";

    public static final String TITLE_PATTERN = "titlePattern";
    public static final String AUTHOR_FIRST_NAME_PATTERN = "authorFirstNamePattern";
    public static final String AUTHOR_LAST_NAME_PATTERN = "authorLastNamePattern";
}
