package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.FORGOT_PASSWORD_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.PASSWORD_WAS_SENT_SUCCESSFULLY;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * ForgotPasswordCommand class.
 * Used to perform generating new password and sending it to user by email.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class ForgotPasswordCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ForgotPasswordCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return FORGOT_PASSWORD_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, LOGIN);
        transferStringFromSessionToRequest(request, ERROR);
        return FORGOT_PASSWORD_PAGE;
    }

    /**
     * Method performs generating new password and sending it to user by email.
     * First of all method gets required param from request.
     * Then method performs generating new password and sending it to user by email Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        String login = request.getParameter(LOGIN);

        try {
            ServiceFactory.getUserService().generateAndSendNewPassword(login);
            request.getSession().setAttribute(MESSAGE, PASSWORD_WAS_SENT_SUCCESSFULLY);

        } catch (ServiceException e) {
            log.warn("Exception while ForgotPasswordCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
            request.getSession().setAttribute(LOGIN,login);
        }
        return CONTROLLER_PAGE + "?" + "command=" + FORGOT_PASSWORD_COMMAND;
    }
}
