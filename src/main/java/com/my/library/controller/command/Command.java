package com.my.library.controller.command;

import com.my.library.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;

/**
 * Command interface.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface Command {
    /**
     * Method should return path that will be used in {@link com.my.library.controller.Controller} for sendRedirect() or forward.
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    String execute(HttpServletRequest request) throws ServiceException;
}