package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.SIGN_UP_LIBRARIAN_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_REGISTER_LIBRARIAN;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ConvertorUtil.convertRequestToUserDTOLibrarian;

/**
 * SignUpLibrarianCommand class.
 * Used to perform signing up librarian.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class SignUpLibrarianCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(SignUpLibrarianCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_BOOK_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferUserFieldsFromSessionToRequest(request);
        transferStringFromSessionToRequest(request, ERROR);
        transferStringFromSessionToRequest(request, MESSAGE);
        return SIGN_UP_LIBRARIAN_PAGE;
    }

    /**
     * Method performs signing up librarian.
     * First of all method gets required params from request and validates them.
     * Then method performs signing up librarian using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = convertRequestToUserDTOLibrarian(request);
        String password = request.getParameter(PASSWORD);
        String repeatPassword = request.getParameter(REPEAT_PASSWORD);

        try {
            ServiceFactory.getUserService().signUp(userDTO,password,repeatPassword);
            request.getSession().setAttribute(MESSAGE, SUCCEED_REGISTER_LIBRARIAN);
            log.info("New librarian with login={} was registered", userDTO.getLogin());

        }catch (ServiceException ex){
            log.warn("Exception while SignUpLibrarianCommand",ex);
            setUserFieldsSessionAttribute(request);
            request.getSession().setAttribute(ERROR,ex.getMessage());
        }
        return CONTROLLER_PAGE + "?" + "command=" + SIGN_UP_LIBRARIAN_COMMAND;
    }
}
