package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;

import static com.my.library.controller.command.constant.CommandName.UPDATE_AUTHOR_COMMAND;
import static com.my.library.controller.command.constant.Page.EDIT_AUTHOR_PAGE;
import static com.my.library.util.CommandUtil.setAuthorEditingFieldsSessionAttribute;
import static com.my.library.util.CommandUtil.transferAuthorEditingFieldsFromSessionToRequest;

/**
 * UpdateAuthorCommand class.
 * Used to perform redirecting to author's edit-page.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateAuthorCommand implements Command {
    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_AUTHOR_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferAuthorEditingFieldsFromSessionToRequest(request);
        return EDIT_AUTHOR_PAGE;
    }

    /**
     * Method transfers attributes from request to session.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        setAuthorEditingFieldsSessionAttribute(request);
        return "controller?command="+ UPDATE_AUTHOR_COMMAND;
    }
}
