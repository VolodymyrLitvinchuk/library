package com.my.library.controller.command.implementation.admin.updateBook;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.UPDATE_COPIES_OWNED_COMMAND;
import static com.my.library.controller.command.constant.Page.EDIT_BOOK_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ValidatorUtil.*;

/**
 * UpdateCopiesOwnedCommand class.
 * Used to perform updating book's copies owned.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateCopiesOwnedCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UpdateCopiesOwnedCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_BOOK_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferBookEditingFieldsFromSessionToRequest(request);
        transferStringFromSessionToRequest(request, COPIES_OWNED, NEW_COPIES_OWNED);
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return EDIT_BOOK_PAGE;
    }

    /**
     * Method performs updating book's copies owned.
     * First of all method gets required params from request and validates them.
     * Then method performs updating using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int bookId = Integer.parseInt(request.getParameter(BOOK_ID));
        int copiesOwned = Integer.parseInt(request.getParameter(NEW_COPIES_OWNED));

        try {
            validateCopiesOwned(copiesOwned);
            validateBookID(bookId);

            ServiceFactory.getBookService().updateBookCopiesOwned(bookId, copiesOwned);

            request.getSession().setAttribute(NEW_COPIES_OWNED, copiesOwned+"");
            request.getSession().setAttribute(MESSAGE, SUCCEED_UPDATE);

        }catch (ServiceException e){
            log.warn("Exception while UpdateCopiesOwnedCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        setBookEditingFieldsSessionAttribute(request);
        return "controller?command="+ UPDATE_COPIES_OWNED_COMMAND;
    }
}
