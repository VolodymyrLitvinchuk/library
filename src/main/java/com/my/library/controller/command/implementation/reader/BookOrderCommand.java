package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.dto.BookOrderDTO;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.ORDER_BOOK_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.dao.mysql.Constant.FINE_VALUE_PER_DAY;
import static com.my.library.dao.mysql.Constant.SUSPENDED_USER_STATUS;
import static com.my.library.exception.constant.Message.BOOK_ALREADY_ORDERED;
import static com.my.library.exception.constant.Message.UNKNOWN_ERROR;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ConvertorUtil.convertRequestToBookOrderDTO;

/**
 * BookOrderCommand class.
 * Used to perform book ordering.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class BookOrderCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(BookOrderCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     */
    @Override
    public String execute(HttpServletRequest request){
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return BOOK_ORDER_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null || !userDTO.getRole().equals("Reader")
                || userDTO.getStatus().equals(SUSPENDED_USER_STATUS)) return BOOKS_PAGE;

        transferGoToBookOrderFieldsFromSessionToRequest(request);
        request.setAttribute(FINE_AMOUNT, FINE_VALUE_PER_DAY +"");
        transferStringFromSessionToRequest(request, ERROR);
        return BOOK_ORDER_PAGE;
    }

    /**
     * Method performs book ordering.
     * First of all method gets required params from request and validates them.
     * Then method performs book ordering using Service layer.
     * If there was an exception thrown from Service layer then this method
     * returns "GET-path" to this command to execute executeGet() method from this class.
     *
     * @param request User's request.
     * @return ORDERS_PAGE from {@link com.my.library.controller.command.constant.Page} if successful.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null || !userDTO.getRole().equals("Reader")
                || userDTO.getStatus().equals(SUSPENDED_USER_STATUS)) return BOOKS_PAGE;

        try{
            BookOrderDTO bookOrderDTO = convertRequestToBookOrderDTO(request);
            if(bookOrderDTO==null) throw new ServiceException(UNKNOWN_ERROR);
            ServiceFactory.getBookOrderService().insert(bookOrderDTO);

        }catch (ServiceException e){
            log.warn("Exception while BookOrderCommand",e);
            if(e.getMessage().equals(BOOK_ALREADY_ORDERED)) return ORDERS_PAGE;
            setGoToBookOrderFieldsSessionAttribute(request);
            request.getSession().setAttribute(ERROR, e.getMessage());
            return "controller?command="+ ORDER_BOOK_COMMAND;
        }
        return ORDERS_PAGE;
    }
}
