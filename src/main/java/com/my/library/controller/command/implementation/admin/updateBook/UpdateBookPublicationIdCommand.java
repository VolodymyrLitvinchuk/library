package com.my.library.controller.command.implementation.admin.updateBook;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.UPDATE_BOOK_PUBLICATION_ID_COMMAND;
import static com.my.library.controller.command.constant.Page.EDIT_BOOK_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ValidatorUtil.*;

/**
 * UpdateBookPublicationIdCommand class.
 * Used to perform updating book's publication's id.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateBookPublicationIdCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UpdateBookPublicationIdCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_BOOK_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferBookEditingFieldsFromSessionToRequest(request);
        transferStringFromSessionToRequest(request, PUBLICATION_ID, NEW_PUBLICATION_ID);
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return EDIT_BOOK_PAGE;
    }

    /**
     * Method performs updating book's publication's id.
     * First of all method gets required params from request and validates them.
     * Then method performs updating using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int bookId = Integer.parseInt(request.getParameter(BOOK_ID));
        int newPublicationID = Integer.parseInt(request.getParameter(NEW_PUBLICATION_ID));

        try {
            validatePublicationID(newPublicationID);
            validateBookID(bookId);

            ServiceFactory.getBookService().updateBookPublicationId(bookId, newPublicationID);

            request.getSession().setAttribute(NEW_PUBLICATION_ID, newPublicationID+"");
            request.getSession().setAttribute(MESSAGE, SUCCEED_UPDATE);

        }catch (ServiceException e){
            log.warn("Exception while UpdateBookPublicationIdCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        setBookEditingFieldsSessionAttribute(request);
        return "controller?command="+ UPDATE_BOOK_PUBLICATION_ID_COMMAND;
    }
}
