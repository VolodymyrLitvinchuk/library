package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.RETURN_BOOK_READER_COMMAND;
import static com.my.library.controller.command.constant.Page.ORDERS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * ReturnBookReaderCommand class.
 * Used to perform returning book by reader.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ReturnBookReaderCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ReturnBookReaderCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return ORDERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, ERROR);
        return ORDERS_PAGE;
    }

    /**
     * Method performs returning book by reader.
     * First of all method gets required param from request.
     * Then method performs returning book by reader using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int orderID = Integer.parseInt(request.getParameter(BOOK_ORDER_ID));

        try {
            ServiceFactory.getBookOrderService().returnBookReader(orderID);

        }catch (ServiceException e){
            log.warn("Exception while ReturnBookReaderCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        return "controller?command="+ RETURN_BOOK_READER_COMMAND;
    }
}
