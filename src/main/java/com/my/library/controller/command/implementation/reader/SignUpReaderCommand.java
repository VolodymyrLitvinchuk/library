package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.SIGN_UP_READER_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ConvertorUtil.convertRequestToUserDTO;

/**
 * SignUpReaderCommand class.
 * Used to perform signing up new reader.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class SignUpReaderCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(SignUpReaderCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request to display error.
     *
     * @param request User's request.
     * @return SIGN_UP_READER_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferUserFieldsFromSessionToRequest(request);
        transferStringFromSessionToRequest(request, ERROR);
        return SIGN_UP_READER_PAGE;
    }

    /**
     * Method performs signing up new reader.
     * First of all method gets required params from request.
     * Then method performs signing up new reader using Service layer.
     * If there was an exception thrown from Service layer then this method returns
     * "GET-path" to this command to execute executeGet() method from this class.
     *
     * @param request User's request.
     * @return SIGN_IN_PAGE from {@link com.my.library.controller.command.constant.Page} if successful.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = convertRequestToUserDTO(request);
        String password = request.getParameter(PASSWORD);
        String repeatPassword = request.getParameter(REPEAT_PASSWORD);

        try {
            ServiceFactory.getUserService().signUp(userDTO,password,repeatPassword);
            log.info("New reader was registered. Login={}",userDTO.getLogin());
            return SIGN_IN_PAGE;

        }catch (ServiceException ex){
            log.warn("Exception while SignUpReaderCommand",ex);
            setUserFieldsSessionAttribute(request);
            request.getSession().setAttribute(ERROR,ex.getMessage());
        }
        return CONTROLLER_PAGE + "?" + "command=" + SIGN_UP_READER_COMMAND;
    }
}
