package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.GO_TO_BOOK_ORDER_PAGE_COMMAND;
import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.dao.mysql.Constant.FINE_VALUE_PER_DAY;
import static com.my.library.dao.mysql.Constant.SUSPENDED_USER_STATUS;
import static com.my.library.exception.constant.Message.READER_IS_SUSPENDED;
import static com.my.library.util.CommandUtil.*;

/**
 * GoToBookOrderPageCommand class.
 * Used to perform redirecting to book ordering page with params.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class GoToBookOrderPageCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(GoToBookOrderPageCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return BOOK_ORDER_PAGE from {@link com.my.library.controller.command.constant.Page} if successful
     */
    private String executeGet(HttpServletRequest request) {
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null || !userDTO.getRole().equals("Reader")) return BOOKS_PAGE;
        if(userDTO.getStatus().equals(SUSPENDED_USER_STATUS)){
            request.setAttribute(ERROR, READER_IS_SUSPENDED);
            return BOOKS_PAGE;
        }

        transferGoToBookOrderFieldsFromSessionToRequest(request);
        request.setAttribute(FINE_AMOUNT, FINE_VALUE_PER_DAY +"");
        return BOOK_ORDER_PAGE;
    }

    /**
     * Method performs redirecting to book ordering page with params.
     * Method gets required params from request and sets them in session.
     * Also, method check user's permission and checks whether this reader has already ordered this book.
     * If reader has already ordered this book then this method returns
     * ORDERS_PAGE from {@link com.my.library.controller.command.constant.Page}.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class if successful.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null || !userDTO.getRole().equals("Reader")) return BOOKS_PAGE;
        if(userDTO.getStatus().equals(SUSPENDED_USER_STATUS)) return "controller?command="+ GO_TO_BOOK_ORDER_PAGE_COMMAND;

        try{
            if (ServiceFactory.getBookOrderService()
                    .isAlreadyOrdered(
                            userDTO.getLogin(),
                            Integer.parseInt(request.getParameter(BOOK_ID)))) return ORDERS_PAGE;

        }catch (Exception e){
            log.warn("Exception while GoToBookOrderPageCommand", e);
            return BOOKS_PAGE;
        }

        setGoToBookOrderFieldsSessionAttribute(request);
        return "controller?command="+ GO_TO_BOOK_ORDER_PAGE_COMMAND;
    }
}
