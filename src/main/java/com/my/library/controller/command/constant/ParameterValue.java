package com.my.library.controller.command.constant;

/**
 * ParameterValue class.
 * Contains parameters' values.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class ParameterValue {
    public static final String SUCCEED_UPDATE = "succeed.update";
    public static final String SUCCEED_DELETE = "succeed.delete";
    public static final String SUCCEED_ADD_BOOK = "succeed.add.book";
    public static final String SUCCEED_REGISTER_LIBRARIAN = "succeed.register.librarian";
    public static final String SUCCEED_CANCELED = "succeed.canceled";
    public static final String SUCCEED_ISSUED = "succeed.issued";
    public static final String SUCCEED_RETURNED = "succeed.returned";
    public static final String SUCCEED_PAID = "succeed.paid";
    public static final String PASSWORD_WAS_SENT_SUCCESSFULLY = "new.password.was.sent.successfully";

    public static final String ACS_ORDER = "ascending";
    public static final String DESC_ORDER = "descending";

    public static final String SORT_BY_ID = "byId";
    public static final String SORT_BY_TITLE = "byTitle";
    public static final String SORT_BY_AUTHOR = "byAuthor";
    public static final String SORT_BY_PUBLICATION = "byPublication";
    public static final String SORT_BY_PUBLICATION_DATE = "byPublicationDate";
    public static final String SORT_BY_LOGIN = "byLogin";
    public static final String SORT_BY_FIRST_NAME = "byFirstName";
    public static final String SORT_BY_LAST_NAME = "byLastName";
    public static final String SORT_BY_EMAIL = "byEmail";

    public static final String LOCALE_UA = "ua";
    public static final String LOCALE_EN = "en";

    public static final String SORT_BY_BOOK_ID = "byBookId";
    public static final String SORT_BY_READER = "byReader";
    public static final String SORT_BY_BOOK_ORDER_TYPE = "byOrderType";
    public static final String SORT_BY_BOOK_ORDER_DATE = "byOrderDate";
    public static final String SORT_BY_BOOK_ORDER_STATUS = "byOrderStatus";

    public static final String SORT_BY_ORDER_ID = "byOrderId";
    public static final String SORT_BY_AMOUNT = "byAmount";
    public static final String SORT_BY_STATUS = "byStatus";

    private ParameterValue() {}
}