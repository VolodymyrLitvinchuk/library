package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.dto.BookDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.BOOKS_PAGE;
import static com.my.library.controller.command.constant.Page.BOOK_BY_ID_PAGE;
import static com.my.library.controller.command.constant.Parameter.BOOK_ID;
import static com.my.library.exception.constant.Message.UNKNOWN_ERROR;
import static com.my.library.util.CommandUtil.setBookFieldToRequest;
import static com.my.library.util.ValidatorUtil.validateBookID;

/**
 * GetBookByIdCommand class.
 * Used to perform getting book by id.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class GetBookByIdCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(GetBookByIdCommand.class);

    /**
     * Method performs getting book by id.
     * First of all method gets required param from request and validates it.
     * Then method performs getting book by id using Service layer.
     *
     * @param request User's request.
     * @return BOOK_BY_ID_PAGE from {@link com.my.library.controller.command.constant.Page}.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String bookId = request.getParameter(BOOK_ID);

        try {
            int id = Integer.parseInt(bookId);
            validateBookID(id);

            BookDTO bookDTO = ServiceFactory.getBookService().getBook(id);
            if(bookDTO==null) throw new ServiceException(UNKNOWN_ERROR);

            request.setAttribute(BOOK_ID, bookId);
            setBookFieldToRequest(request,bookDTO);

        }catch (ServiceException e){
            log.warn("Exception while GetBookByIdCommand", e);
            return BOOKS_PAGE;
        }
        return BOOK_BY_ID_PAGE;
    }
}
