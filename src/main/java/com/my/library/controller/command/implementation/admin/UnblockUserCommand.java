package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.controller.command.constant.Parameter;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.USERS_PAGE;
import static com.my.library.dao.mysql.Constant.ACTIVE_USER_STATUS;

/**
 * UnblockUserCommand class.
 * Used to perform unblocking user.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UnblockUserCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UnblockUserCommand.class);

    /**
     * Method performs unblocking user.
     * First of all method gets login param from request.
     * Then method performs unblocking user using Service layer.
     *
     * @param request User's request.
     * @return USERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String login = request.getParameter(Parameter.LOGIN);

        try {
            ServiceFactory.getUserService().updateStatus(login, ACTIVE_USER_STATUS);
            log.info("User with login={} was unblocked", login);

        }catch (ServiceException e){
            log.warn("Exception while UnblockUserCommand",e);
            request.setAttribute(Parameter.ERROR, e.getMessage());
        }
        return USERS_PAGE;
    }
}
