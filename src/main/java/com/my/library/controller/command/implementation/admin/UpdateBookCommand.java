package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;

import static com.my.library.controller.command.constant.CommandName.UPDATE_BOOK_COMMAND;
import static com.my.library.controller.command.constant.Page.EDIT_BOOK_PAGE;
import static com.my.library.util.CommandUtil.*;

/**
 * UpdateBookCommand class.
 * Used to perform redirecting to book's edit-page.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateBookCommand implements Command {
    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_BOOK_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferBookEditingFieldsFromSessionToRequest(request);
        return EDIT_BOOK_PAGE;
    }

    /**
     * Method transfers attributes from request to session.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        setBookEditingFieldsSessionAttribute(request);
        return "controller?command="+ UPDATE_BOOK_COMMAND;
    }
}
