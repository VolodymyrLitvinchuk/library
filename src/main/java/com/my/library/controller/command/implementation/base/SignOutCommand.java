package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import jakarta.servlet.http.HttpServletRequest;

import static com.my.library.controller.command.constant.Page.MAIN_PAGE;
import static com.my.library.controller.command.constant.Parameter.LOGGED_USER;

/**
 * SignOutCommand class.
 * Used to perform signing out.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class SignOutCommand implements Command {
    /**
     * Method removes {@link com.my.library.dto.UserDTO} object from session.
     *
     * @param request User's request.
     * @return MAIN_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(LOGGED_USER,null);
        return MAIN_PAGE;
    }
}
