package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.DELETE_BOOK_COMMAND;
import static com.my.library.controller.command.constant.Page.BOOKS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_DELETE;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ValidatorUtil.validateBookID;

/**
 * DeleteBookCommand class.
 * Used to perform deleting book.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class DeleteBookCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(DeleteBookCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return BOOKS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return BOOKS_PAGE;
    }

    /**
     * Method performs deleting book.
     * First of all method gets required param from request and validates it.
     * Then method performs deleting using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int bookId = Integer.parseInt(request.getParameter(BOOK_ID));

        try {
            validateBookID(bookId);
            ServiceFactory.getBookService().deleteBook(bookId);

            request.getSession().setAttribute(MESSAGE, SUCCEED_DELETE);
            log.info("Book with id={} was deleted",bookId);

        }catch (ServiceException e){
            log.warn("Exception while DeleteBookCommand. BookID={}",bookId, e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        return "controller?command="+ DELETE_BOOK_COMMAND;
    }
}
