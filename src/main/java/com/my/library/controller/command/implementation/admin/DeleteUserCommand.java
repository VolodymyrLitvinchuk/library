package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.controller.command.constant.Parameter;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.DELETE_USER_COMMAND;
import static com.my.library.controller.command.constant.Page.USERS_PAGE;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_DELETE;
import static com.my.library.exception.constant.Message.ENTER_CORRECT_LOGIN;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;
import static com.my.library.util.ValidatorUtil.validateFormat;
import static com.my.library.util.constant.Regex.LOGIN_REGEX;

/**
 * DeleteUserCommand class.
 * Used to perform deleting user.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class DeleteUserCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(DeleteUserCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return USERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, Parameter.MESSAGE);
        transferStringFromSessionToRequest(request, Parameter.ERROR);
        return USERS_PAGE;
    }

    /**
     * Method performs deleting user.
     * First of all method gets required param from request and validates it.
     * Then method performs deleting using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        String login = request.getParameter(Parameter.LOGIN);

        try {
            validateFormat(login, LOGIN_REGEX, ENTER_CORRECT_LOGIN);
            ServiceFactory.getUserService().deleteUser(login);
            request.getSession().setAttribute(Parameter.MESSAGE, SUCCEED_DELETE);
            log.info("User with login={} was deleted", login);

        }catch (ServiceException e){
            log.warn("Exception while DeleteUserCommand. User login={}", login, e);
            request.getSession().setAttribute(Parameter.ERROR, e.getMessage());
        }
        return "controller?command="+DELETE_USER_COMMAND;
    }
}
