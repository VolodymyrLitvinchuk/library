package com.my.library.controller.command.implementation.updateUser;

import com.my.library.controller.command.Command;
import com.my.library.controller.command.constant.CommandName;
import com.my.library.controller.command.constant.Page;
import com.my.library.controller.command.constant.Parameter;
import com.my.library.controller.command.constant.ParameterValue;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * UpdateUserPasswordCommand class.
 * Used to perform updating user's password.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateUserPasswordCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UpdateUserPasswordCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);

    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return PROFILE_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, Parameter.MESSAGE);
        transferStringFromSessionToRequest(request, Parameter.ERROR);
        return Page.PROFILE_PAGE;
    }

    /**
     * Method performs updating user's password.
     * First of all method gets required params from request.
     * Then method performs updating and validation using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(Parameter.LOGGED_USER);
        String oldPassword = request.getParameter(Parameter.CURRENT_PASSWORD);
        String newPassword = request.getParameter(Parameter.NEW_PASSWORD);
        String repeatPassword = request.getParameter(Parameter.REPEAT_PASSWORD);

        try{
            ServiceFactory.getUserService().updatePassword(userDTO, oldPassword, newPassword, repeatPassword);
            request.getSession().setAttribute(Parameter.MESSAGE, ParameterValue.SUCCEED_UPDATE);

        }catch (ServiceException e){
            log.warn("Exception while UpdateUserPasswordCommand", e);
            request.getSession().setAttribute(Parameter.ERROR, e.getMessage());
        }
        return "controller"+"?"+"command="+ CommandName.UPDATE_USER_PASSWORD_COMMAND;
    }
}
