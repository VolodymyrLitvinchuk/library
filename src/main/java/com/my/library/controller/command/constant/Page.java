package com.my.library.controller.command.constant;

/**
 * Page class.
 * Contains all pages' names.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class Page {
    private Page(){}

    public static final String SIGN_IN_PAGE = "signIn.jsp";
    public static final String SIGN_UP_READER_PAGE = "signUp.jsp";
    public static final String SIGN_UP_LIBRARIAN_PAGE = "signUpLibrarian.jsp";
    public static final String MAIN_PAGE = "index.jsp";
    public static final String PROFILE_PAGE = "profile.jsp";
    public static final String ERROR_PAGE = "error.jsp";
    public static final String CONTROLLER_PAGE = "controller";
    public static final String ADD_BOOK_PAGE = "addBook.jsp";
    public static final String EDIT_BOOK_PAGE = "editBook.jsp";
    public static final String BOOKS_PAGE = "books.jsp";
    public static final String USERS_PAGE = "users.jsp";
    public static final String AUTHORS_PAGE = "authors.jsp";
    public static final String EDIT_AUTHOR_PAGE = "editAuthor.jsp";
    public static final String ACCOUNT_LOCKED_PAGE = "accountLocked.jsp";
    public static final String BOOK_ORDER_PAGE = "orderBook.jsp";
    public static final String ORDERS_PAGE = "orders.jsp";
    public static final String BOOK_BY_ID_PAGE = "bookById.jsp";
    public static final String ACCESS_DENIED_PAGE = "accessDenied.jsp";
    public static final String FORGOT_PASSWORD_PAGE = "forgotPassword.jsp";
    public static final String CONTACT_US_PAGE = "contactUs.jsp";
    public static final String FINES_PAGE = "fines.jsp";
}
