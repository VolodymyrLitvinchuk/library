package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.util.orderByFactory.BookOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.BOOKS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.ValidatorUtil.validateShowingParams;

/**
 * ShowBooksCommand class.
 * Used to get list of books.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ShowBooksCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ShowBooksCommand.class);

    /**
     * Method sets list of books to request attribute.
     * First of all method gets required params from request and validates them.
     * Then method gets {@link BookOrderBy} enum depends on request params.
     * Then method gets list of books and sets it in request params.
     *
     * @param request User's request.
     * @return BOOKS_PAGE from {@link com.my.library.controller.command.constant.Page}
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String sortType = request.getParameter(SORT_TYPE);
        String sortOrder = request.getParameter(SORT_ORDER);
        String pageSize = request.getParameter(PAGE_SIZE);
        String currentPage = request.getParameter(CURRENT_PAGE);
        String locale = (String) request.getSession().getAttribute(LOCALE);

        try {
            validateShowingParams(sortType, sortOrder, pageSize, currentPage);

            int pageSizeInt = Integer.parseInt(pageSize);
            int currentPageInt = Integer.parseInt(currentPage);
            int offset = (pageSizeInt*currentPageInt)-pageSizeInt;

            BookOrderBy orderBy = BookOrderByTypeFactory.getOrderByType(sortType, sortOrder, locale);

            request.setAttribute(BOOKS, ServiceFactory.getBookService().getBooks(orderBy,offset,pageSizeInt));
            request.setAttribute(LAST_PAGE, ServiceFactory.getBookService().getLastPageNumber(pageSizeInt));
            request.setAttribute(CURRENT_PAGE, currentPageInt);
            request.setAttribute(SORT_TYPE, sortType);
            request.setAttribute(SORT_ORDER, sortOrder);
            request.setAttribute(PAGE_SIZE, pageSize);
            request.setAttribute(ERROR, request.getParameter(ERROR));
            request.setAttribute(MESSAGE, request.getParameter(MESSAGE));

        }catch (ServiceException e){
            log.warn("Exception while ShowBooksCommand",e);
            request.setAttribute(ERROR, e.getMessage());
        }
        return BOOKS_PAGE;
    }
}
