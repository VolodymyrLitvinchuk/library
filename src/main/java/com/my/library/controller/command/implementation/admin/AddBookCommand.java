package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.dto.BookDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.ADD_BOOK_COMMAND;
import static com.my.library.controller.command.constant.Page.ADD_BOOK_PAGE;
import static com.my.library.controller.command.constant.Page.CONTROLLER_PAGE;
import static com.my.library.controller.command.constant.Parameter.ERROR;
import static com.my.library.controller.command.constant.Parameter.MESSAGE;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_ADD_BOOK;
import static com.my.library.exception.constant.Message.UNKNOWN_ERROR;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ConvertorUtil.convertRequestToBookDTO;
import static com.my.library.util.ValidatorUtil.validateBook;

/**
 * AddBookCommand class.
 * Used to perform adding book.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class AddBookCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(AddBookCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     */
    @Override
    public String execute(HttpServletRequest request){
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return ADD_BOOK_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        transferBookFieldsFromSessionToRequest(request);
        return ADD_BOOK_PAGE;
    }

    /**
     * Method performs adding book.
     * First of all method gets required params from request and validates them.
     * Then method performs adding book using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        try{
            BookDTO bookDTO = convertRequestToBookDTO(request);
            if(bookDTO==null) throw new ServiceException(UNKNOWN_ERROR);
            validateBook(bookDTO);

            ServiceFactory.getBookService().add(bookDTO);
            request.getSession().setAttribute(MESSAGE, SUCCEED_ADD_BOOK);

        }catch (ServiceException e){
            log.warn("Exception while AddBookCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
            setBookFieldsSessionAttribute(request);
        }

        return CONTROLLER_PAGE + "?" + "command=" + ADD_BOOK_COMMAND;
    }
}
