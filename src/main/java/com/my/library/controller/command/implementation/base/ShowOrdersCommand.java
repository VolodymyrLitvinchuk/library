package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.util.orderByFactory.BookOrderOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.BOOKS_PAGE;
import static com.my.library.controller.command.constant.Page.ORDERS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.ValidatorUtil.validateShowingParams;

/**
 * ShowOrdersCommand class.
 * Used to get list of orders.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ShowOrdersCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ShowOrdersCommand.class);

    /**
     * Method sets list of orders to request attribute.
     * First of all method gets required params from request and validates them.
     * Then method gets {@link BookOrderOrderBy} enum depends on request params.
     * Then method defines whether list of orders is for reader or for librarian.
     * Then method gets list of orders and sets it in request params.
     *
     * @param request User's request.
     * @return ORDERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String sortType = request.getParameter(SORT_TYPE);
        String sortOrder = request.getParameter(SORT_ORDER);
        String pageSize = request.getParameter(PAGE_SIZE);
        String currentPage = request.getParameter(CURRENT_PAGE);

        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null) return BOOKS_PAGE;

        try {
            validateShowingParams(sortType, sortOrder, pageSize, currentPage);

            int pageSizeInt = Integer.parseInt(pageSize);
            int currentPageInt = Integer.parseInt(currentPage);
            int offset = (pageSizeInt*currentPageInt)-pageSizeInt;

            BookOrderOrderBy orderBy = BookOrderOrderByTypeFactory.getOrderByType(sortType, sortOrder);

            if(userDTO.getRole().equals("Librarian")) {
                request.setAttribute(ORDERS, ServiceFactory.getBookOrderService().getActiveBookOrders(orderBy, offset, pageSizeInt));
                request.setAttribute(LAST_PAGE, ServiceFactory.getBookOrderService().getLastPageNumberForLibrarian(pageSizeInt));
            }else if (userDTO.getRole().equals("Reader")) {
                request.setAttribute(ORDERS, ServiceFactory.getBookOrderService().getReadersBookOrders(orderBy, offset, pageSizeInt, userDTO.getLogin()));
                request.setAttribute(LAST_PAGE, ServiceFactory.getBookOrderService().getLastPageNumberForReader(pageSizeInt, userDTO.getLogin()));
            }

            request.setAttribute(CURRENT_PAGE, currentPageInt);
            request.setAttribute(SORT_TYPE, sortType);
            request.setAttribute(SORT_ORDER, sortOrder);
            request.setAttribute(PAGE_SIZE, pageSize);
            request.setAttribute(ERROR, request.getParameter(ERROR));
            request.setAttribute(MESSAGE, request.getParameter(MESSAGE));

        }catch (ServiceException e){
            log.warn("Exception while ShowOrdersCommand",e);
            request.setAttribute(ERROR, e.getMessage());
        }
        return ORDERS_PAGE;
    }
}
