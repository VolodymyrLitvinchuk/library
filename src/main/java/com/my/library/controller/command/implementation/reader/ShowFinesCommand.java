package com.my.library.controller.command.implementation.reader;

import com.my.library.controller.command.Command;
import com.my.library.dao.orderByEnums.FineOrderBy;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import com.my.library.util.orderByFactory.FineOrderByTypeFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.*;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.util.ValidatorUtil.validateShowingParams;

/**
 * ShowFinesCommand class.
 * Used to get list of reader's fines.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class ShowFinesCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ShowFinesCommand.class);

    /**
     * Method sets list of reader's fines to request attribute.
     * First of all method gets required params from request and validates them.
     * Then method gets {@link FineOrderBy} enum depends on request params.
     * Then method gets list of reader's fines and sets it in request params.
     *
     * @param request User's request.
     * @return FINES_PAGE from {@link com.my.library.controller.command.constant.Page}
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String sortType = request.getParameter(SORT_TYPE);
        String sortOrder = request.getParameter(SORT_ORDER);
        String pageSize = request.getParameter(PAGE_SIZE);
        String currentPage = request.getParameter(CURRENT_PAGE);

        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO==null) return BOOKS_PAGE;

        try {
            validateShowingParams(sortType, sortOrder, pageSize, currentPage);

            int pageSizeInt = Integer.parseInt(pageSize);
            int currentPageInt = Integer.parseInt(currentPage);
            int offset = (pageSizeInt*currentPageInt)-pageSizeInt;

            FineOrderBy fineOrderBy = FineOrderByTypeFactory.getOrderByType(sortType, sortOrder);

            request.setAttribute(FINES, ServiceFactory.getFineService().getReaderFinesOrderByLimitOffset(fineOrderBy, userDTO.getLogin(), offset, pageSizeInt));
            request.setAttribute(LAST_PAGE, ServiceFactory.getFineService().getLastPageNumber(userDTO.getLogin(), pageSizeInt));

            request.setAttribute(CURRENT_PAGE, currentPageInt);
            request.setAttribute(SORT_TYPE, sortType);
            request.setAttribute(SORT_ORDER, sortOrder);
            request.setAttribute(PAGE_SIZE, pageSize);
            request.setAttribute(ERROR, request.getParameter(ERROR));
            request.setAttribute(MESSAGE, request.getParameter(MESSAGE));

        }catch (ServiceException e){
            log.warn("Exception while ShowFinesCommand",e);
            request.setAttribute(ERROR, e.getMessage());
        }
        return FINES_PAGE;
    }
}
