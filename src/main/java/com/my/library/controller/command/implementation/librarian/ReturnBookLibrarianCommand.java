package com.my.library.controller.command.implementation.librarian;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.RETURN_BOOK_LIBRARIAN_COMMAND;
import static com.my.library.controller.command.constant.Page.ORDERS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_RETURNED;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * ReturnBookLibrarianCommand class.
 * Used to perform returning book by librarian.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ReturnBookLibrarianCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(ReturnBookLibrarianCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return ORDERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return ORDERS_PAGE;
    }

    /**
     * Method performs returning book by librarian.
     * First of all method gets required param from request.
     * Then method performs returning book by librarian using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int orderID = Integer.parseInt(request.getParameter(BOOK_ORDER_ID));

        try {
            ServiceFactory.getBookOrderService().returnBookLibrarian(orderID);
            request.getSession().setAttribute(MESSAGE, SUCCEED_RETURNED);

        }catch (ServiceException e){
            log.warn("Exception while ReturnBookLibrarianCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        return "controller?command="+ RETURN_BOOK_LIBRARIAN_COMMAND;
    }
}
