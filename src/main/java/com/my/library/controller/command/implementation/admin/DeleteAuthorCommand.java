package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.DELETE_AUTHOR_COMMAND;
import static com.my.library.controller.command.constant.Page.AUTHORS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_DELETE;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;
import static com.my.library.util.ValidatorUtil.validateAuthorID;

/**
 * DeleteAuthorCommand class.
 * Used to perform deleting author.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class DeleteAuthorCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(DeleteAuthorCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return AUTHORS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return AUTHORS_PAGE;
    }

    /**
     * Method performs deleting author.
     * First of all method gets required param from request and validates it.
     * Then method performs deleting using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int authorId = Integer.parseInt(request.getParameter(AUTHOR_ID));

        try {
            validateAuthorID(authorId);

            ServiceFactory.getAuthorService().delete(authorId);
            request.getSession().setAttribute(MESSAGE, SUCCEED_DELETE);
            log.info("Author with id={} was deleted", authorId);

        }catch (ServiceException e){
            log.warn("Exception while DeleteAuthorCommand. AuthorID={}", authorId, e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        return "controller?command="+DELETE_AUTHOR_COMMAND;
    }
}
