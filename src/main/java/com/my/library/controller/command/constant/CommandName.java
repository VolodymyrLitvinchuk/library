package com.my.library.controller.command.constant;

/**
 * CommandName class.
 * Contains all commands' names.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class CommandName {
    private CommandName(){}

    public static final String SIGN_IN_COMMAND = "sign-in";
    public static final String SIGN_UP_READER_COMMAND = "sign-up-reader";
    public static final String SIGN_UP_LIBRARIAN_COMMAND = "sign-up-librarian";
    public static final String SIGN_OUT_COMMAND = "sign-out";

    public static final String UPDATE_USER_LOGIN_COMMAND = "update-user-login";
    public static final String UPDATE_USER_PASSWORD_COMMAND = "update-user-pass";
    public static final String UPDATE_USER_EMAIL_COMMAND = "update-user-email";
    public static final String UPDATE_USER_FIRST_NAME_COMMAND = "update-user-first-name";
    public static final String UPDATE_USER_LAST_NAME_COMMAND = "update-user-last-name";

    public static final String GET_BOOK_BY_ID_COMMAND = "get-book-by-id";
    public static final String ADD_BOOK_COMMAND = "add-book";
    public static final String SHOW_BOOKS_COMMAND = "show-books";
    public static final String SEARCH_BOOKS_COMMAND = "search-books";
    public static final String UPDATE_BOOK_COMMAND = "update-book";
    public static final String DELETE_BOOK_COMMAND = "delete-book";
    public static final String UPDATE_BOOK_TITLE_EN_COMMAND = "update-title-en";
    public static final String UPDATE_BOOK_TITLE_UA_COMMAND = "update-title-ua";
    public static final String UPDATE_BOOK_LANGUAGE_COMMAND = "update-book-language";
    public static final String UPDATE_COPIES_OWNED_COMMAND = "update-copies-owned";
    public static final String UPDATE_BOOK_AUTHOR_ID_COMMAND = "update-book-author-id";
    public static final String UPDATE_BOOK_PUBLICATION_ID_COMMAND = "update-book-publication-id";
    public static final String UPDATE_PUBLICATION_DATE_COMMAND = "update-publication-date";


    public static final String SHOW_USERS_COMMAND = "show-users";

    public static final String BLOCK_USER_COMMAND = "block-user";
    public static final String UNBLOCK_USER_COMMAND = "unblock-user";
    public static final String DELETE_USER_COMMAND = "delete-user";

    public static final String SHOW_AUTHORS_COMMAND = "show-authors";
    public static final String UPDATE_AUTHOR_COMMAND = "update-author";
    public static final String DELETE_AUTHOR_COMMAND = "delete-author";
    public static final String UPDATE_AUTHOR_FIRST_NAME_EN_COMMAND = "update-author-first-name-en";
    public static final String UPDATE_AUTHOR_FIRST_NAME_UA_COMMAND = "update-author-first-name-ua";
    public static final String UPDATE_AUTHOR_LAST_NAME_EN_COMMAND = "update-author-last-name-en";
    public static final String UPDATE_AUTHOR_LAST_NAME_UA_COMMAND = "update-author-last-name-ua";

    public static final String GO_TO_BOOK_ORDER_PAGE_COMMAND = "goto-book-order-page";
    public static final String ORDER_BOOK_COMMAND = "order-book";
    public static final String SHOW_ORDERS_COMMAND = "show-orders";
    public static final String CANCEL_ORDER_COMMAND = "cancel-order";
    public static final String ISSUE_BOOK_COMMAND = "issue-book";
    public static final String RETURN_BOOK_READER_COMMAND = "return-book-reader";
    public static final String RETURN_BOOK_LIBRARIAN_COMMAND = "return-book-librarian";
    public static final String PAY_FINE_COMMAND = "pay-fine";

    public static final String FORGOT_PASSWORD_COMMAND = "forgot-password";

    public static final String SHOW_FINES_COMMAND = "show-fines";
}
