package com.my.library.controller.command.implementation.admin.updateAuthor;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.UPDATE_AUTHOR_LAST_NAME_EN_COMMAND;
import static com.my.library.controller.command.constant.Page.EDIT_AUTHOR_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static com.my.library.exception.constant.Message.ENTER_CORRECT_AUTHOR_LAST_NAME_EN;
import static com.my.library.util.CommandUtil.*;
import static com.my.library.util.ValidatorUtil.validateAuthorID;
import static com.my.library.util.ValidatorUtil.validateFormat;
import static com.my.library.util.constant.Regex.COMPLEX_NAME_EN_REGEX;

/**
 * UpdateAuthorLastNameEnCommand class.
 * Used to perform updating Author's last name in English.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateAuthorLastNameEnCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UpdateAuthorLastNameEnCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);
    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return EDIT_AUTHOR_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferAuthorEditingFieldsFromSessionToRequest(request);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_EN, NEW_AUTHOR_LAST_NAME_EN);
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return EDIT_AUTHOR_PAGE;
    }

    /**
     * Method performs updating author's last name in English.
     * First of all method gets required params from request and validates them.
     * Then method performs updating using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        int authorId = Integer.parseInt(request.getParameter(AUTHOR_ID));
        String newLastNameEN = request.getParameter(NEW_AUTHOR_LAST_NAME_EN);

        try {
            validateFormat(newLastNameEN, COMPLEX_NAME_EN_REGEX, ENTER_CORRECT_AUTHOR_LAST_NAME_EN);
            validateAuthorID(authorId);

            ServiceFactory.getAuthorService().updateLastNameEN(authorId, newLastNameEN);

            request.getSession().setAttribute(NEW_AUTHOR_LAST_NAME_EN, newLastNameEN);
            request.getSession().setAttribute(MESSAGE, SUCCEED_UPDATE);

        }catch (ServiceException e){
            log.warn("Exception while UpdateAuthorLastNameEnCommand",e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        setAuthorEditingFieldsSessionAttribute(request);
        return "controller?command="+ UPDATE_AUTHOR_LAST_NAME_EN_COMMAND;
    }
}
