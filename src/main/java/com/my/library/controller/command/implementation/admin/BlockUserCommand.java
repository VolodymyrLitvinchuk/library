package com.my.library.controller.command.implementation.admin;

import com.my.library.controller.command.Command;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.Page.USERS_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.dao.mysql.Constant.LOCKED_USER_STATUS;

/**
 * BlockUserCommand class.
 * Used to perform blocking user.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class BlockUserCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(BlockUserCommand.class);

    /**
     * Method performs blocking user.
     * First of all method gets login param from request.
     * Then method performs blocking user using Service layer.
     *
     * @param request User's request.
     * @return USERS_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    @Override
    public String execute(HttpServletRequest request){
        String login = request.getParameter(LOGIN);

        try {
            ServiceFactory.getUserService().updateStatus(login, LOCKED_USER_STATUS);
            log.info("User with login={} was blocked",login);
        }catch (ServiceException e){
            log.warn("Exception while BlockUserCommand. User login={}", login, e);
            request.setAttribute(ERROR, e.getMessage());
        }
        return USERS_PAGE;
    }
}
