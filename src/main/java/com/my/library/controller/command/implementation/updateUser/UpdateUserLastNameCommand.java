package com.my.library.controller.command.implementation.updateUser;

import com.my.library.controller.command.Command;
import com.my.library.dto.UserDTO;
import com.my.library.exception.ServiceException;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.my.library.controller.command.constant.CommandName.UPDATE_USER_LAST_NAME_COMMAND;
import static com.my.library.controller.command.constant.Page.PROFILE_PAGE;
import static com.my.library.controller.command.constant.Parameter.*;
import static com.my.library.controller.command.constant.ParameterValue.SUCCEED_UPDATE;
import static com.my.library.util.CommandUtil.transferStringFromSessionToRequest;

/**
 * UpdateUserLastNameCommand class.
 * Used to perform updating user's last name.
 * Implements PRG pattern.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UpdateUserLastNameCommand implements Command {
    private static final Logger log = LoggerFactory.getLogger(UpdateUserLastNameCommand.class);

    /**
     * Entry point of this command.
     * This method checks whether request method is 'POST' or 'GET',
     * and then invokes executePost() or executeGet().
     *
     * @param request User's request.
     * @return Path
     * @throws ServiceException if something goes wrong.
     */
    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        return request.getMethod().equals("POST") ? executePost(request) : executeGet(request);

    }

    /**
     * Method transfers attributes from session to request.
     *
     * @param request User's request.
     * @return PROFILE_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    private String executeGet(HttpServletRequest request) {
        transferStringFromSessionToRequest(request, MESSAGE);
        transferStringFromSessionToRequest(request, ERROR);
        return PROFILE_PAGE;
    }

    /**
     * Method performs updating user's last name.
     * First of all method gets required params from request.
     * Then method performs updating and validation using Service layer.
     *
     * @param request User's request.
     * @return "GET-path" to this command to execute executeGet() method from this class.
     */
    private String executePost(HttpServletRequest request){
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        String newLastName = request.getParameter(LAST_NAME);

        try{
            ServiceFactory.getUserService().updateLastName(userDTO,newLastName);
            request.getSession().setAttribute(LOGGED_USER,userDTO);
            request.getSession().setAttribute(MESSAGE, SUCCEED_UPDATE);

        }catch (ServiceException e){
            log.warn("Exception while UpdateUserLastNameCommand", e);
            request.getSession().setAttribute(ERROR, e.getMessage());
        }
        return "controller"+"?"+"command="+ UPDATE_USER_LAST_NAME_COMMAND;
    }
}
