package com.my.library.controller.command.implementation.base;

import com.my.library.controller.command.Command;
import jakarta.servlet.http.HttpServletRequest;

import static com.my.library.controller.command.constant.Page.MAIN_PAGE;

/**
 * DefaultCommand class.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class DefaultCommand implements Command {
    /**
     * Method returns MAIN_PAGE.
     *
     * @param request User's request.
     * @return MAIN_PAGE from {@link com.my.library.controller.command.constant.Page}
     */
    @Override
    public String execute(HttpServletRequest request){
        return MAIN_PAGE;
    }
}
