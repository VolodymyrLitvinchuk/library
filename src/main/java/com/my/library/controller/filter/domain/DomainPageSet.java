package com.my.library.controller.filter.domain;

import java.util.HashSet;
import java.util.Set;

import static com.my.library.controller.command.constant.Page.*;

/**
 * DomainPageSet class.
 * Contains sets of allowed pages for different users.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public final class DomainPageSet {
    private DomainPageSet(){}

    private static final Set<String> noLoggedUserPages = new HashSet<>();
    private static final Set<String> readerPages = new HashSet<>();
    private static final Set<String> librarianPages = new HashSet<>();
    private static final Set<String> adminPages = new HashSet<>();

    static {
        noLoggedUserPages.add(SIGN_IN_PAGE);
        noLoggedUserPages.add(SIGN_UP_READER_PAGE);
        noLoggedUserPages.add(MAIN_PAGE);
        noLoggedUserPages.add(ERROR_PAGE);
        noLoggedUserPages.add(CONTROLLER_PAGE);
        noLoggedUserPages.add(BOOKS_PAGE);
        noLoggedUserPages.add(ACCOUNT_LOCKED_PAGE);
        noLoggedUserPages.add(ACCESS_DENIED_PAGE);
        noLoggedUserPages.add(FORGOT_PASSWORD_PAGE);
        noLoggedUserPages.add(CONTACT_US_PAGE);

        readerPages.add(MAIN_PAGE);
        readerPages.add(PROFILE_PAGE);
        readerPages.add(ERROR_PAGE);
        readerPages.add(CONTROLLER_PAGE);
        readerPages.add(BOOKS_PAGE);
        readerPages.add(BOOK_ORDER_PAGE);
        readerPages.add(ORDERS_PAGE);
        readerPages.add(BOOK_BY_ID_PAGE);
        readerPages.add(ACCESS_DENIED_PAGE);
        readerPages.add(CONTACT_US_PAGE);
        readerPages.add(FINES_PAGE);

        librarianPages.addAll(readerPages);
        librarianPages.remove(BOOK_ORDER_PAGE);
        librarianPages.remove(FINES_PAGE);

        adminPages.addAll(readerPages);
        adminPages.remove(FINES_PAGE);
        adminPages.remove(BOOK_ORDER_PAGE);
        adminPages.remove(ORDERS_PAGE);
        adminPages.add(SIGN_UP_LIBRARIAN_PAGE);
        adminPages.add(ADD_BOOK_PAGE);
        adminPages.add(EDIT_BOOK_PAGE);
        adminPages.add(USERS_PAGE);
        adminPages.add(AUTHORS_PAGE);
        adminPages.add(EDIT_AUTHOR_PAGE);
    }

    public static Set<String> getNoLoggedUserPages(){
        return noLoggedUserPages;
    }

    public static Set<String> getReaderPages(){
        return readerPages;
    }

    public static Set<String> getLibrarianPages(){
        return librarianPages;
    }

    public static Set<String> getAdminPages(){
        return adminPages;
    }
}
