package com.my.library.controller.filter;

import jakarta.servlet.*;

import java.io.IOException;

/**
 * EncodingFilter class. Sets encoding for user's request and user's response.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class EncodingFilter implements Filter {
    private String encoding;

    /**
     * Method assigns default encoding value to 'encoding' variable.
     *
     * @param config Application's config object.
     */
    @Override
    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter("encoding");
    }

    /**
     * Method sets default encoding for user's request and user's response.
     *
     * @param request User's request.
     * @param response User's response.
     * @param chain Filter's chain.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String codeRequest = request.getCharacterEncoding();
        if (encoding != null && !encoding.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(encoding);
            response.setCharacterEncoding(encoding);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        encoding = null;
    }
}
