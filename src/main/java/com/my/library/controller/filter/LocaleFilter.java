package com.my.library.controller.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

import java.io.IOException;

/**
 * LocaleFilter  class. Sets and changes locale
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class LocaleFilter implements Filter {
    private static final String LOCALE = "locale";
    private String defaultLocale;

    /**
     * Method assigns default locale value to 'defaultLocale' variable.
     *
     * @param config Application's config object.
     */
    @Override
    public void init(FilterConfig config) {
        defaultLocale = config.getInitParameter("defaultLocale");
    }

    /**
     * Method gets locale value from user's request and sets it into session.
     * If there is no locale value in session and there is no locale value in user's request params
     * then this method sets default locale value in session.
     *
     * @param request User's request.
     * @param response User's response.
     * @param chain Filter's chain.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String locale = httpRequest.getParameter(LOCALE);
        if (!isBlank(locale)) {
            httpRequest.getSession().setAttribute(LOCALE, locale);
        } else {
            String sessionLocale = (String) httpRequest.getSession().getAttribute(LOCALE);
            if (isBlank(sessionLocale)) {
                httpRequest.getSession().setAttribute(LOCALE, defaultLocale);
            }
        }
        chain.doFilter(request, response);

    }

    private boolean isBlank(String locale) {
        return locale == null || locale.isEmpty();
    }
}