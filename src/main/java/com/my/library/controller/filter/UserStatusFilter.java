package com.my.library.controller.filter;

import com.my.library.dto.UserDTO;
import com.my.library.service.ServiceFactory;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;

import static com.my.library.controller.command.constant.Parameter.LOGGED_USER;

/**
 * User's status filter.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class UserStatusFilter implements Filter {
    @Override
    public void init(FilterConfig config) {}

    /**
     * Method checks whether user's status is up-to-date.
     * For example user was fined just a second ago and user's status now is SUSPENDED_USER_STATUS from {@link com.my.library.dao.mysql.Constant},
     * but in session user's status is ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     * This method assigns up-to-date status value to user's object in session.
     *
     * @param request User's request.
     * @param response User's response.
     * @param chain Filter's chain.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        try{
            UserDTO userDTO = (UserDTO) httpRequest.getSession().getAttribute(LOGGED_USER);
            if(userDTO!=null){
                ServiceFactory.getUserService().updateStatus(userDTO);
                httpRequest.getSession().setAttribute(LOGGED_USER, userDTO);
            }
        }catch (Exception e){
            httpRequest.getSession().setAttribute(LOGGED_USER, null);
        }
        chain.doFilter(request, response);

    }
}
