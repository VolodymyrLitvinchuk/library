package com.my.library.controller.filter.domain;

import java.util.HashSet;
import java.util.Set;

import static com.my.library.controller.command.constant.CommandName.*;

/**
 * DomainCommandSet class.
 * Contains sets of allowed commands for different users.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public final class DomainCommandSet {
    private DomainCommandSet(){}

    private static final Set<String> noLoggedUserCommands = new HashSet<>();
    private static final Set<String> readerCommands = new HashSet<>();
    private static final Set<String> librarianCommands = new HashSet<>();
    private static final Set<String> adminCommands = new HashSet<>();

    static {
        noLoggedUserCommands.add(SIGN_IN_COMMAND);
        noLoggedUserCommands.add(SIGN_UP_READER_COMMAND);
        noLoggedUserCommands.add(SHOW_BOOKS_COMMAND);
        noLoggedUserCommands.add(SEARCH_BOOKS_COMMAND);
        noLoggedUserCommands.add(FORGOT_PASSWORD_COMMAND);

        readerCommands.add(SIGN_OUT_COMMAND);
        readerCommands.add(UPDATE_USER_LOGIN_COMMAND);
        readerCommands.add(UPDATE_USER_PASSWORD_COMMAND);
        readerCommands.add(UPDATE_USER_EMAIL_COMMAND);
        readerCommands.add(UPDATE_USER_FIRST_NAME_COMMAND);
        readerCommands.add(UPDATE_USER_LAST_NAME_COMMAND);
        readerCommands.add(GET_BOOK_BY_ID_COMMAND);
        readerCommands.add(SHOW_BOOKS_COMMAND);
        readerCommands.add(SEARCH_BOOKS_COMMAND);
        readerCommands.add(GO_TO_BOOK_ORDER_PAGE_COMMAND);
        readerCommands.add(ORDER_BOOK_COMMAND);
        readerCommands.add(SHOW_ORDERS_COMMAND);
        readerCommands.add(RETURN_BOOK_READER_COMMAND);
        readerCommands.add(PAY_FINE_COMMAND);
        readerCommands.add(SHOW_FINES_COMMAND);

        librarianCommands.add(SIGN_OUT_COMMAND);
        librarianCommands.add(UPDATE_USER_LOGIN_COMMAND);
        librarianCommands.add(UPDATE_USER_PASSWORD_COMMAND);
        librarianCommands.add(UPDATE_USER_EMAIL_COMMAND);
        librarianCommands.add(UPDATE_USER_FIRST_NAME_COMMAND);
        librarianCommands.add(UPDATE_USER_LAST_NAME_COMMAND);
        librarianCommands.add(GET_BOOK_BY_ID_COMMAND);
        librarianCommands.add(SHOW_BOOKS_COMMAND);
        librarianCommands.add(SEARCH_BOOKS_COMMAND);
        librarianCommands.add(SHOW_ORDERS_COMMAND);
        librarianCommands.add(CANCEL_ORDER_COMMAND);
        librarianCommands.add(ISSUE_BOOK_COMMAND);
        librarianCommands.add(RETURN_BOOK_LIBRARIAN_COMMAND);

        adminCommands.add(SIGN_OUT_COMMAND);
        adminCommands.add(UPDATE_USER_LOGIN_COMMAND);
        adminCommands.add(UPDATE_USER_PASSWORD_COMMAND);
        adminCommands.add(UPDATE_USER_EMAIL_COMMAND);
        adminCommands.add(UPDATE_USER_FIRST_NAME_COMMAND);
        adminCommands.add(UPDATE_USER_LAST_NAME_COMMAND);
        adminCommands.add(SIGN_UP_LIBRARIAN_COMMAND);
        adminCommands.add(ADD_BOOK_COMMAND);
        adminCommands.add(SHOW_BOOKS_COMMAND);
        adminCommands.add(SEARCH_BOOKS_COMMAND);
        adminCommands.add(UPDATE_BOOK_COMMAND);
        adminCommands.add(DELETE_BOOK_COMMAND);
        adminCommands.add(UPDATE_BOOK_TITLE_EN_COMMAND);
        adminCommands.add(UPDATE_BOOK_TITLE_UA_COMMAND);
        adminCommands.add(UPDATE_BOOK_LANGUAGE_COMMAND);
        adminCommands.add(UPDATE_COPIES_OWNED_COMMAND);
        adminCommands.add(UPDATE_BOOK_AUTHOR_ID_COMMAND);
        adminCommands.add(UPDATE_BOOK_PUBLICATION_ID_COMMAND);
        adminCommands.add(UPDATE_PUBLICATION_DATE_COMMAND);
        adminCommands.add(SHOW_USERS_COMMAND);
        adminCommands.add(BLOCK_USER_COMMAND);
        adminCommands.add(UNBLOCK_USER_COMMAND);
        adminCommands.add(DELETE_USER_COMMAND);
        adminCommands.add(SHOW_AUTHORS_COMMAND);
        adminCommands.add(UPDATE_AUTHOR_COMMAND);
        adminCommands.add(DELETE_AUTHOR_COMMAND);
        adminCommands.add(UPDATE_AUTHOR_FIRST_NAME_EN_COMMAND);
        adminCommands.add(UPDATE_AUTHOR_FIRST_NAME_UA_COMMAND);
        adminCommands.add(UPDATE_AUTHOR_LAST_NAME_EN_COMMAND);
        adminCommands.add(UPDATE_AUTHOR_LAST_NAME_UA_COMMAND);
    }

    public static Set<String> getNoLoggedUserCommands(){
        return noLoggedUserCommands;
    }

    public static Set<String> getReaderCommands(){
        return readerCommands;
    }

    public static Set<String> getLibrarianCommands(){
        return librarianCommands;
    }

    public static Set<String> getAdminCommands(){
        return adminCommands;
    }
}
