package com.my.library.controller.filter;

import com.my.library.controller.filter.domain.Domain;
import com.my.library.dto.UserDTO;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.my.library.controller.command.constant.Page.ACCESS_DENIED_PAGE;
import static com.my.library.controller.command.constant.Parameter.COMMAND;
import static com.my.library.controller.command.constant.Parameter.LOGGED_USER;

/**
 * AccessFiler class. Controls access to pages and command.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class AccessFilter implements Filter {
    /**
     * Slf3j logger
     */
    private static final Logger log = LoggerFactory.getLogger(AccessFilter.class);

    /**
     * Method checks access using {@link AccessFilter#isAccessDenied(String, String, HttpServletRequest)}
     * and then is access is denied performs forward to ACCESS_DENIED_PAGE from {@link com.my.library.controller.command.constant.Page}.
     *
     * @param request User's request.
     * @param response User's response.
     * @param chain Filter's chain.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String servletPath = httpRequest.getServletPath();
        String command = httpRequest.getParameter(COMMAND);

        if (isAccessDenied(servletPath, command, httpRequest)) {
            request.getRequestDispatcher(ACCESS_DENIED_PAGE).forward(request, response);
            log.info("Someone trying to access forbidden page...");
        }else {
            chain.doFilter(request, response);
        }
    }

    private boolean isAccessDenied(String servletPath, String command, HttpServletRequest httpRequest) {
        if(isNoLoggedUser(httpRequest)) return Domain.getDomain(servletPath, command).isAccessDenied();

        UserDTO userDTO = (UserDTO) httpRequest.getSession().getAttribute(LOGGED_USER);
        return Domain.getDomain(servletPath, command, userDTO.getRole()).isAccessDenied();
    }

    private static boolean isNoLoggedUser(HttpServletRequest request) {
        return request.getSession().getAttribute(LOGGED_USER) == null;
    }
}
