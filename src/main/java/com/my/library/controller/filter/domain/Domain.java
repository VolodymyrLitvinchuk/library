package com.my.library.controller.filter.domain;

import java.util.Set;

import static com.my.library.dao.mysql.Constant.*;

/**
 * Domain class. This class check whether user has access to different pages and commands.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class Domain {
    /**
     * Page name.
     */
    private final String servletPath;
    /**
     * Command name.
     */
    private final String command;
    /**
     * Set of allowed pages.
     */
    private Set<String> domainPages;
    /**
     * Set of allowed commands.
     */
    private Set<String> domainCommands;

    /**
     * Constructor for no logged user.
     *
     * @param servletPath Page to access.
     * @param command Command to execute.
     */
    private Domain(String servletPath, String command) {
        this.servletPath = servletPath;
        this.command = command;
        setDomains();
    }

    /**
     * Constructor for logged user.
     *
     * @param servletPath Page to access.
     * @param command Command to execute.
     * @param role User's role.
     */
    private Domain(String servletPath, String command, String role) {
        this.servletPath = servletPath;
        this.command = command;
        setDomains(role);
    }

    /**
     * Return {@link Domain} object for no logged user.
     *
     * @param servletPath Page to access.
     * @param command Command to execute.
     */
    public static Domain getDomain(String servletPath, String command) {
        return new Domain(servletPath, command);
    }

    /**
     * Return {@link Domain} object for logged user.
     *
     * @param servletPath Page to access.
     * @param command Command to execute.
     * @param role User's role.
     */
    public static Domain getDomain(String servletPath, String command, String role) {
        return new Domain(servletPath, command, role);
    }

    /**
     * Set allowed pages and allowed commands for no logged user.
     */
    private void setDomains() {
        domainPages = DomainPageSet.getNoLoggedUserPages();
        domainCommands = DomainCommandSet.getNoLoggedUserCommands();
    }

    /**
     * Set allowed pages and allowed commands for logged user depends on role.
     */
    private void setDomains(String role) {
        switch (role) {
            case READER_ROLE:
                domainPages = DomainPageSet.getReaderPages();
                domainCommands = DomainCommandSet.getReaderCommands();
                break;
            case LIBRARIAN_ROLE:
                domainPages = DomainPageSet.getLibrarianPages();
                domainCommands = DomainCommandSet.getLibrarianCommands();
                break;
            case ADMIN_ROLE:
                domainPages = DomainPageSet.getAdminPages();
                domainCommands = DomainCommandSet.getAdminCommands();
                break;
            default:
                domainPages = DomainPageSet.getNoLoggedUserPages();
                domainCommands = DomainCommandSet.getNoLoggedUserCommands();
        }
    }

    /**
     * Method checks whether user has access.
     *
     * @return True if access is denied.
     */
    public boolean isAccessDenied() {
        return !checkPages() || !checkCommands();
    }

    private boolean checkPages() {
        if (servletPath != null) {
            return domainPages.contains(servletPath.substring(1));
        }
        return true;
    }

    private boolean checkCommands() {
        if (command != null) {
            return domainCommands.contains(command);
        }
        return true;
    }
}
