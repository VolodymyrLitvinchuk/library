package com.my.library.util;

import com.my.library.dto.BookDTO;
import jakarta.servlet.http.HttpServletRequest;

import static com.my.library.controller.command.constant.Parameter.*;

/**
 * CommandUtil class.
 * Contains methods that transfer attributes from session to request.
 * Also, contains methods for setting some attributes to session.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class CommandUtil {
    public static void transferStringFromSessionToRequest(HttpServletRequest request, String attributeName) {
        String attributeValue = (String) request.getSession().getAttribute(attributeName);
        if (attributeValue != null) {
            request.setAttribute(attributeName, attributeValue);
            request.getSession().removeAttribute(attributeName);
        }
    }

    public static void transferStringFromSessionToRequest(HttpServletRequest request, String attributeNameRequest, String attributeNameSession) {
        String attributeValue = (String) request.getSession().getAttribute(attributeNameSession);
        if (attributeValue != null) {
            request.setAttribute(attributeNameRequest, attributeValue);
            request.getSession().removeAttribute(attributeNameSession);
        }
    }

    public static void setBookFieldsSessionAttribute(HttpServletRequest request){
        request.getSession().setAttribute(TITLE_EN,request.getParameter(TITLE_EN));
        request.getSession().setAttribute(TITLE_UA,request.getParameter(TITLE_UA));
        request.getSession().setAttribute(LANGUAGE,request.getParameter(LANGUAGE));
        request.getSession().setAttribute(COPIES_OWNED,request.getParameter(COPIES_OWNED));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_EN,request.getParameter(AUTHOR_FIRST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_UA,request.getParameter(AUTHOR_FIRST_NAME_UA));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_EN,request.getParameter(AUTHOR_LAST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_UA,request.getParameter(AUTHOR_LAST_NAME_UA));
        request.getSession().setAttribute(PUBLICATION_DATE,request.getParameter(PUBLICATION_DATE));
        request.getSession().setAttribute(PUBLICATION_NAME_EN,request.getParameter(PUBLICATION_NAME_EN));
        request.getSession().setAttribute(PUBLICATION_NAME_UA,request.getParameter(PUBLICATION_NAME_UA));
    }

    public static void setBookFieldToRequest(HttpServletRequest request, BookDTO bookDTO){
        request.setAttribute(TITLE_EN, bookDTO.getTitleEN());
        request.setAttribute(TITLE_UA, bookDTO.getTitleUA());
        request.setAttribute(LANGUAGE, bookDTO.getLanguage());
        request.setAttribute(AUTHOR_FIRST_NAME_EN, bookDTO.getAuthorFirstNameEN());
        request.setAttribute(AUTHOR_FIRST_NAME_UA, bookDTO.getAuthorFirstNameUA());
        request.setAttribute(AUTHOR_LAST_NAME_EN, bookDTO.getAuthorLastNameEN());
        request.setAttribute(AUTHOR_LAST_NAME_UA, bookDTO.getAuthorLastNameUA());
        request.setAttribute(PUBLICATION_DATE, bookDTO.getPublicationDate());
        request.setAttribute(PUBLICATION_NAME_EN, bookDTO.getPublicationNameEN());
        request.setAttribute(PUBLICATION_NAME_UA, bookDTO.getPublicationNameUA());
    }

    public static void transferBookFieldsFromSessionToRequest(HttpServletRequest request){
        transferStringFromSessionToRequest(request, TITLE_EN);
        transferStringFromSessionToRequest(request, TITLE_UA);
        transferStringFromSessionToRequest(request, LANGUAGE);
        transferStringFromSessionToRequest(request, COPIES_OWNED);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_UA);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_UA);
        transferStringFromSessionToRequest(request, PUBLICATION_DATE);
        transferStringFromSessionToRequest(request, PUBLICATION_NAME_EN);
        transferStringFromSessionToRequest(request, PUBLICATION_NAME_UA);
    }

    public static void setUserFieldsSessionAttribute(HttpServletRequest request){
        request.getSession().setAttribute(LOGIN,request.getParameter(LOGIN));
        request.getSession().setAttribute(EMAIL,request.getParameter(EMAIL));
        request.getSession().setAttribute(FIRST_NAME,request.getParameter(FIRST_NAME));
        request.getSession().setAttribute(LAST_NAME,request.getParameter(LAST_NAME));
    }

    public static void setBookEditingFieldsSessionAttribute(HttpServletRequest request){
        request.getSession().setAttribute(BOOK_ID,request.getParameter(BOOK_ID));
        request.getSession().setAttribute(TITLE_EN,request.getParameter(TITLE_EN));
        request.getSession().setAttribute(TITLE_UA,request.getParameter(TITLE_UA));
        request.getSession().setAttribute(LANGUAGE,request.getParameter(LANGUAGE));
        request.getSession().setAttribute(COPIES_OWNED,request.getParameter(COPIES_OWNED));
        request.getSession().setAttribute(AUTHOR_ID,request.getParameter(AUTHOR_ID));
        request.getSession().setAttribute(PUBLICATION_ID,request.getParameter(PUBLICATION_ID));
        request.getSession().setAttribute(PUBLICATION_DATE,request.getParameter(PUBLICATION_DATE));
    }

    public static void setGoToBookOrderFieldsSessionAttribute(HttpServletRequest request){
        request.getSession().setAttribute(BOOK_ID,request.getParameter(BOOK_ID));
        request.getSession().setAttribute(TITLE_EN,request.getParameter(TITLE_EN));
        request.getSession().setAttribute(TITLE_UA,request.getParameter(TITLE_UA));
        request.getSession().setAttribute(LANGUAGE,request.getParameter(LANGUAGE));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_EN,request.getParameter(AUTHOR_FIRST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_UA,request.getParameter(AUTHOR_FIRST_NAME_UA));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_EN,request.getParameter(AUTHOR_LAST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_UA,request.getParameter(AUTHOR_LAST_NAME_UA));
        request.getSession().setAttribute(PUBLICATION_NAME_EN,request.getParameter(PUBLICATION_NAME_EN));
        request.getSession().setAttribute(PUBLICATION_NAME_UA,request.getParameter(PUBLICATION_NAME_UA));
        request.getSession().setAttribute(PUBLICATION_DATE,request.getParameter(PUBLICATION_DATE));
    }

    public static void transferGoToBookOrderFieldsFromSessionToRequest(HttpServletRequest request){
        transferStringFromSessionToRequest(request, BOOK_ID);
        transferStringFromSessionToRequest(request, TITLE_EN);
        transferStringFromSessionToRequest(request, TITLE_UA);
        transferStringFromSessionToRequest(request, LANGUAGE);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_UA);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_UA);
        transferStringFromSessionToRequest(request, PUBLICATION_NAME_EN);
        transferStringFromSessionToRequest(request, PUBLICATION_NAME_UA);
        transferStringFromSessionToRequest(request, PUBLICATION_DATE);
    }

    public static void setAuthorEditingFieldsSessionAttribute(HttpServletRequest request){
        request.getSession().setAttribute(AUTHOR_ID, request.getParameter(AUTHOR_ID));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_EN, request.getParameter(AUTHOR_FIRST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_FIRST_NAME_UA, request.getParameter(AUTHOR_FIRST_NAME_UA));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_EN, request.getParameter(AUTHOR_LAST_NAME_EN));
        request.getSession().setAttribute(AUTHOR_LAST_NAME_UA, request.getParameter(AUTHOR_LAST_NAME_UA));
    }

    public static void transferBookEditingFieldsFromSessionToRequest(HttpServletRequest request){
        transferStringFromSessionToRequest(request, BOOK_ID);
        transferStringFromSessionToRequest(request, TITLE_EN);
        transferStringFromSessionToRequest(request, TITLE_UA);
        transferStringFromSessionToRequest(request, LANGUAGE);
        transferStringFromSessionToRequest(request, COPIES_OWNED);
        transferStringFromSessionToRequest(request, AUTHOR_ID);
        transferStringFromSessionToRequest(request, PUBLICATION_ID);
        transferStringFromSessionToRequest(request, PUBLICATION_DATE);
    }

    public static void transferAuthorEditingFieldsFromSessionToRequest(HttpServletRequest request){
        transferStringFromSessionToRequest(request, AUTHOR_ID);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_FIRST_NAME_UA);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_EN);
        transferStringFromSessionToRequest(request, AUTHOR_LAST_NAME_UA);
    }

    public static void transferUserFieldsFromSessionToRequest(HttpServletRequest request){
        transferStringFromSessionToRequest(request, LOGIN);
        transferStringFromSessionToRequest(request, EMAIL);
        transferStringFromSessionToRequest(request, FIRST_NAME);
        transferStringFromSessionToRequest(request, LAST_NAME);
    }
}
