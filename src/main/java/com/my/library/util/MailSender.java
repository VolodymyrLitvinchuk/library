package com.my.library.util;

import com.my.library.exception.ServiceException;
import javax.mail.*;
import javax.mail.internet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.my.library.exception.constant.Message.MAIL_ERROR;

/**
 * MailSender class.
 * This class is used to send email.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public final class MailSender{
    private MailSender(){}
    private static final Logger log = LoggerFactory.getLogger(MailSender.class);
    /**
     * Properties for sending email.
     */
    private static final Properties properties = getProperties();
    private static final Session session;

    static {
        session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        properties.getProperty("mail.username"),
                        properties.getProperty("mail.password"));
            }
        });
    }

    /**
     * Method sends email.
     *
     * @param recipientAddress Email's recipient.
     * @param subject Email's subject.
     * @param body Email's text body.
     * @throws ServiceException if something goes wrong.
     */
    public static void sendEmail(String recipientAddress, String subject, String body) throws ServiceException {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(properties.getProperty("mail.from")));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientAddress));
            message.setSubject(subject);

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(body, "text/html; charset=utf-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
            log.info("Mail was sent to {}", recipientAddress);

        } catch (MessagingException e) {
            log.warn("Exception while sending mail to {}.", recipientAddress, e);
            throw new ServiceException(MAIL_ERROR);
        }
    }

    /**
     * Method gets properties from 'mail.properties' file.
     * So, this file must be created to send email successfully.
     *
     * @return Properties.
     */
    private static Properties getProperties() {
        Properties properties = new Properties();
        String connectionFile = "mail.properties";
        try (InputStream resource = MailSender.class.getClassLoader().getResourceAsStream(connectionFile)){
            properties.load(resource);
        } catch (IOException e) {
            log.warn("Exception while reading 'mail.properties' file", e);
        }
        return properties;
    }
}
