package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.FineOrderBy;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.ParameterValue.*;

/**
 * FineOrderByTypeFactory class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public final class FineOrderByTypeFactory {
    private FineOrderByTypeFactory(){}
    private static final Map<String, FineOrderBy> mapASC = new HashMap<>();
    private static final Map<String, FineOrderBy> mapDESC = new HashMap<>();

    static {
        mapASC.put(SORT_BY_ORDER_ID, FineOrderBy.ORDER_ID);
        mapASC.put(SORT_BY_AMOUNT, FineOrderBy.FINE_AMOUNT);
        mapASC.put(SORT_BY_STATUS, FineOrderBy.FINE_STATUS);

        mapDESC.put(SORT_BY_ORDER_ID, FineOrderBy.ORDER_ID_DESC);
        mapDESC.put(SORT_BY_AMOUNT, FineOrderBy.FINE_AMOUNT_DESC);
        mapDESC.put(SORT_BY_STATUS, FineOrderBy.FINE_STATUS_DESC);

    }

    /**
     * Method returns {@link FineOrderBy} enum depends on params.
     * This method is used to 'transform' sort params from user's request to {@link FineOrderBy} enum.
     *
     * @param type Sort type.
     * @param order Sort order.
     * @return {@link FineOrderBy} enum
     */
    public static FineOrderBy getOrderByType(String type, String order){
        if(order!=null && order.equals(DESC_ORDER)){
            return mapDESC.getOrDefault(type, FineOrderBy.FINE_STATUS);
        }else {
            return mapASC.getOrDefault(type, FineOrderBy.FINE_STATUS);
        }
    }
}
