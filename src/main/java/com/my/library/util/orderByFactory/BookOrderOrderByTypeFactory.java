package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.BookOrderOrderBy;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.ParameterValue.*;

/**
 * BookOrderOrderByTypeFactory class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class BookOrderOrderByTypeFactory {
    private static final Map<String, BookOrderOrderBy> mapASC = new HashMap<>();
    private static final Map<String, BookOrderOrderBy> mapDESC = new HashMap<>();

    static {
        mapASC.put(SORT_BY_ID, BookOrderOrderBy.ID);
        mapASC.put(SORT_BY_BOOK_ID, BookOrderOrderBy.BOOK_ID);
        mapASC.put(SORT_BY_READER, BookOrderOrderBy.READER_LOGIN);
        mapASC.put(SORT_BY_BOOK_ORDER_TYPE, BookOrderOrderBy.TYPE);
        mapASC.put(SORT_BY_BOOK_ORDER_DATE, BookOrderOrderBy.DATE);
        mapASC.put(SORT_BY_BOOK_ORDER_STATUS, BookOrderOrderBy.STATUS);

        mapDESC.put(SORT_BY_ID, BookOrderOrderBy.ID_DESC);
        mapDESC.put(SORT_BY_BOOK_ID, BookOrderOrderBy.BOOK_ID_DESC);
        mapDESC.put(SORT_BY_READER, BookOrderOrderBy.READER_LOGIN_DESC);
        mapDESC.put(SORT_BY_BOOK_ORDER_TYPE, BookOrderOrderBy.TYPE_DESC);
        mapDESC.put(SORT_BY_BOOK_ORDER_DATE, BookOrderOrderBy.DATE_DESC);
        mapDESC.put(SORT_BY_BOOK_ORDER_STATUS, BookOrderOrderBy.STATUS_DESC);

    }

    /**
     * Method returns {@link BookOrderOrderBy} enum depends on params.
     * This method is used to 'transform' sort params from user's request to {@link BookOrderOrderBy} enum.
     *
     * @param type Sort type.
     * @param order Sort order.
     * @return {@link BookOrderOrderBy} enum
     */
    public static BookOrderOrderBy getOrderByType(String type, String order){
        if(order!=null && order.equals(DESC_ORDER)){
            return mapDESC.getOrDefault(type, BookOrderOrderBy.ID_DESC);
        }else {
            return mapASC.getOrDefault(type, BookOrderOrderBy.ID);
        }
    }
}
