package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.BookOrderBy;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.ParameterValue.*;

/**
 * BookOrderByTypeFactory class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class BookOrderByTypeFactory {
    private static final Map<String, BookOrderBy> mapASC = new HashMap<>();
    private static final Map<String, BookOrderBy> mapDESC = new HashMap<>();

    static {
        mapASC.put(SORT_BY_TITLE+LOCALE_EN, BookOrderBy.BOOK_TITLE_EN);
        mapASC.put(SORT_BY_TITLE+LOCALE_UA, BookOrderBy.BOOK_TITLE_UA);
        mapASC.put(SORT_BY_AUTHOR+LOCALE_EN, BookOrderBy.AUTHOR_LAST_NAME_EN);
        mapASC.put(SORT_BY_AUTHOR+LOCALE_UA, BookOrderBy.AUTHOR_LAST_NAME_UA);
        mapASC.put(SORT_BY_PUBLICATION+LOCALE_EN, BookOrderBy.PUBLICATION_NAME_EN);
        mapASC.put(SORT_BY_PUBLICATION+LOCALE_UA, BookOrderBy.PUBLICATION_NAME_UA);
        mapASC.put(SORT_BY_PUBLICATION_DATE+LOCALE_EN, BookOrderBy.PUBLICATION_DATE);
        mapASC.put(SORT_BY_PUBLICATION_DATE+LOCALE_UA, BookOrderBy.PUBLICATION_DATE);

        mapDESC.put(SORT_BY_TITLE+LOCALE_EN, BookOrderBy.BOOK_TITLE_EN_DESC);
        mapDESC.put(SORT_BY_TITLE+LOCALE_UA, BookOrderBy.BOOK_TITLE_UA_DESC);
        mapDESC.put(SORT_BY_AUTHOR+LOCALE_EN, BookOrderBy.AUTHOR_LAST_NAME_EN_DESC);
        mapDESC.put(SORT_BY_AUTHOR+LOCALE_UA, BookOrderBy.AUTHOR_LAST_NAME_UA_DESC);
        mapDESC.put(SORT_BY_PUBLICATION+LOCALE_EN, BookOrderBy.PUBLICATION_NAME_EN_DESC);
        mapDESC.put(SORT_BY_PUBLICATION+LOCALE_UA, BookOrderBy.PUBLICATION_NAME_UA_DESC);
        mapDESC.put(SORT_BY_PUBLICATION_DATE+LOCALE_EN, BookOrderBy.PUBLICATION_DATE_DESC);
        mapDESC.put(SORT_BY_PUBLICATION_DATE+LOCALE_UA, BookOrderBy.PUBLICATION_DATE_DESC);

    }

    /**
     * Method returns {@link BookOrderBy} enum depends on params.
     * This method is used to 'transform' sort params from user's request to {@link BookOrderBy} enum.
     *
     * @param type Sort type.
     * @param order Sort order.
     * @param locale Sort locale.
     * @return {@link BookOrderBy} enum
     */
    public static BookOrderBy getOrderByType(String type, String order, String locale){
        if(order!=null && order.equals(DESC_ORDER)){
            return mapDESC.getOrDefault(type+locale, BookOrderBy.BOOK_TITLE_EN);
        }else {
            return mapASC.getOrDefault(type+locale, BookOrderBy.BOOK_TITLE_EN);
        }
    }
}
