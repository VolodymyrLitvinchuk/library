package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.AuthorOrderBy;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.ParameterValue.*;

/**
 * AuthorOrderByTypeFactory class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class AuthorOrderByTypeFactory {
    private static final Map<String, AuthorOrderBy> mapASC = new HashMap<>();
    private static final Map<String, AuthorOrderBy> mapDESC = new HashMap<>();

    static {
        mapASC.put(SORT_BY_ID+LOCALE_EN, AuthorOrderBy.ID);
        mapASC.put(SORT_BY_ID+LOCALE_UA, AuthorOrderBy.ID);
        mapASC.put(SORT_BY_FIRST_NAME+LOCALE_EN, AuthorOrderBy.FIRST_NAME_EN);
        mapASC.put(SORT_BY_FIRST_NAME+LOCALE_UA, AuthorOrderBy.FIRST_NAME_UA);
        mapASC.put(SORT_BY_LAST_NAME+LOCALE_EN, AuthorOrderBy.LAST_NAME_EN);
        mapASC.put(SORT_BY_LAST_NAME+LOCALE_UA, AuthorOrderBy.LAST_NAME_UA);

        mapDESC.put(SORT_BY_ID+LOCALE_EN, AuthorOrderBy.ID_DESC);
        mapDESC.put(SORT_BY_ID+LOCALE_UA, AuthorOrderBy.ID_DESC);
        mapDESC.put(SORT_BY_FIRST_NAME+LOCALE_EN, AuthorOrderBy.FIRST_NAME_EN_DESC);
        mapDESC.put(SORT_BY_FIRST_NAME+LOCALE_UA, AuthorOrderBy.FIRST_NAME_UA_DESC);
        mapDESC.put(SORT_BY_LAST_NAME+LOCALE_EN, AuthorOrderBy.LAST_NAME_EN_DESC);
        mapDESC.put(SORT_BY_LAST_NAME+LOCALE_UA, AuthorOrderBy.LAST_NAME_UA_DESC);

    }

    /**
     * Method returns {@link AuthorOrderBy} enum depends on params.
     * This method is used to 'transform' sort params from user's request to {@link AuthorOrderBy} enum.
     *
     * @param type Sort type.
     * @param order Sort order.
     * @param locale Sort locale.
     * @return {@link AuthorOrderBy} enum
     */
    public static AuthorOrderBy getOrderByType(String type, String order, String locale){
        if(order!=null && order.equals(DESC_ORDER)){
            return mapDESC.getOrDefault(type+locale, AuthorOrderBy.ID_DESC);
        }else {
            return mapASC.getOrDefault(type+locale, AuthorOrderBy.ID);
        }
    }
}
