package com.my.library.util.orderByFactory;

import com.my.library.dao.orderByEnums.UserOrderBy;

import java.util.HashMap;
import java.util.Map;

import static com.my.library.controller.command.constant.ParameterValue.*;

/**
 * UserOrderByTypeFactory class.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public class UserOrderByTypeFactory {
    private static final Map<String, UserOrderBy> mapASC = new HashMap<>();
    private static final Map<String, UserOrderBy> mapDESC = new HashMap<>();

    static {
        mapASC.put(SORT_BY_LOGIN, UserOrderBy.LOGIN);
        mapASC.put(SORT_BY_FIRST_NAME, UserOrderBy.FIRST_NAME);
        mapASC.put(SORT_BY_LAST_NAME, UserOrderBy.LAST_NAME);
        mapASC.put(SORT_BY_EMAIL, UserOrderBy.EMAIL);


        mapDESC.put(SORT_BY_LOGIN, UserOrderBy.LOGIN_DESC);
        mapDESC.put(SORT_BY_FIRST_NAME, UserOrderBy.FIRST_NAME_DESC);
        mapDESC.put(SORT_BY_LAST_NAME, UserOrderBy.LAST_NAME_DESC);
        mapDESC.put(SORT_BY_EMAIL, UserOrderBy.EMAIL_DESC);

    }

    /**
     * Method returns {@link UserOrderBy} enum depends on params.
     * This method is used to 'transform' sort params from user's request to {@link UserOrderBy} enum.
     *
     * @param type Sort type.
     * @param order Sort order.
     * @return {@link UserOrderBy} enum
     */
    public static UserOrderBy getOrderByType(String type, String order){
        if(order!=null && order.equals(DESC_ORDER)){
            return mapDESC.getOrDefault(type, UserOrderBy.LOGIN);
        }else {
            return mapASC.getOrDefault(type, UserOrderBy.LOGIN);
        }
    }
}
