package com.my.library.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.spec.KeySpec;
import java.util.Base64;

public final class Hasher {
    private Hasher(){}

    private static final int ITER_COUNT = 636;
    private static final int KEY_LENGTH = 128;
    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final String SALT = "qXm2bg2ELPwq";

    public static String hash(String password) throws Exception{
        KeySpec spec = new PBEKeySpec(password.toCharArray(), SALT.getBytes(), ITER_COUNT, KEY_LENGTH);
        SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] hash = f.generateSecret(spec).getEncoded();
        return Base64.getEncoder().encodeToString(hash);
    }
}
