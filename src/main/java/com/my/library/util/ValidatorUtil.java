package com.my.library.util;

import com.my.library.dto.BookDTO;
import com.my.library.exception.IncorrectFormatException;
import com.my.library.exception.IncorrectPasswordException;
import com.my.library.exception.ServiceException;

import java.util.Date;

import static com.my.library.controller.command.constant.ParameterValue.ACS_ORDER;
import static com.my.library.controller.command.constant.ParameterValue.DESC_ORDER;
import static com.my.library.exception.constant.Message.*;
import static com.my.library.util.constant.Regex.*;

/**
 * ValidatorUtil class.
 * Contains different methods that validate different fields and objects.
 *
 * @author Volodymyr Litvicnhuk.
 * @version 1.0
 */
public final class ValidatorUtil {
    private ValidatorUtil(){}

    public static void validateFormat(String name, String regex, String message) throws IncorrectFormatException {
        if (name == null || !name.matches(regex))
            throw new IncorrectFormatException(message);
    }

    public static void verifyPasswordHash(String password, String userPasswordHash) throws IncorrectPasswordException {
        try{
            if(!Hasher.hash(password).equals(userPasswordHash)){
                throw new IncorrectPasswordException();
            }
        }catch (Exception e){
            throw new IncorrectPasswordException();
        }
    }

    public static void verifyRepeatedPassword(String password, String repeatedPassword) throws IncorrectPasswordException {
        if(!password.equals(repeatedPassword)) throw new IncorrectPasswordException();
    }

    public static void validateBookID(int bookId) throws ServiceException{
        if(bookId<1) throw new ServiceException(WRONG_BOOK_ID_FORMAT);
    }

    public static void validateAuthorID(int id) throws ServiceException{
        if(id<1) throw new ServiceException(ENTER_CORRECT_AUTHOR_ID);
    }

    public static void validatePublicationID(int id) throws ServiceException{
        if(id<1) throw new ServiceException(ENTER_CORRECT_PUBLICATION_ID);
    }

    public static void validateCopiesOwned(int copiesOwned) throws ServiceException{
        if(copiesOwned<1 || copiesOwned>100) throw new IncorrectFormatException(ENTER_CORRECT_COPIES_OWNED);
    }

    public static void validatePublicationDate(java.sql.Date date) throws IncorrectFormatException {
        if(date.compareTo(new Date())>0){
            throw new IncorrectFormatException(ENTER_CORRECT_PUBLICATION_DATE);
        }
    }


    public static void validateBook(BookDTO bookDTO) throws IncorrectFormatException {
        if (bookDTO==null) throw new IncorrectFormatException(UNKNOWN_ERROR);

        validateFormat(bookDTO.getTitleEN(), COMPLEX_NAME_EN_REGEX, ENTER_CORRECT_TITLE_EN);
        validateFormat(bookDTO.getTitleUA(), COMPLEX_NAME_UA_REGEX, ENTER_CORRECT_TITLE_UA);
        validateFormat(bookDTO.getLanguage(), NAME_REGEX, ENTER_CORRECT_LANGUAGE);
        if(bookDTO.getCopiesOwned()<1 || bookDTO.getCopiesOwned()>100) throw new IncorrectFormatException(ENTER_CORRECT_COPIES_OWNED);
        validateFormat(bookDTO.getAuthorFirstNameEN(), NAME_EN_REGEX, ENTER_CORRECT_AUTHOR_FIRST_NAME_EN);
        validateFormat(bookDTO.getAuthorFirstNameUA(), NAME_UA_REGEX, ENTER_CORRECT_AUTHOR_FIRST_NAME_UA);
        validateFormat(bookDTO.getAuthorLastNameEN(), NAME_EN_REGEX, ENTER_CORRECT_AUTHOR_LAST_NAME_EN);
        validateFormat(bookDTO.getAuthorLastNameUA(), NAME_UA_REGEX, ENTER_CORRECT_AUTHOR_LAST_NAME_UA);
        if(bookDTO.getPublicationDate().compareTo(new Date())>0) throw new IncorrectFormatException(ENTER_CORRECT_PUBLICATION_DATE);
        validateFormat(bookDTO.getPublicationNameEN(), COMPLEX_NAME_EN_REGEX, ENTER_CORRECT_PUBLICATION_NAME_EN);
        validateFormat(bookDTO.getPublicationNameUA(), COMPLEX_NAME_UA_REGEX, ENTER_CORRECT_PUBLICATION_NAME_UA);
    }

    public static void validateShowingParams(String type, String order, String pageSize, String currentPage) throws ServiceException {
        if(type==null || order == null
                || pageSize==null || currentPage==null
                || type.isBlank() || order.isBlank()
                || pageSize.isBlank() || currentPage.isBlank()) throw new ServiceException(UNKNOWN_ERROR);
        if(!order.equals(ACS_ORDER) && !order.equals(DESC_ORDER)) throw new IncorrectFormatException(WRONG_ORDER_TYPE);
        try{
            int pageSizeInt = Integer.parseInt(pageSize);
            if(pageSizeInt <= 0 ) throw new IncorrectFormatException(WRONG_PAGE_SIZE);
        }catch (Exception e){
            throw new IncorrectFormatException(WRONG_PAGE_SIZE);
        }
        try{
            int currentPageInt = Integer.parseInt(currentPage);
            if(currentPageInt <=0 ) throw new IncorrectFormatException(WRONG_CURRENT_PAGE);
        }catch (Exception e){
            throw new IncorrectFormatException(WRONG_CURRENT_PAGE);
        }

    }

    public static void validateSearchingParams(String titlePattern, String authorFirstNamePatter, String authorLastNamePatter, String locale) throws ServiceException {
        if(titlePattern==null
                || authorFirstNamePatter==null
                || authorLastNamePatter==null ) throw new ServiceException(WRONG_SEARCH_PARAMS);

        if(locale.equals("en")){
            if(!titlePattern.isBlank()) validateFormat(titlePattern, COMPLEX_NAME_EN_REGEX, ENTER_CORRECT_TITLE_EN);
            if(!authorFirstNamePatter.isBlank()) validateFormat(authorFirstNamePatter, NAME_EN_REGEX, ENTER_CORRECT_AUTHOR_FIRST_NAME_EN);
            if(!authorLastNamePatter.isBlank()) validateFormat(authorLastNamePatter, NAME_EN_REGEX, ENTER_CORRECT_AUTHOR_LAST_NAME_EN);

        }else if(locale.equals("ua")){
            if(!titlePattern.isBlank()) validateFormat(titlePattern, COMPLEX_NAME_UA_REGEX, ENTER_CORRECT_TITLE_UA);
            if(!authorFirstNamePatter.isBlank()) validateFormat(authorFirstNamePatter, NAME_UA_REGEX, ENTER_CORRECT_AUTHOR_FIRST_NAME_UA);
            if(!authorLastNamePatter.isBlank()) validateFormat(authorLastNamePatter, NAME_UA_REGEX, ENTER_CORRECT_AUTHOR_LAST_NAME_UA);

        }else throw new ServiceException(UNKNOWN_ERROR);

    }
}
