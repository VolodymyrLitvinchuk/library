package com.my.library.util;

import com.my.library.dao.DAOFactory;
import com.my.library.dto.*;
import com.my.library.dto.builder.BuilderBookDTO;
import com.my.library.dto.builder.BuilderBookDTOImpl;
import com.my.library.dao.entity.book.Author;
import com.my.library.dao.entity.book.Book;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.user.User;
import com.my.library.dao.entity.user.UserRole;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.DAOException;
import jakarta.servlet.http.HttpServletRequest;

import java.sql.Date;
import java.util.Optional;

import static com.my.library.controller.command.constant.Parameter.*;

/**
 * ConvertorUtil class.
 * Contains different methods that convert one type to another.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public final class ConvertorUtil {
    private ConvertorUtil(){}

    public static UserDTO convertUserToDTO(User user){
        return new UserDTO(
                user.getLogin(),
                user.getEmail(),
                user.getFirstName(),
                user.getLastName(),
                user.getRole().getValue(),
                user.getStatus().getValue()
        );
    }

    public static AuthorDTO convertAuthorToDTO(Author author){
        return new AuthorDTO(
                author.getId(),
                author.getFirstNameEN(),
                author.getFirstNameUA(),
                author.getLastNameEN(),
                author.getLastNameUA()
        );
    }


    public static BookDTO convertBookToDTO(Book book) throws DAOException {
        BuilderBookDTO builderBookDTO = new BuilderBookDTOImpl();
        builderBookDTO
                .setTitleEN(book.getTitle_EN())
                .setTitleUA(book.getTitle_UA())
                .setLanguage(book.getLanguage())
                .setPublicationDate(book.getPublication_Date())
                .setCopiesOwned(book.getCopiesOwned())
                .setAuthorFirstNameEN(book.getAuthor().getFirstNameEN())
                .setAuthorFirstNameUA(book.getAuthor().getFirstNameUA())
                .setAuthorLastNameEN(book.getAuthor().getLastNameEN())
                .setAuthorLastNameUA(book.getAuthor().getLastNameUA())
                .setPublicationNameEN(book.getPublication().getNameEN())
                .setPublicationNameUA(book.getPublication().getNameUA());

        BookDTO bookDTO = builderBookDTO.get();
        if(bookDTO==null) return null;

        Optional<Integer> optionalQuantity =
                DAOFactory.getBookAvailableDAO().get(book.getId());
        if(optionalQuantity.isEmpty()) return null;

        bookDTO.setId(book.getId());
        bookDTO.setAuthorId(book.getAuthor().getId());
        bookDTO.setPublicationId(book.getPublication().getId());
        bookDTO.setAvailableQuantity(optionalQuantity.get());

        return bookDTO;
    }

    public static User convertDTOtoUser(UserDTO userDTO, String password) throws DAOException {
        String hashPasswd;
        try {
            hashPasswd = Hasher.hash(password);
        }catch (Exception e){ return null; }

        User user =  new User(
                userDTO.getLogin(),
                hashPasswd,
                userDTO.getEmail(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                null
        );

        if(userDTO.getRole()==null) return null;
        Optional<UserRole> optionalUserRole =
                DAOFactory.getUserRoleDAO().getByValue(userDTO.getRole());
        if(optionalUserRole.isEmpty()) return null;
        optionalUserRole.ifPresent(user::setRole);

        if (userDTO.getStatus() != null) {
            Optional<UserStatus> optionalUserStatus =
                    DAOFactory.getUserStatusDAO().getByValue(userDTO.getStatus());
            optionalUserStatus.ifPresent(user::setStatus);
        }
        return user;
    }

    public static UserDTO convertRequestToUserDTO(HttpServletRequest request){
        return new UserDTO(
                request.getParameter(LOGIN),
                request.getParameter(EMAIL),
                request.getParameter(FIRST_NAME),
                request.getParameter(LAST_NAME),
                "Reader",
                null
        );
    }

    public static UserDTO convertRequestToUserDTOLibrarian(HttpServletRequest request){
        return new UserDTO(
                request.getParameter(LOGIN),
                request.getParameter(EMAIL),
                request.getParameter(FIRST_NAME),
                request.getParameter(LAST_NAME),
                "Librarian",
                null
        );
    }

    public static BookDTO convertRequestToBookDTO(HttpServletRequest request){
        if(request == null) return null;

        BuilderBookDTO builder = new BuilderBookDTOImpl();
        builder.setTitleEN(request.getParameter(TITLE_EN))
                .setTitleUA(request.getParameter(TITLE_UA))
                .setLanguage(request.getParameter(LANGUAGE))
                .setAuthorFirstNameEN(request.getParameter(AUTHOR_FIRST_NAME_EN))
                .setAuthorFirstNameUA(request.getParameter(AUTHOR_FIRST_NAME_UA))
                .setAuthorLastNameEN(request.getParameter(AUTHOR_LAST_NAME_EN))
                .setAuthorLastNameUA(request.getParameter(AUTHOR_LAST_NAME_UA))
                .setPublicationNameEN(request.getParameter(PUBLICATION_NAME_EN))
                .setPublicationNameUA(request.getParameter(PUBLICATION_NAME_UA));
        try {
            builder.setCopiesOwned(Integer.valueOf(request.getParameter(COPIES_OWNED)))
                   .setPublicationDate(Date.valueOf(request.getParameter(PUBLICATION_DATE)));
        }catch (RuntimeException e){
            return null;
        }

        return builder.get();
    }

    public static BookOrderDTO convertRequestToBookOrderDTO(HttpServletRequest request){
        if(request == null) return null;
        UserDTO userDTO = (UserDTO) request.getSession().getAttribute(LOGGED_USER);
        if(userDTO == null) return null;

        try {
            Date date = new Date(new java.util.Date().getTime());
            Date returnedDate = Date.valueOf(date.toLocalDate().plusDays(Long.parseLong(request.getParameter(ORDER_DAYS))));
            return new BookOrderDTO(
                    Integer.parseInt(request.getParameter(BOOK_ID)),
                    userDTO.getLogin(),
                    Integer.parseInt(request.getParameter(ORDER_TYPE)),
                    date,
                    returnedDate);
        }catch (Exception e){
            return null;
        }
    }

    public static BookOrderForReaderDTO convertToBookOrderForReaderDTO(BookOrder bookOrder, int fineAmount){
        return new BookOrderForReaderDTO(
                bookOrder.getId(),
                bookOrder.getBookIdFK(),
                bookOrder.getReaderFK(),
                bookOrder.getType().getId(),
                bookOrder.getType().getValueEN(),
                bookOrder.getType().getValueUA(),
                bookOrder.getDate(),
                bookOrder.getDateReturnPlanned(),
                bookOrder.getStatus().getId(),
                bookOrder.getStatus().getValueEN(),
                bookOrder.getStatus().getValueUA(),
                fineAmount
        );
    }

    public static BookOrderForLibrarianDTO convertToBookOrderForLibrarianDTO(BookOrder bookOrder, int fineAmount, ReaderDTO readerDTO){
        return new BookOrderForLibrarianDTO(
                bookOrder.getId(),
                bookOrder.getBookIdFK(),
                bookOrder.getReaderFK(),
                bookOrder.getType().getId(),
                bookOrder.getType().getValueEN(),
                bookOrder.getType().getValueUA(),
                bookOrder.getDate(),
                bookOrder.getDateReturnPlanned(),
                bookOrder.getStatus().getId(),
                bookOrder.getStatus().getValueEN(),
                bookOrder.getStatus().getValueUA(),
                fineAmount,
                readerDTO
        );
    }
}
