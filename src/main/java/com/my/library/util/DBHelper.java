package com.my.library.util;


import com.my.library.dao.connection.DataSource;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

public class DBHelper {

    public static void main(String[] args) throws SQLException{
        // Creating DB
        Connection con = DataSource.getConnection();
        executeSqlQueryFile(con, "sql/mysql-db-create.sql" );

    }

    public static void executeSqlQueryFile(Connection con, String filename){
        ScriptRunner sr = new ScriptRunner(con);
        Reader reader;
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        sr.runScript(reader);
    }

}
