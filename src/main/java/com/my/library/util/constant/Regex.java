package com.my.library.util.constant;

/**
 * Regex class.
 * Contains regular expressions.
 *
 * @author Volodymyr Litvinchuk.
 * @version 1.0
 */
public final class Regex {
    private Regex(){}

    public static final String LOGIN_REGEX = "^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\\d.-]{0,19}$";
    public static final String EMAIL_REGEX = "^[\\w.%+-]+@[\\w.-]+\\.[a-zA-Z]{2,6}$";
    public static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,20}$";
    public static final String NAME_REGEX = "^[A-Za-zА-ЩЬЮЯҐІЇЄа-щьюяґіїє'\\- ]{1,30}";
    public static final String NAME_EN_REGEX = "^[A-Za-z'\\- ]{1,30}";
    public static final String NAME_UA_REGEX = "^[А-ЩЬЮЯҐІЇЄа-щьюяґіїє'\\- ]{1,30}";
    public static final String COMPLEX_NAME_EN_REGEX = "^[A-Za-z\\s'\\- ]{1,30}";
    public static final String COMPLEX_NAME_UA_REGEX = "^[А-ЩЬЮЯҐІЇЄа-щьюяґіїє\\s'\\- ]{1,30}";

}
