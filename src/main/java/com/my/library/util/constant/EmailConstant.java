package com.my.library.util.constant;

public final class EmailConstant {
    private EmailConstant(){}

    public static final String FORGOT_PASSWORD_SUBJECT = "Your new password";
    public static final String FORGOT_PASSWORD_BODY = "New password was successfully generated for you. <br> Password - %s. <br>Use it to sign in into your account";
}
