package com.my.library.exception.constant;

public final class Message {
    private Message(){}

    public static final String ENTER_CORRECT_LOGIN = "error.login.format";
    public static final String ENTER_CORRECT_PASSWORD = "error.password.format";
    public static final String ENTER_CORRECT_FIRST_NAME = "error.firstName.format";
    public static final String ENTER_CORRECT_LAST_NAME = "error.lastName.format";
    public static final String ENTER_CORRECT_EMAIL = "error.email.format";

    public static final String ACCOUNT_LOCKED = "account.locked";

    public static final String ENTER_CORRECT_TITLE_EN = "error.titleEN.format";
    public static final String ENTER_CORRECT_TITLE_UA = "error.titleUA.format";
    public static final String ENTER_CORRECT_LANGUAGE = "error.language.format";
    public static final String ENTER_CORRECT_COPIES_OWNED = "error.copiesOwned.format";
    public static final String ENTER_CORRECT_AUTHOR_FIRST_NAME_EN = "error.author.firstNameEN.format";
    public static final String ENTER_CORRECT_AUTHOR_FIRST_NAME_UA = "error.author.firstNameUA.format";
    public static final String ENTER_CORRECT_AUTHOR_LAST_NAME_EN = "error.author.lastNameEN.format";
    public static final String ENTER_CORRECT_AUTHOR_LAST_NAME_UA = "error.author.lastNameUA.format";
    public static final String ENTER_CORRECT_PUBLICATION_DATE = "error.publicationDate.format";
    public static final String ENTER_CORRECT_PUBLICATION_NAME_EN = "error.publicationNameEN.format";
    public static final String ENTER_CORRECT_PUBLICATION_NAME_UA = "error.publicationNameUA.format";
    public static final String ENTER_CORRECT_AUTHOR_ID = "error.author.id.format";
    public static final String ENTER_CORRECT_PUBLICATION_ID = "error.publication.id.format";

    public static final String WRONG_LOGIN = "error.login.wrong";
    public static final String WRONG_PASSWORD = "error.password.wrong";
    public static final String WRONG_OLD_PASSWORD = "error.old.password";

    public static final String ERROR_CONVERT = "error.covert";
    public static final String UNKNOWN_ERROR = "error.unknown";

    public static final String DUPLICATE_EMAIL = "error.email.duplicate";
    public static final String DUPLICATE_LOGIN = "error.login.duplicate";
    public static final String DUPLICATE_BOOK = "error.book.duplicate";

    public static final String WRONG_USER_STATUS_VALUE = "error.user.status.value";

    public static final String WRONG_ORDER_TYPE = "error.order.type.format";
    public static final String WRONG_PAGE_SIZE = "error.page.size.format";
    public static final String WRONG_CURRENT_PAGE = "error.current.page.format";

    public static final String USER_HAS_NOT_RETURNED_BOOK = "error.user.has.not.returned.book";

    public static final String WRONG_BOOK_ID = "error.book.id.wrong";
    public static final String WRONG_BOOK_ID_FORMAT = "error.book.id.format";
    public static final String WRONG_AUTHOR_ID = "error.author.id.wrong";
    public static final String WRONG_PUBLICATION_ID = "error.publication.id.wrong";

    public static final String NOT_ALL_BOOKS_ARE_IN_LIBRARY = "error.not.all.books.are.in.library";
    public static final String BOOK_ALREADY_ORDERED = "book.already.ordered";
    public static final String WRONG_BOOK_ORDER_TYPE = "wrong.book.order.type";
    public static final String WRONG_READER_LOGIN = "wrong.reader.login";
    public static final String READER_IS_SUSPENDED = "your.account.suspended";
    public static final String ZERO_QUANTITY = "book.is.not.available";

    public static final String WRONG_BOOK_ORDER_ID = "wrong.book.order.id";
    public static final String ORDER_IS_FINED = "error.order.is.fined";

    public static final String WRONG_SEARCH_PARAMS = "wrong.search.params";

    public static final String MAIL_ERROR = "mail.error";
}
