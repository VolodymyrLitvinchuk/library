package com.my.library.exception;

public class DAOException extends Exception {
    public DAOException(Throwable cause) {
        super(cause);
    }
}