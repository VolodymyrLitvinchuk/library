package com.my.library.exception;

import static com.my.library.exception.constant.Message.DUPLICATE_LOGIN;

public class DuplicateLoginException extends ServiceException{
    public DuplicateLoginException(){
        super(DUPLICATE_LOGIN);
    }
}
