package com.my.library.exception;

import static com.my.library.exception.constant.Message.WRONG_LOGIN;

public class IncorrectLoginException extends ServiceException{
    public IncorrectLoginException() {
        super(WRONG_LOGIN);
    }
}