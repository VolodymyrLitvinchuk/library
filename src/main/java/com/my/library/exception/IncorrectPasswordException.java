package com.my.library.exception;

import static com.my.library.exception.constant.Message.WRONG_PASSWORD;

public class IncorrectPasswordException extends ServiceException{
    public IncorrectPasswordException() {
        super(WRONG_PASSWORD);
    }
}