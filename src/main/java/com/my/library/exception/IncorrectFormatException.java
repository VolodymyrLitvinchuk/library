package com.my.library.exception;

public class IncorrectFormatException extends ServiceException {
    public IncorrectFormatException(String message) {
        super(message);
    }
}