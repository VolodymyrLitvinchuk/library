package com.my.library.exception;

import static com.my.library.exception.constant.Message.DUPLICATE_EMAIL;

public class DuplicateEmailException extends ServiceException{
    public DuplicateEmailException(){
        super(DUPLICATE_EMAIL);
    }
}
