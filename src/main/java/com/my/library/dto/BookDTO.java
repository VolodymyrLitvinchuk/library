package com.my.library.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class BookDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    // BOOK fields
    private Integer id;
    private String titleEN;
    private String titleUA;
    private String language;
    private Date publicationDate;
    private Integer copiesOwned;

    // AUTHOR fields
    private Integer authorId;
    private String authorFirstNameEN;
    private String authorFirstNameUA;
    private String authorLastNameEN;
    private String authorLastNameUA;

    // PUBLICATION fields
    private Integer publicationId;
    private String publicationNameEN;
    private String publicationNameUA;

    /// BOOK AVAILABLE
    private Integer availableQuantity;


    public BookDTO(Integer id,
                   String titleEN,
                   String titleUA,
                   String language,
                   Date publicationDate,
                   Integer copiesOwned,
                   Integer authorId,
                   String authorFirstNameEN,
                   String authorFirstNameUA,
                   String authorLastNameEN,
                   String authorLastNameUA,
                   Integer publicationId,
                   String publicationNameEN,
                   String publicationNameUA,
                   Integer availableQuantity) {
        this.id = id;
        this.titleEN = titleEN;
        this.titleUA = titleUA;
        this.language = language;
        this.publicationDate = publicationDate;
        this.copiesOwned = copiesOwned;
        this.authorId = authorId;
        this.authorFirstNameEN = authorFirstNameEN;
        this.authorFirstNameUA = authorFirstNameUA;
        this.authorLastNameEN = authorLastNameEN;
        this.authorLastNameUA = authorLastNameUA;
        this.publicationId = publicationId;
        this.publicationNameEN = publicationNameEN;
        this.publicationNameUA = publicationNameUA;
        this.availableQuantity = availableQuantity;
    }

    public BookDTO(String titleEN,
                   String titleUA,
                   String language,
                   Date publicationDate,
                   Integer copiesOwned,
                   String authorFirstNameEN,
                   String authorFirstNameUA,
                   String authorLastNameEN,
                   String authorLastNameUA,
                   String publicationNameEN,
                   String publicationNameUA) {
        this.titleEN = titleEN;
        this.titleUA = titleUA;
        this.language = language;
        this.publicationDate = publicationDate;
        this.copiesOwned = copiesOwned;
        this.authorFirstNameEN = authorFirstNameEN;
        this.authorFirstNameUA = authorFirstNameUA;
        this.authorLastNameEN = authorLastNameEN;
        this.authorLastNameUA = authorLastNameUA;
        this.publicationNameEN = publicationNameEN;
        this.publicationNameUA = publicationNameUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDTO bookDTO = (BookDTO) o;
        return Objects.equals(id, bookDTO.id)
                && Objects.equals(titleEN, bookDTO.titleEN)
                && Objects.equals(language, bookDTO.language)
                && Objects.equals(publicationDate, bookDTO.publicationDate)
                && Objects.equals(authorId, bookDTO.authorId)
                && Objects.equals(authorFirstNameEN, bookDTO.authorFirstNameEN)
                && Objects.equals(authorLastNameEN, bookDTO.authorLastNameEN)
                && Objects.equals(publicationId, bookDTO.publicationId)
                && Objects.equals(publicationNameEN, bookDTO.publicationNameEN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                titleEN,
                language,
                publicationDate,
                authorId,
                authorFirstNameEN,
                authorLastNameEN,
                publicationId,
                publicationNameEN);
    }

    public boolean isBuilt(){
        return titleEN!=null && !titleEN.isBlank()
                && titleUA!=null && !titleUA.isBlank()
                && language!=null && !language.isBlank()
                && publicationDate!=null
                && copiesOwned!=null
                && authorFirstNameEN!=null && !authorFirstNameEN.isBlank()
                && authorFirstNameUA!=null && !authorFirstNameUA.isBlank()
                && authorLastNameEN!=null && !authorLastNameEN.isBlank()
                && authorLastNameUA!=null && !authorLastNameUA.isBlank()
                && publicationNameEN!=null && !publicationNameEN.isBlank()
                && publicationNameUA!=null && !publicationNameUA.isBlank();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitleEN() {
        return titleEN;
    }

    public void setTitleEN(String titleEN) {
        this.titleEN = titleEN;
    }

    public String getTitleUA() {
        return titleUA;
    }

    public void setTitleUA(String titleUA) {
        this.titleUA = titleUA;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Integer getCopiesOwned() {
        return copiesOwned;
    }

    public void setCopiesOwned(Integer copiesOwned) {
        this.copiesOwned = copiesOwned;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorFirstNameEN() {
        return authorFirstNameEN;
    }

    public void setAuthorFirstNameEN(String authorFirstNameEN) {
        this.authorFirstNameEN = authorFirstNameEN;
    }

    public String getAuthorFirstNameUA() {
        return authorFirstNameUA;
    }

    public void setAuthorFirstNameUA(String authorFirstNameUA) {
        this.authorFirstNameUA = authorFirstNameUA;
    }

    public String getAuthorLastNameEN() {
        return authorLastNameEN;
    }

    public void setAuthorLastNameEN(String authorLastNameEN) {
        this.authorLastNameEN = authorLastNameEN;
    }

    public String getAuthorLastNameUA() {
        return authorLastNameUA;
    }

    public void setAuthorLastNameUA(String authorLastNameUA) {
        this.authorLastNameUA = authorLastNameUA;
    }

    public Integer getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(Integer publicationId) {
        this.publicationId = publicationId;
    }

    public String getPublicationNameEN() {
        return publicationNameEN;
    }

    public void setPublicationNameEN(String publicationNameEN) {
        this.publicationNameEN = publicationNameEN;
    }

    public String getPublicationNameUA() {
        return publicationNameUA;
    }

    public void setPublicationNameUA(String publicationNameUA) {
        this.publicationNameUA = publicationNameUA;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }
}
