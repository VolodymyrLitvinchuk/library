package com.my.library.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class BookOrderDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private final int bookID;
    private final String readerLogin;
    private final int orderTypeID;
    private final Date orderDate;
    private final Date returnedDatePlanned;
    private int orderStatusID;

    public BookOrderDTO(int id, int bookID, String readerLogin, int orderTypeID, Date orderDate, Date returnedDatePlanned, int orderStatusID) {
        this.id = id;
        this.bookID = bookID;
        this.readerLogin = readerLogin;
        this.orderTypeID = orderTypeID;
        this.orderDate = orderDate;
        this.returnedDatePlanned = returnedDatePlanned;
        this.orderStatusID = orderStatusID;
    }

    public BookOrderDTO(int bookID, String readerLogin, int orderTypeID, Date orderDate, Date returnedDatePlanned) {
        this.bookID = bookID;
        this.readerLogin = readerLogin;
        this.orderTypeID = orderTypeID;
        this.orderDate = orderDate;
        this.returnedDatePlanned = returnedDatePlanned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookOrderDTO that = (BookOrderDTO) o;
        return id == that.id && bookID == that.bookID && orderTypeID == that.orderTypeID && orderStatusID == that.orderStatusID && Objects.equals(readerLogin, that.readerLogin) && Objects.equals(orderDate, that.orderDate) && Objects.equals(returnedDatePlanned, that.returnedDatePlanned);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bookID, readerLogin, orderTypeID, orderDate, returnedDatePlanned, orderStatusID);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrderStatusID(int orderStatusID) {
        this.orderStatusID = orderStatusID;
    }

    public int getBookID() {
        return bookID;
    }

    public String getReaderLogin() {
        return readerLogin;
    }

    public int getOrderTypeID() {
        return orderTypeID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public Date getReturnedDatePlanned() {
        return returnedDatePlanned;
    }

    public int getOrderStatusID() {
        return orderStatusID;
    }
}
