package com.my.library.dto;

import java.io.Serializable;
import java.util.Objects;

public class ReaderDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String login;
    private final int activeOrders;
    private final int finishedOrders;
    private final int canceledOrders;
    private final int unPaidFines;
    private final int paidFines;

    public ReaderDTO(String login, int activeOrders, int finishedOrders, int canceledOrders, int unPaidFines, int paidFines) {
        this.login = login;
        this.activeOrders = activeOrders;
        this.finishedOrders = finishedOrders;
        this.canceledOrders = canceledOrders;
        this.unPaidFines = unPaidFines;
        this.paidFines = paidFines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReaderDTO readerDTO = (ReaderDTO) o;
        return activeOrders == readerDTO.activeOrders && finishedOrders == readerDTO.finishedOrders && canceledOrders == readerDTO.canceledOrders && unPaidFines == readerDTO.unPaidFines && paidFines == readerDTO.paidFines && Objects.equals(login, readerDTO.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, activeOrders, finishedOrders, canceledOrders, unPaidFines, paidFines);
    }

    public String getLogin() {
        return login;
    }

    public int getActiveOrders() {
        return activeOrders;
    }

    public int getFinishedOrders() {
        return finishedOrders;
    }

    public int getCanceledOrders() {
        return canceledOrders;
    }

    public int getUnPaidFines() {
        return unPaidFines;
    }

    public int getPaidFines() {
        return paidFines;
    }
}
