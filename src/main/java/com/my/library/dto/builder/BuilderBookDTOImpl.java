package com.my.library.dto.builder;

import com.my.library.dto.BookDTO;

import java.sql.Date;

public class BuilderBookDTOImpl implements BuilderBookDTO{
    // BOOK fields
    private String titleEN;
    private String titleUA;
    private String language;
    private Date publicationDate;
    private Integer copiesOwned;

    // AUTHOR fields
    private String authorFirstNameEN;
    private String authorFirstNameUA;
    private String authorLastNameEN;
    private String authorLastNameUA;

    // PUBLICATION fields
    private String publicationNameEN;
    private String publicationNameUA;

    @Override
    public BuilderBookDTO setTitleEN(String titleEN) {
        this.titleEN = titleEN;
        return this;
    }

    @Override
    public BuilderBookDTO setTitleUA(String titleUA) {
        this.titleUA = titleUA;
        return this;
    }

    @Override
    public BuilderBookDTO setLanguage(String language) {
        this.language = language;
        return this;
    }

    @Override
    public BuilderBookDTO setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
        return this;
    }

    @Override
    public BuilderBookDTO setCopiesOwned(Integer copiesOwned) {
        this.copiesOwned = copiesOwned;
        return this;
    }

    @Override
    public BuilderBookDTO setAuthorFirstNameEN(String authorFirstNameEN) {
        this.authorFirstNameEN = authorFirstNameEN;
        return this;
    }

    @Override
    public BuilderBookDTO setAuthorFirstNameUA(String authorFirstNameUA) {
        this.authorFirstNameUA = authorFirstNameUA;
        return this;
    }

    @Override
    public BuilderBookDTO setAuthorLastNameEN(String authorLastNameEN) {
        this.authorLastNameEN = authorLastNameEN;
        return this;
    }

    @Override
    public BuilderBookDTO setAuthorLastNameUA(String authorLastNameUA) {
        this.authorLastNameUA = authorLastNameUA;
        return this;
    }

    @Override
    public BuilderBookDTO setPublicationNameEN(String publicationNameEN) {
        this.publicationNameEN = publicationNameEN;
        return this;
    }

    @Override
    public BuilderBookDTO setPublicationNameUA(String publicationNameUA) {
        this.publicationNameUA = publicationNameUA;
        return this;
    }

    @Override
    public BookDTO get() {
        BookDTO bookDTO = new BookDTO(
                titleEN,
                titleUA,
                language,
                publicationDate,
                copiesOwned,
                authorFirstNameEN,
                authorFirstNameUA,
                authorLastNameEN,
                authorLastNameUA,
                publicationNameEN,
                publicationNameUA
        );
        if(bookDTO.isBuilt()) return bookDTO;
        return null;
    }
}
