package com.my.library.dto.builder;

import com.my.library.dto.BookDTO;

import java.sql.Date;

public interface BuilderBookDTO {
    BuilderBookDTO setTitleEN(String titleEN);
    BuilderBookDTO setTitleUA(String titleUA);
    BuilderBookDTO setLanguage(String language);
    BuilderBookDTO setPublicationDate(Date publicationDate);
    BuilderBookDTO setCopiesOwned(Integer copiesOwned);

    BuilderBookDTO setAuthorFirstNameEN(String authorFirstNameEN);
    BuilderBookDTO setAuthorFirstNameUA(String authorFirstNameUA);
    BuilderBookDTO setAuthorLastNameEN(String authorLastNameEN);
    BuilderBookDTO setAuthorLastNameUA(String authorLastNameUA);

    BuilderBookDTO setPublicationNameEN(String publicationNameEN);
    BuilderBookDTO setPublicationNameUA(String publicationNameUA);

    BookDTO get();
}
