package com.my.library.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class BookOrderForLibrarianDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private final int id;
    private final int bookID;
    private final String readerLogin;
    private final int orderTypeID;
    private final String orderTypeEN;
    private final String orderTypeUA;
    private final Date orderDate;
    private final Date returnedDatePlanned;
    private final int orderStatusID;
    private final String orderStatusEN;
    private final String orderStatusUA;
    private final int fineAmount;
    private final ReaderDTO readerDTO;

    public BookOrderForLibrarianDTO(int id,
                                    int bookID,
                                    String readerLogin,
                                    int orderTypeID,
                                    String orderTypeEN,
                                    String orderTypeUA,
                                    Date orderDate,
                                    Date returnedDatePlanned,
                                    int orderStatusID,
                                    String orderStatusEN,
                                    String orderStatusUA,
                                    int fineAmount,
                                    ReaderDTO readerDTO){
        this.id = id;
        this.bookID = bookID;
        this.readerLogin = readerLogin;
        this.orderTypeID = orderTypeID;
        this.orderTypeEN = orderTypeEN;
        this.orderTypeUA = orderTypeUA;
        this.orderDate = orderDate;
        this.returnedDatePlanned = returnedDatePlanned;
        this.orderStatusID = orderStatusID;
        this.orderStatusEN = orderStatusEN;
        this.orderStatusUA = orderStatusUA;
        this.fineAmount = fineAmount;
        this.readerDTO = readerDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookOrderForLibrarianDTO that = (BookOrderForLibrarianDTO) o;
        return id == that.id && bookID == that.bookID && orderTypeID == that.orderTypeID && orderStatusID == that.orderStatusID && fineAmount == that.fineAmount && Objects.equals(readerLogin, that.readerLogin) && Objects.equals(orderTypeEN, that.orderTypeEN) && Objects.equals(orderTypeUA, that.orderTypeUA) && Objects.equals(orderDate, that.orderDate) && Objects.equals(returnedDatePlanned, that.returnedDatePlanned) && Objects.equals(orderStatusEN, that.orderStatusEN) && Objects.equals(orderStatusUA, that.orderStatusUA) && Objects.equals(readerDTO, that.readerDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bookID, readerLogin, orderTypeID, orderTypeEN, orderTypeUA, orderDate, returnedDatePlanned, orderStatusID, orderStatusEN, orderStatusUA, fineAmount, readerDTO);
    }

    public int getId() {
        return id;
    }

    public int getBookID() {
        return bookID;
    }

    public String getReaderLogin() {
        return readerLogin;
    }

    public int getOrderTypeID() {
        return orderTypeID;
    }

    public String getOrderTypeEN() {
        return orderTypeEN;
    }

    public String getOrderTypeUA() {
        return orderTypeUA;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public Date getReturnedDatePlanned() {
        return returnedDatePlanned;
    }

    public int getOrderStatusID() {
        return orderStatusID;
    }

    public String getOrderStatusEN() {
        return orderStatusEN;
    }

    public String getOrderStatusUA() {
        return orderStatusUA;
    }

    public int getFineAmount() {
        return fineAmount;
    }

    public ReaderDTO getReaderDTO() {
        return readerDTO;
    }
}
