package com.my.library.dto;

import java.io.Serializable;
import java.util.Objects;

public class AuthorDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String firstNameEN;
    private String firstNameUA;
    private String lastNameEN;
    private String lastNameUA;

    public AuthorDTO(Integer id, String firstNameEN, String firstNameUA, String lastNameEN, String lastNameUA) {
        this.id = id;
        this.firstNameEN = firstNameEN;
        this.firstNameUA = firstNameUA;
        this.lastNameEN = lastNameEN;
        this.lastNameUA = lastNameUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDTO authorDTO = (AuthorDTO) o;
        return Objects.equals(id, authorDTO.id) && Objects.equals(firstNameEN, authorDTO.firstNameEN) && Objects.equals(firstNameUA, authorDTO.firstNameUA) && Objects.equals(lastNameEN, authorDTO.lastNameEN) && Objects.equals(lastNameUA, authorDTO.lastNameUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstNameEN, firstNameUA, lastNameEN, lastNameUA);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstNameEN() {
        return firstNameEN;
    }

    public void setFirstNameEN(String firstNameEN) {
        this.firstNameEN = firstNameEN;
    }

    public String getFirstNameUA() {
        return firstNameUA;
    }

    public void setFirstNameUA(String firstNameUA) {
        this.firstNameUA = firstNameUA;
    }

    public String getLastNameEN() {
        return lastNameEN;
    }

    public void setLastNameEN(String lastNameEN) {
        this.lastNameEN = lastNameEN;
    }

    public String getLastNameUA() {
        return lastNameUA;
    }

    public void setLastNameUA(String lastNameUA) {
        this.lastNameUA = lastNameUA;
    }
}
