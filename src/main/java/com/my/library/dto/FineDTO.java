package com.my.library.dto;

import java.io.Serializable;

public class FineDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private final int id;
    private final int orderId;
    private final int amount;
    private final int statusId;
    private final String statusEN;
    private final String statusUA;

    public FineDTO(int id, int orderId, int amount, int statusId, String statusEN, String statusUA) {
        this.id = id;
        this.orderId = orderId;
        this.amount = amount;
        this.statusId = statusId;
        this.statusEN = statusEN;
        this.statusUA = statusUA;
    }

    public int getId() {
        return id;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getAmount() {
        return amount;
    }

    public int getStatusId(){return statusId;}

    public String getStatusEN() {
        return statusEN;
    }

    public String getStatusUA() {
        return statusUA;
    }
}
