package com.my.library.service;

import com.my.library.service.abstr.*;
import com.my.library.service.implementation.*;

public final class ServiceFactory {
    private ServiceFactory(){}

    private static final UserService userService;
    private static final BookService bookService;
    private static final AuthorService authorService;
    private static final BookOrderService bookOrderService;
    private static final FineService fineService;
    private static final ReaderService readerService;

    static {
        userService = new UserServiceImpl();
        bookService = new BookServiceImpl();
        authorService = new AuthorServiceImpl();
        bookOrderService = new BookOrderServiceImpl();
        fineService = new FineServiceImpl();
        readerService = new ReaderServiceImpl();
    }

    public static UserService getUserService() {
        return userService;
    }
    public static BookService getBookService() {return bookService;}
    public static AuthorService getAuthorService() {
        return authorService;
    }
    public static BookOrderService getBookOrderService() {
        return bookOrderService;
    }
    public static FineService getFineService() {
        return fineService;
    }
    public static ReaderService getReaderService() {
        return readerService;
    }
}
