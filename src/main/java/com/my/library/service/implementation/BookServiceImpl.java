package com.my.library.service.implementation;

import com.my.library.dao.connection.DataSource;
import com.my.library.dao.DAOFactory;
import com.my.library.dao.entity.book.*;
import com.my.library.dao.entity.builder.*;
import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.dto.BookDTO;
import com.my.library.exception.*;
import com.my.library.service.abstr.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.ORDER_BY_LIMIT_OFFSET;
import static com.my.library.dao.mysql.Constant.SEARCH_BOOK;
import static com.my.library.exception.constant.Message.*;
import static com.my.library.util.ConvertorUtil.convertBookToDTO;

/**
 * Implementation of BookService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class BookServiceImpl implements BookService {
    /**
     * Slf4j logger
     */
    private final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    /**
     * Method inserts new Book in DB, using DAO methods.
     * Method checks whether book author already exists, if there is no such author then inserts new author.
     * Method checks whether book publication already exists, if there is no such publication then inserts new publication.
     * Then method insets new Book using {@link com.my.library.dao.abstr.book.BookDAO#insert(Book, Connection)}
     * The last action is inserting Book available information into DB using {@link com.my.library.dao.abstr.book.BookAvailableDAO#insert(int, int, Connection)}
     *
     * @param bookDTO {@link BookDTO} object.
     * @throws ServiceException if book is already exists in DB.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong during inserting.
     */
    @Override
    public void add(BookDTO bookDTO) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            connection.setAutoCommit(false);

            BookBuilder builder = new BookBuilderImpl();

            // перевіряю чи є автор в БД, якщо немає то створюю його
            Optional<Author> optionalAuthor =
                    DAOFactory.getAuthorDAO()
                            .getByFirstLastNameEN(bookDTO.getAuthorFirstNameEN(),bookDTO.getAuthorLastNameEN());
            if(optionalAuthor.isEmpty()){
                Author author = new Author(
                        bookDTO.getAuthorFirstNameEN(),
                        bookDTO.getAuthorFirstNameUA(),
                        bookDTO.getAuthorLastNameEN(),
                        bookDTO.getAuthorLastNameUA()
                );
                DAOFactory.getAuthorDAO().insert(author, connection);
                if(author.getId()==null){
                    connection.rollback();
                    throw new ServiceException(UNKNOWN_ERROR);
                }
                builder.setAuthor(author);
            }else {
                builder.setAuthor(optionalAuthor.get());
            }

            // перевіряю чи є видавництво в БД, якщо немає то створюю його
            Optional<Publication> optionalPublication =
                    DAOFactory.getPublicationDAO().getByNameEN(bookDTO.getPublicationNameEN());
            if(optionalPublication.isEmpty()){
                Publication publication = new Publication(
                        bookDTO.getPublicationNameEN(),
                        bookDTO.getPublicationNameUA());
                DAOFactory.getPublicationDAO().insert(publication,connection);
                if(publication.getId()==null){
                    connection.rollback();
                    throw new ServiceException(UNKNOWN_ERROR);
                }
                builder.setPublication(publication);
            }else {
                builder.setPublication(optionalPublication.get());
            }

            builder.setTitle_EN(bookDTO.getTitleEN());
            builder.setTitle_UA(bookDTO.getTitleUA());
            builder.setCopiesOwned(bookDTO.getCopiesOwned());
            builder.setLanguage(bookDTO.getLanguage());
            builder.setPublication_Date(bookDTO.getPublicationDate());

            Book book = builder.get();
            if(book==null) throw new ServiceException(UNKNOWN_ERROR);

            DAOFactory.getBookDAO().insert(book,connection);
            if(book.getId()==null){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }

            if(!DAOFactory.getBookAvailableDAO().insert(book.getId(), book.getCopiesOwned(),connection)){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }


            connection.commit();

        }catch (Exception e){
            log.warn("Exception while add()",e);
            if(e.getMessage().contains("Duplicate"))  throw new ServiceException(DUPLICATE_BOOK);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns valid sql query for book searching depends on parameters.
     * If locale equals 'en' then search will be performed by book title in English.
     *
     * @param titlePattern Book title sql pattern.
     * @param authorFirstNamePattern Book author first name sql pattern.
     * @param authorLastNamePattern Book author last name sql pattern.
     * @param locale Current user locale.
     * @return Sql search query.
     */
    @Override
    public String buildSearchQuery(String titlePattern, String authorFirstNamePattern, String authorLastNamePattern, String locale) {
        String query = SEARCH_BOOK;
        boolean notFirst = false;

        if(titlePattern!=null && !titlePattern.isBlank()){
            if(locale!=null && locale.equals("ua")){
                query += "WHERE title_UA LIKE '%%" + titlePattern + "%%' ";
            }else query += "WHERE title_EN LIKE '%%" + titlePattern + "%%' ";
            notFirst = true;
        }

        if(authorFirstNamePattern!=null && !authorFirstNamePattern.isBlank()){
            if(locale!=null && locale.equals("ua")){
                if(notFirst) query += "AND ";
                else query += "WHERE ";
                query += "first_name_UA LIKE '%%" + authorFirstNamePattern + "%%' ";
            }else {
                if(notFirst) query += "AND ";
                else query += "WHERE ";
                query += "first_name_EN LIKE '%%" + authorFirstNamePattern + "%%' ";
            }
            if(!notFirst) notFirst = true;
        }

        if(authorLastNamePattern!=null && !authorLastNamePattern.isBlank()){
            if(locale!=null && locale.equals("ua")){
                if(notFirst) query += "AND ";
                else query += "WHERE ";
                query += "last_name_UA LIKE '%%" + authorLastNamePattern + "%%' ";
            }else {
                if(notFirst) query += "AND ";
                else query += "WHERE ";
                query += "last_name_EN LIKE '%%" + authorLastNamePattern + "%%' ";
            }
        }

        return query + ORDER_BY_LIMIT_OFFSET;
    }

    /**
     * Method returns book specified by id.
     *
     * @param id Book's id.
     * @return {@link BookDTO} object.
     * @throws ServiceException if DAO layer return Optional.empty().
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public BookDTO getBook(int id) throws ServiceException {
        try{
            Optional<Book> optionalBook = DAOFactory.getBookDAO().get(id);
            if(optionalBook.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);

            return convertBookToDTO(optionalBook.get());

        }catch (Exception e){
            if(e.getMessage().equals(WRONG_BOOK_ID)) throw new ServiceException(UNKNOWN_ERROR);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns list of books that meet search params.
     * Books are sorted.
     * Method uses {@link com.my.library.dao.abstr.book.BookDAO#searchBooks(String, BookOrderBy, int, int)} and {@link com.my.library.util.ConvertorUtil#convertBookToDTO(Book)} methods.
     *
     * @param query Sql search query that was built in {@link BookService#buildSearchQuery(String, String, String, String)}
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookDTO}.
     * @throws ServiceException if DAO layer return Optional.empty().
     * @throws ServiceException if {@link com.my.library.util.ConvertorUtil#convertBookToDTO(Book)} return null.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public List<BookDTO> searchBooks(String query, BookOrderBy orderBy, int offset, int limit) throws ServiceException {
        try {
            Optional<List<Book>> optionalBooks =
                    DAOFactory.getBookDAO()
                            .searchBooks(query, orderBy, offset, limit);
            if(optionalBooks.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<Book> bookList = optionalBooks.get();

            List<BookDTO> bookDTOList = new ArrayList<>();
            BookDTO bookDTO;
            for (Book book : bookList){
                bookDTO = convertBookToDTO(book);
                if(bookDTO==null) throw new ServiceException(UNKNOWN_ERROR);

                bookDTOList.add(bookDTO);
            }

            return bookDTOList;

        }catch (DAOException e){
            log.warn("Exception while searchBooks(). Query={}" ,query ,e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns list of books that meet search params.
     * Books are sorted.
     * Method uses {@link com.my.library.dao.abstr.book.BookDAO#searchBooks(String, BookOrderBy, int, int)} and {@link com.my.library.util.ConvertorUtil#convertBookToDTO(Book)} methods.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookDTO}.
     * @throws ServiceException if DAO layer return Optional.empty().
     * @throws ServiceException if {@link com.my.library.util.ConvertorUtil#convertBookToDTO(Book)} return null.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public List<BookDTO> getBooks(BookOrderBy orderBy, int offset, int limit) throws ServiceException {
        try {
            Optional<List<Book>> optionalBooks =
                    DAOFactory.getBookDAO()
                            .getBooksByOrderOffsetLimit(orderBy, offset, limit);
            if(optionalBooks.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<Book> bookList = optionalBooks.get();

            List<BookDTO> bookDTOList = new ArrayList<>();
            BookDTO bookDTO;
            for (Book book : bookList){
                bookDTO = convertBookToDTO(book);
                if(bookDTO==null) throw new ServiceException(UNKNOWN_ERROR);

                bookDTOList.add(bookDTO);
            }

            return bookDTOList;

        }catch (DAOException e){
            log.warn("Exception while getBooks()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns last page number for book's page selectors.
     * If DAO layer return Optional.empty() this method returns 1.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumber(int pageSize) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getBookDAO().getBookQuantity();
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumber()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns last page number for book searching result page selectors.
     * If DAO layer return Optional.empty() this method returns 1.
     *
     * @param searchQuery Sql search query that was built in {@link BookService#buildSearchQuery(String, String, String, String)}
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumberSearchBooks(String searchQuery, int pageSize) throws ServiceException {
        try {
            searchQuery = String.copyValueOf(
                    searchQuery.toCharArray(),
                    0, searchQuery.length()-30)
                    .replace("*","count(*)");

            Optional<Integer> optional =
                    DAOFactory.getBookDAO().getBookQuantity(searchQuery);
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumber()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's title in English.
     *
     * @param bookId Book's id.
     * @param newTitleEN Book's new title in English value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookTitleEN(int bookId, String newTitleEN) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            DAOFactory.getBookDAO().updateTitleEN(book, newTitleEN);
            if(!book.getTitle_EN().equals(newTitleEN)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookTitleEN()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's title in Ukrainian.
     *
     * @param bookId Book's id.
     * @param newTitleUA Book's new title in Ukrainian value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookTitleUA(int bookId, String newTitleUA) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            DAOFactory.getBookDAO().updateTitleUA(book, newTitleUA);
            if(!book.getTitle_UA().equals(newTitleUA)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookTitleUA()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's language.
     *
     * @param bookId Book's id.
     * @param newLanguage Book's new language value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookLanguage(int bookId, String newLanguage) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            DAOFactory.getBookDAO().updateLanguage(book, newLanguage);
            if(!book.getLanguage().equals(newLanguage)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookLanguage()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's copies owned.
     * Also, method checks whether new BookAvailable value will be > 0.
     *
     * @param bookId Book's id.
     * @param newCopiesOwned Book's new copies owned value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if there is no information about available copies.
     * @throws ServiceException if new available copies will be less than 0.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookCopiesOwned(int bookId, int newCopiesOwned) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            Optional<Integer> optionalInteger = DAOFactory.getBookAvailableDAO().get(bookId);
            if(optionalInteger.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);

            int availableOLD = optionalInteger.get();
            int copiesOwnedOLD = optional.get().getCopiesOwned();
            int newAvailable = availableOLD + newCopiesOwned - copiesOwnedOLD;
            if(newAvailable<0) throw new ServiceException(ENTER_CORRECT_COPIES_OWNED);


            connection.setAutoCommit(false);

            DAOFactory.getBookDAO().updateCopiesOwned(book, newCopiesOwned, connection);
            if(book.getCopiesOwned() != newCopiesOwned){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }
            if(!DAOFactory.getBookAvailableDAO().update(bookId, newAvailable, connection )){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }

            connection.commit();

        }catch (Exception e){
            log.warn("Exception while updateBookCopiesOwned()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's author.
     * Method checks whether new author exists.
     *
     * @param bookId Book's id.
     * @param newAuthorId Book's new author value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if there is no such author with this newAuthorId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookAuthorId(int bookId, int newAuthorId) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            Optional<Author> optionalAuthor = DAOFactory.getAuthorDAO().get(newAuthorId);
            if(optionalAuthor.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getBookDAO().updateAuthor(book, optionalAuthor.get());
            if(!book.getAuthor().equals(optionalAuthor.get())) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookAuthorId()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's publication.
     * Method checks whether new publication exists.
     *
     * @param bookId Book's id.
     * @param newPublicationId Book's new publication value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if there is no such publication with this newPublicationId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookPublicationId(int bookId, int newPublicationId) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            Optional<Publication> optionalPublication = DAOFactory.getPublicationDAO().get(newPublicationId);
            if(optionalPublication.isEmpty()) throw new ServiceException(WRONG_PUBLICATION_ID);

            DAOFactory.getBookDAO().updatePublication(book, optionalPublication.get());
            if(!book.getPublication().equals(optionalPublication.get())) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookPublicationId()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs updating book's publication date.
     *
     * @param bookId Book's id.
     * @param newPublicationDate Book's new publication date value.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void updateBookPublicationDate(int bookId, Date newPublicationDate) throws ServiceException {
        try {
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            DAOFactory.getBookDAO().updatePublicationDate(book, newPublicationDate);
            if(!book.getPublication_Date().equals(newPublicationDate)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateBookPublicationDate()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method performs deleting book.
     * Method checks whether all book's copies are in library.
     *
     * @param bookId Book's id.
     * @throws ServiceException if there is no such book with this bookId.
     * @throws ServiceException if not all book's copies are in library.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if something goes wrong while updating.
     */
    @Override
    public void deleteBook(int bookId) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<Book> optional =  DAOFactory.getBookDAO().get(bookId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ID);
            Book book = optional.get();

            Optional<Integer> optionalInteger = DAOFactory.getBookAvailableDAO().get(bookId);
            if(optionalInteger.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            int copiesAvailable = optionalInteger.get();

            if(book.getCopiesOwned() != copiesAvailable) throw new ServiceException(NOT_ALL_BOOKS_ARE_IN_LIBRARY);

            DAOFactory.getBookDAO().delete(book, connection);

        }catch (Exception e){
            log.warn("Exception while deleteBook()",e);
            throw new ServiceException(e.getMessage());
        }
    }
}
