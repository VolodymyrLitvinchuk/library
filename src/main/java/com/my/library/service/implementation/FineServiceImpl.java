package com.my.library.service.implementation;

import com.my.library.dao.connection.DataSource;
import com.my.library.dao.DAOFactory;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.fine.Fine;
import com.my.library.dao.entity.fine.FinePayment;
import com.my.library.dao.entity.fine.FineStatus;
import com.my.library.dao.mysql.Constant;
import com.my.library.dao.orderByEnums.FineOrderBy;
import com.my.library.dto.FineDTO;
import com.my.library.exception.*;
import com.my.library.service.*;
import com.my.library.service.abstr.FineService;
import org.slf4j.*;

import java.sql.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;
import static com.my.library.exception.constant.Message.*;

/**
 * Implementation of FineService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class FineServiceImpl implements FineService {
    /**
     * Slf4j logger
     */
    private static final Logger log = LoggerFactory.getLogger(FineServiceImpl.class);

    private FineDTO mapToDTO(Fine fine){
        if(fine == null) return null;
        return new FineDTO(
                fine.getId(),
                fine.getOrderFK(),
                fine.getAmount(),
                fine.getStatus().getId(),
                fine.getStatus().getValueEN(),
                fine.getStatus().getValueUA()
        );
    }

    /**
     * Method returns sorted list of reader's fines.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link FineOrderBy}
     * @param reader Reader's login.
     * @param limit Max returned list size.
     * @param offset Number of first objects from the search result that need to be skipped.
     * @return Sorted list of {@link FineDTO}.
     * @throws ServiceException DAO layer return Optional.empty().
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public List<FineDTO> getReaderFinesOrderByLimitOffset(FineOrderBy orderBy, String reader, int limit, int offset) throws ServiceException {
        try {
            Optional<List<Fine>> optionalFines =
                    DAOFactory.getFineDAO()
                            .getReaderFinesOrderByLimitOffsetOrder(orderBy, reader, offset, limit);
            if(optionalFines.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<Fine> fines = optionalFines.get();

            List<FineDTO> fineDTOList = new ArrayList<>();
            for (Fine fine : fines){
                fineDTOList.add(mapToDTO(fine));
            }
            return fineDTOList;

        }catch (DAOException e){
            log.warn("Exception while getReaderFinesOrderByLimitOffset()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns last page number for reader's fines page selectors.
     * If DAO layer returns Optional.empty() then this method returns 1.
     *
     * @param reader Reader's login.
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumber(String reader, int pageSize) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getFineDAO().countReaderFines(reader);
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumber()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns unpaid fine amount for order.
     * If there is no such fine or order then this method returns 0.
     *
     * @param orderID BookOrder's id.
     * @return Fine amount for order.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getFineAmountByOrderID(int orderID) throws ServiceException {
        try{
            Optional<Fine> optional = DAOFactory.getFineDAO().getByOrderIdUnpaid(orderID);
            return optional.map(Fine::getAmount).orElse(0);

        }catch (DAOException e){
            log.warn("Exception while getFineAmountByOrderID(). Order id={}",orderID, e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method checks whether order is fined.
     * If there are unpaid fines with this orderId then this method returns true.
     *
     * @param orderID BookOrder's id.
     * @return True if order is fined.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public boolean orderIsFined(int orderID) throws ServiceException {
        try{
            Optional<Fine> optional = DAOFactory.getFineDAO().getByOrderIdUnpaid(orderID);
            return optional.isPresent();

        }catch (DAOException e){
            log.warn("Exception while orderIsFined(). Order id={}",orderID, e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method performs fine payment.
     * First of all method checks whether order exists, otherwise - throw {@link ServiceException}
     * Then method inserts fine payment using {@link com.my.library.dao.abstr.fine.FinePaymentDAO#insert(FinePayment, Connection)}
     * Then method updates fine's status.
     * Then method tries to update reader status to ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant} using {@link com.my.library.service.abstr.ReaderService#tryToChangeReaderStatusToActive(String)}
     *
     * @param orderID BookOrder's if.
     * @throws ServiceException if BoorOrder does not exist.
     * @throws ServiceException if something goes wrong while running method.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public void payFine(int orderID) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            BookOrder order = optional.get();

            Optional<Fine> optionalFine = DAOFactory.getFineDAO().getByOrderIdUnpaid(orderID);
            if(optionalFine.isPresent()){
                Fine fine = optionalFine.get();
                connection.setAutoCommit(false);

                // inserting Fine payment
                FinePayment finePayment = new FinePayment(fine.getId(), new Date(new java.util.Date().getTime()));
                DAOFactory.getFinePaymentDAO().insert(finePayment, connection);
                if(finePayment.getId()==null){
                    connection.rollback();
                    throw new ServiceException(UNKNOWN_ERROR);
                }

                // Updating Fine status
                Optional<FineStatus> optionalFineStatus = DAOFactory.getFineStatusDAO().get(1);
                if(optionalFineStatus.isEmpty()){
                    connection.rollback();
                    throw new ServiceException(UNKNOWN_ERROR);
                }
                FineStatus status = optionalFineStatus.get();

                DAOFactory.getFineDAO().updateStatus(fine,status,connection);
                if(!fine.getStatus().equals(status)){
                    connection.rollback();
                    throw new ServiceException(UNKNOWN_ERROR);
                }

                connection.commit();
                log.info("Successfully paid fine. Order id={}",orderID);
            }

            // trying to change status to Active, if there are no other fines active
            ServiceFactory.getReaderService().tryToChangeReaderStatusToActive(order.getReaderFK());

        }catch (Exception e){
            if(e.getMessage().equals(WRONG_BOOK_ORDER_ID)) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            if(e.getMessage().equals(WRONG_LOGIN)) throw new ServiceException(WRONG_LOGIN);
            if(e.getMessage().equals(WRONG_USER_STATUS_VALUE)) throw new ServiceException(WRONG_USER_STATUS_VALUE);
            log.warn("Exception while paying fine. Order id={}",orderID, e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method performs checking all fines for all readers.
     * First of all method gets list of oll BookOrders with status=2 ('Book is issued to reader')
     * Then using {@link FineService#orderIsFined(int)} this method checks whether order is fined,
     * if so then using {@link FineService#needToBeUpdated(BookOrder)} this method checks whether fine need to be updated,
     * if so then using {@link FineService#update(BookOrder)} this method performs updating fine and updates reader status to SUSPENDED_USER_STATUS from {@link Constant}.
     * Else method perform following actions: using {@link FineService#orderIsFined(int)} this method checks whether order is not fined,
     * if so then using {@link FineService#needToBeFined(BookOrder)} this method checks whether order need to be fined,
     * if so then using {@link FineService#insertFine(BookOrder)} this method inserts new fine and updates reader status to SUSPENDED_USER_STATUS from {@link Constant}.
     */
    @Override
    public void checkFines(){
        try{
            List<BookOrder> orders = ServiceFactory.getBookOrderService().getAllByStatus(1);

            for(BookOrder order : orders){
                if(orderIsFined(order.getId()) && needToBeUpdated(order)){
                    if(update(order)){
                        log.info("Fine was successfully updated. Order id={}",order.getId());
                        ServiceFactory.getUserService().updateStatus(order.getReaderFK(), SUSPENDED_USER_STATUS);
                    }
                } else if (!orderIsFined(order.getId()) && needToBeFined(order)) {
                    if(insertFine(order)){
                        log.info("Fine was successfully inserted. Order id={}",order.getId());
                        ServiceFactory.getUserService().updateStatus(order.getReaderFK(), SUSPENDED_USER_STATUS);
                    }
                }
            }

        }catch (Exception e){
            log.warn("Exception while checking fines.",e);
        }
    }

    /**
     * Method checks whether order need to be fined.
     * Order's returned date is in past and bookOrder's status is 'Book was issued to the reader' then order need to be fined.
     * 
     * @param order BookOrder's id.
     * @return True if order need to be fined, otherwise false.
     */
    @Override
    public boolean needToBeFined(BookOrder order){
        try{
            return (order.getDateReturnPlanned().compareTo(new java.util.Date())<0
                    && order.getStatus().getId()==1);

        }catch (Exception e){
            log.warn("Exception while needToBeFined() orderID={}",order.getId(),e);
            return false;
        }
    }

    /**
     * Method performs inserting new fine for order.
     * Method calculates fine amount using {@link FineService#calculateAmountOfFine(BookOrder)}.
     * If fine amount is >0 then using {@link com.my.library.dao.abstr.fine.FineDAO#insert(Fine)} inserts new fine.
     * 
     * @param order BookOrder's id.
     * @return True if fine was successfully inserted.
     */
    @Override
    public boolean insertFine(BookOrder order){
        try{
            int amount = calculateAmountOfFine(order);
            if(amount<=0) return false;
            Fine fine = new Fine(order.getId(), amount);
            DAOFactory.getFineDAO().insert(fine);
            return fine.getId()!=null;

        }catch (Exception e){
            log.warn("Exception while insertFine() orderID={}",order.getId(),e);
            return false;
        }
    }

    /**
     * Method checks whether order's fine need to be updated.
     * Method calculates fine amount using {@link FineService#calculateAmountOfFine(BookOrder)}.
     * Then this method compares current fine amount with calculated. If they are not equal then this method returns true.
     * If there are no unpaid fines for this order then this method returns false.
     * 
     * @param order BookOrder's id.
     * @return True if order's fine need to be updated.
     */
    @Override
    public boolean needToBeUpdated(BookOrder order){
        try{
            int amount = calculateAmountOfFine(order);

            Optional<Fine> optional = DAOFactory.getFineDAO().getByOrderIdUnpaid(order.getId());
            if(optional.isEmpty()) return false;
            return amount != optional.get().getAmount();

        }catch (Exception e){
            log.warn("Exception while needToBeUpdated() orderID={}",order.getId(),e);
            return false;
        }
    }

    /**
     * Method perform updating order's fine.
     * Method calculates fine amount using {@link FineService#calculateAmountOfFine(BookOrder)}.
     * Then this method inserts new fine using {@link com.my.library.dao.abstr.fine.FineDAO#insert(Fine)}
     * 
     * @param order BookOrder's id.
     * @return True if fine was successfully inserted.
     */
    @Override
    public boolean update(BookOrder order){
        try{
            int amount = calculateAmountOfFine(order);
            if(amount<=0) return false;

            Optional<Fine> optional = DAOFactory.getFineDAO().getByOrderIdUnpaid(order.getId());
            if(optional.isEmpty()) return false;
            Fine fine = optional.get();

            DAOFactory.getFineDAO().updateAmount(fine,amount);
            return fine.getAmount()==amount;

        }catch (Exception e){
            log.warn("Exception while needToBeUpdated() orderID={}",order.getId(),e);
            return false;
        }
    }

    /**
     * Method calculates fine amount for order.
     * Amount = Days * FINE_VALUE_PER_DAY (constant from {@link Constant}).
     * Then method subtracts fine amount that reader has already paid for this order.
     *
     * @param order BookOrder's id.
     * @return Fine amount for order.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int calculateAmountOfFine(BookOrder order) throws ServiceException {
        if(order == null) return 0;
        if(order.getDateReturnPlanned().compareTo(new java.util.Date())>0) return 0;
        long diff = ChronoUnit.DAYS.between(
                LocalDate.ofInstant(new java.util.Date(order.getDateReturnPlanned().getTime()).toInstant(), ZoneId.systemDefault()),
                LocalDate.ofInstant(new java.util.Date().toInstant(), ZoneId.systemDefault()));
        int amount = (int) (diff*FINE_VALUE_PER_DAY);

        try {
            Optional<List<Fine>> optionalFines = DAOFactory.getFineDAO().getByOrderID(order.getId());
            if (optionalFines.isPresent()) {
                for (Fine fine : optionalFines.get()) {
                    if (fine.getStatus().getId() == 1) amount -= fine.getAmount();
                }
            }
        }catch (DAOException e){
            throw new ServiceException(UNKNOWN_ERROR);
        }
        return amount;
    }
}
