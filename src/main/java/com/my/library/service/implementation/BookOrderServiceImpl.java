package com.my.library.service.implementation;

import com.my.library.dao.connection.DataSource;
import com.my.library.dao.DAOFactory;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.bookOrder.BookOrderStatus;
import com.my.library.dao.entity.bookOrder.BookOrderType;
import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.dto.*;
import com.my.library.dao.entity.user.User;
import com.my.library.exception.*;
import com.my.library.service.*;
import com.my.library.service.abstr.BookOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static com.my.library.dao.mysql.Constant.SUSPENDED_USER_STATUS;
import static com.my.library.exception.constant.Message.*;
import static com.my.library.util.ConvertorUtil.*;

/**
 * Implementation of BookOrderService interface.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class BookOrderServiceImpl implements BookOrderService {
    /**
     * Slf4j logger
     */
    private final Logger log = LoggerFactory.getLogger(BookOrderServiceImpl.class);

    /**
     * Method converts {@link BookOrderDTO} to {@link BookOrder}.
     *
     * @param bookOrderDTO Book order DTO object.
     * @param type Book order type.
     * @param status Book order status.
     * @return {@link BookOrder} object.
     */
    private BookOrder mapBookOrderFromDTO(BookOrderDTO bookOrderDTO, BookOrderType type, BookOrderStatus status){
        return new BookOrder(
                bookOrderDTO.getBookID(),
                bookOrderDTO.getReaderLogin(),
                type,
                bookOrderDTO.getOrderDate(),
                bookOrderDTO.getReturnedDatePlanned(),
                status);
    }

    /**
     * Method checks whether reader has already ordered this book.
     * And book must be not returned.
     *
     * @param readerLogin Reader's login.
     * @param bookID Book's id.
     * @return true if reader has already ordered this book, otherwise false.
     */
    @Override
    public boolean isAlreadyOrdered(String readerLogin, int bookID){
        try{
            return DAOFactory.getBookOrderDAO().hasReaderUnfinishedOrder(readerLogin,bookID);
        }catch (DAOException e){
            return true;
        }
    }

    /**
     * Method inserts new Book order into DB.
     * First of all method checks whether reader with such login exists.
     * Then method checks whether reader status is not SUSPENDED_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     * Then method checks whether reader has already ordered this book and has not returned it yet.
     * Then method converts {@link BookOrderDTO} to {@link BookOrder} and inserts into DB using DAO layer.
     * After that, method update book's available quantity.
     *
     * @param bookOrderDTO {@link BookOrderDTO} object.
     * @throws ServiceException if reader login is wrong.
     * @throws ServiceException if reader status is SUSPENDED_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     * @throws ServiceException if reader has already ordered this book and has not returned yet.
     * @throws ServiceException if there are no available copies in library.
     * @throws ServiceException if order type is wrong.
     */
    @Override
    public void insert(BookOrderDTO bookOrderDTO) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(bookOrderDTO.getReaderLogin());
            if(optionalUser.isEmpty()) throw new ServiceException(WRONG_READER_LOGIN);
            if(optionalUser.get().getStatus().getValue().equals(SUSPENDED_USER_STATUS)) throw new ServiceException(READER_IS_SUSPENDED);

            if(isAlreadyOrdered(bookOrderDTO.getReaderLogin(), bookOrderDTO.getBookID())) throw new ServiceException(BOOK_ALREADY_ORDERED);

            Optional<BookOrderType> optionalType =
                    DAOFactory.getBookOrderTypeDAO().get(bookOrderDTO.getOrderTypeID());
            if(optionalType.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_TYPE);
            BookOrderType type = optionalType.get();

            Optional<BookOrderStatus> optionalStatus =
                    DAOFactory.getBookOrderStatusDAO().get(0);
            if(optionalStatus.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            BookOrderStatus status = optionalStatus.get();

            connection.setAutoCommit(false);
            BookOrder bookOrder = mapBookOrderFromDTO(bookOrderDTO, type, status);
            DAOFactory.getBookOrderDAO().insert(bookOrder, connection);
            if(bookOrder.getId() == null){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }

            // Checking and updating available quantity
            Optional<Integer> optionalInteger = DAOFactory.getBookAvailableDAO().get(bookOrderDTO.getBookID());
            if(optionalInteger.isEmpty()){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }
            if(optionalInteger.get()<1){
                connection.rollback();
                throw new ServiceException(ZERO_QUANTITY);
            }

            if(!DAOFactory.getBookAvailableDAO().update(bookOrderDTO.getBookID(), optionalInteger.get()-1, connection)){
                connection.rollback();
                throw new ServiceException(UNKNOWN_ERROR);
            }

            connection.commit();

        }catch (Exception e){
            log.warn("Exception while insert()",e);
            if(e.getMessage().equals(ZERO_QUANTITY)) throw new ServiceException(ZERO_QUANTITY);
            if(e.getMessage().equals(READER_IS_SUSPENDED)) throw new ServiceException(READER_IS_SUSPENDED);
            if(e.getMessage().equals(WRONG_READER_LOGIN)) throw new ServiceException(WRONG_READER_LOGIN);
            if(e.getMessage().equals(WRONG_BOOK_ORDER_TYPE)) throw new ServiceException(WRONG_BOOK_ORDER_TYPE);
            if(e.getMessage().equals(BOOK_ALREADY_ORDERED)) throw new ServiceException(BOOK_ALREADY_ORDERED);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns list of {@link BookOrder} specified by status.
     *
     * @param status BookOrders' status.
     * @return List of {@link BookOrder}.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if DAO layer returns Optional.empty().
     */
    @Override
    public List<BookOrder> getAllByStatus(int status) throws ServiceException {
        try{
            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAllByStatus(status);
            if(optional.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);

            return optional.get();

        }catch (Exception e){
            log.warn("Exception while getAllByStatus()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns list of {@link BookOrderForReaderDTO} objects for certain reader.
     * Objects are sorted.
     * Method uses {@link com.my.library.dao.abstr.bookOrder.BookOrderDAO#getAllByReaderOrderByOffsetLimit(BookOrderOrderBy, int, int, String)} method.
     * Also, method checks fine amount for each order and adds information about fine to result's object.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderOrderBy}
     * @param offset Number of first objects from the search result that need be skipped.
     * @param limit Max returned list size.
     * @param readerLogin Reader's login.
     * @return List of {@link BookOrderForReaderDTO}.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if DAO layer returns Optional.empty().
     */
    @Override
    public List<BookOrderForReaderDTO> getReadersBookOrders(BookOrderOrderBy orderBy,int offset, int limit, String readerLogin) throws ServiceException {
        try{
            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAllByReaderOrderByOffsetLimit(orderBy, offset, limit, readerLogin);
            if(optional.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<BookOrder> bookOrders = optional.get();

            List<BookOrderForReaderDTO> list = new ArrayList<>();
            int fineAmount;
            for(BookOrder bookOrder : bookOrders){
                fineAmount = ServiceFactory.getFineService().getFineAmountByOrderID(bookOrder.getId());
                list.add(convertToBookOrderForReaderDTO(bookOrder,fineAmount));
            }
            return list;

        }catch (Exception e){
            log.warn("Exception while getReadersBookOrders()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns last page number for specific reader's bookOrder's page selectors.
     * If DAO layer returns Optional.empty() then this method returns 1.
     *
     * @param pageSize Page size.
     * @param readerLogin Reader's login.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumberForReader(int pageSize, String readerLogin) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getBookOrderDAO().countAllReaderOrders(readerLogin);
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumberForReader()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns last page number for all librarians' bookOrder's page selectors.
     * If DAO layer returns Optional.empty() then this method return 1.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumberForLibrarian(int pageSize) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getBookOrderDAO().countAllActiveOrders();
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumberForLibrarian()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns list of {@link BookOrderForLibrarianDTO}.
     * Objects are sorted.
     * Method uses {@link com.my.library.dao.abstr.bookOrder.BookOrderDAO#getAllActiveOrderByOffsetLimit(BookOrderOrderBy, int, int)} method.
     * Also, method adds information about reader and information about fine to each result's bookOrder object.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookOrderForLibrarianDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if DAO layer returns Optional.empty().
     */
    @Override
    public List<BookOrderForLibrarianDTO> getActiveBookOrders(BookOrderOrderBy orderBy, int offset, int limit) throws ServiceException {
        try{
            Optional<List<BookOrder>> optional = DAOFactory.getBookOrderDAO().getAllActiveOrderByOffsetLimit(orderBy, offset, limit);
            if(optional.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<BookOrder> bookOrders = optional.get();

            List<BookOrderForLibrarianDTO> list = new ArrayList<>();
            int fineAmount;
            ReaderDTO readerDTO;
            for(BookOrder bookOrder : bookOrders){
                fineAmount = ServiceFactory.getFineService().getFineAmountByOrderID(bookOrder.getId());
                readerDTO = ServiceFactory.getReaderService().getReader(bookOrder.getReaderFK());
                list.add(convertToBookOrderForLibrarianDTO(bookOrder,fineAmount,readerDTO));
            }
            return list;

        }catch (Exception e){
            log.warn("Exception while getActiveBookOrders()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method checks whether bookOrder is active by checking bookOrder status
     * If there are no such bookOrder, then this method returns false.
     *
     * @param orderID Order id.
     * @return True if order is active, otherwise false.
     * @throws ServiceException is DAOException from DAO layer was thrown.
     */
    @Override
    public boolean isOrderActive(int orderID) throws ServiceException {
        try{
            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) return false;
            BookOrder bookOrder = optional.get();

            return bookOrder.getStatus().getId() != 3 && bookOrder.getStatus().getId() != 4;

        }catch (Exception e){
            log.warn("Exception while isOrderActive()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method performs book issuing.
     * First of all method checks whether bookOrder exists.
     * Then method changes bookOrder's status.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if bookOrder's id is wrong.
     * @throws ServiceException if bookOrder's status id !=0.
     * @throws ServiceException if DAO layer cannot find bookOrder's status with id=1.
     */
    @Override
    public void issueBook(int orderID) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            BookOrder bookOrder = optional.get();
            if(bookOrder.getStatus().getId()!=0) throw new ServiceException(UNKNOWN_ERROR);

            Optional<BookOrderStatus> optionalStatus = DAOFactory.getBookOrderStatusDAO().get(1);
            if(optionalStatus.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            BookOrderStatus status = optionalStatus.get();

            DAOFactory.getBookOrderDAO().updateStatus(bookOrder,status,connection);
            if(!bookOrder.getStatus().equals(status)){
                throw new ServiceException(UNKNOWN_ERROR);
            }

        }catch (Exception e){
            log.warn("Exception while issueBook()",e);
            if(e.getMessage().equals(WRONG_BOOK_ORDER_ID)) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method perform returning book by Reader.
     * First of all method checks whether order is fined.
     * If order is fined then reader cannot return book and must pay fine first.
     * Then method checks whether bookOrder exists.
     * Then method changes bookOrder's status.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if bookOrder's is fined.
     * @throws ServiceException if bookOrder's id is wrong.
     * @throws ServiceException if bookOrder's status id !=1.
     * @throws ServiceException if DAO layer cannot find bookOrder's status with id=2.
     */
    @Override
    public void returnBookReader(int orderID) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            if(ServiceFactory.getFineService().orderIsFined(orderID)) throw new ServiceException(ORDER_IS_FINED);

            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            BookOrder bookOrder = optional.get();
            if(bookOrder.getStatus().getId()!=1) throw new ServiceException(UNKNOWN_ERROR);

            Optional<BookOrderStatus> optionalStatus = DAOFactory.getBookOrderStatusDAO().get(2);
            if(optionalStatus.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            BookOrderStatus status = optionalStatus.get();

            DAOFactory.getBookOrderDAO().updateStatus(bookOrder,status,connection);
            if(!bookOrder.getStatus().equals(status)){
                throw new ServiceException(UNKNOWN_ERROR);
            }

        }catch (Exception e){
            log.warn("Exception while returnBookReader()",e);
            if(e.getMessage().equals(ORDER_IS_FINED)) throw new ServiceException(ORDER_IS_FINED);
            if(e.getMessage().equals(WRONG_BOOK_ORDER_ID)) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method perform returning book by Librarian.
     * First of all method checks whether bookOrder exists.
     * Then method changes bookOrder's status.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if bookOrder's id is wrong.
     * @throws ServiceException if bookOrder's status id !=2.
     * @throws ServiceException if DAO layer cannot find bookOrder's status with id=3.
     */
    @Override
    public void returnBookLibrarian(int orderID) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            BookOrder bookOrder = optional.get();
            if(bookOrder.getStatus().getId()!=2) throw new ServiceException(UNKNOWN_ERROR);

            Optional<BookOrderStatus> optionalStatus = DAOFactory.getBookOrderStatusDAO().get(3);
            if(optionalStatus.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            BookOrderStatus status = optionalStatus.get();

            updateStatusAndQuantityPlusOne(connection, bookOrder, status);

        }catch (Exception e){
            log.warn("Exception while returnBookLibrarian()",e);
            if(e.getMessage().equals(WRONG_BOOK_ORDER_ID)) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method perform canceling book by Librarian.
     * First of all method checks whether bookOrder exists.
     * Then method changes bookOrder's status.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if bookOrder's id is wrong.
     * @throws ServiceException if bookOrder's status id !=0.
     * @throws ServiceException if DAO layer cannot find bookOrder's status with id=4.
     */
    @Override
    public void cancel(int orderID) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<BookOrder> optional = DAOFactory.getBookOrderDAO().get(orderID);
            if(optional.isEmpty()) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            BookOrder bookOrder = optional.get();
            if(bookOrder.getStatus().getId()!=0) throw new ServiceException(UNKNOWN_ERROR);

            Optional<BookOrderStatus> optionalStatus = DAOFactory.getBookOrderStatusDAO().get(4);
            if(optionalStatus.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            BookOrderStatus status = optionalStatus.get();

            updateStatusAndQuantityPlusOne(connection, bookOrder, status);

        }catch (Exception e){
            log.warn("Exception while cancel()",e);
            if(e.getMessage().equals(WRONG_BOOK_ORDER_ID)) throw new ServiceException(WRONG_BOOK_ORDER_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method updates book's quantity and updates bookOrder's status.
     *
     * @param connection Object that will be used by DAO layer.
     * @param bookOrder BookOrder.
     * @param status New bookOrder's status.
     * @throws ServiceException if something goes wrong while updating.
     * @throws SQLException if something with connection object goes wrong.
     * @throws DAOException if exception from DAO layer was thrown.
     */
    private void updateStatusAndQuantityPlusOne(Connection connection, BookOrder bookOrder, BookOrderStatus status) throws ServiceException, SQLException, DAOException {
        connection.setAutoCommit(false);

        Optional<Integer> optionalInteger = DAOFactory.getBookAvailableDAO().get(bookOrder.getBookIdFK());
        if (optionalInteger.isEmpty()) {
            connection.rollback();
            throw new ServiceException(UNKNOWN_ERROR);
        }
        if (!DAOFactory.getBookAvailableDAO().update(bookOrder.getBookIdFK(), optionalInteger.get() + 1, connection)) {
            connection.rollback();
            throw new ServiceException(UNKNOWN_ERROR);
        }

        DAOFactory.getBookOrderDAO().updateStatus(bookOrder,status,connection);
        if(!bookOrder.getStatus().equals(status)){
            connection.rollback();
            throw new ServiceException(UNKNOWN_ERROR);
        }

        connection.commit();
    }

}
