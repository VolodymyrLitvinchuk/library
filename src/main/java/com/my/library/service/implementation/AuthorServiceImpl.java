package com.my.library.service.implementation;

import com.my.library.dao.connection.DataSource;
import com.my.library.dao.DAOFactory;
import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dto.AuthorDTO;
import com.my.library.dao.entity.book.Author;
import com.my.library.exception.*;
import com.my.library.service.abstr.AuthorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.*;

import static com.my.library.exception.constant.Message.*;
import static com.my.library.util.ConvertorUtil.convertAuthorToDTO;

/**
 * Implementation of AuthorService interface.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class AuthorServiceImpl implements AuthorService {
    /**
     * Slf4j logger
     */
    private final Logger log = LoggerFactory.getLogger(AuthorServiceImpl.class);

    /**
     * Method invokes {@link com.my.library.dao.abstr.book.AuthorDAO#getAuthorsByOrderOffsetLimit(AuthorOrderBy, int, int)}.
     * Method checks if optional returned by DAO is present and converts each {@link Author} entity to {@link AuthorDTO}.
     * Then return list of AuthorDTOs
     *
     * @see Author
     * @see AuthorDTO
     * @see com.my.library.util.ConvertorUtil#convertAuthorToDTO(Author)
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link AuthorOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of authors.
     * @throws ServiceException if DAOException from DAO layer was thrown or if DAO layer return Optional.empty().
     */
    @Override
    public List<AuthorDTO> getAuthors(AuthorOrderBy orderBy, int offset, int limit) throws ServiceException {
        try {
            Optional<List<Author>> optionalAuthors =
                    DAOFactory.getAuthorDAO()
                            .getAuthorsByOrderOffsetLimit(orderBy, offset, limit);
            if(optionalAuthors.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<Author> authorList = optionalAuthors.get();

            List<AuthorDTO> authorDTOList = new ArrayList<>();
            for (Author author : authorList){
                authorDTOList.add(convertAuthorToDTO(author));
            }
            return authorDTOList;

        }catch (DAOException e){
            log.warn("Exception while getAuthors()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method returns last page number for page selectors.
     * If DAO layer returns Optional.empty(), then this method returns 1.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumber(int pageSize) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getAuthorDAO().getAuthorQuantity();
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumber()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * First of all method checks whether author with such id exists.
     * And then method updates author's first name in English using DAO layer.
     *
     * @param authorId Author's id.
     * @param newFirstNameEN Author's new first name in English value.
     * @throws ServiceException if DAOException from DAO layer was thrown or author with such id does not exist
     */
    @Override
    public void updateFirstNameEN(int authorId, String newFirstNameEN) throws ServiceException {
        try {
            Optional<Author> optional = DAOFactory.getAuthorDAO().get(authorId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getAuthorDAO().updateFirstNameEN(optional.get(), newFirstNameEN);
            if(!optional.get().getFirstNameEN().equals(newFirstNameEN)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateFirstNameEN()",e);
            if(e.getMessage().equals(WRONG_AUTHOR_ID)) throw new ServiceException(WRONG_AUTHOR_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * First of all method checks whether author with such id exists.
     * And then method updates author's last name in English, using DAO layer.
     *
     * @param authorId Author's id.
     * @param newLastNameEN Author's new last name in English value.
     * @throws ServiceException if DAOException from DAO layer was thrown or author with such id does not exist
     */
    @Override
    public void updateLastNameEN(int authorId, String newLastNameEN) throws ServiceException {
        try {
            Optional<Author> optional = DAOFactory.getAuthorDAO().get(authorId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getAuthorDAO().updateLastNameEN(optional.get(), newLastNameEN);
            if(!optional.get().getLastNameEN().equals(newLastNameEN)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateLastNameEN()",e);
            if(e.getMessage().equals(WRONG_AUTHOR_ID)) throw new ServiceException(WRONG_AUTHOR_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * First of all method checks whether author with such id exists.
     * And then method updates author's first name in Ukrainian, using DAO layer.
     *
     * @param authorId Author's id.
     * @param newFirstNameUA Author's new first name in Ukrainian value.
     * @throws ServiceException if DAOException from DAO layer was thrown or author with such id does not exist
     */
    @Override
    public void updateFirstNameUA(int authorId, String newFirstNameUA) throws ServiceException {
        try {
            Optional<Author> optional = DAOFactory.getAuthorDAO().get(authorId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getAuthorDAO().updateFirstNameUA(optional.get(), newFirstNameUA);
            if(!optional.get().getFirstNameUA().equals(newFirstNameUA)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateFirstNameUA()",e);
            if(e.getMessage().equals(WRONG_AUTHOR_ID)) throw new ServiceException(WRONG_AUTHOR_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * First of all method checks whether author with such id exists.
     * And then method updates author's last name in Ukrainian, using DAO layer.
     *
     * @param authorId Author's id.
     * @param newLastNameUA Author's new last name in Ukrainian value.
     * @throws ServiceException if DAOException from DAO layer was thrown or author with such id does not exist
     */
    @Override
    public void updateLastNameUA(int authorId, String newLastNameUA) throws ServiceException {
        try {
            Optional<Author> optional = DAOFactory.getAuthorDAO().get(authorId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getAuthorDAO().updateLastNameUA(optional.get(), newLastNameUA);
            if(!optional.get().getLastNameUA().equals(newLastNameUA)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateLastNameUA()",e);
            if(e.getMessage().equals(WRONG_AUTHOR_ID)) throw new ServiceException(WRONG_AUTHOR_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * First of all method checks whether author with such id exists.
     * And then method deletes author using DAO layer method.
     *
     * @param authorId Author's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or author with such id does not exist.
     */
    @Override
    public void delete(int authorId) throws ServiceException {
        try(Connection connection = DataSource.getConnection()){
            Optional<Author> optional = DAOFactory.getAuthorDAO().get(authorId);
            if(optional.isEmpty()) throw new ServiceException(WRONG_AUTHOR_ID);

            DAOFactory.getAuthorDAO().delete(optional.get(), connection);

        }catch (Exception e){
            log.warn("Exception while delete()",e);
            if(e.getMessage().equals(WRONG_AUTHOR_ID)) throw new ServiceException(WRONG_AUTHOR_ID);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }
}
