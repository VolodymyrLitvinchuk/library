package com.my.library.service.implementation;

import com.my.library.dao.DAOFactory;
import com.my.library.dto.ReaderDTO;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.entity.fine.Fine;
import com.my.library.exception.ServiceException;
import com.my.library.service.abstr.ReaderService;
import com.my.library.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

import static com.my.library.dao.mysql.Constant.*;
import static com.my.library.exception.constant.Message.*;

/**
 * Implementation of ReaderService interface.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class ReaderServiceImpl implements ReaderService {
    /**
     * Slf4j logger
     */
    private final Logger log = LoggerFactory.getLogger(ReaderServiceImpl.class);

    /**
     * Method collects information about reader and create {@link ReaderDTO} object.
     * Method uses {@link com.my.library.dao.abstr.bookOrder.BookOrderDAO#countReaderOrders(String, int)}
     * and {@link com.my.library.dao.abstr.fine.FineDAO#countReaderFines(String, int)}
     *
     * @param login Reader's login.
     * @return {@link ReaderDTO} object.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public ReaderDTO getReader(String login) throws ServiceException {
        try{
            Optional<Integer> optional = DAOFactory.getBookOrderDAO().countAllReaderOrders(login);
            if(optional.isEmpty()) throw new Exception();
            int allOrders = optional.get();

            optional = DAOFactory.getBookOrderDAO().countReaderOrders(login,ORDER_FINISHED_STATUS);
            if(optional.isEmpty()) throw new Exception();
            int finishedOrders = optional.get();

            optional = DAOFactory.getBookOrderDAO().countReaderOrders(login,ORDER_CANCELED_STATUS);
            if(optional.isEmpty()) throw new Exception();
            int canceledOrders = optional.get();

            optional = DAOFactory.getFineDAO().countReaderFines(login,0);
            if(optional.isEmpty()) throw new Exception();
            int unPaidFines = optional.get();

            optional = DAOFactory.getFineDAO().countReaderFines(login,1);
            if(optional.isEmpty()) throw new Exception();
            int paidFines = optional.get();

            return new ReaderDTO(
                    login,
                    allOrders-finishedOrders-canceledOrders,
                    finishedOrders,
                    canceledOrders,
                    unPaidFines,
                    paidFines
            );

        }catch (Exception e){
            log.warn("Exception while getReader()",e);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }

    /**
     * Method tries to update reader status to ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     * If reader hasn't any unpaid fines then method updates reader's status to ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     * @see com.my.library.dao.abstr.fine.FineDAO#getByOrderIdUnpaid(int) 
     * @see com.my.library.service.abstr.UserService#updateStatus(String, String)
     *
     * @param login Reader's login.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if login is wrong.
     * @throws ServiceException if DAO layer cannot find ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant} in DB.
     */
    @Override
    public void tryToChangeReaderStatusToActive(String login) throws ServiceException {
        try{
            Optional<List<BookOrder>> optionalBookOrderList = DAOFactory.getBookOrderDAO().getAllByReaderFK(login);
            if(optionalBookOrderList.isPresent()){
                List<BookOrder> bookOrders = optionalBookOrderList.get();
                boolean hasActiveFines = false;
                Optional<Fine> optionalFine;

                for(BookOrder order : bookOrders){
                    optionalFine = DAOFactory.getFineDAO().getByOrderIdUnpaid(order.getId());
                    if(optionalFine.isPresent()){
                        hasActiveFines = true;
                        break;
                    }
                }

                if(!hasActiveFines){
                    ServiceFactory.getUserService().updateStatus(login,ACTIVE_USER_STATUS);
                }
            }

        }catch (Exception e){
            log.warn("Exception while tryToChangeReaderStatusToActive()",e);
            if(e.getMessage().equals(WRONG_LOGIN)) throw new ServiceException(WRONG_LOGIN);
            if(e.getMessage().equals(WRONG_USER_STATUS_VALUE)) throw new ServiceException(WRONG_USER_STATUS_VALUE);
            throw new ServiceException(UNKNOWN_ERROR);
        }
    }
}
