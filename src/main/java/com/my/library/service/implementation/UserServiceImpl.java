package com.my.library.service.implementation;

import com.my.library.dao.DAOFactory;
import com.my.library.dao.orderByEnums.UserOrderBy;
import com.my.library.dto.UserDTO;
import com.my.library.dao.entity.user.User;
import com.my.library.dao.entity.user.UserStatus;
import com.my.library.exception.*;
import com.my.library.service.abstr.UserService;
import com.my.library.util.Hasher;
import com.my.library.util.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.my.library.dao.mysql.Constant.LOCKED_USER_STATUS;
import static com.my.library.exception.constant.Message.*;
import static com.my.library.util.ValidatorUtil.*;
import static com.my.library.util.ConvertorUtil.*;
import static com.my.library.util.constant.EmailConstant.FORGOT_PASSWORD_BODY;
import static com.my.library.util.constant.EmailConstant.FORGOT_PASSWORD_SUBJECT;
import static com.my.library.util.constant.Regex.*;

/**
 * Implementation of UserService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public class UserServiceImpl implements UserService {
    /**
     * Slf4j logger
     */
    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    /**
     * Method checks whether user login and password is correct.
     * Method checks whether user with such login exists using {@link com.my.library.dao.abstr.user.UserDAO#get(String)}.
     * Then checks password using {@link com.my.library.util.ValidatorUtil#verifyPasswordHash(String, String)}.
     * And then this method checks whether user status not equals LOCKED_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     *
     * @param login User's login.
     * @param password User's password.
     * @return {@link UserDTO} object is success.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if login or password == null.
     * @throws IncorrectLoginException if login incorrect.
     * @throws IncorrectPasswordException if password incorrect.
     * @throws ServiceException if user status equals LOCKED_USER_STATUS from {@link com.my.library.dao.mysql.Constant}
     */
    @Override
    public UserDTO signIn(String login, String password) throws ServiceException {
        if(login==null || password==null) throw new ServiceException();
        try{
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(login);
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();
            verifyPasswordHash(password, user.getPasswordHash());
            if(user.getStatus() == null || user.getStatus().getValue().equals(LOCKED_USER_STATUS)){
                throw new ServiceException(ACCOUNT_LOCKED);
            }
            return convertUserToDTO(user);

        }catch (Exception e){
            if(e.getMessage().equals(WRONG_LOGIN)) throw new IncorrectLoginException();
            if(e.getMessage().equals(WRONG_PASSWORD)) throw new IncorrectPasswordException();
            log.warn("Exception while signIn()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method generates new password and sends it to user by email.
     *
     * @param login User's login.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if there was an exception thrown while sending email.
     */
    @Override
    public void generateAndSendNewPassword(String login) throws ServiceException {
        if(login==null ) throw new IncorrectLoginException();
        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(login);
            if (optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();
            String oldPasswordHash = user.getPasswordHash();
            String newPassword = generateRandomPassword(10);

            try {
                DAOFactory.getUserDAO().updatePassword(user, Hasher.hash(newPassword));
                if(!user.getPasswordHash().equals(Hasher.hash(newPassword))) throw new ServiceException();
            }catch (Exception e){ throw new ServiceException(UNKNOWN_ERROR);}

            try{
                MailSender.sendEmail(
                        user.getEmail(),
                        FORGOT_PASSWORD_SUBJECT,
                        String.format(FORGOT_PASSWORD_BODY, newPassword));

            }catch (Exception e){
                try { DAOFactory.getUserDAO().updatePassword(user, oldPasswordHash);
                }catch (Exception ex){ throw new ServiceException(UNKNOWN_ERROR);}
                throw new ServiceException(MAIL_ERROR);
            }

        }catch (Exception e){
            if(e.getMessage().equals(WRONG_LOGIN)) throw new IncorrectLoginException();
            if(e.getMessage().equals(MAIL_ERROR)) throw new ServiceException(MAIL_ERROR);
            log.warn("Exception while generateAndSendNewPassword()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method generates random password.
     *
     * @param length Password's length.
     * @return Generated password.
     */
    private String generateRandomPassword(int length){
        String LOWER = "abcdefghijklmnopqrstuvwxyz";
        String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String DIGITS = "0123456789";
        String OTHER_SYMBOLS = "@#$%&";

        if (length <= 0) return "";

        StringBuilder password = new StringBuilder(length);
        Random random = new Random(System.nanoTime());

        List<String> charCategories = new ArrayList<>(4);
        charCategories.add(LOWER);
        charCategories.add(UPPER);
        charCategories.add(DIGITS);
        charCategories.add(OTHER_SYMBOLS);

        // Build the password.
        for (int i = 0; i < length; i++) {
            String charCategory = charCategories.get(random.nextInt(charCategories.size()));
            int position = random.nextInt(charCategory.length());
            password.append(charCategory.charAt(position));
        }
        return new String(password);
    }

    /**
     * Method performs signing up a new user.
     * First of all method validates userDTO object using {@link com.my.library.util.ValidatorUtil}
     * Then method checks whether user with such login or with such email exists.
     * Then this method inserts new User using {@link com.my.library.dao.abstr.user.UserDAO#insert(User)}
     * and assigns a status value to userDTO object.
     *
     * @param userDTO UserDTO object to signUp.
     * @param password User password.
     * @param repeatPassword User password repeat.
     * @throws ServiceException if userDTO fields are not valid.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws DuplicateLoginException if user with such login does not exist.
     * @throws DuplicateEmailException if user with such email does not exist.
     */
    @Override
    public void signUp(UserDTO userDTO, String password, String repeatPassword) throws ServiceException {
        validateFormat(userDTO.getLogin(), LOGIN_REGEX, ENTER_CORRECT_LOGIN);
        verifyRepeatedPassword(password,repeatPassword);
        validateFormat(password,PASSWORD_REGEX,ENTER_CORRECT_PASSWORD);
        validateFormat(userDTO.getFirstName(), NAME_REGEX, ENTER_CORRECT_FIRST_NAME);
        validateFormat(userDTO.getLastName(), NAME_REGEX, ENTER_CORRECT_LAST_NAME);
        validateFormat(userDTO.getEmail(), EMAIL_REGEX, ENTER_CORRECT_EMAIL);

        try{
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(userDTO.getLogin());
            if(optionalUser.isPresent()) throw new DuplicateLoginException();
            optionalUser = DAOFactory.getUserDAO().getByEmail(userDTO.getEmail());
            if (optionalUser.isPresent()) throw new DuplicateEmailException();

            User user = convertDTOtoUser(userDTO, password);
            if(user==null) throw new ServiceException(ERROR_CONVERT);

            DAOFactory.getUserDAO().insert(user);

            userDTO.setStatus(user.getStatus().getValue());

        }catch (DAOException e){
            log.warn("Exception while signUp()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method checks whether user status field is valid.
     * If user status field is not valid, then method assigns new status value.
     * 
     * @param userDTO UserDTO object
     */
    @Override
    public void updateStatus(UserDTO userDTO) {
        if(userDTO!=null) {
            try {
                Optional<User> optionalUser = DAOFactory.getUserDAO().get(userDTO.getLogin());
                if (optionalUser.isPresent()){
                    User user = optionalUser.get();

                    if(userDTO.getFirstName().equals(user.getFirstName())
                            && userDTO.getLastName().equals(user.getLastName())
                            && userDTO.getEmail().equals(user.getEmail())) {

                        userDTO.setStatus(user.getStatus().getValue());

                    }else userDTO = null;

                }else {
                    userDTO = null;
                }

            } catch (DAOException e) {
                log.warn("Exception while updateStatus()",e);
                userDTO = null;
            }
        }
    }

    /**
     * Method returns list of {@link UserDTO}. List is sorted.
     * Method uses {@link com.my.library.dao.abstr.user.UserDAO#getUsersByOrderOffsetLimit(UserOrderBy, int, int)} 
     * and {@link com.my.library.util.ConvertorUtil#convertUserToDTO(User)}
     * 
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link UserOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return Sorted list of {@link UserDTO}.
     * @throws ServiceException DAO layer return Optional.empty().
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public List<UserDTO> getUsers(UserOrderBy orderBy, int offset, int limit) throws ServiceException {
        try {
            Optional<List<User>> optionalUsers =
                    DAOFactory.getUserDAO()
                            .getUsersByOrderOffsetLimit(orderBy, offset, limit);
            if(optionalUsers.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            List<User> userList = optionalUsers.get();

            List<UserDTO> userDTOList = new ArrayList<>();
            for (User user : userList){
                userDTOList.add(convertUserToDTO(user));
            }
            return userDTOList;

        }catch (DAOException e){
            log.warn("Exception while getUsers()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method returns last page number for users' page selectors.
     * If DAO layer returns Optional.empty() then this method returns 1.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     */
    @Override
    public int getLastPageNumber(int pageSize) throws ServiceException {
        try {
            Optional<Integer> optional =
                    DAOFactory.getUserDAO().countAllUsers();
            if(optional.isEmpty() || optional.get().equals(0)) return 1;
            else {
                return optional.get()/pageSize + ((optional.get()%pageSize == 0) ? 0:1);
            }

        }catch (DAOException e){
            log.warn("Exception while getLastPageNumber()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's login.
     * Method uses {@link com.my.library.dao.abstr.user.UserDAO#updateLogin(User, String)}.
     *
     * @param userDTO User's object.
     * @param newLogin User's new login value.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws DuplicateLoginException if user with such login as new login param exists.
     */
    @Override
    public void updateLogin(UserDTO userDTO, String newLogin) throws ServiceException {
        validateFormat(newLogin, LOGIN_REGEX, ENTER_CORRECT_LOGIN);

        try {
            User user = convertDTOtoUser(userDTO, null);
            if(user==null) throw new ServiceException(ERROR_CONVERT);

            DAOFactory.getUserDAO().updateLogin(user, newLogin);

            if(user.getLogin().equals(newLogin)) userDTO.setLogin(newLogin);

        } catch (DAOException e) {
            log.warn("Exception while updateLogin()",e);
            if(e.getMessage().contains("Duplicate")) throw new DuplicateLoginException();
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's password.
     * First of all method validates params using {@link com.my.library.util.ValidatorUtil}.
     * Then method verifies repeated passwords.
     * Then method checks whether user with such login exists.
     * Then method verifies old password.
     * Then methods updates password using {@link com.my.library.dao.abstr.user.UserDAO#updatePassword(User, String)}
     * and {@link Hasher#hash(String)}
     *
     * @param userDTO User's object.
     * @param oldPassword User's old password.
     * @param password User's new password.
     * @param repeatPassword User's new password repeat.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws IncorrectPasswordException if password is incorrect.
     * @throws IncorrectLoginException if login is incorrect.
     * @throws ServiceException if userDTO fields is not valid.
     */
    @Override
    public void updatePassword(UserDTO userDTO, String oldPassword, String password, String repeatPassword) throws ServiceException {
        validateFormat(oldPassword,PASSWORD_REGEX,ENTER_CORRECT_PASSWORD);
        validateFormat(password,PASSWORD_REGEX,ENTER_CORRECT_PASSWORD);
        validateFormat(repeatPassword,PASSWORD_REGEX,ENTER_CORRECT_PASSWORD);
        verifyRepeatedPassword(password,repeatPassword);

        try{
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(userDTO.getLogin());
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();
            try {
                verifyPasswordHash(oldPassword, user.getPasswordHash());
            }catch (Exception e){
                throw new ServiceException(WRONG_OLD_PASSWORD);
            }

            try {
                DAOFactory.getUserDAO().updatePassword(user, Hasher.hash(password));
            }catch (Exception e){
                throw new ServiceException(UNKNOWN_ERROR);
            }

        }catch (DAOException e){
            log.warn("Exception while updatePassword()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's email.
     * First of all method validates newEmail using {@link com.my.library.util.ValidatorUtil}.
     * Then method checks whether user with such email exists.
     * Then method updates email using {@link com.my.library.dao.abstr.user.UserDAO#updateEmail(User, String)}
     *
     * @param userDTO User's object.
     * @param newEmail User's new email value.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if newEmail is not valid.
     * @throws ServiceException if user with such current email does not exist.
     */
    @Override
    public void updateEmail(UserDTO userDTO, String newEmail) throws ServiceException {
        validateFormat(newEmail, EMAIL_REGEX, ENTER_CORRECT_EMAIL);

        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().getByEmail(userDTO.getEmail());
            if(optionalUser.isEmpty()) throw new ServiceException(UNKNOWN_ERROR);
            User user = optionalUser.get();

            DAOFactory.getUserDAO().updateEmail(user,newEmail);
            if(user.getEmail().equals(newEmail)) userDTO.setEmail(newEmail);

        }catch (DAOException e){
            log.warn("Exception while updateEmail()",e);
            if(e.getMessage().contains("Duplicate")) throw new DuplicateEmailException();
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's first name.
     * First of all method validates newFirstName using {@link com.my.library.util.ValidatorUtil}.
     * Then method checks whether user with such login exists.
     * Then method updates user's first name using {@link com.my.library.dao.abstr.user.UserDAO#updateFirstName(User, String)}.
     *
     * @param userDTO User's object.
     * @param newFirstName User's new first name value.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws IncorrectLoginException if login is incorrect.
     * @throws ServiceException if newFirstName is not valid.
     */
    @Override
    public void updateFirstName(UserDTO userDTO, String newFirstName) throws ServiceException {
        validateFormat(newFirstName, NAME_REGEX, ENTER_CORRECT_FIRST_NAME);

        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(userDTO.getLogin());
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();

            DAOFactory.getUserDAO().updateFirstName(user,newFirstName);
            if(user.getFirstName().equals(newFirstName)) userDTO.setFirstName(newFirstName);

        }catch (DAOException e){
            log.warn("Exception while updateFirstName()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's last name.
     * First of all method validates newFirstName using {@link com.my.library.util.ValidatorUtil}.
     * Then method checks whether user with such login exists.
     * Then method updates user's last name using {@link com.my.library.dao.abstr.user.UserDAO#updateLastName(User, String)}.
     *
     * @param userDTO User's object.
     * @param newLastName User's new last name value.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws IncorrectLoginException if login is incorrect.
     * @throws ServiceException if newLastName is not valid.
     */
    @Override
    public void updateLastName(UserDTO userDTO, String newLastName) throws ServiceException {
        validateFormat(newLastName, NAME_REGEX, ENTER_CORRECT_LAST_NAME);

        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(userDTO.getLogin());
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();

            DAOFactory.getUserDAO().updateLastName(user,newLastName);
            if(user.getLastName().equals(newLastName)) userDTO.setLastName(newLastName);

        }catch (DAOException e){
            log.warn("Exception while updateLastName()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method updates user's status.
     * Method checks whether user with such login exists.
     * Then method checks whether newStatus exists.
     * Then method updates user's status using {@link com.my.library.dao.abstr.user.UserDAO#updateStatus(User, UserStatus)}.
     *
     * @param login User's login.
     * @param newStatus User's new status value.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws IncorrectLoginException if login is incorrect.
     * @throws ServiceException if newStatus does not exist.
     */
    @Override
    public void updateStatus(String login, String newStatus) throws ServiceException {
        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(login);
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();

            Optional<UserStatus> optionalUserStatus = DAOFactory.getUserStatusDAO().getByValue(newStatus);
            if(optionalUserStatus.isEmpty()) throw new ServiceException(WRONG_USER_STATUS_VALUE);
            UserStatus status = optionalUserStatus.get();

            DAOFactory.getUserDAO().updateStatus(user, status);
            if(!user.getStatus().equals(status)) throw new ServiceException(UNKNOWN_ERROR);

        }catch (DAOException e){
            log.warn("Exception while updateStatus()",e);
            throw new ServiceException(e.getMessage());
        }
    }

    /**
     * Method deletes user.
     * Method checks whether user with such login exists.
     * Then method checks whether user has returned all books that he had ordered before using {@link com.my.library.dao.abstr.bookOrder.BookOrderDAO#userHasUnfinishedOrders(String)}.
     * Then method deletes user using {@link com.my.library.dao.abstr.user.UserDAO#delete(User)}.
     *
     * @param login User's login.
     * @throws ServiceException if DAOException from DAO layer was thrown.
     * @throws ServiceException if user has not returned book.
     * @throws IncorrectLoginException if login is incorrect.
     */
    @Override
    public void deleteUser(String login) throws ServiceException {
        try {
            Optional<User> optionalUser = DAOFactory.getUserDAO().get(login);
            if(optionalUser.isEmpty()) throw new IncorrectLoginException();
            User user = optionalUser.get();

            if(DAOFactory.getBookOrderDAO().userHasUnfinishedOrders(login)) throw new ServiceException(USER_HAS_NOT_RETURNED_BOOK);

            DAOFactory.getUserDAO().delete(user);

        }catch (DAOException e){
            log.warn("Exception while deleteUser()",e);
            throw new ServiceException(e.getMessage());
        }
    }
}
