package com.my.library.service.abstr;

import com.my.library.dao.orderByEnums.UserOrderBy;
import com.my.library.exception.ServiceException;
import com.my.library.dto.UserDTO;

import java.util.List;

/**
 * UserService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface UserService {
    /**
     * Method should check whether login and password is correct.
     *
     * @param login User's login.
     * @param password User's password.
     * @return {@link UserDTO} if login and password is correct.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    UserDTO signIn(String login, String password) throws ServiceException;

    /**
     * Method should generate new password and send it to user by email.
     *
     * @param login User's login.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void generateAndSendNewPassword(String login) throws ServiceException;

    /**
     * Method should perform singing up new user.
     * {@link UserDTO} param fields must be not null, except status field.
     *
     * @param userDTO UserDTO object to signUp.
     * @param password User's password.
     * @param repeatPassword User's password repeat.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void signUp(UserDTO userDTO, String password, String repeatPassword) throws ServiceException;

    /**
     * Method should check whether {@link UserDTO} status field is valid.
     * If status field is not valid, then method should assign valid status value.
     *
     * @param userDTO UserDTO object.
     */
    void updateStatus(UserDTO userDTO);

    /**
     * Method should return list of {@link UserDTO} objects.
     * List must be sorted.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link UserOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return Sorted list of {@link UserDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<UserDTO> getUsers(UserOrderBy orderBy, int offset, int limit) throws ServiceException;

    /**
     * Method should return last page number for users' page selectors.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumber(int pageSize) throws ServiceException;

    /**
     * Method should update user's login.
     *
     * @param userDTO User's object.
     * @param newLogin User's new login value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateLogin(UserDTO userDTO, String newLogin) throws  ServiceException;

    /**
     * Method should update user's password.
     *
     * @param userDTO User's object.
     * @param oldPassword User's old password.
     * @param password User's new password.
     * @param repeatPassword User's new password repeat.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updatePassword(UserDTO userDTO, String oldPassword, String password, String repeatPassword) throws ServiceException;

    /**
     * Method should update user's email.
     *
     * @param userDTO User's object.
     * @param newEmail User's new email value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateEmail(UserDTO userDTO, String newEmail) throws ServiceException;

    /**
     * Method should update user's first name.
     *
     * @param userDTO User's object.
     * @param newFirstName User's new first name value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateFirstName(UserDTO userDTO, String newFirstName) throws ServiceException;

    /**
     * Method should update user's last name.
     *
     * @param userDTO User's object.
     * @param newLastName User's new last name value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateLastName(UserDTO userDTO, String newLastName) throws ServiceException;

    /**
     * Method should update user's status.
     *
     * @param login User's login.
     * @param newStatus User's new status value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateStatus(String login, String newStatus) throws ServiceException;

    /**
     * Method should delete user.
     *
     * @param login User's login.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void deleteUser(String login) throws ServiceException;
}
