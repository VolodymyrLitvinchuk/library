package com.my.library.service.abstr;

import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.dao.orderByEnums.FineOrderBy;
import com.my.library.dto.FineDTO;
import com.my.library.exception.ServiceException;

import java.util.List;

/**
 * FineService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface FineService {
    /**
     * Method should return sorted list of reader's fines.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link FineOrderBy}
     * @param reader Reader's login.
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return Sorted list of {@link FineDTO}.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<FineDTO> getReaderFinesOrderByLimitOffset(FineOrderBy orderBy, String reader, int limit, int offset) throws ServiceException;

    /**
     * Method should return last page number for reader's fines page selectors.
     *
     * @param reader Reader's login.
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumber(String reader, int pageSize) throws ServiceException;

    /**
     * Method should return fine amount for bookOrder.
     *
     * @param orderID BookOrder's id.
     * @return Fine amount.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getFineAmountByOrderID(int orderID) throws ServiceException;

    /**
     * Method should check whether order is fined.
     *
     * @param orderID BookOrder's id.
     * @return True if order is fined, otherwise false.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    boolean orderIsFined(int orderID) throws ServiceException;

    /**
     * Method should perform fine payment.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void payFine(int orderID) throws ServiceException;

    /**
     * Method should check fines for all orders and for all readers.
     */
    void checkFines();

    /**
     * Method should check whether order need to be fined.
     *
     * @param order BookOrder's id.
     * @return True if order need to be fined, otherwise false.
     */
    boolean needToBeFined(BookOrder order);

    /**
     * Method should insert new fine for order.
     *
     * @param order BookOrder's id.
     * @return True if fine was successfully inserted.
     */
    boolean insertFine(BookOrder order);

    /**
     * Method should check whether order's fine need to be updated.
     *
     * @param order BookOrder's id.
     * @return True if order's fine need to be updated.
     */
    boolean needToBeUpdated(BookOrder order);

    /**
     * Method should perform updating order's fine.
     *
     * @param order BookOrder's id.
     * @return True if order's fine was successfully updated.
     */
    boolean update(BookOrder order);

    /**
     * Method should return fine amount for order.
     *
     * @param order BookOrder's id.
     * @return Fine amount for order.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int calculateAmountOfFine(BookOrder order) throws ServiceException;
}
