package com.my.library.service.abstr;

import com.my.library.dao.orderByEnums.BookOrderOrderBy;
import com.my.library.dto.BookOrderDTO;
import com.my.library.dto.BookOrderForLibrarianDTO;
import com.my.library.dto.BookOrderForReaderDTO;
import com.my.library.dao.entity.bookOrder.BookOrder;
import com.my.library.exception.ServiceException;

import java.util.List;

/**
 * BookOrderService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface BookOrderService {
    /**
     * Method should check whether reader has already ordered this book and has not returned it yet.
     *
     * @param readerLogin Reader's login.
     * @param bookID Book's id.
     * @return True if reader has already ordered this book, otherwise false.
     */
    boolean isAlreadyOrdered(String readerLogin, int bookID);

    /**
     * Method should insert new bookOrder in DB.
     *
     * @param bookOrderDTO {@link BookOrderDTO} object.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void insert(BookOrderDTO bookOrderDTO) throws ServiceException;

    /**
     * Method should return list of {@link BookOrder} objects specified by status.
     *
     * @param status BookOrders' status.
     * @return List of {@link BookOrder}.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<BookOrder> getAllByStatus(int status) throws ServiceException;

    /**
     * Method should return last page number for specific reader's bookOrder's page selectors.
     *
     * @param pageSize Page size.
     * @param readerLogin Reader's login.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumberForReader(int pageSize, String readerLogin) throws ServiceException;

    /**
     * Method should return list of {@link BookOrderForReaderDTO} objects for certain reader.
     * Objects must be sorted.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderOrderBy}
     * @param offset Number of first objects from the search result that need be skipped.
     * @param limit Max returned list size.
     * @param readerLogin Reader's login.
     * @return List of {@link BookOrderForReaderDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<BookOrderForReaderDTO> getReadersBookOrders(BookOrderOrderBy orderBy, int offset, int limit, String readerLogin) throws ServiceException;

    /**
     * Method should return last page number for all librarians' bookOrder's page selector
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumberForLibrarian(int pageSize) throws ServiceException;

    /**
     * Method should return list of {@link BookOrderForLibrarianDTO} objects.
     * Objects must be sorted.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookOrderForLibrarianDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<BookOrderForLibrarianDTO> getActiveBookOrders(BookOrderOrderBy orderBy, int offset, int limit) throws ServiceException;

    /**
     * Method should check whether order is active.
     *
     * @param orderID Order's id.
     * @return True if order is active, otherwise false.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    boolean isOrderActive(int orderID) throws ServiceException;

    /**
     * Method that should issue book.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void issueBook(int orderID) throws ServiceException;

    /**
     * Method should perform returning book by Reader.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void returnBookReader(int orderID) throws ServiceException;

    /**
     * Method that should perform returning book by Librarian.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void returnBookLibrarian(int orderID) throws ServiceException;

    /**
     * Method that should cancel bookOrder.
     *
     * @param orderID BookOrder's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void cancel(int orderID) throws ServiceException;
}
