package com.my.library.service.abstr;

import com.my.library.dao.orderByEnums.BookOrderBy;
import com.my.library.dto.BookDTO;
import com.my.library.exception.ServiceException;

import java.sql.Date;
import java.util.List;

/**
 * BookService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface BookService {
    /**
     * Method should insert new book into DB.
     *
     * @param bookDTO {@link BookDTO} object.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void add(BookDTO bookDTO) throws ServiceException;

    /**
     * Method should return valid sql query for book searching depends on parameters.
     *
     * @param titlePattern Book title sql pattern.
     * @param authorFirstNamePattern Book author first name sql pattern.
     * @param authorLastNamePattern Book author last name sql pattern.
     * @param locale Current user locale.
     * @return Sql query.
     */
    String buildSearchQuery(String titlePattern, String authorFirstNamePattern, String authorLastNamePattern, String locale);

    /**
     * Method should return {@link BookDTO} object.
     *
     * @param id Book's id
     * @return {@link BookDTO} object.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    BookDTO getBook(int id) throws ServiceException;

    /**
     * Method should return list of books that meet search params.
     * Books must be sorted.
     *
     * @param query Sql searching query, building from {@link BookService#buildSearchQuery(String, String, String, String)}
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<BookDTO> searchBooks(String query, BookOrderBy orderBy, int offset, int limit) throws ServiceException;

    /**
     * Method should return list of books.
     * Books must be sorted.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link BookOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link BookDTO} objects
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<BookDTO> getBooks(BookOrderBy orderBy, int offset, int limit) throws ServiceException;

    /**
     * Method should return last page number for books' page selectors.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumber(int pageSize) throws ServiceException;

    /**
     * Method should return last page number for book searching result page selectors.
     *
     * @param searchQuery Sql searching query, building from {@link BookService#buildSearchQuery(String, String, String, String)}.
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumberSearchBooks(String searchQuery, int pageSize) throws ServiceException;

    /**
     * Method should update book title in English.
     *
     * @param bookId Book's id.
     * @param newTitleEN Book's new title in English value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookTitleEN(int bookId, String newTitleEN) throws ServiceException;

    /**
     * Method should update book title in Ukrainian.
     *
     * @param bookId Book's id.
     * @param newTitleUA Book's new title in Ukrainian value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookTitleUA(int bookId, String newTitleUA) throws ServiceException;

    /**
     * Method should update book language.
     *
     * @param bookId Book's id
     * @param newLanguage Book's new language value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookLanguage(int bookId, String newLanguage) throws ServiceException;

    /**
     * Method should update book copies owned.
     *
     * @param bookId Book's id.
     * @param newCopiesOwned Book's new copies owned value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookCopiesOwned(int bookId, int newCopiesOwned) throws ServiceException;

    /**
     * Method should update book author id.
     *
     * @param bookId Book's id.
     * @param newAuthorId Book's new author id value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookAuthorId(int bookId, int newAuthorId) throws ServiceException;

    /**
     * Method should update book publication id.
     *
     * @param bookId Book's id.
     * @param newPublicationId Book's new publication id value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookPublicationId(int bookId, int newPublicationId) throws ServiceException;

    /**
     * Method should update book publication date.
     *
     * @param bookId Book's id.
     * @param newPublicationDate Book's new publication date value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateBookPublicationDate(int bookId, Date newPublicationDate) throws ServiceException;

    /**
     * Method should delete book.
     *
     * @param bookId Book's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void deleteBook(int bookId) throws ServiceException;
}
