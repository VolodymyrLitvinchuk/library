package com.my.library.service.abstr;

import com.my.library.dto.ReaderDTO;
import com.my.library.exception.ServiceException;

/**
 * ReaderService interface
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface ReaderService {
    /**
     * Method should return {@link ReaderDTO} object.
     *
     * @param login Reader's login
     * @return {@link ReaderDTO} object.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    ReaderDTO getReader(String login) throws ServiceException;

    /**
     * Method should try to change reader status to ACTIVE_USER_STATUS from {@link com.my.library.dao.mysql.Constant}.
     *
     * @param login Reader's login.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void tryToChangeReaderStatusToActive(String login) throws ServiceException;
}
