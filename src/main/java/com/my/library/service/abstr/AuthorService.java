package com.my.library.service.abstr;

import com.my.library.dao.orderByEnums.AuthorOrderBy;
import com.my.library.dto.AuthorDTO;
import com.my.library.exception.ServiceException;

import java.util.List;

/**
 * AuthorService interface.
 *
 * @author Volodymyr Litvinchuk
 * @version 1.0
 */
public interface AuthorService {
    /**
     * Method should return a sorted list of authors using DAO.
     *
     * @param orderBy Enum that defines the column by which the list need to be sorted and the sorting order. {@link AuthorOrderBy}
     * @param offset Number of first objects from the search result that need to be skipped.
     * @param limit Max returned list size.
     * @return List of {@link AuthorDTO} objects.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    List<AuthorDTO> getAuthors(AuthorOrderBy orderBy, int offset, int limit) throws ServiceException;

    /**
     * Method should return last page number for page selectors.
     *
     * @param pageSize Page size.
     * @return Last page number.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    int getLastPageNumber(int pageSize) throws ServiceException;

    /**
     * Method should update Author's first name in English using DAO layer.
     *
     * @param authorId Author's id.
     * @param newFirstNameEN Author's new first name in English value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateFirstNameEN(int authorId, String newFirstNameEN) throws ServiceException;

    /**
     * Method should update Author's last name in English using DAO layer.
     *
     * @param authorId Author's id.
     * @param newLastNameEN Author's new last name in English value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateLastNameEN(int authorId, String newLastNameEN) throws ServiceException;

    /**
     * Method should update Author's first name in Ukrainian using DAO layer.
     *
     * @param authorId Author's id.
     * @param newFirstNameUA Author's new first name in Ukrainian value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateFirstNameUA(int authorId, String newFirstNameUA) throws ServiceException;

    /**
     * Method should update Author's last name in Ukrainian using DAO layer.
     *
     * @param authorId Author's id.
     * @param newLastNameUA Author's new last name in Ukrainian value.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void updateLastNameUA(int authorId, String newLastNameUA) throws ServiceException;

    /**
     * Method  should delete Author using DAO layer.
     *
     * @param authorId Author's id.
     * @throws ServiceException if DAOException from DAO layer was thrown or by another mistakes.
     */
    void delete(int authorId) throws ServiceException;
}
