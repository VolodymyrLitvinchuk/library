## LIBRARY

**There are roles:**
- Reader
- Librarian
- Admin

*Unregistered user cannot order a book, but can view list of books,  sort this list and search for a book by various parameters.* &nbsp;&nbsp;

**Reader** can order a book.    
**Librarian** gives the reader a book on a subscription or to the reading room.

The book is issued to the reader for ~~unlimited period~~ a certain period.     
If the book is not returned within the specified period, the **reader will be fined**.

The book may be present in the library in one or more copies. The system keeps track of the available number of books.  
Each user has a personal account that displays registration information.    
&nbsp;&nbsp;

**Reader** can see list of books on the subscription and date of possible return.
*If the date is overdue, the amount of the fine is displayed*  
Also, **reader** can see list of his/her fines.

**Librarian** can see list of readers and their subscriptions.    
&nbsp;&nbsp;

**Admin has the rights:**
- adding/deleting book
- editing information about the book
- create/delete librarian
- block/unblock user
  &nbsp;&nbsp;  
  &nbsp;&nbsp;


**Additional functionality:**
- Password hashing.
- User can receive a new randomly generated password by mail *('forgot password')*.
- Reader can see list of his/her fines.
- 'Contact us' button on each page. User can tell about a problem or suggest some improvement for the site.

